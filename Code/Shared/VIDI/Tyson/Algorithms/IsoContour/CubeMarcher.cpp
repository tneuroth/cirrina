
#include "Algorithms/IsoContour/CubeMarcher.hpp"

namespace TN
{

using namespace MarcherConsts;

float CubeMarcher::fGetOffset( float fValue1, float fValue2, float fValueDesired)
{
    double fDelta = fValue2 - fValue1;

    if( fDelta == 0.0 )
    {
        return 0.5;
    }
    return ( fValueDesired - fValue1 ) / fDelta;
}

void CubeMarcher::addNormal( const Vec3< float > & vertex, const Vec3< float > & normal )
{
    std::unordered_map< Vec3< float >, Vec3< float > >::iterator it;
    if( ( it = m_adjacentFaceNormals.find( vertex ) ) != m_adjacentFaceNormals.end() )
    {
        it->second = it->second + normal;
    }
    else
    {
        m_adjacentFaceNormals.insert( std::pair< Vec3< float >, Vec3< float >  >( vertex, normal ) );
    }
}

void CubeMarcher::marchCube( const std::vector< float > & volume, int x, int y, int z )
{
    Vec3< float > asEdgeVertex[12];

    const int YZ = m_dimY * m_dimZ;
    const int YZx = YZ*x;
    const int YZxp1 = YZ*(x+1);
    const int yZ = m_dimZ*y;
    const int yZp1 = m_dimZ*(y+1);
    const int zp1 = z+1;

    const float cubeValues [8] =
    {
        volume[   YZx +   yZ + z ],
        volume[ YZxp1 +   yZ + z ],
        volume[ YZxp1 + yZp1 + z ],
        volume[   YZx + yZp1 + z ],

        volume[   YZx +   yZ + zp1 ],
        volume[ YZxp1 +   yZ + zp1 ],
        volume[ YZxp1 + yZp1 + zp1 ],
        volume[   YZx + yZp1 + zp1 ]
    };

    int insideFlags = 0;
    for(int i = 0; i < 8; ++i )
    {
        if( cubeValues[ i ] <= m_targetValue )
        {
            insideFlags |= 1 << i;
        }
    }

    int edgeFlags = aiCubeEdgeFlags[ insideFlags ];

    if( edgeFlags == 0 )
    {
        return;
    }

    for( int edge = 0; edge < 12; ++edge )
    {
        if( edgeFlags & ( 1 << edge ) )
        {
            float offset = fGetOffset( cubeValues[ a2iEdgeConnection[ edge ][ 0 ] ],
                                       cubeValues[ a2iEdgeConnection[ edge ][ 1 ] ], m_targetValue );

            asEdgeVertex[ edge ].x(
                x + ( a2fVertexOffset[ a2iEdgeConnection[ edge ][ 0 ] ][ 0 ]  +  offset * a2fEdgeDirection[ edge ][ 0 ] ) );

            asEdgeVertex[ edge ].y(
                y + ( a2fVertexOffset[ a2iEdgeConnection[ edge ][ 0 ] ][ 1 ]  +  offset * a2fEdgeDirection[ edge ][ 1 ] ) );

            asEdgeVertex[ edge ].z(
                z + ( a2fVertexOffset[ a2iEdgeConnection[ edge ][ 0 ] ][ 2 ]  +  offset * a2fEdgeDirection[ edge ][ 2 ] ) );

            asEdgeVertex[ edge ].z( asEdgeVertex[ edge ].z()*-1 + m_dimZ );
        }
    }

    for( int t = 0; t < 5; ++t )
    {
        const int t3 = 3*t;

        if( a2iTriangleConnectionTable[ insideFlags ][ t3 ] < 0 )
            break;

        Vec3< float > v1 = asEdgeVertex[ a2iTriangleConnectionTable[ insideFlags ][ t3     ] ];
        Vec3< float > v2 = asEdgeVertex[ a2iTriangleConnectionTable[ insideFlags ][ t3 + 1 ] ];
        Vec3< float > v3 = asEdgeVertex[ a2iTriangleConnectionTable[ insideFlags ][ t3 + 2 ] ];

        m_vertices.push_back( v1 );
        m_vertices.push_back( v2 );
        m_vertices.push_back( v3 );

        const Vec3< float > normal = Vec3< float >::surfaceNormal( v3, v2, v1 ) * m_nMdirection;

        v1 = v1.rounded( 1 );
        v2 = v2.rounded( 1 );
        v3 = v3.rounded( 1 );

        m_normals.push_back( normal );
        m_normals.push_back( normal );
        m_normals.push_back( normal );

        addNormal( v1, normal );
        addNormal( v2, normal );
        addNormal( v3, normal );
    }
}

void CubeMarcher::marchCubes( const std::vector< float > & volume )
{
    for( int x = 0; x < m_dimX-1; ++x )
    {
        for( int y = 0; y < m_dimY-1; ++y )
        {
            for( int z = 0; z < m_dimZ-1; ++z )
            {
                marchCube( volume, x, y, z );
            }
        }
    }

    const int SZ = static_cast< int >( m_vertices.size() );
    m_normals.resize( SZ );
    for( int i = 0; i < SZ; ++i )
    {
        std::unordered_map< Vec3< float >, Vec3< float > >::iterator it;
        if( ( it = m_adjacentFaceNormals.find( m_vertices[ i ].rounded( 1 ) ) ) != m_adjacentFaceNormals.end() )
        {
            m_normals[ i ] = it->second;
            m_normals[ i ].normalize();
        }
    }
}

CubeMarcher::CubeMarcher() {}

void CubeMarcher::operator()(
    const std::vector< float > & volume,
    int dimX,
    int dimY,
    int dimZ,
    double targetValue )
{
    m_nMdirection = targetValue < 0 ? 1 : -1;
    m_targetValue = targetValue;
    m_vertices.clear();
    m_normals.clear();
    m_adjacentFaceNormals.clear();

    m_dimX = dimX;
    m_dimY = dimY;
    m_dimZ = dimZ;

    marchCubes( volume );
}

const std::vector< Vec3< float > > & CubeMarcher::getVerts() const
{
    return m_vertices;
}

const std::vector< Vec3< float > > & CubeMarcher::getNormals() const
{
    return m_normals;
}

}
