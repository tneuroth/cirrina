
#ifndef EXPRESSIONSYMBOLS_HPP
#define EXPRESSIONSYMBOLS_HPP

#include <set>
#include <string>

namespace TN {

const std::set< std::string > keyWords =
{
    "abs",  "acos",  "acosh",  "and",  "asin",  "asinh", "atan",
    "atanh", "atan2", "avg",  "break", "case", "ceil",  "clamp",
    "continue",   "cos",   "cosh",   "cot",   "csc",  "default",
    "deg2grad",  "deg2rad",   "equal",  "erf",   "erfc",  "exp",
    "expm1",  "false",   "floor",  "for",   "frac",  "grad2deg",
    "hypot", "iclamp", "if",  "else", "ilike", "in",  "inrange",
    "like",  "log",  "log10", "log2",  "logn",  "log1p", "mand",
    "max", "min",  "mod", "mor",  "mul", "ncdf",  "nand", "nor",
    "not",   "not_equal",   "null",   "or",   "pow",  "rad2deg",
    "repeat", "return", "root", "round", "roundn", "sec", "sgn",
    "shl", "shr", "sin", "sinc", "sinh", "sqrt",  "sum", "swap",
    "switch", "tan",  "tanh", "true",  "trunc", "until",  "var",
    "while", "xnor", "xor"
};

const std::set< std::string > operators =
{
    "<",  "<=", "==",
    "=",  "!=", "<>",
    ">=",  ">",
    "+", "-", "*", "/", "%", "^",
    ":=", "+=", "-=",
    "*=", "/=", "%=",
    "&", "|",
    "<=>", "?", ",", ")", "(", "[", "]", "{", "}", "~", "$", ":"
};

const std::set< char > illegalFirst =
{
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'
};


}

#endif // EXPRESSIONSYMBOLS_HPP
