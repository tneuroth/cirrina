#ifndef AORENDEROBJECT_HPP
#define AORENDEROBJECT_HPP

#include "OpenGL/Qt/Windows/RenderWindow.hpp"
#include "OpenGL/Rendering/RenderObject.hpp"
#include "Types/Vec.hpp"
#include "Cameras/TrackBallCamera.hpp"
#include "OpenGL/Rendering/RenderParams.hpp"
#include "Algorithms/Standard/Util.hpp"

#include <QVector3D>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

#include <memory>
#include <iostream>
#include <vector>

namespace TN
{

struct AORenderScene
{
    RenderWindow * parentWindow;

    std::vector< RenderObject * > renderObjects;

    std::string shaderPath;

    int xSize;
    int ySize;

    TrackBallCam m_camera;

    std::unique_ptr< QOpenGLShaderProgram > m_singleColorProg;
    std::unique_ptr< QOpenGLShaderProgram > m_multiColorProg;

    std::unique_ptr< QOpenGLShaderProgram > m_aoShaderProgram;
    std::unique_ptr< QOpenGLShaderProgram > m_aoBlurProgram;
    std::unique_ptr< QOpenGLShaderProgram > m_aoLightingProgram;

    bool initialized;

    unsigned int gBuffer;
    unsigned int gPosition, gNormal, gAlbedo;
    unsigned int ssaoFBO, ssaoBlurFBO;
    unsigned int ssaoColorBuffer, ssaoColorBufferBlur;
    unsigned int noiseTexture;

    const int MAX_KERNEL_SIZE = 512;
    std::vector<QVector3D> ssaoKernel;
    std::vector<TN::Vec3<float>> ssaoNoise;

    unsigned int aoKernelSize;
    float m_occlusionBias;
    float m_occlusionNoiseScale;
    float m_occlusionRadius;

    QOpenGLShaderProgram *m_pathTubeProgram;

    Light m_lightSource1;
    Light m_lightSource2;

    AORenderScene() : initialized( false )
    {
        xSize = 1;
        ySize = 1;
    }

    void compileShaders()
    {
        m_multiColorProg.reset( new QOpenGLShaderProgram( parentWindow ) );

        m_multiColorProg->addShaderFromSourceFile(QOpenGLShader::Vertex,  ( shaderPath + "/flatMesh.vert" ).c_str() );
        m_multiColorProg->addShaderFromSourceFile(QOpenGLShader::Fragment, ( shaderPath + "/AO/ssao_geometry.fs" ).c_str() );

        if( ! m_multiColorProg->link() )
        {
            qDebug() << "Shader Program linkage failed";
        }

        //

        m_singleColorProg.reset( new QOpenGLShaderProgram( parentWindow ) );

        m_singleColorProg->addShaderFromSourceFile(QOpenGLShader::Vertex, ( shaderPath + "/mesh.vert" ).c_str() );
        m_singleColorProg->addShaderFromSourceFile(QOpenGLShader::Fragment, ( shaderPath + "/AO/ssao_geometry.fs").c_str() );

        if( ! m_singleColorProg->link() )
        {
            qDebug() << "Shader Program linkage failed";
        }

        //

        m_aoLightingProgram.reset( new QOpenGLShaderProgram( parentWindow ) );

        m_aoLightingProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ( shaderPath + "/AO/ssao.vs" ).c_str() );
        m_aoLightingProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ( shaderPath + "/AO/ssao_lighting.fs" ).c_str() );

        if( ! m_aoLightingProgram->link() )
        {
            qDebug() << "Shader Program linkage failed";
        }

        //

        m_aoShaderProgram.reset( new QOpenGLShaderProgram( parentWindow ) );

        m_aoShaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ( shaderPath + "/AO/ssao.vs" ).c_str() );
        m_aoShaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ( shaderPath + "/AO/ssao.fs" ).c_str() );

        if( ! m_aoShaderProgram->link() )
        {
            qDebug() << "Shader Program linkage failed";
        }

        //

        m_aoBlurProgram.reset( new QOpenGLShaderProgram( parentWindow ) );

        m_aoBlurProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ( shaderPath + "/AO/ssao.vs" ).c_str() );
        m_aoBlurProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ( shaderPath + "/AO/ssao_blur.fs" ).c_str() );

        if( ! m_aoBlurProgram->link() )
        {
            qDebug() << "Shader Program linkage failed";
        }
    }

    void addRenderObject( RenderObject * renderObject )
    {
        renderObjects.push_back( renderObject );
    }

    void init( const std::string & basePath, RenderWindow * parent )
    {
        parentWindow = parent;

         shaderPath = basePath +
            std::string( "/Platform/" ) +
            ( VIDI_GL_MAJOR_VERSION == 3 ? "GL3" : "GL4" ) +
            std::string( "/shaders" );

        m_lightSource1.Ia = QVector3D( 0.5f, 0.5f, 0.5f );
        m_lightSource1.Id = QVector3D( 0.5f, 0.5f, 0.5f );
        m_lightSource1.Is = QVector3D( 0.5f, 0.5f, 0.5f );

        m_lightSource1.Ia_scale = 1.0;
        m_lightSource1.Id_scale = 1.0;
        m_lightSource1.Is_scale = 1.0;

        m_lightSource1.direction = QVector3D(.1f, 0.5f, 0.3f );
        m_lightSource1.type = 0;
        m_lightSource1.position = QVector3D( 0, 10, 100 );

        m_lightSource2.Ia = QVector3D( 0.5f, 0.5f, 0.5f );
        m_lightSource2.Id = QVector3D( 0.5f, 0.5f, 0.5f );
        m_lightSource2.Is = QVector3D( 0.5f, 0.5f, 0.5f );

        m_lightSource2.Ia_scale = 1.0;
        m_lightSource2.Id_scale = 1.0;
        m_lightSource2.Is_scale = 1.0;

        m_lightSource2.direction = -m_lightSource1.direction;
        m_lightSource2.type = 0;
        m_lightSource2.position = QVector3D( 0, 10, 100 );

        glGenFramebuffers(1, &gBuffer);
        glGenTextures(1, &gPosition);
        glGenTextures(1, &gNormal);
        glGenTextures(1, &gAlbedo);
        glGenFramebuffers(1, &ssaoFBO);
        glGenFramebuffers(1, &ssaoBlurFBO);
        glGenTextures(1, &ssaoColorBuffer);
        glGenTextures(1, &ssaoColorBufferBlur);
        glGenTextures(1, &noiseTexture);

        aoKernelSize = 256;
        m_occlusionBias = 2.0;
        m_occlusionRadius = 1.0;
        m_occlusionNoiseScale = 1.0;

        std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
        std::default_random_engine generator;

        for (unsigned int i = 0; i < aoKernelSize; ++i)
        {
            TN::Vec3<float> sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
            sample.normalize();
            sample = sample * randomFloats( generator );
            float scale = float(i) / aoKernelSize;

            // scale samples s.t. they're more aligned to center of kernel
            scale = .1 + ( scale * scale ) * ( 1.0 - .1f ); //lerp( 0.1f, 1.0f, scale * scale );
            sample = sample * scale;
            ssaoKernel.push_back( QVector3D( sample.x(), sample.y(), sample.b() ) );
        }

        for (unsigned int i = 0; i < 16; i++)
        {
            TN::Vec3< float > noise(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, 0.0f); // rotate around z-axis (in tangent space)
            ssaoNoise.push_back(noise);
        }

        glBindTexture(GL_TEXTURE_2D, noiseTexture );
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        compileShaders();

        initialized = true;
    }

    enum
    {
        MULTI_COLOR_TRIANGLE_MESH_INDEXED,
        MULTI_COLOR_TRIANGLE_MESH,
        SINGLE_COLOR_TRIANGLE_MESH
    };

    virtual ~AORenderScene()
    {
        if( initialized )
        {
            glDeleteFramebuffers(1, &gBuffer);
            glDeleteFramebuffers(1, &ssaoFBO);
            glDeleteFramebuffers(1, &ssaoBlurFBO);

            glDeleteTextures(1, &gPosition);
            glDeleteTextures(1, &gNormal);
            glDeleteTextures(1, &gAlbedo);
            glDeleteTextures(1, &ssaoColorBuffer);
            glDeleteTextures(1, &ssaoColorBufferBlur);
            glDeleteTextures(1, &noiseTexture);
        }
    }

    void setViewPortSize( double width, double height )
    {
        xSize = width;
        ySize = height;

        m_camera.setAspectRatio( width / height );

        glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

        checkError( "bind frame buffer" );

        glBindTexture(GL_TEXTURE_2D, gPosition);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);

        checkError( "make g position tecture" );

        // normal color buffer
        glBindTexture(GL_TEXTURE_2D, gNormal);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);

        checkError( "make gnormal" );

        // color + specular color buffer
        glBindTexture(GL_TEXTURE_2D, gAlbedo);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedo, 0);

        checkError( "make galbedo" );

        // tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
        unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
        glDrawBuffers(3, attachments);

        checkError( "color attatchment" );

        // create and attach depth buffer (renderbuffer)
        unsigned int rboDepth;
        glGenRenderbuffers(1, &rboDepth);
        glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height );
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);

        checkError( "depth buffer" );

        // finally check if framebuffer is complete
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "Framebuffer not complete!" << std::endl;

        glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);

        checkError( "after bind ssao FBO" );

        // SSAO color buffer
        glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBuffer, 0);
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "SSAO Framebuffer not complete!" << std::endl;

        checkError( "tex 1" );

        // and blur stage
        glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
        glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBufferBlur, 0);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "SSAO Blur Framebuffer not complete!" << std::endl;

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
};

}

#endif // AORENDEROBJECT_HPP
