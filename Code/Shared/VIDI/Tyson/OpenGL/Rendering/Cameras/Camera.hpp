
#ifndef CAMERA2D_HPP_INCLUDED
#define CAMERA2D_HPP_INCLUDED

#include "Types/Vec.hpp"

#include <QMatrix4x4>
#include <QVector4D>
#include <QVector2D>

namespace TN {

class Camera {

protected:

    QMatrix4x4 m_view;
    QMatrix4x4 m_proj;
    double     m_width;
    double     m_height;
    double     m_zoom;

public:

    double getZoom() const { return m_zoom; }
    const QMatrix4x4 & view() const { return m_view; }
    const QMatrix4x4 & proj() const { return m_proj; }

    virtual void setSize( double width, double height ) = 0;
    virtual void setZoom( double z ) = 0;
    virtual double wheel( double degrees ) = 0;
    virtual Vec2< float > dragLeftButton(  Vec2< float > delta ) = 0;
    virtual Vec2< float > dragRightButton( Vec2< float > delta ) = 0;
    virtual void reset() = 0;


    virtual ~Camera() {}
};




}

#endif
