


#ifndef SAMPLER_HPP
#define SAMPLER_HPP

#include "Types/Vec.hpp"
#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"
#include "OpenGL/Widgets/Viewport/ViewPort.hpp"
#include "OpenGL/Widgets/ComboWidget.hpp"
#include "OpenGL/Widgets/SliderWidget.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"
#include "Data/Managers/ParticleDataManager.hpp"

#include <QDebug>

#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include <unordered_map>

namespace TN
{

enum class SamplingMethod
{
    PANNING_WINDOW,
    LASSO,
    COORDINATES,
    CONTOURS,
    LINE,
    DRAW_CURVE
};

struct SamplerParams
{
    Vec2<float> upperLeftScreenWorldSpace;
    Vec2<float> offsetScreenSpace;
    double       conversionRatio;

    SamplerParams() {}

    SamplerParams(
        const Vec2<float> & ul,
        const Vec2<float> & of,
        double cr )
        : upperLeftScreenWorldSpace( ul ),
          offsetScreenSpace( of ),
          conversionRatio( cr )
    {}
};

template < typename PointType, typename ValueType >
class Sampler : public TN::Widget
{

public:

    const float SMALL_EDGE_MARGIN = 20;
    const float BIG_EDGE_MARGIN = 60;
    const int CONTROL_PANEL_SPACING = 22;
    const int BUTTON_HEIGHT = 24;
    const int CONTROL_PANEL_HEIGHT = 3 * BUTTON_HEIGHT + 4 * CONTROL_PANEL_SPACING / 2.0;

private:

    int m_populationDataType;
    int m_buttonWidth;

public:

    SliderWidget numPartitionsSlider;

    ComboWidget  normalizationCombo;
    SliderWidget normalizationSlider;

    SliderWidget resolutionSliderX;
    SliderWidget resolutionSliderY;

    SliderWidget valueWidthSliderX;
    SliderWidget valueWidthSliderY;

    SliderWidget particleOpacitySlider;
    SliderWidget histogramOpacitySlider;

    const double RANGE_FACTOR = 10.f;
    const int m_maxPartitions =  128;

    SliderWidget projectionScaleSliderX;
    SliderWidget projectionScaleSliderY;

    PressButton resetToDefaultButton;
    PressButton resetCameraButton;

    PressButton linkResolutionSlidersButton;
    PressButton linkValueWidthSlidersButton;

private:

    //////////////////////////////////////////////////////////////////////////////
    // panning window mode

    //! maps particle indices to grid cells (histogram)
    std::vector< std::vector< int > > m_gridMap;

    //! maps particles to gid cells, and bin's in those cells (histogram + bin)
    std::vector< std::pair< int, unsigned int > > ptclMap;

    /*! stores non-empty histograms, non-empty bins, and their counts/frequencies
     [gridCellId]->( [ bin ID ]->count )   */
    std::unordered_map< int, std::unordered_map< int, double > > histogramMap;

    //! corners of that define the layout in world space
    std::vector< Vec2<float> > gridCorners;
    std::vector< Vec2<float> > nonEmptyGridCorners;

    //! center points for each cell in world space
    std::vector< Vec2<float> > gridCenters;
    std::vector< Vec2<float> > nonEmptyGridCenters;

    /*! Orientation
          1 3
          2 4  */
    Vec2<double> gridCorner1;
    Vec2<double> gridCorner2;
    Vec2<double> gridCorner3;
    Vec2<double> gridCorner4;

    double gridWidth;
    double gridHeight;

    unsigned int numRows;
    unsigned int numCols;

    unsigned int oldNumRows;
    unsigned int oldNumCols;

    double cellHeight;
    double cellWidth;
    double cellHalfWidth;

    float rowsOverHeight;
    float colsOverWidth;

    int lastRow;
    int lastCol;

    I2 m_defaultHistResolution;

    int focusedCell;
    int selectedCell;

    int focusedBin;
    int selectedBin;

    ////////////////////////////////////////////////////////////////////////////////////

    std::vector< float > selectedHistogramFrequencies;

    std::vector< unsigned int  > focusedSubSampleIndices;
    std::vector< unsigned int  > selectedSubSampleIndices;

    SamplingMethod samplingMethod;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////


    int gridCellIndexBruteForce( const PointType & p )
    {
        PointType c1, c2, c3, c4;
        int index = -1;
        const int SZ = gridCorners.size() - 3;

        #pragma omp parallel for
        for ( int k = 0; k < SZ; k += 4 )
        {
            c1 = gridCorners[k];
            c2 = gridCorners[k+1];
            c3 = gridCorners[k+2];
            c4 = gridCorners[k+3];

            if ( p.x() <= c1.x() && p.x() >= c3.x()
                    && p.y() <= c1.y() && p.y() >= c2.y() )
            {
                index = k/4;
            }
        }

        return index;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

public:

    std::pair<int, int> getBinFromPointInWorldSpace2D( const PointType & p )
    {
        int cellIndex = gridCellIndex( p.x(), p.y() );

        int cellRow = std::floor( ( ( p.y() - gridCorners[ cellIndex * 4 + 1 ].y() ) / ( double ) cellHeight ) * (getAdjustedHistResolution().b() ) );
        int cellCol = std::floor( ( ( p.x() - gridCorners[ cellIndex * 4 + 2 ].x() ) / ( double )  cellWidth ) * (getAdjustedHistResolution().a() ) );

        cellRow = std::min( cellRow, getAdjustedHistResolution().b() - 1 );
        cellCol = std::min( cellCol, getAdjustedHistResolution().b() - 1 );

        return std::pair< int, int >( cellIndex, cellRow * getAdjustedHistResolution().a() + cellCol );
    }

private:

    //////////////////////////////////////////////////////////////////////////////////////////////

    std::pair<int, int> getBinFromPointInWorldSpace1D( const PointType & p )
    {

        int cellIndex = gridCellIndex( p );
        int cellCol = std::round( ( ( p.x() - gridCorners[ cellIndex * 4 + 2 ].x() ) / ( double )  cellWidth ) * (getAdjustedHistResolution().a()-1) );

        return std::pair< int, int >( cellIndex, cellCol );
    }

    //! focus the particles in the histogram bin at position x,y in screen space
    int setFocus( const PointType & p, int histDim, const std::string & target = "cell" )
    {
        focusedSubSampleIndices.clear();
        focusedBin  = -1;
        focusedCell = -1;

        if ( p.x() <= gridCorner1.x() || p.x() >= gridCorner3.x()
                || p.y() <= gridCorner2.y() || p.y() >= gridCorner1.y() )
        {
            return 0;
        }

        std::pair< int, int > id = histDim == 2 ? getBinFromPointInWorldSpace2D( p )
                                   : getBinFromPointInWorldSpace1D( p );

        std::unordered_map< int, std::unordered_map< int, double > >::const_iterator it;

        if( ( it = histogramMap.find( id.first ) ) == histogramMap.end() )
        {
            return 0;
        }

        focusedCell = id.first;

        int binIdx = id.second;
        if( target == "cell" && selectedBin != -1 )
        {
            binIdx = selectedBin;
        }
        else if ( target == "cell" )
        {
            return 0;
        }

        std::unordered_map< int, double >::const_iterator it2;
        if( ( it2 = it->second.find( binIdx ) ) == it->second.end() )
        {
            return 0;
        }

        const unsigned int COUNT = it->second.find( binIdx )->second;
        if( COUNT == 0 )
        {
            return 0;
        }

        if( target == "bin" || selectedBin != -1 )
        {
            focusedBin = binIdx;
        }

        focusedSubSampleIndices.reserve( COUNT );
        const int SZ = ptclMap.size();
        for( int i = 0; i < SZ; ++i )
        {
            if( ptclMap[i].first == focusedCell && ptclMap[i].second == focusedBin )
            {
                focusedSubSampleIndices.push_back( i );
            }
        }

        return COUNT;
    }

    void setFocusDistribution( const PointType & p )
    {
        focusedSubSampleIndices.clear();
        focusedBin  = -1;
        focusedCell = -1;

        if ( p.x() <= gridCorner1.x() || p.x() >= gridCorner3.x()
                || p.y() <= gridCorner2.y() || p.y() >= gridCorner1.y() )
        {
            return;
        }

        focusedCell = gridCellIndex( p.x(), p.y() );
        if( ! m_gridMap[ focusedCell ].size() )
        {
            focusedCell = -1;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////

    /*! selects the particles in the histogram bin at position x,y in world space */
    int select( const PointType & p, int histDim, const std::string & target )
    {        
        int numFound = setFocus( p, histDim, target );

        selectedBin = focusedBin;
        int previousCell = selectedCell;

        selectedCell = focusedCell;

        if( selectedCell == -1 )
        {
            clearFocus();
            clearSelection();
            return 0;
        }

        if( selectedCell != -1 && selectedBin == -1 || ( target == "cell" && previousCell == selectedCell ) )
        {
            focusedSubSampleIndices.clear();
            selectedSubSampleIndices.clear();

            focusedBin = -1;
            selectedBin = -1;
            return 0;
        }
        else
        {
            selectedSubSampleIndices = focusedSubSampleIndices;
        }

        return numFound;
    }

    //////////////////////////////////////////////////////////////////////////////////

    void calculatePanningWindows( const SamplerParams & params )
    {
        double detailFactor = numPartitionsSlider.value() * m_maxPartitions / 20.0;

        double nl = std::max( std::ceil( detailFactor * 2 ), 1.0 );
        double h = ( height() - CONTROL_PANEL_HEIGHT - BIG_EDGE_MARGIN ) * params.conversionRatio;
        double w =  ( width() - BIG_EDGE_MARGIN ) * params.conversionRatio;

        //qDebug() << "width is " << width();
        //qDebug() << "position is " << position().x();
        //qDebug() << "grid width is " << ( width() - BIG_EDGE_MARGIN );

        int cols;
        int rows;

        double d;

        if( h > w )
        {
            w -= SMALL_EDGE_MARGIN *  params.conversionRatio;
            d = w / nl;
            cols = nl;
            rows = h / d;
        }
        else
        {
            h -= SMALL_EDGE_MARGIN *  params.conversionRatio;
            d = h / nl;
            rows = nl;
            cols = w / d;
        }

        //qDebug() << "d is " << d << ", cols is " << cols << "d screen is " << d / params.conversionRatio;

        if( cols <= 0 || rows <= 0 )
        {
            //qDebug() << "no cols or rows";
            return;
        }

        double x = params.upperLeftScreenWorldSpace.x() + ( params.offsetScreenSpace.x() + BIG_EDGE_MARGIN ) * params.conversionRatio;
        double y = params.upperLeftScreenWorldSpace.y() - ( height() + 40 ) * params.conversionRatio + rows * d + BIG_EDGE_MARGIN * params.conversionRatio;

        //qDebug() << "x is " << x << ", in screen space " << ( x - params.upperLeftScreenWorldSpace.x() ) / params.conversionRatio;

        gridCorners.resize( rows*cols*4 );
        gridCenters.resize( rows*cols );

        #pragma omp parallel for
        for ( int c = 0; c < cols; ++c )
        {
            for ( int r = 0; r < rows; ++r )
            {
                Vec2< float > c1( (x+d*( c+1 ) ), ( y - d*( r     ) ) );
                Vec2< float > c2( (x+d*( c+1 ) ), ( y - d*( r + 1 ) ) );
                Vec2< float > c3( (x+d*( c   ) ), ( y - d*( r     ) ) );
                Vec2< float > c4( (x+d*( c   ) ), ( y - d*( r + 1 ) ) );

                gridCorners[ ( c*rows+r )*4     ] = c1;
                gridCorners[ ( c*rows+r )*4 + 1 ] = c2;
                gridCorners[ ( c*rows+r )*4 + 2 ] = c3;
                gridCorners[ ( c*rows+r )*4 + 3 ] = c4;

                gridCenters[ c*rows+r ] = Vec2< float >( c1.x() - d / 2.f, c1.y() - d / 2.f );
            }
        }

        //1 3
        //2 4

        gridCorner1 = gridCorners[0+2];
        gridCorner2 = gridCorners[(rows-1)*4+3];
        gridCorner3 = gridCorners[(rows*(cols-1))*4+0];
        gridCorner4 = gridCorners[(rows*cols - 1 )*4+1];

        gridHeight  = gridCorner1.y() - gridCorner2.y();
        gridWidth   = gridCorner3.x() - gridCorner1.x();

        //qDebug() << "new grid width is " << gridWidth /  params.conversionRatio;
        //qDebug() << "grid right is " << gridCorner3.x() << ( gridCorner3.x() - params.upperLeftScreenWorldSpace.x() ) / params.conversionRatio;

        cellWidth     = gridWidth / numCols;
        cellHalfWidth = cellWidth / 2.0;
        cellHeight    = gridHeight / numRows;

        numRows = rows;
        numCols = cols;

        lastRow = rows-1;
        lastCol = cols-1;

        colsOverWidth  = numCols / gridWidth;
        rowsOverHeight = numRows / gridHeight;

        if ( oldNumCols != numCols ||oldNumRows != numRows )
        {
            updateGridMap();
            oldNumCols = numCols;
            oldNumRows = numRows;
        }

        return;
    }

    void clearFocus()
    {
        focusedSubSampleIndices.clear();
        focusedBin  = -1;
        focusedCell = -1;
    }

    void clearSelection()
    {
        selectedSubSampleIndices.clear();
        selectedBin  = -1;
        selectedCell = -1;
    }

public:

    void updateSelection()
    {
        if( selectedCell < 0 )
        {
            clearFocus();
            clearSelection();
            return;
        }
        else if( selectedBin < 0 )
        {
            // clear everything except the selected cell
            focusedBin = -1;
            focusedCell = -1;
            selectedBin = -1;
            focusedSubSampleIndices.clear();
            selectedSubSampleIndices.clear();
        }
        else
        {
            // we have a selected cell and bin, so update the selected sub samples based on the current gridMap
            focusedBin = -1;
            focusedCell = -1;
            focusedSubSampleIndices.clear();

            selectedSubSampleIndices.clear();
            const int SZ = ptclMap.size();
            for( int i = 0; i < SZ; ++i )
            {
                if( ptclMap[i].first == selectedCell && ptclMap[i].second == selectedBin )
                {
                    selectedSubSampleIndices.push_back( i );
                }
            }
        }
    }

    void resetToDefault()
    {
        resolutionSliderX.setSliderPos( 1.0 / RANGE_FACTOR );
        resolutionSliderY.setSliderPos( 1.0 / RANGE_FACTOR );

        valueWidthSliderX.setSliderPos( 1.0 );
        valueWidthSliderY.setSliderPos( 1.0 );

        histogramOpacitySlider.setSliderPos( 1.0 );
        particleOpacitySlider.setSliderPos( 0.2 );

        projectionScaleSliderX.setSliderPos( 0.5 );
        projectionScaleSliderY.setSliderPos( 0.5 );
    }

    int gridCellIndex( const PointType & p )
    {
        // it's outside the grid
        if ( p.x() <= gridCorner1.x() || p.x() >= gridCorner3.x()
          || p.y() <= gridCorner2.y() || p.y() >= gridCorner1.y() )
        {
            return  -1;
        }

        float dx = p.x() - gridCorner1.x();
        float dy = gridCorner1.y() - p.y();

        int col = std::min( static_cast< int >( std::floor( colsOverWidth  * dx ) ), lastCol );
        int row = std::min( static_cast< int >( std::floor( rowsOverHeight * dy ) ), lastRow );

        return col * numRows + row;
    }

    Sampler() :
        focusedBin( -1 ),
        selectedBin( -1 ),
        focusedCell( -1 ),
        selectedCell( -1 ),
        samplingMethod( SamplingMethod::PANNING_WINDOW )
    {
        linkValueWidthSlidersButton.setPressed( true );
        linkResolutionSlidersButton.setPressed( true );
    }

    void dblClick( const PointType & p, int histDim, const std::string & target )
    {
        if( samplingMethod == SamplingMethod::PANNING_WINDOW )
        {
            if( m_populationDataType == PopulationDataType::PARTICLES )
            {
                select( p, histDim, target );
            }
            else if( m_populationDataType == PopulationDataType::GRID_POINTS )
            {
                selectedCell = gridCellIndex( p.x(), p.y() );
            }
        }
    }

    void mouseMove( const PointType & p, int histDim, const std::string & target = "cell" )
    {
        if( samplingMethod == SamplingMethod::PANNING_WINDOW )
        {
            if( m_populationDataType == PopulationDataType::PARTICLES )
            {
                setFocus( p, histDim, target );
            }
            else if( m_populationDataType == PopulationDataType::GRID_POINTS )
            {
                setFocusDistribution( p );
            }
        }
    }

    void mouseDrag( const PointType & p )
    {
        if( samplingMethod == SamplingMethod::PANNING_WINDOW )
        {
            clearFocus();
            clearSelection();
        }
    }

    void mouseOut()
    {
        if( samplingMethod == SamplingMethod::PANNING_WINDOW )
        {
            clearFocus();
        }
    }

    void dblRightClick( const PointType & p )
    {

    }

    void rightMouseDrag( const PointType & p, const Vec2< float > & delta )
    {

    }

    void resize( float width, float height )
    {
        setSize( width, height );
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int gridCellIndex( float x, float y )
    {
        // it's outside the grid
        if ( x <= gridCorner1.x() || x >= gridCorner3.x()
          || y <= gridCorner2.y() || y >= gridCorner1.y() )
        {
            return  -1;
        }

        const float dx = x - gridCorner1.x();
        const float dy = gridCorner1.y() - y;

        const int col = std::min( static_cast< int >( std::floor( colsOverWidth  * dx ) ), lastCol );
        const int row = std::min( static_cast< int >( std::floor( rowsOverHeight * dy ) ), lastRow );

        return col * numRows + row;
    }

    void map2DParticlesTo2DHistograms(
        const std::vector< float > & xSpatial,
        const std::vector< float > & ySpatial,
        const std::vector< float > & xBinning,
        const std::vector< float > & yBinning,
        const I2                   & adjustedResolution,
        const std::array< Vec2< float >, 2 > & histThresholds,
        const Vec2< float >        & spatialScaling  )
    {
//        m_defaultHistResolution = histResolution;
        histogramMap.clear();

        const int SZ = xSpatial.size();
        ptclMap.resize( SZ );

        const float X_WIDTH = histThresholds[ 0 ].b() - histThresholds[ 0 ].a();
        const float Y_WIDTH = histThresholds[ 1 ].b() - histThresholds[ 1 ].a();

        #pragma omp parallel for
        for ( int i = 0; i < SZ; ++i )
        {
            const int gridCell = gridCellIndex( xSpatial[i] * spatialScaling.x(), ySpatial[ i ] * spatialScaling.y() );
            if ( gridCell != -1 )
            {
                int r = std::floor( ( ( yBinning[ i ] - histThresholds[ 1 ].a() ) / Y_WIDTH ) * adjustedResolution.b() );
                int c = std::floor( ( ( xBinning[ i ] - histThresholds[ 0 ].a() ) / X_WIDTH ) * adjustedResolution.a() );

                //! clamping, probably should be an option
                r = std::max( std::min( r, adjustedResolution.b() - 1 ), 0 );
                c = std::max( std::min( c, adjustedResolution.a() - 1 ), 0 );

                ptclMap[i].first = gridCell;
                ptclMap[i].second = r * adjustedResolution.a() + c;
            }
            else
            {
                ptclMap[i].first = -1;
            }
        }

        // accumulates/reduces to get a count for each bin
        for ( int i = 0; i < SZ; ++i )
        {
            if ( ptclMap[i].first == -1 )
            {
                continue;
            }

            auto it = histogramMap.find( ptclMap[i].first );
            if ( it == histogramMap.end() )
            {
                std::unordered_map< int, double > m;
                m.insert( std::pair< int, double >( ptclMap[i].second, 1.f ) );

                histogramMap.insert(
                    std::pair<
                    int,
                    std::unordered_map< int, double > > ( ptclMap[i].first, m ) );
            }
            else
            {
                auto it2 = it->second.find( ptclMap[i].second );
                if( it2 == it->second.end() )
                {
                    it->second.insert( std::pair< int, double >( ptclMap[i].second, 1.f ) );
                }
                else
                {
                    it2->second += 1.f;
                }
            }
        }

        const size_t HMSZ = histogramMap.size();

        nonEmptyGridCenters.resize( HMSZ     );
        nonEmptyGridCorners.resize( HMSZ * 4 );

        int i = 0;
        for( auto e : histogramMap )
        {
            const size_t idx = e.first;
            nonEmptyGridCenters[ i         ] = gridCenters[ idx         ];
            nonEmptyGridCorners[ i * 4     ] = gridCorners[ idx * 4     ];
            nonEmptyGridCorners[ i * 4 + 1 ] = gridCorners[ idx * 4 + 1 ];
            nonEmptyGridCorners[ i * 4 + 2 ] = gridCorners[ idx * 4 + 2 ];
            nonEmptyGridCorners[ i * 4 + 3 ] = gridCorners[ idx * 4 + 3 ];
            ++i;
        }
    }

    void map2DParticlesTo2DHistograms(
        const std::vector< float > & xSpatial,
        const std::vector< float > & ySpatial,
        const std::vector< float > & xBinning,
        const std::vector< float > & yBinning,
        const std::vector< float > & weights,
        const I2                   & adjustedResolution,
        const std::array< Vec2< float >, 2 > & histThresholds,
        const Vec2< float >        & spatialScaling  )
    {
//        m_defaultHistResolution = histResolution;
        histogramMap.clear();

        const int SZ = xSpatial.size();
        ptclMap.resize( SZ );

        const float X_WIDTH = histThresholds[ 0 ].b() - histThresholds[ 0 ].a();
        const float Y_WIDTH = histThresholds[ 1 ].b() - histThresholds[ 1 ].a();

        #pragma omp parallel for
        for ( int i = 0; i < SZ; ++i )
        {
            const int gridCell = gridCellIndex( xSpatial[i] * spatialScaling.x(), ySpatial[ i ] * spatialScaling.y() );
            if ( gridCell != -1 )
            {
                int r = std::floor( ( ( yBinning[ i ] - histThresholds[ 1 ].a() ) / Y_WIDTH ) * adjustedResolution.b() );
                int c = std::floor( ( ( xBinning[ i ] - histThresholds[ 0 ].a() ) / X_WIDTH ) * adjustedResolution.a() );

                //! clamping, probably should be an option
                r = std::max( std::min( r, adjustedResolution.b() - 1 ), 0 );
                c = std::max( std::min( c, adjustedResolution.a() - 1 ), 0 );

                ptclMap[i].first = gridCell;
                ptclMap[i].second = r * adjustedResolution.a() + c;
            }
            else
            {
                ptclMap[i].first = -1;
            }
        }

        // accumulates/reduces to get a count for each bin
        for ( int i = 0; i < SZ; ++i )
        {
            if ( ptclMap[i].first == -1 )
            {
                continue;
            }

            auto it = histogramMap.find( ptclMap[i].first );
            if ( it == histogramMap.end() )
            {
                std::unordered_map< int, double > m;
                m.insert( std::pair< int, double >( ptclMap[i].second, weights[ i ] ) );

                histogramMap.insert(
                    std::pair<
                    int,
                    std::unordered_map< int, double > > ( ptclMap[i].first, m ) );
            }
            else
            {
                auto it2 = it->second.find( ptclMap[i].second );
                if( it2 == it->second.end() )
                {
                    it->second.insert( std::pair< int, double >( ptclMap[i].second, weights[ i ] ) );
                }
                else
                {
                    it2->second += weights[ i ];
                }
            }
        }

        const size_t HMSZ = histogramMap.size();

        nonEmptyGridCenters.resize( HMSZ     );
        nonEmptyGridCorners.resize( HMSZ * 4 );

        int i = 0;
        for( auto e : histogramMap )
        {
            const size_t idx = e.first;
            nonEmptyGridCenters[ i         ] = gridCenters[ idx         ];
            nonEmptyGridCorners[ i * 4     ] = gridCorners[ idx * 4     ];
            nonEmptyGridCorners[ i * 4 + 1 ] = gridCorners[ idx * 4 + 1 ];
            nonEmptyGridCorners[ i * 4 + 2 ] = gridCorners[ idx * 4 + 2 ];
            nonEmptyGridCorners[ i * 4 + 3 ] = gridCorners[ idx * 4 + 3 ];
            ++i;
        }
    }

    Vec2< float > getAdjustedValueRanges( const Vec2< float > & vRange, double modifier )
    {
        double adjustedValueOffset = ( ( vRange.b() - vRange.a() ) / 2.0 ) * ( 1.0 - modifier );
        return Vec2< float >( vRange.a() + adjustedValueOffset, vRange.b() - adjustedValueOffset );
    }

    void mapParticlesToHistograms(
        const std::vector< float > & xParition,
        const std::vector< float > & yParition,
        const std::vector< float > & xBinning,
        const std::vector< float > & yBinning,
        const std::vector< Vec2< float > > & valueRange,
        const std::vector< float > & valueRangeModifiers,
        const std::vector< double > & spatialScaling )
    {
        //! ...
        map2DParticlesTo2DHistograms(
            xParition,
            yParition,
            xBinning,
            yBinning,
            getAdjustedHistResolution(),
            std::array< Vec2< float >, 2 >(
        {
            getAdjustedValueRanges( valueRange[ 0 ], valueRangeModifiers[ 0 ] ),
            getAdjustedValueRanges( valueRange[ 1 ], valueRangeModifiers[ 1 ] )
        } ),
        Vec2< float >( spatialScaling[ 0 ],  spatialScaling[ 1 ]  ) );
    }

    void mapParticlesToHistograms(
        const std::vector< float > & xParition,
        const std::vector< float > & yParition,
        const std::vector< float > & xBinning,
        const std::vector< float > & yBinning,
        const std::vector< float > & weights,
        const std::vector< Vec2< float > > & valueRange,
        const std::vector< float > & valueRangeModifiers,
        const std::vector< double > & spatialScaling )
    {
        //! ...
        map2DParticlesTo2DHistograms(
            xParition,
            yParition,
            xBinning,
            yBinning,
            weights,
            getAdjustedHistResolution(),
            std::array< Vec2< float >, 2 >(
        {
            getAdjustedValueRanges( valueRange[ 0 ], valueRangeModifiers[ 0 ] ),
            getAdjustedValueRanges( valueRange[ 1 ], valueRangeModifiers[ 1 ] )
        } ),
        Vec2< float >( spatialScaling[ 0 ],  spatialScaling[ 1 ]  ) );
    }

    void clusterGridPoints(
        const std::vector< float > & xPos,
        const std::vector< float > & yPos,
        double xScale, double yScale )
    {
        for ( size_t i = 0, end = m_gridMap.size(); i < end; ++i )
        {
            m_gridMap[i].clear();
        }

        const int SZ = xPos.size();
        for ( int i = 0; i < SZ; ++i )
        {
            int index = gridCellIndex( xPos[i] * xScale, yPos[ i ] * yScale );
            if ( index < 0 )
            {
                continue;
            }

            m_gridMap[ index ].push_back( i );
        }
    }

    void updateGridMap()
    {
        int SZ = numRows*numCols;
        m_gridMap.resize( numRows*numCols );

        for( int i = 0; i < SZ; ++i )
        {
            m_gridMap[i].clear();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    void recalculate( const SamplerParams & params )
    {
        if( samplingMethod == SamplingMethod::PANNING_WINDOW )
        {
            //qDebug() << "calcilating panning windows";
            return calculatePanningWindows( params );
        }
        else
        {
            return;
        }
    }
    void setViewPort( const ViewPort & vp )
    {
        m_viewPort = vp;
        //if( m_populationDataType != PopulationDataType::GRID_POINTS )
        //{
        m_buttonWidth = ( vp.width() - 5 * CONTROL_PANEL_SPACING - 7 ) / 4.0;
        // }
        // else
        //{
        //    m_buttonWidth = ( vp.width() - 4 * CONTROL_PANEL_SPACING - 7 ) / 3.0;
        // }
        float labelWidth = 120;
        float linkButtonWidth = 14;

        //

        float offX = 0;

        //

        float ySpace = CONTROL_PANEL_SPACING / 2.0;

        numPartitionsSlider.setSize( m_buttonWidth - labelWidth, BUTTON_HEIGHT );
        numPartitionsSlider.setPosition( vp.offsetX() + CONTROL_PANEL_SPACING + labelWidth, vp.offsetY() + vp.height() - ( BUTTON_HEIGHT + ySpace ) * 3 );

        offX += m_buttonWidth + CONTROL_PANEL_SPACING;

        //

        normalizationCombo.setSize( m_buttonWidth - labelWidth, BUTTON_HEIGHT );
        normalizationSlider.setSize( m_buttonWidth - labelWidth, BUTTON_HEIGHT );

        normalizationCombo.setPosition(
            vp.offsetX() + CONTROL_PANEL_SPACING + ( m_buttonWidth + CONTROL_PANEL_SPACING ) + labelWidth,
            vp.offsetY() + vp.height() - BUTTON_HEIGHT - ySpace );
        normalizationSlider.setPosition( normalizationCombo.position().x(), normalizationCombo.position().y() - ( BUTTON_HEIGHT + ySpace ) );

        //

        particleOpacitySlider.setSize( m_buttonWidth - labelWidth , BUTTON_HEIGHT );
        particleOpacitySlider.setPosition(
            vp.offsetX() + CONTROL_PANEL_SPACING + ( m_buttonWidth + CONTROL_PANEL_SPACING ) * 2 + labelWidth,
            vp.offsetY() + vp.height() - ( BUTTON_HEIGHT + ySpace ) * 3 );

        histogramOpacitySlider.setSize( m_buttonWidth - labelWidth, BUTTON_HEIGHT );
        histogramOpacitySlider.setPosition(
            particleOpacitySlider.position().x(),
            vp.offsetY() + vp.height() - ( BUTTON_HEIGHT + ySpace ) * 2 );

        //if( m_populationDataType != "distribution" )
        //{
//        resolutionSliderX.setSize( m_buttonWidth - labelWidth - linkButtonWidth - CONTROL_PANEL_SPACING, BUTTON_HEIGHT );
        resolutionSliderX.setSize( m_buttonWidth - labelWidth, BUTTON_HEIGHT );
        resolutionSliderX.setPosition( normalizationCombo.position().x(), vp.offsetY() + vp.height() - ( BUTTON_HEIGHT + ySpace ) * 3 );

        //resolutionSliderY.setSize( resolutionSliderX.size().x(), BUTTON_HEIGHT );
        //resolutionSliderY.setPosition( vp.offsetX() + CONTROL_PANEL_SPACING + ( m_buttonWidth + CONTROL_PANEL_SPACING ) + labelWidth, resolutionSliderX.position().y() - ( BUTTON_HEIGHT + ySpace ) );

//        linkResolutionSlidersButton.setSize( linkButtonWidth, BUTTON_HEIGHT  + ySpace + 4 );
//        linkResolutionSlidersButton.setPosition(
//            resolutionSliderX.position().x() + resolutionSliderX.size().x() + CONTROL_PANEL_SPACING,
//            resolutionSliderY.position().y() + BUTTON_HEIGHT / 2.0 - 2
//        );
        //

        valueWidthSliderX.setSize( m_buttonWidth - labelWidth - linkButtonWidth - CONTROL_PANEL_SPACING, BUTTON_HEIGHT );
        valueWidthSliderX.setPosition(
            vp.offsetX() + CONTROL_PANEL_SPACING + ( m_buttonWidth + CONTROL_PANEL_SPACING ) * 3 + labelWidth,
            vp.offsetY() + vp.height() - ( BUTTON_HEIGHT + ySpace ) );

        valueWidthSliderY.setSize( m_buttonWidth - labelWidth - linkButtonWidth - CONTROL_PANEL_SPACING, BUTTON_HEIGHT );
        valueWidthSliderY.setPosition(
            valueWidthSliderX.position().x(),
            vp.offsetY() + vp.height() - ( BUTTON_HEIGHT + ySpace ) * 2 );

        linkValueWidthSlidersButton.setSize( linkButtonWidth, BUTTON_HEIGHT  + ySpace + 4 );
        linkValueWidthSlidersButton.setPosition(
            valueWidthSliderX.position().x() + valueWidthSliderX.size().x() + CONTROL_PANEL_SPACING,
            valueWidthSliderY.position().y() + BUTTON_HEIGHT / 2.0 - 2
        );
        //    }

        projectionScaleSliderX.setSize( m_buttonWidth - labelWidth, BUTTON_HEIGHT );
        projectionScaleSliderX.setPosition(
            vp.offsetX() + CONTROL_PANEL_SPACING + ( m_buttonWidth + CONTROL_PANEL_SPACING ) * 2 + labelWidth,
            vp.offsetY() + vp.height() - ( BUTTON_HEIGHT + ySpace ) );

        projectionScaleSliderY.setSize( m_buttonWidth - labelWidth, BUTTON_HEIGHT );
        projectionScaleSliderY.setPosition(
            projectionScaleSliderX.position().x(),
            projectionScaleSliderX.position().y() - ( BUTTON_HEIGHT + ySpace ) );

        resetCameraButton.setSize( ( m_buttonWidth - CONTROL_PANEL_SPACING ) / 2.0, BUTTON_HEIGHT );
        resetCameraButton.setPosition( valueWidthSliderX.position().x() - labelWidth, projectionScaleSliderY.position().y() - ( BUTTON_HEIGHT + ySpace ) );

        resetToDefaultButton.setSize( ( m_buttonWidth - CONTROL_PANEL_SPACING ) / 2.0, BUTTON_HEIGHT );
        resetToDefaultButton.setPosition(
            resetCameraButton.position().x() + resetCameraButton.size().x() + CONTROL_PANEL_SPACING,
            projectionScaleSliderY.position().y() - ( BUTTON_HEIGHT + ySpace ) );
    }

    const std::vector< Vec2< float > > & getGridCorners( ) const
    {
        return gridCorners;
    }
    const std::vector< Vec2< float > > & getNonEmptyGridCorners( ) const
    {
        return nonEmptyGridCorners;
    }
    const std::vector< Vec2< float > > & getNonEmptyGridCenters( ) const
    {
        return nonEmptyGridCenters;
    }
    const std::vector< Vec2< float > > & getGridCenters( ) const
    {
        return gridCenters;
    }
    const std::unordered_map< int, std::unordered_map< int, double > > &
    getHistogramMap() const
    {
        return histogramMap;
    }

    const std::vector< unsigned int > & getHighlightIndices( ) const
    {
        return focusedSubSampleIndices.size() ? focusedSubSampleIndices : selectedSubSampleIndices;
    }

    const std::vector< float > & getSelectedHistogramFrequencies()
    {
        selectedHistogramFrequencies.resize( getAdjustedHistResolution().a() * getAdjustedHistResolution().b() );
        std::fill( selectedHistogramFrequencies.begin(), selectedHistogramFrequencies.end(), 0 );

        const auto & it = histogramMap.find( ( ( focusedBin >= 0 )  ? focusedCell : selectedCell ) );
        if( it != histogramMap.end() )
        {
            for( const auto & e : it->second )
            {
                selectedHistogramFrequencies[ e.first ] = e.second;
            }
        }
        return selectedHistogramFrequencies;
    }

    int getSelectedCellId( )
    {
        if( focusedCell >= 0 )
        {
            return focusedCell;
        }
        return selectedCell;
    }

    int getSelectedBinId( )
    {
        if( focusedBin >= 0 )
        {
            return focusedBin;
        }
        return selectedBin;
    }

    int getFocusBinId()
    {
        return focusedBin;
    }

    I2 getAdjustedHistResolution()
    {
        if( m_populationDataType == PopulationDataType::PARTICLES )
        {
            float r1 = resolutionSliderX.sliderPosition();
            float r2 = resolutionSliderY.sliderPosition();
            I2 newResolution( r1 * m_defaultHistResolution.a() * RANGE_FACTOR, r2 * m_defaultHistResolution.b() * RANGE_FACTOR );
            return I2( std::max( newResolution.a(), 1 ), std::max( newResolution.b(), 1 ) );
        }
        else
        {
            return m_defaultHistResolution;
        }
    }

    void clear()
    {
        clearSelection();
        clearFocus();
        nonEmptyGridCenters.clear();
        nonEmptyGridCorners.clear();
    }

    bool hasCellSelection()
    {
        return focusedCell >= 0 || selectedCell >= 0;
    }

    int clickSelectedCellId()
    {
        return selectedCell;
    }

    bool hasPointSelection()
    {
        return focusedBin >= 0 || selectedBin >= 0;
    }

//    const std::vector< float > & getHistogramStackFrequencies()
//    {
//        return selected;
//    }

    void setSamplingMethod( SamplingMethod method )
    {
        samplingMethod = method;
    }

    virtual bool pointInViewPort( const Vec2< float > & p )
    {
        return (
                   p.x() > position().x() &&
                   p.x() < position().x() + size().x() &&
                   p.y() > position().y() &&
                   p.y() < position().y() + size().y() - CONTROL_PANEL_HEIGHT
               );
    }

    void setDefaultHistResolution( I2 resolution )
    {
        m_defaultHistResolution = resolution;
    }

    void setPopulationDataType( int dataType )
    {
        m_populationDataType = dataType;
    }

    std::vector< std::vector< int > > & gridMap()
    {
        return m_gridMap;
    }

    const std::vector< std::pair< int, unsigned int > > & particleMap() const
    {
        return ptclMap;
    }

    I2 dims() const
    {
        return I2( numRows, numCols );
    }
};
}

#endif // SAMPLER_HPP

