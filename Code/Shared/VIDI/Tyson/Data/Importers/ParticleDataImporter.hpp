
#ifndef TN_PARTICLE_DATA_IMPORTER
#define TN_PARTICLE_DATA_IMPORTER

#include "Data/Definitions/Attributes.hpp"
#include "Data/Importers/DataImporter.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Data/Interpolators/FieldInterpolator.h"

#include <set>
#include <memory>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The ParticleDataImporter class
 */
class ParticleDataImporter :  public DataImporter< TargetFloatType >
{

public:

    /**
     * @brief init
     * @return
     */
    virtual bool init( const BasicDataInfo & ) = 0;

    /**
     * @brief loadTemporallyStaticAttributes
     * @param particleVariantAttributes
     * @param constants
     * @param simulationTimeSteps
     * @param realTime
     */
    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes,
        std::map< std::string, std::map< std::string, Vec2< double > > > & variantAttributeRanges,
        std::map< std::string, std::vector< TargetFloatType > >  & particleVariantAttributes,
        std::map< std::string, BaseConstant > & constants,
        std::map< std::string, std::map< std::string, DerivedVariable > > & derivedVariables,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::map< std::string, std::map< std::string, LookupTexture< TargetFloatType > > > & lookupTextures,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        std::int32_t & numTimeSteps,
        std::map< std::string, std::size_t > & numParticle ) = 0;

        virtual void loadDefaultSubsetNames( std::vector< std::string > & names ) = 0;
        virtual void loadTimeStepsMasks( std::vector< std::int32_t > & masks, size_t t ) = 0;

    /**
     * @brief loadTimeStep
     * @param timeStep the time step to load into memory
     * @param needToLoad which data to load ( particle key, time-variant attribute key )
     * @param m_distrobutions a reference to the data structure to put load the data into
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes
    ) = 0;

    /**
     * @brief ~ParticleDataImporter
     */
    virtual ~ParticleDataImporter() {}
};

}

#endif // PARTICLEDATAIMPORTER

