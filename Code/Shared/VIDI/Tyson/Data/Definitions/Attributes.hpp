
#ifndef TN_ATTRIBUTE
#define TN_ATTRIBUTE

#include "Types/Vec.hpp"

#include <QJsonObject>

#include <string>

namespace TN
{

class Quantity
{
    std::string m_name;
    std::string m_description;

public:

    void name( const std::string & _name ) { m_name = _name; }
    void description( const std::string & _desc ) { m_description = _desc; }

    const std::string & name()        const { return m_name;        }
    const std::string & description() const { return m_description; }

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "name", m_name.c_str() );
        obj.insert( "description", m_description.c_str() );
        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        m_name = obj[ "name" ].toString().toStdString();
        m_description = obj[ "description" ].toString().toStdString();
    }
};

class Variable : public Quantity {

    Vec2< double > m_range;

public:

    void range( const Vec2< double > & range ) { m_range = range; }
    const Vec2< double > & range() const { return m_range; }
};

class BaseVariable : public Variable
{

public:

    QJsonObject toJSON() const
    {
        return Quantity::toJSON();
    }

    void fromJSON( const QJsonObject & obj )
    {
        Quantity::fromJSON( obj );
    }
};

class DerivedVariable : public Variable
{
    std::string m_expression;

public :

    QJsonObject toJSON() const
    {
        QJsonObject obj = Quantity::toJSON();
        obj.insert( "expression", m_expression.c_str() );
        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        Quantity::fromJSON( obj );
        m_expression = obj[ "expression" ].toString().toStdString();
    }

    const std::string & expression() const { return m_expression;  }
    void expression( const std::string & exp ) { m_expression = exp;  }
};

class BaseConstant : public Quantity
{
    long double m_value;

public:

    QJsonObject toJSON() const
    {
        QJsonObject obj = Quantity::toJSON();
        obj.insert( "value", (double) m_value );
        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        Quantity::fromJSON( obj );
        m_value = obj[ "value" ].toDouble();
    }

    long double value() const { return m_value; }
    void value( long double v ) { m_value = v; }
};

class CustomConstant : public Quantity
{
    std::string m_expression;

public:

    QJsonObject toJSON() const
    {
        QJsonObject obj = Quantity::toJSON();
        obj.insert( "expression", m_expression.c_str() );
        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        Quantity::fromJSON( obj );
        m_expression = obj[ "expression" ].toString().toStdString();
    }

    const std::string & expression() const { return m_expression; }
    void expression( const std::string & exp ) { m_expression = exp; }
};

}

#endif // TN_ATTRIBUTE

