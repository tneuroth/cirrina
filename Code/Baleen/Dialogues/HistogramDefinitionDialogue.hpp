
#ifndef TN_HIST_DEFINITION_DIALOGUE
#define TN_HIST_DEFINITION_DIALOGUE

#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"

namespace TN {

class HistogramDefinitionDialogue : public BaseDataDialogue
{
    Q_OBJECT

    std::map< std::string, HistogramDefinition > definedHistograms;

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    QVBoxLayout * PlotAttributeSection;
    QHBoxLayout * ConstraintSection;

    /////////////////////////////////////////////////////////////////////

    // Sampling Space

//    QLabel    * spaceAttrLabel;
//    QLabel    * spaceDimsLabel;
//    QLabel    * spaceScalingLabel;

//    QComboBox * spaceScalingCombo;
//    QComboBox * spaceDimsCombo;

//    QComboBox * spaceAttrCombo1;
//    QComboBox * spaceAttrCombo2;
//    QComboBox * spaceAttrCombo3;

//    QVBoxLayout * spacec2;
//    QVBoxLayout * spacec3;

//    QHBoxLayout * spaceLayout;

    //////////////////////////////////////////////////////

    // Binning

    QLabel    * regionAttrLabel;
    QLabel    * binWidthLabel;

    QLabel    * regionDimsLabel;
//    QLabel    * startLabel;
//    QLabel    * endLabel;

    QComboBox * regionDimsCombo;

    QComboBox * regionAttrCombo1;
    QLineEdit * binWidthLineEdit1;

    QComboBox * regionAttrCombo2;
    QLineEdit * binWidthLineEdit2;

    QComboBox * regionAttrCombo3;
    QLineEdit * binWidthLineEdit3;

//    QLineEdit * startLineEdit1;
//    QLineEdit * startLineEdit2;
//    QLineEdit * startLineEdit3;

//    QLineEdit * endLineEdit1;
//    QLineEdit * endLineEdit2;
//    QLineEdit * endLineEdit3;

    QVBoxLayout * c1;
    QVBoxLayout * c2;
    QVBoxLayout * c3;
    QVBoxLayout * c4;
    QVBoxLayout * c5;

    QVBoxLayout * HistogramLayout;
    QHBoxLayout * regionLayout;
    QComboBox * weightCombo;

    QLabel    * HistogramCalculationOptionsLabel;

    //////////////////////////////////////////////////////////////////////

    // Histogram Nameing

    QLabel    * histogramNameingLabel;
    QLineEdit * histogramNameingLineEdit;

    ///////////////////////////////////////////////////////////////////////

    // Histogram description

    QVBoxLayout * histogramDescriptionLayout;
    QLabel    * histogramDescriptionLabel;
    QTextEdit * histogramDescriptionBox;

public:

    ~HistogramDefinitionDialogue();

signals:

    void cancelDefinition();
    void finalizeDefinition(
        const std::string &,
        const std::map< std::string, HistogramDefinition > &,
        const std::map< std::string, BaseVariable > &,
        const std::map< std::string, DerivedVariable > &,
        const std::map< std::string, BaseConstant > &,
        const std::map< std::string, CustomConstant > & );

//    void updateDerivations(
//        const std::map<std::string, DerivedVariable> &,
//        const std::map<std::string, CustomConstant> & );

private slots:

    void histogramDescriptionChanged();

    virtual void attemptDefineConstant( ) override;
    virtual void attemptDeriveVariable( ) override;

    void changeRegionDims( QString dimsStr );
    void changeSpaceDims( QString dimsStr );
    void updateBinningAttributeLineEdits( QString str );
    void finalize();
    void cancel();

public:

    void update(
        const std::map< std::string, HistogramDefinition > & _histMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap );

    void fromDefinition( const HistogramDefinition & def );
    void setStatus( const std::string & status );

    HistogramDefinitionDialogue( std::string _key, QWindow *parentWindow );
};

}

#endif

