#version 400



out float fragColor[2];

in G2F{
    float value;
    flat int select;
}g2f;


void main(void)
{
    fragColor[ 0 ] = g2f.value;

    if( bool( g2f.select ) )
    {
        fragColor[ 1 ] = g2f.value;
    }
    else
    {
       discard;
    }
}
