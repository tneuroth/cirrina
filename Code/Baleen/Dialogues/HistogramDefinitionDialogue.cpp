
#include "Types/Vec.hpp"
#include "Algorithms/Standard/Util.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"
#include "Dialogues/HistogramDefinitionDialogue.hpp"

#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Expressions/Calculator/expressionsymbols.hpp"
#include "Expressions/Calculator/Expression.hpp"

#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QDebug>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN {

    HistogramDefinitionDialogue::~HistogramDefinitionDialogue() {}

    void HistogramDefinitionDialogue::histogramDescriptionChanged()
    {

    }

    void HistogramDefinitionDialogue::attemptDefineConstant( )
    {
        if( ! validateName( defineNameEdit->text() ) )
        {
            return;
        }

        if( ! validateConstExpression( defineEdit->text() ) )
        {
            return;
        }

        CustomConstant cc;
        cc.name( defineNameEdit->text().toStdString() );
        cc.expression( defineEdit->text().toStdString() );

        customConstants.insert( { cc.name(), cc } );

        std::string _nm = cc.name() + " = " + cc.expression();
        if( ! m_expressionCalculator->parser().validateNumber( cc.expression() ) )
        {
            _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( cc.expression(), baseConstants, customConstants ), 10 );
        }
        userConstantList->addItem( _nm.c_str() );
        defineNameEdit->setStyleSheet( "QLineEdit { color : gray; }" );

        weightCombo->addItem( ( cc.name() + " = " + cc.expression() ).c_str() );

        emit updateAttributes( baseVariables, derivedVariables, baseConstants, customConstants  );
    }


    void HistogramDefinitionDialogue::attemptDeriveVariable( )
    {
        BaseDataDialogue::attemptDeriveVariable();

        std::string nm = deriveNameEdit->text().toStdString();
        std::string expr = deriveEdit->text().toStdString();

        weightCombo->addItem( ( nm + " = " + expr ).c_str() );
        regionAttrCombo1->addItem( ( nm + " = " + expr ).c_str() );
        regionAttrCombo2->addItem( ( nm + " = " + expr ).c_str() );
        regionAttrCombo3->addItem( ( nm + " = " + expr ).c_str() );
//        spaceAttrCombo1->addItem( ( nm + " = " + expr ).c_str() );
//        spaceAttrCombo2->addItem( ( nm + " = " + expr ).c_str() );
//        spaceAttrCombo3->addItem( ( nm + " = " + expr ).c_str() );

        emit updateAttributes( baseVariables, derivedVariables, baseConstants, customConstants  );
    }

    void HistogramDefinitionDialogue::changeRegionDims( QString dimsStr )
    {
        int dims = std::stoi( dimsStr.toStdString() );
        if( dims > 1 )
        {
            c2->addWidget( regionAttrCombo2 );
            c3->addWidget( binWidthLineEdit2 );
//            c4->addWidget( startLineEdit2 );
//            c5->addWidget( endLineEdit2 );

            regionAttrCombo2->setVisible( true );
            binWidthLineEdit2->setVisible( true );
//            startLineEdit2->setVisible( true );
//            endLineEdit2->setVisible( true );
        }
        else
        {
            c2->removeWidget( regionAttrCombo2 );
            c3->removeWidget( binWidthLineEdit2 );
//            c4->removeWidget( startLineEdit2 );
//            c5->removeWidget( endLineEdit2 );

            regionAttrCombo2->setVisible( false );
            binWidthLineEdit2->setVisible( false );
//            startLineEdit2->setVisible( false );
//            endLineEdit2->setVisible( false );
        }

        if( dims > 2 )
        {
            c2->addWidget( regionAttrCombo3 );
            c3->addWidget( binWidthLineEdit3 );
//            c4->addWidget( startLineEdit3 );
//            c5->addWidget( endLineEdit3 );

            regionAttrCombo3->setVisible( true );
            binWidthLineEdit3->setVisible( true );
//            startLineEdit3->setVisible( true );
//            endLineEdit3->setVisible( true );
        }
        else
        {
            c2->removeWidget( regionAttrCombo3 );
            c3->removeWidget( binWidthLineEdit3 );
//            c4->removeWidget( startLineEdit3 );
//            c5->removeWidget( endLineEdit3 );

            regionAttrCombo3->setVisible( false );
            binWidthLineEdit3->setVisible( false );
//            startLineEdit3->setVisible( false );
//            endLineEdit3->setVisible( false );
        }
    }


    void HistogramDefinitionDialogue::changeSpaceDims( QString dimsStr )
    {
//        int dims = std::stoi( dimsStr.toStdString() );
//        if( dims > 1 )
//        {
//            spacec2->addWidget( spaceAttrCombo2 );
//            spaceAttrCombo2->setVisible( true );
//        }
//        else
//        {
//            spacec2->removeWidget( spaceAttrCombo2 );
//            spaceAttrCombo2->setVisible( false );
//        }

//        if( dims > 2 )
//        {
//            spacec2->addWidget( spaceAttrCombo3 );
//            spaceAttrCombo3->setVisible( true );
//        }
//        else
//        {
//            spacec2->removeWidget( spaceAttrCombo3 );
//            spaceAttrCombo3->setVisible( false );
//        }
    }

    void HistogramDefinitionDialogue::updateBinningAttributeLineEdits( QString str )
    {
//        QComboBox * comboPtr = (QComboBox*) QObject::sender();
//        const Vec2< double > & r = baseVariables.find( str.toStdString() )->second.range();

//        if( comboPtr == regionAttrCombo1 )
//        {
//            startLineEdit1->setText( QString::number( r.a(), 'g', 6 ) );
//            endLineEdit1->setText( QString::number( r.b(), 'g', 6 ) );
//        }
//        else if( comboPtr == regionAttrCombo2 )
//        {
//            startLineEdit2->setText( QString::number( r.a(), 'g', 6 ) );
//            endLineEdit2->setText( QString::number( r.b(), 'g', 6 ) );
//        }
//        else if ( comboPtr == regionAttrCombo3 )
//        {
//            startLineEdit3->setText( QString::number( r.a(), 'g', 6 ) );
//            endLineEdit3->setText( QString::number( r.b(), 'g', 6 ) );
//        }
    }

    void HistogramDefinitionDialogue::finalize()
    {
        //qDebug() << "finalizing definition";

        std::string name = histogramNameingLineEdit->text().toStdString();
        if( name.size() == 0 )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }
        if( ( m_status != "defined" ) && ( definedHistograms.find( name ) != definedHistograms.end() ) )
        {
            errorMessageLabel->setText( "Name/key already in use" );
            return;
        }
        if( name == "temp" )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }

        //qDebug() << "name";

        HistogramDefinition definition;

        int binningDims = std::stoi( regionDimsCombo->currentText().toStdString() );
//        int spatialDims = std::stoi( spaceDimsCombo->currentText().toStdString() );

        //////////////////////////////

        if( binningDims >= 1 )
        {
            // in the combo, derived variables have " = <expression>" attatched
            std::string bKey = regionAttrCombo1->currentText().toStdString();
            bKey = bKey.substr(0, bKey.find("="));
            if( bKey.back() == ' ' ){
                bKey.pop_back();
            }

            definition.binningSpace.push_back( bKey );
            if( ! validateInteger( binWidthLineEdit1->text() ) )
            {
                errorMessageLabel->setText( "Resolution must be integer" );
                return;
            }

            definition.resolution.push_back( std::stoi( binWidthLineEdit1->text().toStdString() ) );

//            if( ! ( validateNumber( startLineEdit1->text() ) && validateNumber( endLineEdit1->text() ) ) )
//            {
//                errorMessageLabel->setText( "Thresholds must be numeric" );
//                return;
//            }
//            Vec2< float > rng( std::stof( startLineEdit1->text().toStdString() ), std::stof( endLineEdit1->text().toStdString() ) );
//            if( rng.b() <= rng.a() )
//            {
//                errorMessageLabel->setText( "Threshold range must be > 0" );
//                return;
//            }
//            definition.valueRange.push_back( rng );
        }
        if( binningDims >= 2 )
        {

            std::string bKey = regionAttrCombo2->currentText().toStdString();
            bKey = bKey.substr(0, bKey.find("=") );
            if( bKey.back() == ' ' ){
                bKey.pop_back();
            }

            definition.binningSpace.push_back( bKey );
            if( ! validateInteger( binWidthLineEdit2->text() ) )
            {
                errorMessageLabel->setText( "Resolution must be integer" );
                return;
            }
            definition.resolution.push_back( std::stoi( binWidthLineEdit2->text().toStdString() ) );

//            if( ! ( validateNumber( startLineEdit2->text() ) && validateNumber( endLineEdit2->text() ) ) )
//            {
//                errorMessageLabel->setText( "Thresholds must be numeric" );
//                return;
//            }
//            Vec2< float > rng( std::stof( startLineEdit2->text().toStdString() ), std::stof( endLineEdit2->text().toStdString() ) );
//            if( rng.b() <= rng.a() )
//            {
//                errorMessageLabel->setText( "Threshold range must be > 0" );
//                return;
//            }
//            definition.valueRange.push_back( rng );
        }
        if( binningDims >= 3 )
        {
            std::string bKey = regionAttrCombo3->currentText().toStdString();
            bKey = bKey.substr(0, bKey.find("="));
            if( bKey.back() == ' ' ){
                bKey.pop_back();
            }

            definition.binningSpace.push_back( bKey );
            if( ! validateInteger( binWidthLineEdit3->text() ) )
            {
                errorMessageLabel->setText( "Resolution must be integer" );
                return;
            }
            definition.resolution.push_back( std::stoi( binWidthLineEdit3->text().toStdString() ) );

//            if( ! ( validateNumber( startLineEdit3->text() ) && validateNumber( endLineEdit3->text() ) ) )
//            {
//                errorMessageLabel->setText( "Thresholds must be numeric" );
//                return;
//            }
//            Vec2< float > rng( std::stof( startLineEdit3->text().toStdString() ), std::stof( endLineEdit3->text().toStdString() ) );
//            if( rng.b() <= rng.a() )
//            {
//                errorMessageLabel->setText( "Threshold range must be > 0" );
//                return;
//            }
//            definition.valueRange.push_back( rng );
        }


        //////////////////////////

//        if( spatialDims >= 1 )
//        {
//            std::string sKey = spaceAttrCombo1->currentText().toStdString();
//            sKey = sKey.substr(0, sKey.find("="));
//            if( sKey.back() == ' ' ){
//                sKey.pop_back();
//            }

//            definition.partitionSpace.push_back( sKey );
//            definition.partitionSpaceScaling.push_back( 1.0 );
//        }
//        if( spatialDims >= 2 )
//        {
//            std::string sKey = spaceAttrCombo2->currentText().toStdString();
//            sKey = sKey.substr(0, sKey.find("="));
//            if( sKey.back() == ' ' ){
//                sKey.pop_back();
//            }

//            definition.partitionSpace.push_back( sKey );
//            definition.partitionSpaceScaling.push_back( 1.0 );
//        }
//        if( spatialDims >= 3 )
//        {
//            std::string sKey = spaceAttrCombo3->currentText().toStdString();
//            sKey = sKey.substr(0, sKey.find("="));
//            if( sKey.back() == ' ' ){
//                sKey.pop_back();
//            }

//            definition.partitionSpace.push_back( sKey );
//            definition.partitionSpaceScaling.push_back( 1.0 );
//        }

//        definition.preserveAspectRatio = spaceScalingCombo->currentText() == "Preserve Aspect Ratio";

        //////////////////////

        definition.name = m_key;
        definition.description = histogramDescriptionBox->toPlainText().toStdString();

//        std::set< std::string > spaceAttrs;
        std::set< std::string > binAttrs;
//        spaceAttrs.insert( definition.partitionSpace.begin(), definition.partitionSpace.end() );
        binAttrs.insert( definition.binningSpace.begin(), definition.binningSpace.end() );
        if( ! ( binAttrs.size() == definition.binningSpace.size() /*&& spaceAttrs.size() == definition.partitionSpace.size()*/ ) )
        {
            errorMessageLabel->setText( "Axis must be unqiue" );
            return;
        }

        std::string wKey = weightCombo->currentText().toStdString();
        wKey = wKey.substr( 0, wKey.find( "=" ) );
        if( wKey.back() == ' ' ) {
            wKey.pop_back();
        }
        definition.weight = wKey;

        if( definition.weight == "1" )
        {
            definition.weightType = HistogramWeightType::NONE;
        }
        else if ( baseConstants.count( definition.weight ) )
        {
            definition.weightType = HistogramWeightType::CONSTANT;
            definition.constWeightValue = baseConstants.at( definition.weight ).value();
        }
        else if ( customConstants.count( definition.weight )  )
        {
            definition.weightType = HistogramWeightType::CONSTANT;
            definition.constWeightValue = m_expressionCalculator->parser().evaluateConstExpression(
                customConstants.at( definition.weight ).expression(), baseConstants, customConstants );
        }
        else
        {
            definition.weightType = HistogramWeightType::VARIABLE;
        }

        m_key = name;
        definition.name = m_key;
        definition.valueRange.resize( binningDims );

        if( m_status == "undefined" )
        {
            definedHistograms.insert( { m_key, definition } );
        }
        else
        {
            definedHistograms.find( m_key )->second = definition;
        }

        setStatus( "defined" );

//        qDebug() << "updated";

        emit finalizeDefinition(
            m_key,
            definedHistograms,
            baseVariables,
            derivedVariables,
            baseConstants,
            customConstants
        );

        close();
    }

    void HistogramDefinitionDialogue::cancel()
    {
        emit cancelDefinition();
        close();
    }

    void HistogramDefinitionDialogue::update(
        const std::map< std::string, HistogramDefinition > & _histMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap )
    {
        if( m_status == "defined" )
        {
            for( auto & dv : _derivedVarMap )
            {
                if( ! derivedVariables.count( dv.second.name() ) )
                {
                    std::string _nm = dv.second.name() + " = " + dv.second.expression();
                    derivedAttributeList->addItem( _nm.c_str() );
                    weightCombo->addItem( _nm.c_str() );

                    regionAttrCombo1->addItem( _nm.c_str() );
                    regionAttrCombo2->addItem( _nm.c_str() );
                    regionAttrCombo3->addItem( _nm.c_str() );

//                    spaceAttrCombo1->addItem( _nm.c_str() );
//                    spaceAttrCombo2->addItem( _nm.c_str() );
//                    spaceAttrCombo3->addItem( _nm.c_str() );
                }
            }
            for( auto & cc : _customConstMap)
            {
                if( ! customConstants.count( cc.second.name() ) )
                {
                    std::string _nm = cc.second.name()
                            + " = " + cc.second.expression();

                    if( ! m_expressionCalculator->parser().validateNumber( cc.second.expression() ) )
                    {
                        _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( cc.second.expression(), _baseConstMap, _customConstMap ), 10 );
                    }

                    weightCombo->addItem( _nm.c_str() );
                    userConstantList->addItem( _nm.c_str() );
                }
            }
        }

        baseVariables = _baseVarMap;
        derivedVariables = _derivedVarMap;
        customConstants = _customConstMap;
        baseConstants = _baseConstMap;
        definedHistograms = _histMap;

        if( m_status != "defined" )
        {
            attributeList->setStyleSheet( "QListWidget { color : green }" );
            constantList->setStyleSheet( "QListWidget { color : green }" );

            constantList->clear();
            userConstantList->clear();
            derivedAttributeList->clear();
            attributeList->clear();

//            spaceDimsCombo->clear();
            regionDimsCombo->clear();

            regionAttrCombo1->clear();
            regionAttrCombo2->clear();
            regionAttrCombo3->clear();

//            spaceAttrCombo1->clear();
//            spaceAttrCombo2->clear();
//            spaceAttrCombo3->clear();

            weightCombo->clear();
            weightCombo->addItem( "1" );

            for( auto & baseConst : _baseConstMap )
            {
                std::string _nm = baseConst.first + " = " + to_string_with_precision( baseConst.second.value(), 10 );
                constantList->addItem( _nm.c_str() );
                weightCombo->addItem( _nm.c_str() );
            }

            for( auto & custConst : _customConstMap )
            {
                std::string _nm = custConst.second.name()
                        + " = " + custConst.second.expression();

                if( ! m_expressionCalculator->parser().validateNumber( custConst.second.expression() ) )
                {
                    _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( custConst.second.expression(), _baseConstMap, _customConstMap ), 10 );
                }

                weightCombo->addItem( _nm.c_str() );
                userConstantList->addItem( _nm.c_str() );
            }

            for( auto & dv : _derivedVarMap )
            {
                std::string _nm = dv.second.name() + " = " + dv.second.expression();
                derivedAttributeList->addItem( _nm.c_str() );
                weightCombo->addItem( _nm.c_str() );

                regionAttrCombo1->addItem( _nm.c_str() );
                regionAttrCombo2->addItem( _nm.c_str() );
                regionAttrCombo3->addItem( _nm.c_str() );

//                spaceAttrCombo1->addItem( _nm.c_str() );
//                spaceAttrCombo2->addItem( _nm.c_str() );
//                spaceAttrCombo3->addItem( _nm.c_str() );
            }

            for( auto & baseVarE : baseVariables )
            {
                std::string _nm = baseVarE.first.c_str();

                attributeList->addItem( _nm.c_str() );

                weightCombo->addItem( _nm.c_str() );

                regionAttrCombo1->addItem( _nm.c_str() );
                regionAttrCombo2->addItem( _nm.c_str() );
                regionAttrCombo3->addItem( _nm.c_str() );

//                spaceAttrCombo1->addItem( _nm.c_str() );
//                spaceAttrCombo2->addItem( _nm.c_str() );
//                spaceAttrCombo3->addItem( _nm.c_str() );

                //const Vec2< double > & r = baseVarE.second.range();

//                startLineEdit1->setText( QString::number( r.a(), 'g', 6 ) );
//                endLineEdit1->setText( QString::number( r.b(), 'g', 6 ) );

//                startLineEdit2->setText( QString::number( r.a(), 'g', 6 ) );
//                endLineEdit2->setText( QString::number( r.b(), 'g', 6 ) );

//                startLineEdit3->setText( QString::number( r.a(), 'g', 6 ) );
//                endLineEdit3->setText( QString::number( r.b(), 'g', 6 ) );

                binWidthLineEdit1->setText( "17" );
                binWidthLineEdit2->setText( "17" );
                binWidthLineEdit3->setText( "17" );
            }

//            spaceDimsCombo->addItem( "1" );
//            spaceDimsCombo->addItem( "2" );
//            spaceDimsCombo->addItem( "3" );

            regionDimsCombo->addItem( "1" );
            regionDimsCombo->addItem( "2" );
            regionDimsCombo->addItem( "3" );

//            spaceDimsCombo->setCurrentIndex( 1 );
            regionDimsCombo->setCurrentIndex( 1 );

            if( definedHistograms.count( m_key ) )
            {
                if( m_key != "temp" )
                {
                    fromDefinition( definedHistograms.at( m_key ) );
                }
            }

            connect( regionDimsCombo, SIGNAL(currentTextChanged(QString) ), this, SLOT( changeRegionDims( QString ) ) );
//            connect( spaceDimsCombo, SIGNAL(currentTextChanged(QString) ), this, SLOT( changeSpaceDims( QString ) ) );

//            spaceDimsCombo->setEnabled( false );
            regionDimsCombo->setEnabled( false );
        }
    }

    void HistogramDefinitionDialogue::fromDefinition( const HistogramDefinition & def )
    {
        histogramDescriptionBox->setText( def.description.c_str() );

        histogramNameingLineEdit->setText( m_key.c_str() );

        QString weight = def.weight.c_str();

        for( auto idx = 0; idx < weightCombo->count(); ++idx )
        {
            if( weightCombo->itemText( idx ).split(" ").at( 0 ) == weight )
            {
                weightCombo->setCurrentIndex( idx );
                break;
            }
        }

        if( def.binningSpace.size() > 0 )
        {
            regionDimsCombo->setCurrentIndex( def.binningSpace.size() - 1 );
        }

        for( unsigned i = 0; i < def.binningSpace.size(); ++i )
        {
            if( i == 0 )
            {
                for( auto idx = 0; idx < regionAttrCombo1->count(); ++idx )
                {
                    if( regionAttrCombo1->itemText( idx ).split(" ").at( 0 ).toStdString() == def.binningSpace[ i ] )
                    {
                        regionAttrCombo1->setCurrentIndex( idx );
                        break;
                    }
                }
            }
            if( i == 1 )
            {
                for( auto idx = 0; idx < regionAttrCombo2->count(); ++idx )
                {
                    if( regionAttrCombo2->itemText( idx ).split(" ").at( 0 ).toStdString() == def.binningSpace[ i ] )
                    {
                        regionAttrCombo2->setCurrentIndex( idx );
                        break;
                    }
                }
            }
            if( i == 2 )
            {
                for( auto idx = 0; idx < regionAttrCombo3->count(); ++idx )
                {
                    if( regionAttrCombo3->itemText( idx ).split(" ").at( 0 ).toStdString() == def.binningSpace[ i ] )
                    {
                        regionAttrCombo3->setCurrentIndex( idx );
                        break;
                    }
                }
            }
        }

        errorMessageLabel->setText( "" );
        setStatus( "defined" );
    }

    void HistogramDefinitionDialogue::setStatus( const std::string & status )
    {
        m_status = status;
        if( status == "defined" )
        {
            acceptButton->setText( "update" );
            histogramNameingLineEdit->setEnabled( false );
        }
    }

    HistogramDefinitionDialogue::HistogramDefinitionDialogue( std::string _key, QWindow *parentWindow ) : BaseDataDialogue( _key, parentWindow )
    {
        PlotAttributeSection = new QVBoxLayout;
        ConstraintSection    = new QHBoxLayout;

        acceptButton = new QPushButton( "create" );
        cancelButton = new QPushButton( "cancel" );
        histogramNameingLabel = new QLabel( "Histogram Name/Key" );
        histogramNameingLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt; }" );
        histogramNameingLineEdit = new QLineEdit;

        connect( cancelButton, SIGNAL( clicked( bool ) ), this, SLOT(   cancel() ) );
        connect( acceptButton, SIGNAL( clicked( bool ) ), this, SLOT( finalize() ) );

        ///////////////////////////////////////////////////////////////////////////

        // Type / Calculation / Expressions / Validation

        HistogramLayout = new QVBoxLayout;

        weightCombo = new QComboBox;
        QSizePolicy szp( QSizePolicy::MinimumExpanding, QSizePolicy::Fixed  );
        weightCombo->setSizePolicy( szp );

        HistogramCalculationOptionsLabel = new QLabel( "Histogram Calculation" );
        HistogramCalculationOptionsLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt; }" );

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        regionAttrLabel = new QLabel( "Attribute" );
        binWidthLabel   = new QLabel( "Resolution" );
        regionDimsLabel = new QLabel( "Dimensionality" );
//        startLabel      = new QLabel( "Default Min Threshold" );
//        endLabel        = new QLabel( "Default Max Threshold" );

        QLabel * binningRegionLabel = new QLabel(  "Binning" );
        binningRegionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        QHBoxLayout * frabl = new QHBoxLayout;
        frabl->addWidget( binningRegionLabel );
        frabl->addStretch();

//        startLineEdit1 = new QLineEdit;
//        startLineEdit2 = new QLineEdit;
//        startLineEdit3 = new QLineEdit;

//        endLineEdit1 = new QLineEdit;
//        endLineEdit2 = new QLineEdit;
//        endLineEdit3 = new QLineEdit;

        regionDimsCombo = new QComboBox;
        regionDimsCombo->setMinimumWidth( 100 );

        QHBoxLayout * frabl2 = new QHBoxLayout;
        frabl2->addWidget( regionDimsLabel );
        frabl2->addWidget( regionDimsCombo );
        frabl2->addStretch();

        regionAttrCombo1  = new QComboBox;
        binWidthLineEdit1 = new QLineEdit;

        regionAttrCombo2  = new QComboBox;
        binWidthLineEdit2 = new QLineEdit;

        regionAttrCombo3  = new QComboBox;
        binWidthLineEdit3 = new QLineEdit;

        regionAttrCombo1->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
        regionAttrCombo2->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
        regionAttrCombo3->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );

        c1 = new QVBoxLayout;
        c2 = new QVBoxLayout;
        c3 = new QVBoxLayout;
        c4 = new QVBoxLayout;
        c5 = new QVBoxLayout;

        regionLayout = new QHBoxLayout;

        c2->addWidget( regionAttrLabel  );
        c2->addWidget( regionAttrCombo1 );
        c2->addWidget( regionAttrCombo2 );

        c3->addWidget(     binWidthLabel );
        c3->addWidget( binWidthLineEdit1 );
        c3->addWidget( binWidthLineEdit2 );

//        c4->addWidget( startLabel     );
//        c4->addWidget( startLineEdit1 );
//        c4->addWidget( startLineEdit2 );

//        c5->addWidget( endLabel     );
//        c5->addWidget( endLineEdit1 );
//        c5->addWidget( endLineEdit2 );

        QHBoxLayout * HistogramLabelLayout = new QHBoxLayout;
        HistogramLabelLayout->addWidget( HistogramCalculationOptionsLabel );
        HistogramLabelLayout->addStretch();

        QHBoxLayout * HistogramExpressionLayout = new QHBoxLayout;
        HistogramExpressionLayout->addWidget( new QLabel( "Weight" ) );
        HistogramExpressionLayout->addWidget( weightCombo );

        HistogramLayout->addLayout( HistogramLabelLayout );
        HistogramLayout->addLayout( HistogramExpressionLayout );

        regionLayout->addLayout( c2 );
        regionLayout->addLayout( c3 );
        regionLayout->addLayout( c4 );
        regionLayout->addLayout( c5 );

        regionDimsLabel->setBuddy( regionDimsCombo );

        PlotAttributeSection->addLayout( HistogramLayout   );
        QFrame *l1 = new QFrame;
        l1->setFrameShape( QFrame::HLine );
        l1->setFrameShadow( QFrame::Sunken );
        l1->setStyleSheet( "QFrame { background-color : white; }" );

        PlotAttributeSection->addWidget( l1 );

        connect( regionAttrCombo1, SIGNAL(currentTextChanged( QString ) ), this, SLOT( updateBinningAttributeLineEdits( QString ) ) );
        connect( regionAttrCombo2, SIGNAL(currentTextChanged( QString ) ), this, SLOT( updateBinningAttributeLineEdits( QString ) ) );
        connect( regionAttrCombo3, SIGNAL(currentTextChanged( QString ) ), this, SLOT( updateBinningAttributeLineEdits( QString ) ) );

//        connect( startLineEdit1, SIGNAL(textChanged(QString)), this, SLOT(valueEditChanged(QString)) );
//        connect( startLineEdit2, SIGNAL(textChanged(QString)), this, SLOT(valueEditChanged(QString)) );
//        connect( startLineEdit3, SIGNAL(textChanged(QString)), this, SLOT(valueEditChanged(QString)) );

//        connect( endLineEdit1, SIGNAL(textChanged(QString)), this, SLOT(valueEditChanged(QString)) );
//        connect( endLineEdit2, SIGNAL(textChanged(QString)), this, SLOT(valueEditChanged(QString)) );
//        connect( endLineEdit3, SIGNAL(textChanged(QString)), this, SLOT(valueEditChanged(QString)) );

        //////////////////////////////////////////////////////////////////////////////

        // space

//        spaceAttrLabel = new QLabel( "Attribute" );
//        spaceDimsLabel = new QLabel( "Dimensionality" );
//        spaceScalingLabel = new QLabel( "Scaling" );

//        QLabel * spatialRegionLabel = new QLabel(  "Spatial Layout" );
//        spatialRegionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );

//        QHBoxLayout * frabskjdkjdskl = new QHBoxLayout;
//        frabskjdkjdskl->addWidget(  spatialRegionLabel );
//        frabskjdkjdskl->addStretch();

//        spaceDimsCombo = new QComboBox;
//        spaceScalingCombo = new QComboBox;
//        spaceDimsCombo->setMinimumWidth( 100 );

//        spaceScalingCombo->addItem( "Preserve Aspect Ratio" );
//        spaceScalingCombo->addItem( "Normalize" );

//        QHBoxLayout * frabsdsdsfl2 = new QHBoxLayout;
//        frabsdsdsfl2->addWidget( spaceDimsLabel );
//        frabsdsdsfl2->addWidget( spaceDimsCombo );
//        frabsdsdsfl2->addWidget( spaceScalingLabel );
//        frabsdsdsfl2->addWidget( spaceScalingCombo );
//        frabsdsdsfl2->addStretch();

//        spaceAttrCombo1 = new QComboBox;
//        spaceAttrCombo2 = new QComboBox;
//        spaceAttrCombo3 = new QComboBox;

//        spaceAttrCombo1->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
//        spaceAttrCombo2->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
//        spaceAttrCombo3->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );

//        spacec2 = new QVBoxLayout;
//        spacec3 = new QVBoxLayout;
//        spaceLayout = new QHBoxLayout;

//        spacec2->addWidget( spaceAttrLabel  );
//        spacec2->addWidget( spaceAttrCombo1 );
//        spacec2->addWidget( spaceAttrCombo2 );

//        spaceLayout->addLayout( spacec2 );
//        spaceLayout->addLayout( spacec3 );

//        spaceDimsLabel->setBuddy( spaceDimsCombo );

        //////////////////////////////////////////////////////////////////////////////

        PlotAttributeSection->addSpacerItem( new QSpacerItem( 8, 8 ) );
        PlotAttributeSection->addLayout( frabl );
        PlotAttributeSection->addLayout( frabl2 );
        PlotAttributeSection->addLayout( regionLayout );
        PlotAttributeSection->addSpacerItem( new QSpacerItem( 8, 8 ) );

//        QFrame *l1dsdds = new QFrame;
//        l1dsdds->setFrameShape( QFrame::HLine );
//        l1dsdds->setFrameShadow( QFrame::Sunken );
//        l1dsdds->setStyleSheet( "QFrame { background-color : white; }" );
//        PlotAttributeSection->addWidget( l1dsdds );
//        PlotAttributeSection->addLayout( frabskjdkjdskl );
//        PlotAttributeSection->addLayout( frabsdsdsfl2 );
//        PlotAttributeSection->addLayout( spaceLayout );

        ///////////////////////////////////////////////////////////////////////////////

        // Description

        histogramDescriptionLayout = new QVBoxLayout;
        histogramDescriptionLabel = new QLabel( "Histogram Description" );
        histogramDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        histogramDescriptionBox = new QTextEdit;
        histogramDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );

        QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
        kjncewionoicejnoinwe->addWidget( histogramDescriptionLabel );
        kjncewionoicejnoinwe->addStretch();

        histogramDescriptionLayout->addLayout( kjncewionoicejnoinwe );
        histogramDescriptionLayout->addWidget( histogramDescriptionBox );
        histogramDescriptionLayout->addWidget( errorMessageLabel );

        //////////////////////////////////////////////////////////////////////////////

        // main layout

        m_mainLayout->addLayout( PlotAttributeSection );

        QFrame *line1 = new QFrame;
        line1->setFrameShape( QFrame::HLine );
        line1->setFrameShadow( QFrame::Sunken );
        line1->setStyleSheet( "QFrame { background-color : white; }" );

        m_mainLayout->addWidget( line1 );

        m_mainLayout->addLayout( ConstraintSection );
        m_mainLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );

        m_mainLayout->addLayout( histogramDescriptionLayout );

        QHBoxLayout *dll = new QHBoxLayout;
        dll->addWidget( histogramNameingLabel );
        dll->addWidget( histogramNameingLineEdit );
        dll->addWidget( cancelButton );
        dll->addWidget( acceptButton );
        m_mainLayout->addLayout( dll );
    }
}

