#version 400



layout(location = 0) out float freq;

const float unit = 1.0 / 50.0;

void main(void)
{
    freq = unit;
}
