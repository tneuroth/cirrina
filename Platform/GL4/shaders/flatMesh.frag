#version 130



uniform vec4 cla;

in vec3 normal;
in vec3 eye;
in vec3 pos;

out vec4 fragColor;

void main() {

    vec3 cl = cla.xyz * .05; //vec3( 0.0, 0.0, 0.0 );

    vec3 Nm = normalize( normal );
    vec3 Vm = normalize( eye );

    vec3 lightDirection;
    float attenuation;

    lightDirection = normalize( vec3( -.5, -0.4f, -1.0f ) );

    float angle = dot( Nm, lightDirection );

    if ( angle > 0.0) {
        vec3 Rm = normalize( reflect( lightDirection, Nm ) );
        cl += 1.2 * cla.xyz * angle + 1.0 * pow( max( dot( Rm, Vm ), 0.0 ), 5.0 ) * ( 2*cla.xyz + vec3( 1, 1, 1 ) ) / 3.0;
    }

    fragColor = vec4( min( cl, vec3( 1.0 ) ), cla.a );
}
