

#include "OpenGL/Rendering/Renderer.hpp"
#include "OpenGL/Widgets/TimeLineWidget.hpp"
#include "OpenGL/Rendering/Cameras/TrackBallCamera.hpp"
#include "OpenGL/Widgets/ResizeWidget.hpp"
#include "OpenGL/Widgets/Viewport/BoxShadowFrame.hpp"
#include "OpenGL/Widgets/Viewport/ViewPort.hpp"

#include <QGLFramebufferObjectFormat>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFramebufferObject>

namespace TN
{

void Renderer::init()
{
    m_vao = std::unique_ptr<QOpenGLVertexArrayObject>(new QOpenGLVertexArrayObject());
    m_vbo = std::unique_ptr<QOpenGLBuffer>(new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer ) );
    m_ibo = std::unique_ptr<QOpenGLBuffer>(new QOpenGLBuffer( QOpenGLBuffer::IndexBuffer ) );
    m_vbo->setUsagePattern( QOpenGLBuffer::DynamicCopy);
    m_vao->create();
    m_vbo->create();
    m_ibo->create();

    // Quad Texture
    glGenTextures( 1, &quadTexId );
    glBindTexture( GL_TEXTURE_2D, quadTexId );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Font
    FT_Library ft;

    // All functions return a value different than 0 whenever an error occurred
    if (FT_Init_FreeType(&ft))
        std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;

    // Load font as face
    FT_Face veryLargeFace;
    if (FT_New_Face(ft, ( RELATIVE_PATH_PREFIX + "Fonts/OpenSans/OpenSans-Semibold.ttf" ).c_str(), 0, &veryLargeFace))
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
    FT_Set_Pixel_Sizes( veryLargeFace, 0, 50 );

    // Load font as face
    FT_Face largeFace;
    if (FT_New_Face(ft, ( RELATIVE_PATH_PREFIX + "Fonts/OpenSans/OpenSans-Semibold.ttf" ).c_str(), 0, &largeFace))
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
    FT_Set_Pixel_Sizes( largeFace, 0, 17 );

    // Load font as face
    FT_Face face;
    if (FT_New_Face(ft, ( RELATIVE_PATH_PREFIX + "Fonts/OpenSans/OpenSans-Regular.ttf" ).c_str(), 0, &face))
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;

    FT_Set_Pixel_Sizes( face, 0, 14 );
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

    for (GLubyte c = 0; c < 128; c++)
    {
        // VeryLargeFace
        // Load character glyph
        if (FT_Load_Char( veryLargeFace, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }

        // Generate texture
        GLuint texture2;
        glGenTextures(1, &texture2);
        glBindTexture(GL_TEXTURE_2D, texture2);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            veryLargeFace->glyph->bitmap.width,
            veryLargeFace->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            veryLargeFace->glyph->bitmap.buffer
        );

        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Now store character for later use

        Character character2 =
        {
            static_cast< GLuint >( texture2 ),
            Vec2< float >(veryLargeFace->glyph->bitmap.width, veryLargeFace->glyph->bitmap.rows),
            Vec2< float >(veryLargeFace->glyph->bitmap_left, veryLargeFace->glyph->bitmap_top),
            static_cast< GLuint >( veryLargeFace->glyph->advance.x )
        };

        VeryLargerCharacters.insert(std::pair< GLchar, Character>(c, character2));

        //////////////////////////////////////////////////////////////////////////////////////

        // LargeFace
        // Load character glyph
        if (FT_Load_Char( largeFace, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }

        // Generate texture
        GLuint texture1;
        glGenTextures(1, &texture1);
        glBindTexture(GL_TEXTURE_2D, texture1);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            largeFace->glyph->bitmap.width,
            largeFace->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            largeFace->glyph->bitmap.buffer
        );

        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Now store character for later use

        Character character1 =
        {
            static_cast< GLuint >( texture1 ),
            Vec2< float >(largeFace->glyph->bitmap.width, largeFace->glyph->bitmap.rows),
            Vec2< float >(largeFace->glyph->bitmap_left, largeFace->glyph->bitmap_top),
            static_cast< GLuint >( largeFace->glyph->advance.x )
        };

        LargerCharacters.insert(std::pair< GLchar, Character>(c, character1));

        // Small Face
        // Load character glyph
        if (FT_Load_Char( face, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }

        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer
        );

        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Now store character for later use

        Character character =
        {
            static_cast< GLuint > ( texture ),
            Vec2< float >(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            Vec2< float >(face->glyph->bitmap_left, face->glyph->bitmap_top),
            static_cast< GLuint >( face->glyph->advance.x )
        };

        Characters.insert(std::pair< GLchar, Character>(c, character));
    }

    // Destroy FreeType once we're finished

    FT_Done_Face(veryLargeFace);
    FT_Done_Face(largeFace);
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
}

void Renderer::renderLoadingScreen(
    const std::unique_ptr<QOpenGLShaderProgram> & textProgram,
    const std::unique_ptr<QOpenGLShaderProgram> & bkgProgram,
    const std::unique_ptr<QOpenGLShaderProgram> & outlineProg,
    int screenWidth,
    int screenHeight,
    const std::string & mssg,
    int percentDone )
{
    const int XDIM = 800;
    const int YDIM = 70;

    //glViewport( 0, 0, screenWidth, screenHeight );
    glViewport( screenWidth / 2.0 - XDIM / 2, screenHeight / 2.0 - 20, XDIM, YDIM );
    clear( QVector4D( 0.96, 1.0, 0.98, 1 ), bkgProgram );
    renderPopSquare( outlineProg, 2.0 );

    glViewport( 0, 0, screenWidth, screenHeight );
    renderText( true,
                textProgram,
                screenWidth,
                screenHeight,
                mssg + std::to_string( percentDone ) + "%",
                screenWidth / 2.0 - XDIM / 2 + 10,
                screenHeight / 2.0, 4.0,
                Vec3< float >( 0, 0, 0 ),
                false );
}

void Renderer::computePixelDensityByAngle( const std::unique_ptr<QOpenGLShaderProgram> & program, std::vector< float > & result )
{
    result.clear();
    QGLFramebufferObjectFormat f;
    f.setInternalTextureFormat( GL_R32F );
    f.setAttachment( QGLFramebufferObject::NoAttachment );
    f.setMipmap(  0 );
    f.setSamples( 0 );
    f.setTextureTarget( GL_TEXTURE_2D );
    QGLFramebufferObject fb( 500, 500, f );

    //checkError( "after make frame buffer" );

    fb.bind();

    //checkError( "after bind frame buffer" );

    glViewport( 0, 0, 500, 500 );

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);
    glDisable( GL_POINT_SMOOTH );
    glDisable( GL_LINE_SMOOTH );

    glLineWidth(1.0);

    std::vector< float > x( 2 );
    std::vector< float > y( 2 );

    std::vector< float > im( 500*500, 0.f );

    for( double a = 0.0; a < 3.14159265359 / 2.0; a += 0.001 )
    {
        std::fill( im.begin(), im.end(), 0.f );

        x[ 0 ] = 0;
        y[ 0 ] = 0;

        x[ 1 ] = std::cos( a )*.9;
        y[ 1 ] = std::sin( a )*.9;

        glClearColor( 0.0, 0.0, 0.0, 1.0 );
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast< size_t >( m_vbo->size() ) < x.size() * 2 * sizeof( float )  )
        {
            m_vbo->allocate( x.size() * 2 * sizeof( float )  );
        }

        m_vbo->write( 0, x.data(), x.size() * sizeof( float ) );
        m_vbo->write( x.size() *  sizeof( float ), y.data(), x.size() * sizeof( float ) );

        m_vbo->release();
        m_vao->release();

        program->bind();

        GLuint xAttr = program->attributeLocation(    "xAttr"    );
        GLuint yAttr = program->attributeLocation(    "yAttr"    );

        m_vao->bind();
        m_vbo->bind();

        program->enableAttributeArray( xAttr );
        program->enableAttributeArray( yAttr );

        program->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
        program->setAttributeBuffer( yAttr, GL_FLOAT, x.size()*sizeof( float ), 1 );

        glDrawArrays( GL_LINES, 0, x.size() );

        program->disableAttributeArray( xAttr );
        program->disableAttributeArray( yAttr );

        m_vbo->release();
        m_vao->release();

        program->release();

        glReadPixels( 0, 0, 500, 500, GL_RED, GL_FLOAT, im.data() );

        float sum = 0;
        for ( int p = 0; p < 500*500; ++p )
        {
            sum += im[ p ];
        }

        result.push_back( sum );
    }

    fb.release();
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_POINT_SMOOTH );
    glDisable( GL_LINE_SMOOTH );
}

std::pair< std::vector< Vec2< float > >, std::vector< Vec2< float > > >
Renderer::renderTextMultiColor( bool enlarge,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    int W_WIDTH,
    int W_HEIGHT,
    const std::string & text,
    const std::vector< Vec3< float> > & colors,
    GLfloat x,
    GLfloat y,
    GLfloat scale,
    bool vertical  )
{
    const float texcoords[] =
    {
        0.f, 0.f,
        0.f, 1.f,
        1.f, 1.f,
        0.f, 0.f,
        1.f, 1.f,
        1.f, 0.f
    };

    bool veryLarge = false;
    if( scale >= 2.f )
    {
        scale = 1.f;
        veryLarge = true;
    }

    program->bind();

    m_vao->bind();
    m_vbo->bind();

    QMatrix4x4 P;
    P.ortho( 0, W_WIDTH, 0, W_HEIGHT, 0, 1.0 );
    GLuint loc = program->uniformLocation( "P" );
    program->setUniformValue( loc, P );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glActiveTexture( GL_TEXTURE0 );

    x = std::round( x );
    y = std::round( y );

    std::vector< Vec2< float > > positions;
    std::vector< Vec2< float > > sizes;

    int i = 0;
    for ( auto c : text )
    {
        Character ch = veryLarge ? VeryLargerCharacters[ c ] : ( enlarge ? LargerCharacters[ c ] : Characters[c] );

        GLfloat xpos = x + ch.Bearing.x() * scale;

        GLfloat ypos = vertical ? ( y + ch.Size.x() * scale )
                       : ( y - ( ch.Size.y() - ch.Bearing.y() ) * scale );

        GLfloat w = ch.Size.x() * scale;
        GLfloat h = ch.Size.y() * scale;

        positions.push_back( Vec2< float >( xpos + w, ypos ) );
        sizes.push_back( Vec2< float >( w, h ) );

        const float quad[] =
        {
            xpos,     ypos + h,
            xpos,     ypos,
            xpos + w, ypos,
            xpos,     ypos + h,
            xpos + w, ypos,
            xpos + w, ypos + h
        };

        const float quadV[] =
        {
            xpos - h, ypos - w,
            xpos,     ypos - w,
            xpos,     ypos,
            xpos - h, ypos - w,
            xpos,     ypos,
            xpos - h, ypos
        };

        if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
        {
            m_vbo->allocate( 24 * sizeof( float ) );
        }

        m_vbo->write( 0, vertical ? quadV : quad, 12 * sizeof( float ) );
        m_vbo->write( 12 * sizeof(float), texcoords, 12 * sizeof( float ) );

        loc = program->uniformLocation( "textColor" );
        program->setUniformValue( loc, QVector3D( colors[ i ].r(), colors[ i ].g(), colors[ i ].b() ) );

        glBindTexture( GL_TEXTURE_2D, ch.TextureID );
        glDrawArrays( GL_TRIANGLES, 0, 6 );

        if( vertical )
        {
            y += ( ch.Advance >> 6 ) * scale;
        }
        else
        {
            x += (ch.Advance >> 6 ) * scale;
        }
        ++i;
    }

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();

    m_vbo->release();
    m_vao->release();

    return std::pair< std::vector< Vec2< float > >,
                      std::vector< Vec2< float > > >( { positions, sizes } );
}

void Renderer::renderText( bool enlarge,
                           const std::unique_ptr<QOpenGLShaderProgram> & program,
                           int W_WIDTH,
                           int W_HEIGHT,
                           const std::string & text,
                           GLfloat x,
                           GLfloat y,
                           GLfloat scale,
                           const Vec3< float > & color, bool vertical  )
{
    const float texcoords[] =
    {
        0.f, 0.f,
        0.f, 1.f,
        1.f, 1.f,
        0.f, 0.f,
        1.f, 1.f,
        1.f, 0.f
    };

    bool veryLarge = false;
    if( scale >= 2.f )
    {
        scale = 1.f;
        veryLarge = true;
    }

    program->bind();

    m_vao->bind();
    m_vbo->bind();

    QMatrix4x4 P;
    P.ortho( 0, W_WIDTH, 0, W_HEIGHT, 0, 1.0 );
    GLuint loc = program->uniformLocation( "P" );
    program->setUniformValue( loc, P );

    loc = program->uniformLocation( "textColor" );
    program->setUniformValue( loc, QVector3D( color.r(), color.g(), color.b() ) );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glActiveTexture( GL_TEXTURE0 );

    x = std::round( x );
    y = std::round( y );

    for ( auto c : text )
    {
        Character ch = veryLarge ? VeryLargerCharacters[ c ] : ( enlarge ? LargerCharacters[ c ] : Characters[c] );

        GLfloat xpos = x + ch.Bearing.x() * scale;

        GLfloat ypos = vertical ? ( y + ch.Size.x() * scale )
                       : ( y - ( ch.Size.y() - ch.Bearing.y() ) * scale );

        GLfloat w = ch.Size.x() * scale;
        GLfloat h = ch.Size.y() * scale;

        const float quad[] =
        {
            xpos,     ypos + h,
            xpos,     ypos,
            xpos + w, ypos,
            xpos,     ypos + h,
            xpos + w, ypos,
            xpos + w, ypos + h
        };

        const float quadV[] =
        {
            xpos - h, ypos - w,
            xpos,     ypos - w,
            xpos,     ypos,
            xpos - h, ypos - w,
            xpos,     ypos,
            xpos - h, ypos
        };

        if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
        {
            m_vbo->allocate( 24 * sizeof( float ) );
        }

        m_vbo->write( 0, vertical ? quadV : quad, 12 * sizeof( float ) );
        m_vbo->write( 12 * sizeof(float), texcoords, 12 * sizeof( float ) );

        glBindTexture( GL_TEXTURE_2D, ch.TextureID );

        glDrawArrays( GL_TRIANGLES, 0, 6 );

        if( vertical )
        {
            y += ( ch.Advance >> 6 ) * scale;
        }
        else
        {
            x += (ch.Advance >> 6 ) * scale;
        }
    }

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();

    m_vbo->release();
    m_vao->release();
}

void Renderer::renderClusterLayer(
    Texture2D1 & tex,
    TNR::Texture1D3  & tf,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    const QMatrix4x4 & TR )
{
    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
    {
        m_vbo->allocate( 24 * sizeof( float ) );
    }

    m_vbo->write( 0, quad, 12 * sizeof( float ) );
    m_vbo->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    program->bind();

    glActiveTexture(GL_TEXTURE0);
    tex.bind();

    glActiveTexture(GL_TEXTURE1);
    tf.bind();

    glActiveTexture(GL_TEXTURE0);

    program->setUniformValue( "MVP", TR );
    program->setUniformValue( "density", 0 );
    program->setUniformValue( "tf",  1 );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();
    m_vbo->release();
    m_vao->release();
}

void Renderer::renderGrid(
    const Vec2< float > & size,
    const Vec2< float > & position,
    const Vec2< float > & scissor,
    int gridSpacing,
    const QVector4D & gridColor,
    const QVector4D & backgroundColor,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    const std::unique_ptr<QOpenGLShaderProgram> & program2,
    bool withClear )
{
    glViewport( position.x(), position.y(), size.x(), size.y() );
    glScissor(  position.x() + scissor.a(), position.y(), size.x() - ( scissor.a() + scissor.b() ), size.y() );
    glEnable( GL_SCISSOR_TEST );

    if( withClear )
    {
        clear( backgroundColor, program );
    }

    renderPopSquare( program2, 1.0 );

    int rows = std::max( size.y() / gridSpacing, 2.f );
    int cols = std::max( size.x() / gridSpacing, 2.f );

    rows += rows % 2;
    cols += cols % 2;

    static std::vector< Vec2< float > > lines;
    lines.clear();

    float gridSpacingY = 2 / ( float ) rows;
    float gridSpacingX = 2 / ( float ) cols;

    for( int i = 1; i < rows; ++i )
    {
        lines.push_back( Vec2< float >( -1, -1 + i * gridSpacingY ) );
        lines.push_back( Vec2< float >(  1, -1 + i * gridSpacingY ) );
    }

    for( int j = 1; j < cols; ++j )
    {
        lines.push_back( Vec2< float >( -1 + j * gridSpacingX, -1 ) );
        lines.push_back( Vec2< float >( -1 + j * gridSpacingX,  1 ) );
    }

    glLineWidth( 1.0 );
    renderPrimitivesFlat( lines, program, gridColor, QMatrix4x4(), GL_LINES );
}

void Renderer::renderGrid(
    const Vec2< float > & size,
    const Vec2< float > & position,
    int gridSpacing,
    const QVector4D & gridColor,
    const QVector4D & backgroundColor,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    const std::unique_ptr<QOpenGLShaderProgram> & program2 )
{
    renderGrid(
        size,
        position,
        Vec2< float >( 0, 0 ),
        gridSpacing,
        gridColor,
        backgroundColor,
        program,
        program2,
        true );
}

void Renderer::renderGrid(
    int screenWidth,
    int screenHeight,
    const Vec2< float > & mins,
    const Vec2< float > & maxes,
    int ROWS,
    int COLS,
    const QMatrix4x4 & M,
    const Vec2< float > & gc1,
    const Vec2< float > & gc2,
    const QVector4D & color,
    const QVector4D & color2,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    const std::unique_ptr<QOpenGLShaderProgram> & program2,
    bool cellSelected )
{

    Vec2< float > a = maxes;
    Vec2< float > b( maxes.x(), mins.y() );
    Vec2< float > c( mins.x(), maxes.y() );

    QVector4D aP = M * QVector4D( a.x(), a.y(), 0, 1.0 );
    QVector4D bP = M * QVector4D( b.x(), b.y(), 0, 1.0 );
    QVector4D cP = M * QVector4D( c.x(), c.y(), 0, 1.0 );

    float x1 = ( aP.x() / 2.0 + .5 ) * screenWidth;
    float x3 = ( cP.x() / 2.0 + .5 ) * screenWidth;
    float y1 = ( aP.y() / 2.0 + .5 ) * screenHeight;
    float y2 = ( bP.y() / 2.0 + .5 ) * screenHeight;

    glViewport( x3-1, y2-1, x1 - x3 + 2, y1 - y2 + 2 );
    clear( QVector4D( 0.9, 0.9, 0.9, 1.0 ), program );
    renderPopSquare( program2, 1.0 );

    glViewport( 0, 0, screenWidth, screenHeight );

    //qDebug() << "rendered grid background";

    static std::vector< Vec2< float > > lines;

    Vec2< float > widths = ( maxes - mins );
    widths.x( widths.x() );
    widths.y( widths.y() );

    //qDebug() << "rows = " << ROWS <<", cols = " << COLS;

    float  dy = (  widths.y() ) / ROWS;

    //qDebug() << "dy=" << dy;

    COLS = ( widths.x() ) / dy;
    float dx = ( widths.x() ) / COLS;

    //qDebug() << "adjusted: rows = " << ROWS <<", cols = " << COLS;

    //qDebug() << "lines size is :" << ( ROWS + COLS - 2 ) * 2;

    if( COLS >= 2 && ROWS >= 2 )
    {
        lines.resize( ( ROWS + COLS - 2 ) * 2 );

        int nextIdx = 0;
        for( int c = 1; c < COLS; ++c )
        {
            lines[ nextIdx++ ] = Vec2< float >( mins.x() + dx*c, mins.y() );
            lines[ nextIdx++ ] = Vec2< float >( mins.x() + dx*c, maxes.y() );
        }
        for( int r = 1; r < ROWS; ++r )
        {
            lines[ nextIdx++ ] = Vec2< float >(  mins.x(), mins.y() + dy*r );
            lines[ nextIdx++ ] = Vec2< float >( maxes.x(), mins.y() + dy*r );
        }

        //qDebug() << "nextId is : " << nextIdx;

        glLineWidth( 1.0 );
        renderPrimitivesFlat( lines, program, color, M, GL_LINES );
    }
    //qDebug() << "rendered lines";

    if( cellSelected )
    {
        //qDebug() << "cell selected is true";

        if( gc1.x() > mins.x() && gc1.x() < maxes.x() && gc1.y() > mins.y() && gc1.y() < maxes.y()
                && gc2.x() > mins.x() && gc2.x() < maxes.x() && gc2.y() > mins.y() && gc2.y() < maxes.y() )
        {
            glLineWidth( 2.0 );
            renderPrimitivesFlat(
                std::vector< Vec2< float > > (
            {
                Vec2< float >( mins.x(), gc1.y() ), Vec2< float >( gc1.x(), gc1.y() ),
                Vec2< float >( gc1.x(), gc1.y() ), Vec2< float >( gc1.x(),  mins.y() ),
                Vec2< float >( mins.x(), gc2.y() ), Vec2< float >( gc2.x(), gc2.y() ),
                Vec2< float >( gc2.x(), gc2.y() ), Vec2< float >( gc2.x(), mins.y() )
            } ),
            program,
            QVector4D( .2, .2, .2, 1 ),
            M,
            GL_LINES );
        }
    }

    //qDebug() << "done";
}

void Renderer::renderColorLegend(
    const Vec2< float > & pos,
    const Vec2< float > & size,
    TNR::Texture1D3 & tf,
    const std::unique_ptr<QOpenGLShaderProgram> & program )
{
    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    glViewport( pos.x(), pos.y(), size.x(), size.y() );

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
    {
        m_vbo->allocate( 24 * sizeof( float ) );
    }

    m_vbo->write( 0, quad, 12 * sizeof( float ) );
    m_vbo->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    program->bind();

    glActiveTexture(GL_TEXTURE0);
    tf.bind();
    program->setUniformValue( "tf", 0 );

    //checkError( "after bind textures" );

    program->setUniformValue( "tf", 0 );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    //checkError( "after set u and a" );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();

    m_vbo->release();
    m_vao->release();

    //checkError( "after render heat map" );
}

void Renderer::renderHeatMapTexture(
    Texture2D1 & tex,
    TNR::Texture1D3 & tf,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    float mn,
    float mx,
    bool fade )
{
    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
    {
        m_vbo->allocate( 24 * sizeof( float ) );
    }

    m_vbo->write( 0, quad, 12 * sizeof( float ) );
    m_vbo->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    program->bind();

    glActiveTexture(GL_TEXTURE0);
    tex.bind();
    program->setUniformValue( "tex", 0 );

    glActiveTexture(GL_TEXTURE1);
    tf.bind();

    glActiveTexture(GL_TEXTURE0);

    //checkError( "after bind textures" );

    program->setUniformValue( "tex", 0 );
    program->setUniformValue( "tf",  1 );

    program->setUniformValue( "mx", mx );
    program->setUniformValue( "mn", mn );
    program->setUniformValue( "fade", fade );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    //checkError( "after set u and a" );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();

    m_vbo->release();
    m_vao->release();

    //checkError( "after render heat map" );
}

void Renderer::renderFocusContextTextures(
    TNR::Texture2D1 & tex1,
    TNR::Texture2D1 & tex2,
    TNR::Texture1D3 & tf1,
    TNR::Texture1D3 & tf2,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    float mx1,
    float mx2,
    float mn1,
    float mn2,
    float texWidth,
    float texHeight,
    int mode,
    int which,
    bool localize,
    const Vec2< float > & localCenter,
    const Vec2< float > & localRadius,
    const Vec2< float>  & localRange,
    bool forground )
{   
    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
    {
        m_vbo->allocate( 24 * sizeof( float ) );
    }

    m_vbo->write( 0, quad, 12 * sizeof( float ) );
    m_vbo->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    program->bind();

    glActiveTexture( GL_TEXTURE0 );
    tex1.bind();

    glGenerateMipmap( GL_TEXTURE_2D );
    //checkError( "generate mipmap" );

    glActiveTexture( GL_TEXTURE1 );
    tex2.bind();

    glGenerateMipmap( GL_TEXTURE_2D );
    //checkError( "generate mipmap" );

    glActiveTexture(GL_TEXTURE2);
    tf1.bind();

    glActiveTexture(GL_TEXTURE3);
    tf2.bind();

    glActiveTexture(GL_TEXTURE0);

    program->setUniformValue( "tex1", 0 );
    program->setUniformValue( "tex2", 1 );
    program->setUniformValue( "tf1",  2 );
    program->setUniformValue( "tf2",  3 );

    program->setUniformValue( "mx1", mx1 );
    program->setUniformValue( "mx2", mx2 );

    program->setUniformValue( "mn1", mn1 );
    program->setUniformValue( "mn2", mn2 );

    program->setUniformValue( "outline", true );
    program->setUniformValue( "mode",    mode );

    program->setUniformValue( "which", which );

    program->setUniformValue( "inverseTexWidth", 1.f / texWidth );
    program->setUniformValue( "inverseTexHeight", 1.f / texHeight );

    program->setUniformValue( "localize", localize );
    program->setUniformValue( "localCenter", QVector2D( localCenter.x(), localCenter.y() ) );
    program->setUniformValue( "localRadius", QVector2D( localRadius.x(), localRadius.y() ) );
    program->setUniformValue( "localRange",  QVector2D( localRange.x(),  localRange.y() ) );
    program->setUniformValue( "forground", forground );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();

    m_vbo->release();
    m_vao->release();
}

void Renderer::renderHeatMapTexture(
    Texture2D1 & tex,
    TNR::Texture1D3 & tf,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    float mx,
    bool fade,
    float texWidth,
    float texHeight,
    bool outline,
    bool onlyOutline )
{
    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
    {
        m_vbo->allocate( 24 * sizeof( float ) );
    }

    m_vbo->write( 0, quad, 12 * sizeof( float ) );
    m_vbo->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    program->bind();

    glActiveTexture( GL_TEXTURE0 );
    tex.bind();

    glGenerateMipmap( GL_TEXTURE_2D );
    //checkError( "generate mipmap" );

    glActiveTexture(GL_TEXTURE1);
    tf.bind();

    glActiveTexture(GL_TEXTURE0);

    //checkError( "after bind textures" );

    program->setUniformValue( "tex", 0 );
    program->setUniformValue( "tf",  1 );

    program->setUniformValue( "mx", mx );
    program->setUniformValue( "fade", fade );
    program->setUniformValue( "outline", outline );
    program->setUniformValue( "onlyOutline", onlyOutline );

    program->setUniformValue( "inverseTexWidth", 1.f / texWidth );
    program->setUniformValue( "inverseTexHeight", 1.f / texHeight );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    //checkError( "after set u and a" );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();

    m_vbo->release();
    m_vao->release();

    //checkError( "after render heat map" );
}

void Renderer::renderGrid(
    int screenWidth,
    int screenHeight,
    const Vec2< float > & mins,
    const Vec2< float > & maxes,
    int ROWS,
    int COLS,
    int TIC_INTERVAL,
    const QMatrix4x4 & M,
    const Vec2< float > & gc1,
    const Vec2< float > & gc2,
    const QVector4D & color,
    const QVector4D & color2,
    const std::unique_ptr<QOpenGLShaderProgram> & program,
    const std::unique_ptr<QOpenGLShaderProgram> & program2,
    bool cellSelected )
{
    Vec2< float > a = maxes;
    Vec2< float > b( maxes.x(), mins.y() );
    Vec2< float > c( mins.x(), maxes.y() );

    QVector4D aP = M * QVector4D( a.x(), a.y(), 0, 1.0 );
    QVector4D bP = M * QVector4D( b.x(), b.y(), 0, 1.0 );
    QVector4D cP = M * QVector4D( c.x(), c.y(), 0, 1.0 );

    float x1 = ( aP.x() / 2.0 + .5 ) * screenWidth;
    float x3 = ( cP.x() / 2.0 + .5 ) * screenWidth;
    float y1 = ( aP.y() / 2.0 + .5 ) * screenHeight;
    float y2 = ( bP.y() / 2.0 + .5 ) * screenHeight;

    glViewport( x3-1, y2-1, x1 - x3 + 2, y1 - y2 + 2 );
    clear( QVector4D( 0.9, 0.9, 0.9, 1.0 ), program );
    renderPopSquare( program2, 1.0 );

    glViewport( 0, 0, screenWidth, screenHeight );

    static std::vector< Vec2< float > > lines;
    static std::vector< Vec2< float > > Tics;

    Vec2< float > widths = ( maxes - mins );
    widths.x( widths.x() );
    widths.y( widths.y() );

    float  dy = (  widths.y() ) / ROWS;
    float dx = ( widths.x() ) / COLS;

    lines.clear();
    Tics.clear();

    Tics.push_back( Vec2< float >( mins.x(), mins.y() ) );
    Tics.push_back( Vec2< float >( maxes.x(), mins.y() ) );

    Tics.push_back( Vec2< float >( mins.x(), mins.y() ) );
    Tics.push_back( Vec2< float >( mins.x(), maxes.y() ) );

    for( int c = 0; c <= COLS; ++c )
    {
        if( TIC_INTERVAL > 0 )
        {
            if( c % ( ( COLS ) / 2 ) == 0  )
            {
                Tics.push_back( Vec2< float >( mins.x() + dx*c, mins.y() ) );
                Tics.push_back( Vec2< float >( mins.x() + dx*c, mins.y() - dx / 3.0 ) );
            }
        }
        if( c < COLS && c > 0 )
        {
            lines.push_back( Vec2< float >( mins.x() + dx*c, mins.y() ) );
            lines.push_back( Vec2< float >( mins.x() + dx*c, maxes.y() ) );
        }
    }
    for( int r = 0; r <= ROWS; ++r )
    {
        if( TIC_INTERVAL > 0 )
        {
            if( r % ( ROWS / 2 ) == 0 )
            {
                Tics.push_back( Vec2< float >( mins.x(), mins.y() + dy*r ) );
                Tics.push_back( Vec2< float >( mins.x() - dx / 3.0, mins.y() + dy*r ) );
            }
        }
        if( r < ROWS && r > 0 )
        {
            lines.push_back( Vec2< float >(  mins.x(), mins.y() + dy*r ) );
            lines.push_back( Vec2< float >( maxes.x(), mins.y() + dy*r ) );
        }
    }

    if( TIC_INTERVAL < 0 )
    {
        Tics.push_back( Vec2< float >( maxes.x(), mins.y() ) );
        Tics.push_back( Vec2< float >( maxes.x(), mins.y() - dx / 3.0 ) );

        Tics.push_back( Vec2< float >( mins.x(), mins.y() ) );
        Tics.push_back( Vec2< float >( mins.x(), mins.y() - dx / 3.0 ) );

        Tics.push_back( Vec2< float >( mins.x(), maxes.y() ) );
        Tics.push_back( Vec2< float >( mins.x() - dx / 3.0, maxes.y() ) );

        Tics.push_back( Vec2< float >( mins.x(), mins.y() ) );
        Tics.push_back( Vec2< float >( mins.x() - dx / 3.0, mins.y() ) );
    }

    glLineWidth( 1.0 );
    renderPrimitivesFlat( lines, program, color, M, GL_LINES );

    glLineWidth( 2.0 );
    renderPrimitivesFlat( Tics, program, QVector4D( .3, .3, .3, 1.0 ), M, GL_LINES );

    if( cellSelected )
    {
        if( gc1.x() > mins.x() && gc1.x() < maxes.x() && gc1.y() > mins.y()  && gc1.y() < maxes.y() )
        {
            glLineWidth( 2.0 );
            renderPrimitivesFlat(
                std::vector< Vec2< float > > (
            {
                Vec2< float >( mins.x(), gc1.y() ), Vec2< float >( gc1.x(), gc1.y() ),
                Vec2< float >( gc1.x(), gc1.y() ), Vec2< float >( gc1.x(),  mins.y() ),
                Vec2< float >( mins.x(), gc2.y() ), Vec2< float >( gc2.x(), gc2.y() ),
                Vec2< float >( gc2.x(), gc2.y() ), Vec2< float >( gc2.x(), mins.y() )
            } ),
            program,
            QVector4D( .2, .2, .2, 1 ),
            M,
            GL_LINES );
        }
    }
}

void Renderer::renderTexturedQuad(
    const std::vector< float > & data,
    int COLS,
    int ROWS,
    const std::unique_ptr<QOpenGLShaderProgram> & program )
{

    glBindTexture( GL_TEXTURE_2D, quadTexId );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_R32F, COLS, ROWS, 0, GL_RED, GL_FLOAT, data.data() );

    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
    {
        m_vbo->allocate( 24 * sizeof( float ) );
    }

    m_vbo->write( 0, quad, 12 * sizeof( float ) );
    m_vbo->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    program->bind();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture( GL_TEXTURE_2D, quadTexId );

    program->setUniformValue( "tex", 0 );

    GLuint posAttr = program->attributeLocation( "ps" );
    GLuint uvAttr  = program->attributeLocation( "tc" );

    program->enableAttributeArray( posAttr );
    program->enableAttributeArray( uvAttr );

    program->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    program->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    program->disableAttributeArray( posAttr );
    program->disableAttributeArray( uvAttr );

    program->release();

    m_vbo->release();
    m_vao->release();
}

void Renderer::renderTriangleMesh(
    const std::vector< Vec3< float > > & verts,
    const std::vector< Vec3< float > > & normals,
    const std::vector< I3 >   & indices,
    const std::vector< Vec3< float > > & colors,
    const Material & _material,
    const Light    & _light,
    const QMatrix4x4 & M,
    const QMatrix4x4 & V,
    const QMatrix4x4 & P,
    const std::unique_ptr< QOpenGLShaderProgram > & prog,
    bool useOpacity )
{
    //checkError( "before allocate" );

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast<size_t> ( m_vbo->size() ) < verts.size() * sizeof( Vec3< float > ) * 3  )
    {
        m_vbo->allocate( verts.size() * sizeof( Vec3< float > ) * 3 );
    }

    //checkError( "after allocate verts and normals" );

    m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
    m_vbo->write( verts.size() * sizeof( Vec3< float > ),
                  normals.data(), normals.size() * sizeof( Vec3< float > ) );
    m_vbo->write( verts.size() * sizeof( Vec3< float > ) * 2,
                  colors.data(), colors.size() * sizeof( Vec3< float > ) );

    //checkError( "after write points and normals" );

    m_ibo->bind();
    if( static_cast< size_t >( m_ibo->size() ) < indices.size() * sizeof( indices[0] ) )
    {
        m_ibo->allocate( indices.size() * sizeof( indices[0] ) );
    }

    //checkError( "after allocate indices" );

    m_ibo->write( 0, indices.data(), indices.size() * sizeof( indices[0] ) );
    m_vao->release();

    //

    //checkError( "after write indices" );

    prog->bind();

    float opacity = useOpacity ? _material.opacity : 1.f;

    QMatrix4x4 MVP = P*V*M;
    GLuint loc = prog->uniformLocation( "mvp" );
    prog->setUniformValue( loc, MVP );

    QMatrix4x4 MV = V*M;
    loc = prog->uniformLocation( "modelView" );
    prog->setUniformValue( loc, MV );

    loc = prog->uniformLocation( "model" );
    prog->setUniformValue( loc, M );

    loc = prog->uniformLocation( "useColorAttr" );
    prog->setUniformValue( loc, false );

    loc = prog->uniformLocation( "M.Ka_scale" );
    prog->setUniformValue( loc, _material.Ka_scale );

    loc = prog->uniformLocation( "M.Kd_scale" );
    prog->setUniformValue( loc, _material.Kd_scale );

    loc = prog->uniformLocation( "M.Ks_scale" );
    prog->setUniformValue( loc, _material.Ks_scale );

    loc = prog->uniformLocation( "M.Ka" );
    prog->setUniformValue( loc, _material.Ka_scale  * _material.Ka );

    loc = prog->uniformLocation( "M.Kd" );
    prog->setUniformValue( loc, _material.Kd_scale  * _material.Kd );

    loc = prog->uniformLocation( "M.Ks" );
    prog->setUniformValue( loc, _material.Ks_scale  * _material.Ks );

    loc = prog->uniformLocation( "M.alpha" );
    prog->setUniformValue( loc, _material.shininess );

    loc = prog->uniformLocation( "M.opacity" );
    prog->setUniformValue( loc, opacity );

    loc = prog->uniformLocation( (QString("enabled[") + QString::number(0) + QString( "]" ) ) );
    prog->setUniformValue( loc, true );
    for( int i = 1; i < 8; ++i )
    {
        loc = prog->uniformLocation( (QString("enabled[") + QString::number(i) + QString( "]" ) ) );
        prog->setUniformValue( loc, false );
    }

    loc = prog->uniformLocation( ( QString("L[0].type" ) ) );
    prog->setUniformValue( loc, _light.type );

    loc = prog->uniformLocation( "normalMatrix" );
    prog->setUniformValue( loc, MV.normalMatrix() );

    loc = prog->uniformLocation( "useColorAttr" );
    prog->setUniformValue( loc, true );

    loc = prog->uniformLocation( (QString("L[0].direction" ) ) );
    prog->setUniformValue( loc, _light.direction  );

    loc = prog->uniformLocation( (QString("L[0].Ia" ) ) );
    prog->setUniformValue( loc, _light.Ia * _light.Ia_scale );

    loc = prog->uniformLocation( (QString("L[0].Id" ) ) );
    prog->setUniformValue( loc, _light.Id * _light.Id_scale );

    loc = prog->uniformLocation( (QString("L[0].Is" ) ) );
    prog->setUniformValue( loc, _light.Is * _light.Is_scale );

    GLuint posAttr = prog->attributeLocation( "posAttr" );
    GLuint normAttr = prog->attributeLocation( "normalAttr" );
    GLuint colorAttr = prog->attributeLocation( "colorAttr" );

    //checkError( "after set vars" );

    m_vao->bind();
    m_vbo->bind();
    m_ibo->bind();

    //checkError( "after bind buffers" );

    prog->enableAttributeArray( posAttr );
    prog->enableAttributeArray( normAttr );
    prog->enableAttributeArray( colorAttr );

    //checkError( "after enable" );

    prog->setAttributeBuffer( posAttr, GL_FLOAT, 0, 3 );
    prog->setAttributeBuffer( normAttr, GL_FLOAT, verts.size()*sizeof( Vec3< float > ), 3 );
    prog->setAttributeBuffer( colorAttr, GL_FLOAT, verts.size()*sizeof( Vec3< float > )*2, 3 );

    //checkError( "after set atrs" );

    glDrawElements(GL_TRIANGLES, indices.size() * 3, GL_UNSIGNED_INT, 0 );

    //checkError( "after render" );

    prog->disableAttributeArray( posAttr );
    prog->disableAttributeArray( normAttr );
    prog->disableAttributeArray( colorAttr );

    m_ibo->release();
    m_vbo->release();
    m_vao->release();

    prog->release();

    //checkError( "everything" );
}

void Renderer::renderTriangleMesh(
    const std::vector< Vec3< float > > & verts,
    const std::vector< Vec3< float > > & normals,
    const std::vector< Vec3< float > > & colors,
    const Material & _material,
    const Light    & _light,
    const QMatrix4x4 & M,
    const QMatrix4x4 & V,
    const QMatrix4x4 & P,
    const std::unique_ptr<QOpenGLShaderProgram> & prog,
    bool  useOpacity )
{

    //checkError( "before allocate" );

    m_vao->bind();
    m_vbo->bind();
    if ( static_cast< size_t >( m_vbo->size() ) < verts.size() * sizeof( Vec3< float > ) * 3  )
    {
        m_vbo->allocate( verts.size() * sizeof( Vec3< float > ) * 3 );
    }

    //checkError( "after allocate verts and normals" );

    m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
    m_vbo->write( verts.size() * sizeof( Vec3< float > ),
                  normals.data(), normals.size() * sizeof( Vec3< float > ) );
    m_vbo->write( verts.size() * sizeof( Vec3< float > ) * 2,
                  colors.data(), colors.size() * sizeof( Vec3< float > ) );

    //checkError( "after write points and normals" );

    m_vao->release();

    //

    //checkError( "after write indices" );

    prog->bind();

    float opacity = useOpacity ? _material.opacity : 1.f;

    QMatrix4x4 MVP = P*V*M;
    GLuint loc = prog->uniformLocation( "mvp" );
    prog->setUniformValue( loc, MVP );

    QMatrix4x4 MV = V*M;
    loc = prog->uniformLocation( "modelView" );
    prog->setUniformValue( loc, MV );

    loc = prog->uniformLocation( "model" );
    prog->setUniformValue( loc, M );

    loc = prog->uniformLocation( "useColorAttr" );
    prog->setUniformValue( loc, false );

    loc = prog->uniformLocation( "M.Ka" );
    prog->setUniformValue( loc, _material.Ka_scale  * _material.Ka );

    loc = prog->uniformLocation( "M.Kd" );
    prog->setUniformValue( loc, _material.Kd_scale  * _material.Kd );

    loc = prog->uniformLocation( "M.Ks" );
    prog->setUniformValue( loc, _material.Ks_scale  * _material.Ks );

    loc = prog->uniformLocation( "M.alpha" );
    prog->setUniformValue( loc, _material.shininess );

    loc = prog->uniformLocation( "M.opacity" );
    prog->setUniformValue( loc, opacity );

    loc = prog->uniformLocation( (QString("enabled[") + QString::number(0) + QString( "]" ) ) );
    prog->setUniformValue( loc, true );
    for( int i = 1; i < 8; ++i )
    {
        loc = prog->uniformLocation( (QString("enabled[") + QString::number(i) + QString( "]" ) ) );
        prog->setUniformValue( loc, false );
    }

    loc = prog->uniformLocation( ( QString("L[0].type" ) ) );
    prog->setUniformValue( loc, _light.type );

    loc = prog->uniformLocation( "normalMatrix" );
    prog->setUniformValue( loc, MV.normalMatrix() );

    loc = prog->uniformLocation( "useColorAttr" );
    prog->setUniformValue( loc, true );

    loc = prog->uniformLocation( (QString("L[0].direction" ) ) );
    prog->setUniformValue( loc, _light.direction  );

    loc = prog->uniformLocation( "M.Ka_scale" );
    prog->setUniformValue( loc, _material.Ka_scale );

    loc = prog->uniformLocation( "M.Kd_scale" );
    prog->setUniformValue( loc, _material.Kd_scale );

    loc = prog->uniformLocation( "M.Ks_scale" );
    prog->setUniformValue( loc, _material.Ks_scale );

    loc = prog->uniformLocation( (QString("L[0].Ia" ) ) );
    prog->setUniformValue( loc, _light.Ia * _light.Ia_scale );

    loc = prog->uniformLocation( (QString("L[0].Id" ) ) );
    prog->setUniformValue( loc, _light.Id * _light.Id_scale );

    loc = prog->uniformLocation( (QString("L[0].Is" ) ) );
    prog->setUniformValue( loc, _light.Is * _light.Is_scale );

    GLuint posAttr = prog->attributeLocation( "posAttr" );
    GLuint normAttr = prog->attributeLocation( "normalAttr" );
    GLuint colorAttr = prog->attributeLocation( "colorAttr" );

    //checkError( "after set vars" );

    m_vao->bind();
    m_vbo->bind();

    //checkError( "after bind buffers" );

    prog->enableAttributeArray( posAttr );
    prog->enableAttributeArray( normAttr );
    prog->enableAttributeArray( colorAttr );

    //checkError( "after enable" );

    prog->setAttributeBuffer( posAttr, GL_FLOAT, 0, 3 );
    prog->setAttributeBuffer( normAttr, GL_FLOAT, verts.size()*sizeof( Vec3< float > ), 3 );
    prog->setAttributeBuffer( colorAttr, GL_FLOAT, verts.size()*sizeof( Vec3< float > )*2, 3 );

    //checkError( "after set atrs" );

    glDrawArrays(GL_TRIANGLES, 0, verts.size() );

    //checkError( "after render" );

    prog->disableAttributeArray( posAttr );
    prog->disableAttributeArray( normAttr );
    prog->disableAttributeArray( colorAttr );

    m_vbo->release();
    m_vao->release();

    prog->release();

    //checkError( "everything" );
}

void Renderer::renderTriangleMesh(
    const std::vector< Vec3< float > > & verts,
    const std::vector< Vec3< float > > & normals,
    const QVector4D & color,
    const QMatrix4x4 & M,
    const QMatrix4x4 & V,
    const QMatrix4x4 & P,
    const std::unique_ptr<QOpenGLShaderProgram> & prog )
{
    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < verts.size() * sizeof( Vec3< float > ) * 2  )
    {
        m_vbo->allocate( verts.size() * sizeof( Vec3< float > ) * 2 );
    }

    m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
    m_vbo->write( verts.size() * sizeof( Vec3< float > ),
                  normals.data(), normals.size() * sizeof( Vec3< float > ) );

    m_vao->release();

    prog->bind();

    QMatrix4x4 MVP = P*V*M;
    GLuint loc = prog->uniformLocation( "mvp" );
    prog->setUniformValue( loc, MVP );

    QMatrix4x4 MV = V*M;
    loc = prog->uniformLocation( "modelView" );
    prog->setUniformValue( loc, MV );

    loc = prog->uniformLocation( "model" );
    prog->setUniformValue( loc, M );

    loc = prog->uniformLocation( "normalMatrix" );
    prog->setUniformValue( loc, MV.normalMatrix() );

    loc = prog->uniformLocation( "cla" );
    prog->setUniformValue( loc, color );

    GLuint posAttr = prog->attributeLocation( "posAttr" );
    GLuint normAttr = prog->attributeLocation( "normalAttr" );

    m_vao->bind();
    m_vbo->bind();

    prog->enableAttributeArray( posAttr );
    prog->enableAttributeArray( normAttr );

    prog->setAttributeBuffer( posAttr, GL_FLOAT, 0, 3 );
    prog->setAttributeBuffer( normAttr, GL_FLOAT, verts.size()*sizeof( Vec3< float > ), 3 );

    glDrawArrays(GL_TRIANGLES, 0, verts.size() );

    prog->disableAttributeArray( posAttr );
    prog->disableAttributeArray( normAttr );

    m_vbo->release();
    m_vao->release();

    prog->release();
}

void Renderer::renderPathTubes(
    const std::vector< Vec3< float > > & verts,
    const std::vector< unsigned int > & indices,
    const std::vector< int > & breaks,
    const Material & material,
    const Light & light,
    const TrackBallCam & cam,
    float radius,
    const std::unique_ptr< QOpenGLShaderProgram> & prog )
{
    //checkError( "before allocate" );

    m_vao->bind();
    m_vbo->bind();
    if ( static_cast< size_t >( m_vbo->size() ) < verts.size() * ( sizeof( Vec3< float > ) + sizeof( int ) ) )
    {
        m_vbo->allocate( verts.size() * ( sizeof( Vec3< float > ) + sizeof( int ) ) );
    }

    //checkError( "after allocate verts" );

    m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
    m_vbo->write( verts.size() * sizeof( Vec3< float > ), breaks.data(), breaks.size() * sizeof( int ) );

    //checkError( "after write points" );

    m_ibo->bind();
    if( static_cast< size_t >( m_ibo->size() ) < indices.size() * sizeof( indices[0] ) )
    {
        m_ibo->allocate( indices.size() * sizeof( indices[0] ) );
    }

    //checkError( "after allocate indices" );

    m_ibo->write( 0, indices.data(), indices.size() * sizeof( indices[0] ) );

    m_vao->release();

    //

    //checkError( "after write indices" );

    prog->bind();

    QMatrix4x4 M;
    M.setToIdentity();

    QMatrix4x4 proj = cam.matProj();
    GLuint loc = prog->uniformLocation( "prj" );
    prog->setUniformValue( loc, proj );

    QMatrix4x4 MV = cam.matView( 1.0 ) * M;
    loc = prog->uniformLocation( "mvm" );
    prog->setUniformValue( loc, MV );

    loc = prog->uniformLocation( "normmtx" );
    prog->setUniformValue( loc, MV.normalMatrix() );

    //checkError( "after mats" );

    loc = prog->uniformLocation( "M.Ka" );
    prog->setUniformValue( loc, material.Ka_scale  * material.Ka );

    loc = prog->uniformLocation( "M.Kd" );
    prog->setUniformValue( loc, material.Kd_scale  * material.Kd );

    loc = prog->uniformLocation( "M.Ks" );
    prog->setUniformValue( loc, material.Ks_scale  * material.Ks );

    loc = prog->uniformLocation( "M.alpha" );
    prog->setUniformValue( loc, material.shininess );

    loc = prog->uniformLocation( "M.opacity" );
    prog->setUniformValue( loc, 1.f );

    loc = prog->uniformLocation( (QString("enabled[") + QString::number(0) + QString( "]" ) ) );
    prog->setUniformValue( loc, true );
    for( int i = 1; i < 8; ++i )
    {
        loc = prog->uniformLocation( (QString("enabled[") + QString::number(i) + QString( "]" ) ) );
        prog->setUniformValue( loc, false );
    }

    loc = prog->uniformLocation( ( QString("L[0].type" ) ) );
    prog->setUniformValue( loc, light.type );

    loc = prog->uniformLocation( (QString("L[0].direction" ) ) );
    prog->setUniformValue( loc, cam.getEye()  );

    loc = prog->uniformLocation( (QString("L[0].Ia" ) ) );
    prog->setUniformValue( loc, light.Ia * light.Ia_scale );

    loc = prog->uniformLocation( (QString("L[0].Id" ) ) );
    prog->setUniformValue( loc, light.Id * light.Id_scale );

    loc = prog->uniformLocation( (QString("L[0].Is" ) ) );
    prog->setUniformValue( loc, light.Is * light.Is_scale );

    //checkError( "after set most unforms" );

    //

    loc = prog->uniformLocation( "radius" );
    prog->setUniformValue( loc, (float)radius );

    //

    GLuint posAttr    = prog->attributeLocation( "posAttr"    );
    GLuint breakAttr    = prog->attributeLocation( "breakAttr"    );

    //checkError( "after set attr" );

    m_vao->bind();
    m_vbo->bind();
    m_ibo->bind();

    //checkError( "after bind buffers" );

    prog->enableAttributeArray( posAttr   );
    prog->enableAttributeArray( breakAttr );

    //checkError( "after enable" );

    prog->setAttributeBuffer( posAttr,    GL_FLOAT, 0, 3 );
    prog->setAttributeBuffer( breakAttr,  GL_FLOAT, verts.size() * sizeof( Vec3< float > ), 1 );

    //checkError( "after set atrs" );

    unsigned int num = indices.size();
    glDrawElements(GL_LINE_STRIP_ADJACENCY, num, GL_UNSIGNED_INT, 0 );

    //checkError( "after render" );

    prog->disableAttributeArray( breakAttr );
    prog->disableAttributeArray( posAttr );

    m_ibo->release();
    m_vbo->release();
    m_vao->release();

    prog->release();

    //checkError( "render pathtubes" );
}

void Renderer::renderColorMapped(
    const std::vector<Vec3< float >> & _verts,
    const std::vector<Vec3< float >> & _colors,
    float mx,
    const QMatrix4x4 & transform,
    unsigned int mode,
    float opacity,
    const std::unique_ptr<QOpenGLShaderProgram> & prog,
    bool fade )
{
    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < _verts.size() * 6 * sizeof( float ) )
    {
        m_vbo->allocate( _verts.size() * 6 * sizeof( float ) );
    }

    m_vbo->write( 0, _verts.data(), _verts.size() * 3 * sizeof( float ) );
    m_vbo->write( _verts.size() * 3 * sizeof( float ),
                  _colors.data(), _colors.size() * 3 * sizeof( float ) );

    m_vbo->release();
    m_vao->release();

    /////////////////////////////////////////////////////////////////////

    prog->bind();

    GLuint loc = prog->uniformLocation( "MVP" );
    prog->setUniformValue( loc, transform );

    GLuint posAttr = prog->attributeLocation( "posAttr" );
    GLuint colAttr = prog->attributeLocation( "velAttr" );

    loc = prog->uniformLocation( "opacity" );
    prog->setUniformValue( loc, opacity );

    loc = prog->uniformLocation( "rga" );
    prog->setUniformValue( loc, fade );

    loc = prog->uniformLocation( "mx" );
    prog->setUniformValue( loc, mx );

    loc = prog->uniformLocation( "mode" );
    prog->setUniformValue( loc, 0 );

    m_vao->bind();
    m_vbo->bind();

    prog->enableAttributeArray( posAttr );
    prog->enableAttributeArray( colAttr );

    prog->setAttributeBuffer( posAttr, GL_FLOAT, 0, 3 );
    prog->setAttributeBuffer(
        colAttr,
        GL_FLOAT,
        _verts.size() * 3 * sizeof( float ),
        3 );

    glDrawArrays( mode, 0, _verts.size() );

    prog->disableAttributeArray( posAttr );
    prog->disableAttributeArray( colAttr );

    m_vbo->release();
    m_vao->release();

    prog->release();

    //checkError( "After Rendering Pathlines" );
}

void Renderer::boxShadow(
    float height,
    float width,
    const std::unique_ptr<QOpenGLShaderProgram> & prog )
{
    m_boxShadow.width(  width );
    m_boxShadow.height( height );
    m_boxShadow.generateGeometry();
    renderPrimitivesColored( m_boxShadow.verts(), m_boxShadow.colors(), prog, QMatrix4x4(), GL_TRIANGLES );
}

void Renderer::clear(
    const QVector4D & color,
    const std::unique_ptr<QOpenGLShaderProgram> & program )
{
    std::vector< Vec2< float > > S =
    {
        Vec2< float >( -1.f,  1.f ),
        Vec2< float >( -1.f, -1.f ),
        Vec2< float >(  1.f,  1.f ),
        Vec2< float >(  1.f,  1.f ),
        Vec2< float >( -1.f, -1.f ),
        Vec2< float >(  1.f, -1.f )
    };

    renderPrimitivesFlat(
        S,
        program,
        color,
        QMatrix4x4(),
        GL_TRIANGLES );
}

void Renderer::renderViewPortBoxShadow( const ViewPort & viewPort, const std::unique_ptr<QOpenGLShaderProgram> & program )
{
    glViewport(
        viewPort.offsetX(),
        viewPort.offsetY(),
        viewPort.width(),
        viewPort.height()
    );

    boxShadow(
        viewPort.height(),
        viewPort.width(),
        program );
}

void Renderer::clearViewPortInverse( int height, int width, const ViewPort & vp, const QVector4D & cl, const std::unique_ptr<QOpenGLShaderProgram> & program )
{
    if( vp.offsetX() > 0 )
    {
        glViewport( 0, 0, vp.offsetX()+1, height );
        clear( cl, program );
    }
    if( width - ( vp.offsetX() + vp.width() ) > 0 )
    {
        glViewport( vp.offsetX() + vp.width() - 1, 0, width - ( vp.offsetX() + vp.width() ) + 2, height );
        clear( cl, program );
    }
    if( vp.offsetY() > 0 )
    {
        glViewport( 0, 0, width, vp.offsetY() + 1 );
        clear( cl, program );
    }
    if( height - ( vp.offsetY() + vp.height() ) > 0 )
    {
        glViewport( 0, vp.offsetY() + vp.height() - 1, width, height - ( vp.offsetY() + vp.height() ) + 2 );
        clear( cl, program );
    }
}

void Renderer::clearViewPort( const ViewPort & vp, const std::unique_ptr<QOpenGLShaderProgram> & program )
{
    glViewport(
        vp.offsetX(),
        vp.offsetY(),
        vp.width(),
        vp.height()
    );

    clear( QVector4D( vp.bkgColor().r, vp.bkgColor().g, vp.bkgColor().b, vp.bkgColor().a ), program );
}

void Renderer::renderPopSquare( const std::unique_ptr<QOpenGLShaderProgram> & program, double lineWidth )
{
    std::vector< Vec2< float > > S =
    {
        Vec2< float >( -1.f,  1.f ),
        Vec2< float >(  1.f,  1.f ),
        Vec2< float >( 1.f,  1.f ),
        Vec2< float >( 1.f, -1.f ),
        Vec2< float >( 1.f, -1.f ),
        Vec2< float >( -1.f,  -1.f ),
        Vec2< float >( -1.f, -1.f ),
        Vec2< float >( -1.f,  1.f )
    };
    std::vector< QVector4D > colors =
    {
        QVector4D( 1.f, 1.f, 1.f, .4 ),
        QVector4D( 1.f, 1.f, 1.f, .4 ),

        QVector4D(  0.f, 0.f, 0.f, .25 ),
        QVector4D(  0.f, 0.f, 0.f, .25 ),

        QVector4D(  0.f, 0.f, 0.f, .25 ),
        QVector4D(  0.f, 0.f, 0.f, .25 ),

        QVector4D( 1.f, 1.f, 1.f, .4 ),
        QVector4D( 1.f, 1.f, 1.f, .4 )
    };

    glLineWidth( lineWidth );
    renderPrimitivesColored(
        S,
        colors,
        program,
        QMatrix4x4(),
        GL_LINES );
}

void Renderer::renderPopSquare( const std::unique_ptr<QOpenGLShaderProgram> & program )
{
    renderPopSquare( program, 2.0 );
}

void Renderer::renderTimeLinePlot(
    TimeLineWidget & timeLine,
    std::unique_ptr< QOpenGLShaderProgram > & shader1,
    std::unique_ptr< QOpenGLShaderProgram > & shader2,
    const QVector4D & color )
{
//    ViewPort vp;
//    vp.setSize( timeLine.plotSize().x(), timeLine.plotSize().y() );
//    vp.setPosition( timeLine.plotPosition().x(), timeLine.plotPosition().y() );
//    vp.bkgColor( Vec4( 1, 1, 1, 1 ) );
//    clearViewPort( vp, shader1 );

    glViewport(
        timeLine.plotPosition().x(),
        timeLine.plotPosition().y(),
        timeLine.plotSize().x(),
        timeLine.plotSize().y() );   

//    glLineWidth( 1.0 );
//    renderPrimitivesFlat(
//        timeLine.grid(),
//        shader1,
//        QVector4D( .5, .5, .5, 0.28  ),
//        QMatrix4x4(),
//        GL_LINES
//    );
    glLineWidth( 1.0 );

    float w = timeLine.xRange().b() - timeLine.xRange().a();
    float h = timeLine.yRange().b() - timeLine.yRange().a();

    float cX = ( timeLine.xRange().a() + timeLine.xRange().b() ) / 2.f;
    float cY = ( timeLine.yRange().a() + timeLine.yRange().b() ) / 2.f;

    QMatrix4x4 TR;
    TR.setToIdentity();

    float scaleFactorX = 2.0 / w;
    float scaleFactorY = 2.0 / h;
    TR.scale( scaleFactorX, scaleFactorY );
    TR.translate( -cX, -cY, 0.f );

    if( timeLine.x().size() >= 2 )
    {
//            glLineWidth( 5.0 );
//            renderPrimitivesFlatSerial(
//                timeLine.x(),
//                timeLine.y(),
//                shader2,
//                QVector4D( 0.5f, 0.5f, 0.5f, 1.0f ),
//                TR,
//                GL_LINE_STRIP
//            );
//            glLineWidth( 1.0 );
    }

    glLineWidth( 3.0 );
    renderPrimitivesFlatSerial(
        std::vector< float >( { static_cast< float > ( timeLine.currentTimeStep() ), static_cast< float >( timeLine.currentTimeStep() ) } ),
        std::vector< float >( { timeLine.yRange().a(), timeLine.yRange().b() } ),
        shader2,
        color,
        TR,
        GL_LINE_STRIP
    );
    glLineWidth( 1.0 );
}

void Renderer::renderSlider( SliderWidget & slider, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 )
{
    if( slider.size().x() <= 0 )
    {
        return;
    }

    glViewport(
        slider.position().x(),
        slider.position().y(),
        slider.size().x(),
        slider.size().y() );

    std::vector< Vec2< float > > line( 2, Vec2< float >( 0, 0 ) );

    line[0].x( -1 );
    line[1].x(  1 );
    line[0].y(  0 );
    line[1].y(  0 );

    glLineWidth( 3 );
    glViewport( slider.position().x(), slider.position().y(), slider.size().x(), slider.size().y() );
    renderPrimitivesFlat( line, shader1, QVector4D( 1.0, 1.0, 1.0, 1.0 ), QMatrix4x4(), GL_LINES );

    glLineWidth( 1 );
    glViewport( slider.position().x()+1, slider.position().y()+1, slider.size().x() - 1, slider.size().y() );
    renderPrimitivesFlat( line, shader1, QVector4D( 0, 0, 0, 1.0 ), QMatrix4x4(), GL_LINES );

    float pos = slider.sliderPosition();
    glViewport( slider.position().x() + pos * slider.size().x() - 7, slider.position().y(), 14, slider.size().y() );
    clear( QVector4D( .80, .80, .80, 1.0  ), shader1);
    renderPopSquare( shader2 );
}

void Renderer::renderCombo( ComboWidget & combo, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 )
{
    glViewport( combo.position().x(), combo.position().y(), combo.size().x(), combo.size().y() );

    if( ! combo.flash() )
    {
        clear( QVector4D( .96, .96, .96, 1 ), shader1 );
    }
    else
    {
        Vec3< float > c = combo.flashColor()*.5 + Vec3< float >( 1, 1, 1 ) * .5;
        QVector4D cl1( combo.flashColor().r(), combo.flashColor().g(), combo.flashColor().b(), 1 );
        QVector4D cl2( c.r(), c.g(), c.b(), 1 );

        float r = std::abs( std::cos( combo.flashPhase() ) );
        clear( (1-r)*cl1 + r*cl2, shader1 );
    }

    renderPopSquare( shader2 );

    glViewport( combo.position().x() + combo.size().x() - 20, combo.position().y(), 20, combo.size().y() );
    clear( QVector4D( .87, .87, .87, 1 ), shader1 );
    renderPopSquare( shader2 );

    if( combo.isPressed() )
    {
        for( int i = 1; i < combo.numItems() + 1; ++i )
        {
            glViewport( combo.position().x(), combo.position().y() - ( combo.size().y() )*i, combo.size().x(), combo.size().y() );
            clear( QVector4D( 1, 1, 1, 1 ), shader1 );
            renderPopSquare( shader2 );
        }

        if( combo.hoveredItem() >= 0 )
        {
            glViewport( combo.position().x(), combo.position().y() - ( combo.size().y() )*( combo.hoveredItem()+1 ), combo.size().x(), combo.size().y() );
            clear( QVector4D( .95, .98, .98, 1 ), shader1 );
            renderPopSquare( shader2 );
        }
    }
}

void Renderer::renderResizeWidget( const ResizeWidget & w, std::unique_ptr< QOpenGLShaderProgram > & shader1 )
{
    if( w.isPressed() )
    {
        glViewport( w.position().x(), w.position().y(), w.size().x(), w.size().y() );
        clear( QVector4D( 1.0, 0.5, 0.0, .2 ), shader1 );
    }
//        else if( w.isMousedOver() )
//        {
//            glViewport( w.position().x(), w.position().y(), w.size().x(), w.size().y() );
//            clear( QVector4D( 0.0, 0.0, 0.0, .2 ), shader1 );
//        }
}

void Renderer::renderTexturedPressButton( TexturedPressButton & button, std::unique_ptr< QOpenGLShaderProgram > & texShader, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 )
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glViewport( button.position().x(), button.position().y(), button.size().x(), button.size().y() );

    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    m_vao->bind();
    m_vbo->bind();

    if ( static_cast< size_t >( m_vbo->size() ) < 24 * sizeof( float ) )
    {
        m_vbo->allocate( 24 * sizeof( float ) );
    }

    m_vbo->write( 0, quad, 12 * sizeof( float ) );
    m_vbo->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    texShader->bind();

    glActiveTexture(GL_TEXTURE0);
    button.bind( button.isPressed() );

    texShader->setUniformValue( "tex", 0 );

    GLuint posAttr = texShader->attributeLocation( "ps" );
    GLuint uvAttr  = texShader->attributeLocation( "tc" );

    texShader->enableAttributeArray( posAttr );
    texShader->enableAttributeArray( uvAttr );

    texShader->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    texShader->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    texShader->disableAttributeArray( posAttr );
    texShader->disableAttributeArray( uvAttr );

    texShader->release();

    m_vbo->release();
    m_vao->release();

    button.release( button.isPressed() );

//    if( ! button.isPressed() )
//    {
//        renderPopSquare( shader2, 1 );
//    }
//    else
//    {
//        renderPrimitivesColored(
//            button.boxShadow().verts(),
//            button.boxShadow().colors(),
//            shader2,
//            QMatrix4x4(),
//            GL_TRIANGLES
//        );
//    }
}

void Renderer::renderArrow(
    const Vec2< float > & pos,
    const Vec2< float > & size,
    const QVector4D & color,
    int direction,
    std::unique_ptr< QOpenGLShaderProgram > & shader )
{
    glViewport( pos.x(), pos.y(), size.x(), size.y() );

    std::vector< Vec2< float > > lines( 6 );

    glLineWidth( 2 );
    glPointSize( 2 );

    float R1 = .15;
    float R2 = .3;

    if( direction == LEFT )
    {
        lines[ 0 ] = { -R1,  0 };
        lines[ 1 ] = {  R1,  R2 };

        lines[ 2 ] = { -R1,  0 };
        lines[ 3 ] = {  R1, -R2 };

        lines[ 4 ] = {  R1,  R2 };
        lines[ 5 ] = {  R1, -R2 };
    }
    else if( direction == RIGHT )
    {
        lines[ 0 ] = {  R1, 0 };
        lines[ 1 ] = { -R1, R2 };

        lines[ 2 ] = {  R1,  0 };
        lines[ 3 ] = { -R1, -R2 };

        lines[ 4 ] = { -R1,  R2 };
        lines[ 5 ] = { -R1, -R2 };
    }
    else if( direction == UP )
    {
        lines[ 0 ] = {    0,  R1 };
        lines[ 1 ] = {  -R2, -R1 };

        lines[ 2 ] = {   0,  R1 };
        lines[ 3 ] = {  R2, -R1 };

        lines[ 4 ] = { -R2, -R1 };
        lines[ 5 ] = {  R2, -R1 };
    }
    else
    {
        lines[ 0 ] = {   0, -R1 };
        lines[ 1 ] = { -R2,  R1 };

        lines[ 2 ] = {   0, -R1 };
        lines[ 3 ] = {  R2,  R1 };

        lines[ 4 ] = { -R2,  R1 };
        lines[ 5 ] = {  R2,  R1 };
    }

    renderPrimitivesFlat(
        lines,
        shader,
        QVector4D( 1, 1, 1, 1 ),
        QMatrix4x4(),
        GL_LINES );

    renderPrimitivesFlat(
        lines,
        shader,
        QVector4D( 1, 1, 1, 1 ),
        QMatrix4x4(),
        GL_POINTS );

    for( auto & p : lines )
    {
        p = p * .5;
    }

    renderPrimitivesFlat(
        lines,
        shader,
        QVector4D( 1, 1, 1, 1 ),
        QMatrix4x4(),
        GL_LINES );

    renderPrimitivesFlat(
        lines,
        shader,
        QVector4D( 1, 1, 1, 1 ),
        QMatrix4x4(),
        GL_POINTS );

    ///

    glViewport( pos.x()-1, pos.y()+1, size.x(), size.y() );

    if( direction == LEFT )
    {
        lines[ 0 ] = { -R1,  0 };
        lines[ 1 ] = {  R1,  R2 };

        lines[ 2 ] = { -R1,  0 };
        lines[ 3 ] = {  R1, -R2 };

        lines[ 4 ] = {  R1,  R2 };
        lines[ 5 ] = {  R1, -R2 };
    }
    else if( direction == RIGHT )
    {
        lines[ 0 ] = {  R1, 0 };
        lines[ 1 ] = { -R1, R2 };

        lines[ 2 ] = {  R1,  0 };
        lines[ 3 ] = { -R1, -R2 };

        lines[ 4 ] = { -R1,  R2 };
        lines[ 5 ] = { -R1, -R2 };
    }
    else if( direction == UP )
    {
        lines[ 0 ] = {    0,  R1 };
        lines[ 1 ] = {  -R2, -R1 };

        lines[ 2 ] = {   0,  R1 };
        lines[ 3 ] = {  R2, -R1 };

        lines[ 4 ] = { -R2, -R1 };
        lines[ 5 ] = {  R2, -R1 };
    }
    else
    {
        lines[ 0 ] = {   0, -R1 };
        lines[ 1 ] = { -R2,  R1 };

        lines[ 2 ] = {   0, -R1 };
        lines[ 3 ] = {  R2,  R1 };

        lines[ 4 ] = { -R2,  R1 };
        lines[ 5 ] = {  R2,  R1 };
    }


    renderPrimitivesFlat(
        lines,
        shader,
        color,
        QMatrix4x4(),
        GL_LINES );

    renderPrimitivesFlat(
        lines,
        shader,
        color,
        QMatrix4x4(),
        GL_POINTS );

    for( auto & p : lines )
    {
        p = p * .5;
    }

    renderPrimitivesFlat(
        lines,
        shader,
        color,
        QMatrix4x4(),
        GL_LINES );

    renderPrimitivesFlat(
        lines,
        shader,
        color,
        QMatrix4x4(),
        GL_POINTS );

    //
}

void Renderer::renderScrollBar( ScrollBar & scrollBar, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 )
{
    glViewport( scrollBar.position().x(), scrollBar.position().y(), scrollBar.size().x(), scrollBar.size().y() );
    clear( QVector4D( 0.8, 0.8, 0.8, 1 ), shader1 );

    renderPrimitivesColored(
        scrollBar.boxShadow().verts(),
        scrollBar.boxShadow().colors(),
        shader2,
        QMatrix4x4(),
        GL_TRIANGLES
    );

    QVector4D arrowColor( .55, .55, .55, 1 );

    if( scrollBar.direction == ScrollBar::HORIZONTEL )
    {
        // left arrow
        glViewport( scrollBar.position().x(), scrollBar.position().y() + 1, scrollBar.size().y(), scrollBar.size().y() - 2 );
        clear( QVector4D( .87, .87, .87, 1 ), shader1 );
        renderPopSquare( shader2 );

        renderArrow(
            { scrollBar.position().x(), scrollBar.position().y() + 1 },
            { scrollBar.size().y(), scrollBar.size().y() - 2 },
            arrowColor,
            LEFT,
            shader1 );

        // right arrow
        glViewport( scrollBar.position().x() + scrollBar.size().x() - scrollBar.size().y(), scrollBar.position().y() + 1, scrollBar.size().y(), scrollBar.size().y() - 2 );
        clear( QVector4D( .87, .87, .87, 1 ), shader1 );
        renderPopSquare( shader2 );

        renderArrow(
            { scrollBar.position().x() + scrollBar.size().x() - scrollBar.size().y(), scrollBar.position().y() + 1 },
            { scrollBar.size().y(), scrollBar.size().y() - 2 },
            arrowColor,
            RIGHT,
            shader1 );

        //handle
        glViewport( scrollBar.handlePosition().x(), scrollBar.handlePosition().y() + 1, scrollBar.handleSize().x(), scrollBar.handleSize().y() - 1 );
        clear( QVector4D( .87, .87, .87, 1 ), shader1 );
        renderPopSquare( shader2 );
    }
    else
    {
        // lower arrow
        glViewport( scrollBar.position().x() + 1, scrollBar.position().y(), scrollBar.size().x() - 2, scrollBar.size().x() );
        clear( QVector4D( .87, .87, .87, 1 ), shader1 );
        renderPopSquare( shader2 );

        renderArrow(
            { scrollBar.position().x() + 1, scrollBar.position().y() },
            { scrollBar.size().x() - 2, scrollBar.size().x() },
            arrowColor,
            DOWN,
            shader1 );

        // upper arrow
        glViewport( scrollBar.position().x() + 1, scrollBar.position().y() + scrollBar.size().y() - scrollBar.size().x(), scrollBar.size().x() - 2, scrollBar.size().x() );
        clear( QVector4D( .87, .87, .87, 1 ), shader1 );
        renderPopSquare( shader2 );

        renderArrow(
            { scrollBar.position().x() + 1, scrollBar.position().y() + scrollBar.size().y() - scrollBar.size().x() },
            { scrollBar.size().x() - 2, scrollBar.size().x() },
            arrowColor,
            UP,
            shader1 );

        //handle
        glViewport( scrollBar.handlePosition().x() + 1, scrollBar.handlePosition().y(), scrollBar.handleSize().x() - 1, scrollBar.handleSize().y() );
        clear( QVector4D( .87, .87, .87, 1 ), shader1 );
        renderPopSquare( shader2 );
    }
}

void Renderer::renderPressButton( PressButton & button, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 )
{
    glViewport( button.position().x(), button.position().y(), button.size().x(), button.size().y() );

    if( button.isPressed() )
    {
        clear( QVector4D( 0.8, 0.8, 0.8, 1 ), shader1 );
        renderPrimitivesColored(
            button.boxShadow().verts(),
            button.boxShadow().colors(),
            shader2,
            QMatrix4x4(),
            GL_TRIANGLES
        );
    }
    else
    {
        if( button.flash() )
        {
            Vec3< float > c = button.flashColor()*.4 + Vec3< float >( 1, 1, 1 ) * .6;
            QVector4D cl1( button.flashColor().r(), button.flashColor().g(), button.flashColor().b(), 1 );
            QVector4D cl2( c.r(), c.g(), c.b(), 1 );

            float r = std::abs( std::cos( button.flashPhase() ) );
            clear( (1-r)*cl1 + r*cl2, shader1 );
        }
        else
        {
            clear( QVector4D( .87, .87, .87, 1 ), shader1 );
        }
        renderPopSquare( shader2 );
    }
}

void Renderer::renderLinkButton( PressButton & button, std::unique_ptr< QOpenGLShaderProgram > & shader1, std::unique_ptr< QOpenGLShaderProgram > & shader2 )
{
    renderPressButton( button, shader1, shader2 );

    const int MARGIN = 4;
    const int DIM = button.size().x() - MARGIN*2;

    QVector4D cl = button.isPressed() ? QVector4D( .3, .3, .3, 1.0 ) : QVector4D( 0.3, 0.3, 0.3, 1.0 );

    glViewport( button.position().x() + MARGIN, button.position().y() + MARGIN, DIM, DIM );
    clear( cl, shader1 );

    glViewport( button.position().x() + MARGIN, button.position().y() + button.size().y() - DIM - MARGIN, DIM, DIM );
    clear( cl, shader1 );

    glLineWidth( 2.0 );

    if( button.isPressed() )
    {
        glViewport( button.position().x() + MARGIN, button.position().y() + MARGIN, DIM, button.size().y() - 2*MARGIN );
        renderPrimitivesFlat( std::vector< Vec2< float > >( { Vec2< float >( 0, 1 ), Vec2< float >( 0, -1 ) } ), shader1, cl, QMatrix4x4(), GL_LINES );
    }
    else
    {
        glViewport( button.position().x() + MARGIN, button.position().y() + MARGIN, DIM, button.size().y() - 2*MARGIN );
        renderPrimitivesFlat( std::vector< Vec2< float > >( { Vec2< float >( 0, 1 ), Vec2< float >( 0, .2 ) } ), shader1, cl, QMatrix4x4(), GL_LINES );
        renderPrimitivesFlat( std::vector< Vec2< float > >( { Vec2< float >( 0, -.2 ), Vec2< float >( 0, -1 ) } ), shader1, cl, QMatrix4x4(), GL_LINES );
    }
}

//void Renderer::renderAORenderObject( AORenderObject & obj )
//{
//    GLuint loc;
//    QMatrix4x4 proj = obj.m_camera.matProj();

//    QMatrix4x4 M;
//    M.setToIdentity();

//    QMatrix4x4 MV = obj.m_camera.matView( 1.0 ) * M;
//    QMatrix3x3 normalMtx = MV.normalMatrix();

//    glDisable (GL_BLEND);
//    glDisable( GL_MULTISAMPLE );

//    glBindFramebuffer(GL_FRAMEBUFFER, obj.gBuffer);

//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//    obj.m_aoGeometryProgram->bind();

//    loc = obj.m_aoGeometryProgram->uniformLocation( "prj" );
//    obj.m_aoGeometryProgram->setUniformValue( loc, proj );

//    loc = obj.m_aoGeometryProgram->uniformLocation( "mvm" );
//    obj.m_aoGeometryProgram->setUniformValue( loc, MV );

//    loc = obj.m_aoGeometryProgram->uniformLocation( "normmtx" );
//    obj.m_aoGeometryProgram->setUniformValue( loc, normalMtx );

//    GLuint xAttr = obj.m_aoGeometryProgram->attributeLocation( "x" );
//    GLuint yAttr = obj.m_aoGeometryProgram->attributeLocation( "y" );
//    GLuint zAttr = obj.m_aoGeometryProgram->attributeLocation( "z" );

//    obj.m_vao->bind();
//    obj.m_vbo->bind();
//    obj.m_ibo->bind();

//    obj.m_aoGeometryProgram->enableAttributeArray( xAttr );
//    obj.m_aoGeometryProgram->enableAttributeArray( yAttr );
//    obj.m_aoGeometryProgram->enableAttributeArray( zAttr );

//    obj.m_aoGeometryProgram->setAttributeBuffer( xAttr,  GL_FLOAT, 0, 1 );
//    obj.m_aoGeometryProgram->setAttributeBuffer( yAttr,  GL_FLOAT, x.size() * sizeof( float ), 1 );
//    obj.m_aoGeometryProgram->setAttributeBuffer( zAttr,  GL_FLOAT, x.size() * sizeof( float )*2, 1 );

//    //glMultiDrawArrays( GL_LINE_STRIP_ADJACENCY, lineBundle.lineOffsets.data(), lineBundle.counts.data(), lineBundle.counts.size() );

//    obj.m_aoGeometryProgram->disableAttributeArray( zAttr );
//    obj.m_aoGeometryProgram->disableAttributeArray( yAttr );
//    obj.m_aoGeometryProgram->disableAttributeArray( xAttr );

//    obj.m_aoGeometryProgram->release();

//    glBindFramebuffer(GL_FRAMEBUFFER, obj.ssaoFBO );

//    glClear(GL_COLOR_BUFFER_BIT);

//    obj.m_aoShaderProgram->bind();

//    loc = obj.m_aoShaderProgram->uniformLocation( "gPosition" );
//    obj.m_aoShaderProgram->setUniformValue( loc, 0 );

//    loc = obj.m_aoShaderProgram->uniformLocation( "gNormal" );
//    obj.m_aoShaderProgram->setUniformValue( loc, 1 );

//    loc = obj.m_aoShaderProgram->uniformLocation( "texNoise" );
//    obj.m_aoShaderProgram->setUniformValue( loc, 2 );

//    // Send kernel + rotation
//    for (unsigned int i = 0; i < obj.aoKernelSize; ++i)
//    {
//        loc = obj.m_aoShaderProgram->uniformLocation( "samples[" + QString::number(i) + "]" );
//        obj.m_aoShaderProgram->setUniformValue( loc, obj.ssaoKernel[i] );
//    }

//    loc = obj.m_aoShaderProgram->uniformLocation( "projection" );
//    obj.m_aoShaderProgram->setUniformValue( loc, proj );

//    loc = obj.m_aoShaderProgram->uniformLocation( "width" );
//    obj.m_aoShaderProgram->setUniformValue( loc, obj.xSize );

//    loc = obj.m_aoShaderProgram->uniformLocation( "height" );
//    obj.m_aoShaderProgram->setUniformValue( loc, obj.ySize );

//    loc = obj.m_aoShaderProgram->uniformLocation( "occlusionRadius" );
//    obj.m_aoShaderProgram->setUniformValue( loc, obj.m_occlusionRadius );

//    loc = obj.m_aoShaderProgram->uniformLocation( "bias" );
//    obj.m_aoShaderProgram->setUniformValue( loc, obj.m_occlusionBias );

//    loc = obj.m_aoShaderProgram->uniformLocation( "noise" );
//    obj.m_aoShaderProgram->setUniformValue( loc, obj.m_occlusionNoiseScale );

//    loc = obj.m_aoShaderProgram->uniformLocation( "kernelSize" );
//    obj.m_aoShaderProgram->setUniformValue( loc, obj.aoKernelSize );

//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, obj.gPosition);

//    glActiveTexture(GL_TEXTURE1);
//    glBindTexture(GL_TEXTURE_2D, obj.gNormal);

//    glActiveTexture(GL_TEXTURE2);
//    glBindTexture(GL_TEXTURE_2D, obj.noiseTexture);

//    renderQuad();

//    obj.m_aoShaderProgram->release();

//    // Generate Blur Buffer

//    glBindFramebuffer(GL_FRAMEBUFFER, obj.ssaoBlurFBO);

//    glClear(GL_COLOR_BUFFER_BIT);

//    obj.m_aoBlurProgram->bind();

//    loc = obj.m_aoBlurProgram->uniformLocation( "ssaoInput" );
//    obj.m_aoBlurProgram->setUniformValue( loc, 0 );

//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, obj.ssaoColorBuffer);

//    renderQuad();

//    //checkError( "render quad 2" );

//    obj.m_aoBlurProgram->release();

//    //checkError( "blur stage done" );

//    glBindFramebuffer(GL_FRAMEBUFFER, 0);

//    // Render lighting

//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//    obj.m_aoLightingProgram->bind();

//    glDisable (GL_BLEND);
//    glDisable( GL_MULTISAMPLE );

//    loc = obj.m_aoLightingProgram->uniformLocation( "gPosition" );
//    obj.m_aoLightingProgram->setUniformValue( loc, 0 );

//    loc = obj.m_aoLightingProgram->uniformLocation( "gNormal" );
//    obj.m_aoLightingProgram->setUniformValue( loc, 1 );

//    loc = obj.m_aoLightingProgram->uniformLocation( "gAlbedo" );
//    obj.m_aoLightingProgram->setUniformValue( loc, 2 );

//    loc = obj.m_aoLightingProgram->uniformLocation( "ssao" );
//    obj.m_aoLightingProgram->setUniformValue( loc, 3 );

//    loc = obj.m_aoLightingProgram->uniformLocation( (QString( "light.direction" ) ) );
//    obj.m_aoLightingProgram->setUniformValue( loc, light.direction  );

//    loc = obj.m_aoLightingProgram->uniformLocation( "material.Ka" );
//    obj.m_aoLightingProgram->setUniformValue( loc, m_material.Ka_scale );

//    loc = obj.m_aoLightingProgram->uniformLocation( "material.Kd" );
//    obj.m_aoLightingProgram->setUniformValue( loc, m_material.Kd_scale );

//    loc = obj.m_aoLightingProgram->uniformLocation( "material.Ks" );
//    obj.m_aoLightingProgram->setUniformValue( loc, m_material.Ks_scale );

//    loc = obj.m_aoLightingProgram->uniformLocation( "material.alpha" );
//    obj.m_aoLightingProgram->setUniformValue( loc, m_material.shininess );

//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, obj.gPosition);

//    glActiveTexture(GL_TEXTURE1);
//    glBindTexture(GL_TEXTURE_2D, obj.gNormal);

//    glActiveTexture(GL_TEXTURE2);
//    glBindTexture(GL_TEXTURE_2D, obj.gAlbedo);

//    glActiveTexture(GL_TEXTURE3); // add extra SSAO texture to lighting pass
//    glBindTexture(GL_TEXTURE_2D, obj.ssaoColorBufferBlur);

//    glEnable (GL_BLEND);
//    glEnable( GL_MULTISAMPLE );

//    renderQuad();

//    obj.m_aoLightingProgram->release();

//    glEnable (GL_BLEND);
//    glEnable( GL_MULTISAMPLE );
//}

}
