
#ifndef TN_DISTRIBUTION_GRID_MANAGER
#define TN_DISTRIBUTION_GRID_MANAGER

#include "Types/Vec.hpp"
#include "Data/Importers/DistributionDataImporter.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"

#include <atomic>
#include <fstream>
#include <memory>
#include <cstdint>
#include <vector>
#include <iostream>
#include <map>
#include <set>

namespace TN {

class DistGridTimeChunkMemoryState
{
    Vec2< uint64_t > integrationSequence;
};

template< typename TargetFloatType >
class DistributionGridManager
{
    /** The object which manages the retrieval of data from files on the disk
     *  The pointer is non-owning */
    DistributionDataImporter< TargetFloatType > *m_dataImporter;

    /** The main data structure through which the Distribution data is stored/accessed.
       The first dimension corresponds to the different types of particles available.
       The second dimension corresoponds to the different types of Distributions which are available.
       The third dimensions corresponds to the available time steps.
       The unique_ptr< DistributionGrid > will be equal to nullptr unless loaded in memory */
    std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > m_distributions;

    //! For example, the spatial positions of each grid point, the volumes associated with their cells, etc.
    std::map< std::string, std::vector< TargetFloatType > > m_perGridPointAttributes;

    //!
    std::map< std::string, Vec2< double > > m_perGridPointAttributeRanges;

    //! The keys into the per grid point attributes which give you the spatial attributes of the grid points
    std::vector< std::string > m_gridPointSpatialAttributes;

    //! In case each distribution has different parameters
    std::map< std::string, std::map< std::string, PredefinedHistogram > > m_perParticleTypePerDistributionTypeParameters;

    //! Constants associated with the particles or individuals of a population
    std::map< std::string, std::map< std::string, double > > m_perParticleTypeConstants;

    //! The set of particle types available
    std::set< std::string > m_particleTypes;

    //! The set of Distribution types available by particle
    std::map< std::string, std::set< std::string > > m_perParticleTypeDistributionTypes;

    //! The available simulation time steps
    std::vector< std::int32_t > m_simulationTimeSteps;

    /** The physical times of the simulation time steps */
    std::vector< double > m_realTimeValues;

    /** Used to indicate the state of a time chunk of data in order to determine what needs to be done when managing memory */
    std::vector< DistGridTimeChunkMemoryState > m_distTimeChunkMemoryState;

    /** The first time step in memory */
    TimeSeriesDefinition m_currentTimeSeries;
    TimeSeriesDefinition m_previousTimeSeries;

    TimeSeriesDefinition m_fullTimeSeries;

    //! The number of time steps available
    std::int32_t m_numTimeSteps;

    std::map< std::string, VisualContextModel< TargetFloatType > > m_visualContextModels;

    //! release memory for a combination of time steps, particle types and distribution types
    void release( 
        const std::vector< std::int32_t > & iSteps, const std::map< std::string, std::set< std::string > > & perParticleTypeDistributionTypes )
    {
        for( auto i : iSteps )
        {
            for( auto partType : perParticleTypeDistributionTypes )
            {
                for( auto distType : partType.second )
                {
                    //qDebug()() << "released " << i  << " " << partType.first.c_str() << " " << distType.c_str();
                    m_distributions.find( partType.first )->second.find( distType )->second[ i ].reset( nullptr );
                }
            }
        }
    }

    void release( const std::string & ptype, const std::set< std::string > & dtypes )
    {
        std::vector< std::int32_t > iSteps( m_numTimeSteps );
        for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
        {
            for( auto & dist : m_distributions.at( ptype ) )
            {
                if( dtypes.count( dist.first ) )
                {
                    dist.second[ i ].reset( nullptr );
                }
            }
        }
    }

    void release( const std::string & ptype )
    {
        std::vector< std::int32_t > iSteps( m_numTimeSteps );
        for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
        {
            for( auto & dist : m_distributions.at( ptype ) )
            {
                dist.second[ i ].reset( nullptr );
            }
        }
    }

    //! release all memory held by the manager
    void release( const std::vector< std::int32_t > & iSteps )
    {
        release( iSteps, m_perParticleTypeDistributionTypes );
    }

public:

    DistributionGridManager() {}
    ~DistributionGridManager() {}

    /**
     * @brief release
     */
    void release()
    {
        std::vector< std::int32_t > iSteps( m_numTimeSteps );
        for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
        {
            iSteps[ i ] = i;
        }
        release( iSteps );
    }

    void clear()
    {
        release();
    }

    /**
     * @brief reset
     * @param dataSetMetaData
     * @param dataImporter
     */
    void reset( const BasicDataInfo & basicDataInfo, DistributionDataImporter< TargetFloatType > * dataImporter )
    {

        m_dataImporter = dataImporter;
        m_dataImporter->init( basicDataInfo );

        //qDebug()() << "init data importer";

        // get non-time varying parameters
        m_dataImporter->loadTemporallyStaticAttributes(
            m_distributions,
            m_perGridPointAttributes,
            m_perGridPointAttributeRanges,
            m_gridPointSpatialAttributes,
            m_perParticleTypePerDistributionTypeParameters,
            m_perParticleTypeConstants,
            m_particleTypes,
            m_perParticleTypeDistributionTypes,
            m_simulationTimeSteps,
            m_realTimeValues,
            m_visualContextModels,
            m_numTimeSteps
        );

        release();

        m_distTimeChunkMemoryState.resize( m_numTimeSteps );
        m_distTimeChunkMemoryState.shrink_to_fit();

        m_fullTimeSeries.description = "All";
        m_fullTimeSeries.firstIdx = 0;
        m_fullTimeSeries.lastIdx = m_numTimeSteps - 1;
        m_fullTimeSeries.idxStride = 1;
        m_fullTimeSeries.integrate = false;

        m_currentTimeSeries = m_fullTimeSeries;

        //qDebug()() << "done with reset";
    }

    void load( 
        const std::map< std::string, std::set< std::string > > & toLoad, std::atomic< int > & progress )
    {
//        // release data for time steps we don't need anymore
        if( m_currentTimeSeries.integrate == false && m_previousTimeSeries.integrate == false )
        {
            // release data for other particle types, and distrobution types we don't need
            for( auto partType : m_perParticleTypeDistributionTypes )
            {
                if( ! toLoad.count( partType.first ) )
                {
                    release( partType.first );
                }
                else
                {
                    std::set< std::string > releaseDists;
                    for( auto distType : partType.second )
                    {
                        if( ! toLoad.at( partType.first ).count( distType ) )
                        {
                            releaseDists.insert( distType );
                        }
                    }
                    release( partType.first, releaseDists );
                }
            }

            std::vector< std::int32_t > releaseSteps;
            releaseSteps.reserve( m_numTimeSteps );
            for( std::int32_t i = 0; i < m_numTimeSteps; ++i )
            {
                // if it's not something we will want to load
                if( ( i < m_currentTimeSeries.firstIdx ) // out of range
                 || ( i > m_currentTimeSeries.lastIdx )  // out of range
                 || ( ( ( i - m_currentTimeSeries.firstIdx ) % m_currentTimeSeries.idxStride ) != 0 ) ) // not on desired interval
                {
                    releaseSteps.push_back( i );
                }
            }
            release( releaseSteps );
        }
//         some sophisticated methods for minimizing disk access can be included here in the future
//         for now just release everything and start from scratch
        else
        {
            release();
            //qDebug()() << "released all";
        }

        const int NSTEPS = m_currentTimeSeries.numSteps();

        if( ! m_currentTimeSeries.integrate )
        {
            for( std::int32_t i = m_currentTimeSeries.firstIdx, c=0; i <= m_currentTimeSeries.lastIdx; i += m_currentTimeSeries.idxStride )
            {
                std::map< std::string, std::set< std::string > > needToLoad;
                bool everythingAleadyInMemory = true;

                for( auto pE : toLoad )
                {
                    for( auto dE : pE.second )
                    {
                        if( m_distributions.find( pE.first )->second.find( dE )->second[ i ] == nullptr )
                        {
                            if( needToLoad.find( pE.first ) == needToLoad.end() )
                            {
                                needToLoad.insert( std::pair< std::string, std::set< std::string > >( pE.first, std::set< std::string >( ) ) );
                            }
                            needToLoad.find( pE.first )->second.insert( dE );
                            everythingAleadyInMemory = false;
                        }
                    }
                }
                if( ! everythingAleadyInMemory )
                {
                     m_dataImporter->loadTimeStep(
                         i,
                         needToLoad,
                         m_distributions
                     );
                }
                ++c;
                progress = ( 100 * c ) / NSTEPS;
            }
        }
        else
        {
//            //qDebug()() << "integrating " << m_currentTimeSeries.firstIdx << " " << m_currentTimeSeries.lastIdx <<  " " << m_currentTimeSeries.idxStride;

            for( std::int32_t i = m_currentTimeSeries.firstIdx, c = 0; i <= m_currentTimeSeries.lastIdx; i += m_currentTimeSeries.idxStride )
            {
                for( int j = i, end = i + m_currentTimeSeries.idxStride; j < end && j <= m_currentTimeSeries.lastIdx; ++j )
                {
                    std::map< std::string, std::set< std::string > > needToLoad;
                    bool everythingAleadyInMemory = true;

                    for( auto pE : toLoad )
                    {
                        for( auto dE : pE.second )
                        {
                                if( m_distributions.find( pE.first )->second.find( dE )->second[ j ] == nullptr )
                                {
                                    if( needToLoad.find( pE.first ) == needToLoad.end() )
                                    {
                                        needToLoad.insert( std::pair< std::string, std::set< std::string > >( pE.first, std::set< std::string >( ) ) );
                                    }
                                    needToLoad.find( pE.first )->second.insert( dE );
                                    everythingAleadyInMemory = false;
                                }
                            }
                    }
                    if( ! everythingAleadyInMemory )
                    {
                         m_dataImporter->loadTimeStep(
                             j,
                             needToLoad,
                             m_distributions
                         );
                    }

                    if( i != j )
                    {
                        for( auto pE : toLoad )
                        {
                            for( auto dE : pE.second )
                            {
                                std::vector< TargetFloatType > & integrated = *( m_distributions.find( pE.first )->second.find( dE )->second[ i ] );
                                std::vector< TargetFloatType > & temporary  = *( m_distributions.find( pE.first )->second.find( dE )->second[ j ] );
                                for( size_t k = 0, endK = integrated.size(); k < endK; ++k )
                                {
                                     integrated[ k ] += temporary[ k ];
                                     if( j == end - 1 )
                                     {
                                         integrated[ k ] /= double( m_currentTimeSeries.idxStride );
                                     }
                                }
                                m_distributions.find( pE.first )->second.find( dE )->second[ j ].reset( nullptr );
                            }
                        }
                    }
                }
                ++c;
                progress = ( 100 * c ) / NSTEPS;
            }
        }
    }   

    void load( const std::string & ptype, const std::set< std::string > & pdfs, const TimeSeriesDefinition & timeSeries,
               std::atomic< int > & progress, std::atomic< bool > & status )
    {
        m_previousTimeSeries = m_currentTimeSeries;
        m_currentTimeSeries = timeSeries;
        std::map< std::string, std::set< std::string > > toLoad( { { ptype, pdfs } } );
        load( toLoad, progress );
        status = true;
    }

    void sparsify( std::string ptype )
    {
        qDebug() << ptype.c_str() << "\n";

        // upper:0.016734   lower:-0.0177801   width:0.0345141
        const float EPSILON = 0.0345141f / 256.f; //= 0.0001348207

        for( auto & dist : m_distributions.at( ptype ) )
        {
            qDebug() << dist.first.c_str();

            const PredefinedHistogram & histDef = m_perParticleTypePerDistributionTypeParameters.at( ptype ).at( dist.first );
            I2 resolution( histDef.resolution[ 0 ], histDef.resolution[ 1 ] );

            std::atomic< int > progress;
            std::atomic< bool > status;

            load( ptype, { dist.first }, m_fullTimeSeries, progress, status );

            const size_t BINS_PER_HIST = resolution.a() * resolution.b();
            const size_t N_GRIDPOINTS = m_perGridPointAttributes.at( "volume" ).size();

            const std::vector< std::unique_ptr< std::vector< TargetFloatType > > > & values
                    = m_distributions.find( ptype )->second.find( dist.first )->second;

            qDebug() << N_GRIDPOINTS << " " << BINS_PER_HIST << " " << m_numTimeSteps;

            size_t completeTotal = 0;
            float upper = 0.f, lower = 99999999999.f;
            for( size_t gpIndex = 0; gpIndex < N_GRIDPOINTS; ++ gpIndex )
            {
                size_t totalCount = 0;
                for( size_t binIndex = 0; binIndex < BINS_PER_HIST; ++binIndex )
                {
                    const int IDX = gpIndex * BINS_PER_HIST + binIndex;
                    float currentValue = ( * values[ 0 ] )[ IDX ];
                    size_t count = 0;
                    for( size_t i = 1; i < m_numTimeSteps; ++i )
                    {
                        upper = std::max( upper, ( * values[ i ] )[ IDX ] );
                        lower = std::min( lower, ( * values[ i ] )[ IDX ] );

                        if( std::abs( ( * values[ i ] )[ IDX ] - currentValue ) > EPSILON )
                        {
                            currentValue = ( * values[ i ] )[ IDX ];
                            ++count;
                        }
                    }
                    totalCount += count;
                }
                qDebug() << "GP total " << totalCount << "/" << BINS_PER_HIST * m_numTimeSteps;
                completeTotal += totalCount;
            }
            qDebug() << "complete total " << completeTotal << "/" << BINS_PER_HIST * size_t( m_numTimeSteps ) * N_GRIDPOINTS;
            qDebug() << upper << " " << lower << " " << upper - lower;
            break;
        }
    }

    /**
     * @brief availableData
     * @return
     */
    const std::map< std::string, std::set< std::string > > & availableData() const
    {
        return m_perParticleTypeDistributionTypes;
    }

    /**
     * @brief VisualContextModel
     * @return
     */
    const std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels() const { return m_visualContextModels; }

    /**
     * @brief perParticleTypePerDistributionTypeParameters
     * @return
     */
    const std::map< std::string, std::map< std::string, PredefinedHistogram > > & perParticleTypePerDistributionTypeParameters() const {  return m_perParticleTypePerDistributionTypeParameters; }

    /**
     * @brief values
     * @return
     */
    const std::vector< TargetFloatType > & values( const std::string & attribute ) const
    {
        return m_perGridPointAttributes.find(  attribute )->second;
    }

    const std::map< std::string, std::vector< TargetFloatType > > & perGridPointAttributes() const
    {
        return m_perGridPointAttributes;
    }

    const std::set< std::string > attributeKeys() const
    {
        std::set< std::string > keys;
        for( auto & p : m_perGridPointAttributeRanges )
        {
            keys.insert( p.first );
        }
        return keys;
    }


    const std::map< std::string, Vec2< double > > & gridAttributeRanges() const
    {
        return m_perGridPointAttributeRanges;
    }

    /**
     * @brief ranges
     * @param attribute
     * @return
     */
    Vec2< double > range( const std::string & attribute ) const { return m_perGridPointAttributeRanges.find( attribute )->second; }

    /**
     * @brief distributions
     * @param ptype
     * @param dtype
     * @param timeStep
     * @return
     */
    const std::unique_ptr< std::vector< TargetFloatType > > & distributions(
        const std::string & ptype,
        const std::string & dtype,
        std::int32_t timeStep ) const
    {
        return m_distributions.find( ptype )->second.find( dtype )->second[ timeStep ];
    }

    /**
     * @brief numTimeSteps
     * @return
     */
    std::int32_t numTimeSteps() const { return m_numTimeSteps; }

    std::int32_t numLoadedTimeSteps() const {
        return m_currentTimeSeries.numSteps();
    }

    void setTimeSeries( const TimeSeriesDefinition & timeSeries )
    {
        m_currentTimeSeries = timeSeries;
    }

    /**
     * @brief currentTimeSeriesInMemory
     * @return
     */
    TimeSeriesDefinition currentTimeSeries() const
    {
        return m_currentTimeSeries;
    }

    TimeSeriesDefinition fullTimeSeries() const
    {
        return m_fullTimeSeries;
    }

    /**
     * @brief realTime
     * @return
     */
    const std::vector< double > & realTime() const { return m_realTimeValues; }

    /**
     * @brief simTime
     * @return
     */
    const std::vector< std::int32_t > & simTime() const { return m_simulationTimeSteps; }

    std::set< std::string > particleTypes() const
    {
        return m_particleTypes;
    }
};

}

#endif
