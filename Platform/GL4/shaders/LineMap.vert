#version 400



layout(location = 0) in float xAttr;
layout(location = 1) in float yAttr;

out vData
{
    flat int select;
    flat int isReal;
} vertex;

uniform mat4 M;

void main(void)
{
    vertex.select = 1;
    vertex.isReal = 1;
    gl_Position = vec4( xAttr, yAttr, 0.0, 1.0 );
}
