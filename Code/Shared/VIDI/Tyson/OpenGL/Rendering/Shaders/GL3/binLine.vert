#version 130

uniform mat4 MVP;
in float xAttr;
in float yAttr;
in float dAttr;

out float d;

void main(void)
{
    d = dAttr;
    gl_Position = MVP*vec4( xAttr, yAttr, 0, 1.0 );
}
