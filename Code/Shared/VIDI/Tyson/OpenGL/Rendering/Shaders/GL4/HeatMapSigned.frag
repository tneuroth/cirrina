#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform sampler1D tf;

uniform float mx;
uniform float mn;

uniform bool fade;

void main(){

    // Take the central sample first...
    float count = texture( tex, uv.st ).r / mx;
    if( count < 0 )
    {
        color = vec4( 1, 1, 0 , 0.5 );
    }
    else
    {
        color = vec4( 0, 0, 1 , 0.5 );
    }


    //    if( count <= 0 )
//    {
//        color = vec4( 0, 0, 0, 0 );
//    }
//    else{
//        if( fade )
//        {
//            color = vec4( count, count, count, .4 );
//        }
//        else
//        {
//            color = vec4( count, count, count, 1.0 );
//        }
//    }
}
