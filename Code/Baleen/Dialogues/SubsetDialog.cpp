
#include "Types/Vec.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/SubsetDefinition.hpp"

#include "Expressions/Dialogs/BaseDataDialogue.hpp"
#include "Dialogues/SubsetDialog.hpp"

#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Expressions/Calculator/expressionsymbols.hpp"
#include "Expressions/Calculator/Expression.hpp"

#include "Dialogues/DialogHelpers.hpp"

#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QDebug>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>
#include <QHeaderView>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN {

    SubsetDialog::~SubsetDialog() {}

    void SubsetDialog::finalize()
    {
        if( seriesValid == false )
        {
            errorMessageLabel->setText( "Must Select a Filtering Window" );
            errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );

            return;
        }

        std::string name = filterNameingLineEdit->text().toStdString();
        if( name.size() == 0 )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );

            return;
        }
        if( ( m_status != "defined" ) && ( subsetDefinitions.find( name ) != subsetDefinitions.end() ) )
        {
            errorMessageLabel->setText( "Name/key already in use" );
            errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );
            return;
        }
        if( name == "temp" )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );
            return;
        }

        m_key = name;

        TimeSeriesDefinition timeSeriesDef;
        timeSeriesDef.name = "filter window";
        timeSeriesDef.firstIdx = seriesTable->item( 2, 0 )->text().toInt();
        timeSeriesDef.lastIdx = seriesTable->item( 2, 1 )->text().toInt();
        timeSeriesDef.idxStride = seriesTable->item( 2, 2 )->text().toInt();
        timeSeriesDef.integrate = false;

        SubsetDefinition definition;
        definition.name = m_key;
        definition.description = filterDescriptionBox->toPlainText().toStdString();
        definition.expression = filterExpressionEdit->text().toStdString();
        definition.window = timeSeriesDef;
        definition.window.description = seriesDescriptionBox->toPlainText().toStdString();

        if( m_status == "undefined" )
        {
            subsetDefinitions.insert( { m_key, definition } );
        }
        else
        {
            subsetDefinitions.find( m_key )->second = definition;
        }

        setStatus( "defined" );

        emit finalizeDefinition(
            m_key,
            subsetDefinitions,
            baseVariables,
            derivedVariables,
            baseConstants,
            customConstants
        );

        close();
    }

    void SubsetDialog::cancel()
    {
        emit cancelDefinition();
        close();
    }

    void SubsetDialog::setActiveCell( int i, int j )
    {
        activeCell.i1 = i;
        activeCell.i2 = j;
    }

    void SubsetDialog::seriesEdited( int i, int j )
    {
        if( activeCell.i1 != i || activeCell.i2 != j )
        {
            return;
        }

        errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );

        QString text = seriesTable->item( i, j )->text();
        if( ! validateInteger( text ) )
        {
            errorMessageLabel->setText( "Time step must be an integer" );
            seriesValid = false;
            return;
        }
        else if ( text.toInt() < 0 )
        {
            errorMessageLabel->setText( "Time step must be non-negative" );
            seriesValid = false;
            return;
        }
        else
        {
            int val = text.toInt();
            int simStepStrideFull = fullSeriesTable->item( 1, 2 )->text().toInt();
            int lastSimStep = fullSeriesTable->item( 1, 1 )->text().toInt();
            int firstSimStep = fullSeriesTable->item( 1, 0 )->text().toInt();
            bool realTimeAvailable = fullSeriesTable->item( 0, 1 )->text() != "NA";

            if( i == 1 )
            {
                if( val > lastSimStep )
                {
                    errorMessageLabel->setText( "Time step is out of range" );
                    seriesValid = false;
                    return;
                }

                else if( val % simStepStrideFull != 0 || val < simStepValues[ 0 ] )
                {
                    errorMessageLabel->setText( "Time step not available" );
                    seriesValid = false;
                    return;
                }

                else if( j == 0 || j == 1 )
                {
                    int output = ( val - firstSimStep ) / simStepStrideFull;
                    seriesTable->item( 2, j )->setText( QString::number( output ) );
                    if ( realTimeAvailable )
                    {
                        double real = realTimeValues[ output ];
                        seriesTable->item( 0, j )->setText( QString::number( real ) );
                    }
                }
                else
                {
                    seriesTable->item( 2, j )->setText( QString::number( val / simStepStrideFull ) );
                    if ( realTimeAvailable )
                    {
                        double real = realTimeValues[ 1 ] - realTimeValues[ 0 ];
                        seriesTable->item( 0, j )->setText( QString::number( real ) );
                    }
                }
            }
            else if ( i == 2 )
            {
                if( val >=  ( int ) simStepValues.size() )
                {
                    errorMessageLabel->setText( "Time step is out of range" );
                    seriesValid = false;
                    return;
                }
                else if( j == 0 || j == 1 )
                {
                    seriesTable->item( 1, j )->setText( QString::number( simStepValues[ val ] ) );
                    if ( realTimeAvailable )
                    {
                        seriesTable->item( 0, j )->setText( QString::number( realTimeValues[ val ] ) );
                    }
                }
                else
                {
                    seriesTable->item( 1, j )->setText( QString::number( val * simStepStrideFull ) );
                    if ( realTimeAvailable )
                    {
                        double real = realTimeValues[ 1 ] - realTimeValues[ 0 ];
                        seriesTable->item( 0, j )->setText( QString::number( real ) );
                    }
                }
            }
        }

        QString firstVal = seriesTable->item(   1, 0 )->text();
        QString lastVal = seriesTable->item(    1, 1 )->text();
        QString strideVal = seriesTable->item(  1, 2 )->text();

        if( !( validateInteger( firstVal ) && validateInteger( lastVal ) &&  validateInteger( strideVal ) ) )
        {
            errorMessageLabel->setText( "Incomplete Definition" );
            seriesValid = false;
            return;
        }
        else if( firstVal.toInt() > lastVal.toInt() )
        {
            seriesValid = false;
            errorMessageLabel->setText( "Last cannot be greater than first" );
            return;
        }
        else if( ( lastVal.toInt() - firstVal.toInt() ) % strideVal.toInt() != 0 )
        {
            seriesValid = true;
            errorMessageLabel->setStyleSheet( "QLabel { color : orange; }" );
            errorMessageLabel->setText( "Warning: interval is not divisible by stride, series will be truncated" );
            return;
        }
        else
        {
            seriesValid = true;
            errorMessageLabel->setText( "Series is valid" );
            errorMessageLabel->setStyleSheet( "QLabel { color : green; }" );
        }
    }

    void SubsetDialog::update(
        const std::map< std::string, SubsetDefinition > & _filterMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap,
        const TimeSeriesDefinition & fullSeriesDefinition,
        const std::vector< double > & realTime,
        const std::vector< std::int32_t > & steps )
    {
        if( m_status == "defined" )
        {
            for( auto & dv : _derivedVarMap )
            {
                if( ! derivedVariables.count( dv.second.name() ) )
                {
                    std::string _nm = dv.second.name() + " = " + dv.second.expression();
                    derivedAttributeList->addItem( _nm.c_str() );
                }
            }
            for( auto & cc : _customConstMap)
            {
                if( ! customConstants.count( cc.second.name() ) )
                {
                    std::string _nm = cc.second.name()
                            + " = " + cc.second.expression();

                    if( ! m_expressionCalculator->parser().validateNumber( cc.second.expression() ) )
                    {
                        _nm += " = " + std::to_string( m_expressionCalculator->parser().evaluateConstExpression( cc.second.expression(), _baseConstMap, _customConstMap ) );
                    }
                    userConstantList->addItem( _nm.c_str() );
                }
            }
        }

        baseVariables = _baseVarMap;
        derivedVariables = _derivedVarMap;
        customConstants = _customConstMap;
        baseConstants = _baseConstMap;
        subsetDefinitions = _filterMap;

        if( m_status != "defined" )
        {
            attributeList->setStyleSheet( "QListWidget { color : green }" );
            constantList->setStyleSheet( "QListWidget { color : green }" );

            constantList->clear();
            userConstantList->clear();
            derivedAttributeList->clear();
            attributeList->clear();

            for( auto & baseConst : _baseConstMap )
            {
                std::string _nm = baseConst.first + " = " + std::to_string( baseConst.second.value() );
                constantList->addItem( _nm.c_str() );
            }

            for( auto & custConst : _customConstMap )
            {
                std::string _nm = custConst.second.name()
                        + " = " + custConst.second.expression();

                if( ! m_expressionCalculator->parser().validateNumber( custConst.second.expression() ) )
                {
                    _nm += " = " + std::to_string( m_expressionCalculator->parser().evaluateConstExpression( custConst.second.expression(), _baseConstMap, _customConstMap ) );
                }

                userConstantList->addItem( _nm.c_str() );
            }

            for( auto & dv : _derivedVarMap )
            {
                std::string _nm = dv.second.name() + " = " + dv.second.expression();
                derivedAttributeList->addItem( _nm.c_str() );
            }

            for( auto & baseVarE : baseVariables )
            {
                std::string _nm = baseVarE.first.c_str();

                attributeList->addItem( _nm.c_str() );
            }
        }

        simStepValues = steps;
        realTimeValues = realTime;

        fullSeriesDescriptionBox->setText( fullSeriesDefinition.description.c_str() );

        if( (int) realTime.size() >= fullSeriesDefinition.lastIdx )
        {
            fullSeriesTable->item( 0, 0 )->setText( QString::number( realTime[ fullSeriesDefinition.firstIdx  ] ) );
            fullSeriesTable->item( 0, 1 )->setText( QString::number( realTime[ fullSeriesDefinition.lastIdx   ] ) );
            fullSeriesTable->item( 0, 2 )->setText( QString::number( realTime[ fullSeriesDefinition.firstIdx + 1 ] - realTime[ fullSeriesDefinition.firstIdx ] ) );
        }
        else
        {
            seriesTable->item( 0, 0 )->setText( "NA" );
            seriesTable->item( 0, 1 )->setText( "NA" );
            seriesTable->item( 0, 2 )->setText( "NA" );

            fullSeriesTable->item( 0, 0 )->setText( "NA" );
            fullSeriesTable->item( 0, 1 )->setText( "NA" );
            fullSeriesTable->item( 0, 2 )->setText( "NA" );
        }

        fullSeriesTable->item( 1, 0 )->setText( QString::number( steps[ fullSeriesDefinition.firstIdx  ] ) );
        fullSeriesTable->item( 1, 1 )->setText( QString::number( steps[ fullSeriesDefinition.lastIdx   ] ) );
        fullSeriesTable->item( 1, 2 )->setText( QString::number( steps[ fullSeriesDefinition.firstIdx + 1 ] - steps[ fullSeriesDefinition.firstIdx ]  ) );

        fullSeriesTable->item( 2, 0 )->setText( QString::number( fullSeriesDefinition.firstIdx ) );
        fullSeriesTable->item( 2, 1 )->setText( QString::number( fullSeriesDefinition.lastIdx ) );
        fullSeriesTable->item( 2, 2 )->setText( QString::number( 1 ) );\

        for (int i = 0; i < fullSeriesTable->rowCount(); ++i ) {
            for (int j = 0; j < fullSeriesTable->columnCount(); ++j ) {
                fullSeriesTable->item( i, j )->setFlags( fullSeriesTable->item( i, j )->flags() & ~Qt::ItemIsEditable );
            }
        }
    }

    void SubsetDialog::fromDefinition( const SubsetDefinition & def )
    {
        filterDescriptionBox->setText( def.description.c_str() );
        filterExpressionEdit->setText( def.expression.c_str() );
        seriesDescriptionBox->setText( def.window.description.c_str() );

        setTableFromSeries( def.window, seriesTable, realTimeValues, simStepValues );

        filterNameingLineEdit->setText( def.name.c_str() );
        seriesValid = true;
        errorMessageLabel->setText( "" );

        setStatus( "defined" );
    }

    void SubsetDialog::setStatus( const std::string & status )
    {
        m_status = status;
        if( status == "defined" )
        {
            acceptButton->setText( "update" );
            filterNameingLineEdit->setEnabled( false );
        }
    }

    SubsetDialog::SubsetDialog( std::string _key, QWindow *parentWindow ) : BaseDataDialogue( _key, parentWindow )
    {
        filterExpressionLabel = new QLabel( "Filter Expression" );
        filterExpressionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        filterExpressionInstrLabel = new QLabel( "( particle is removed completely if the boolean expression is true at any point in the filtering window, and the removal will be reflected in memory usage )" );

        filterExpressionEdit = new QLineEdit;
        filterExpressionContainer = new QVBoxLayout;

        filterExpressionContainer->addWidget( filterExpressionLabel );
        filterExpressionContainer->addWidget( filterExpressionInstrLabel );
        filterExpressionContainer->addWidget( filterExpressionEdit );

        connect( filterExpressionEdit, SIGNAL(textChanged(QString)), this, SLOT(expressionLineEditChanged(QString)) );

        //////////////////////////////////////////////////////////////////////

        acceptButton = new QPushButton( "create" );
        cancelButton = new QPushButton( "cancel" );
        filterNameingLabel = new QLabel( "Filter Name/Key" );
        filterNameingLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        filterNameingLineEdit = new QLineEdit;

        errorMessageLabel = new QLabel;

        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

        ///////////////////////////////////////////////////////////////////////////////

        // Available Time Steps

        seriesValid = false;

        fullSeriesDescriptionLayout = new QVBoxLayout;
        fullSeriesDescriptionLabel = new QLabel( "Full Series Description" );
        fullSeriesDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        fullSeriesDescriptionBox = new QTextEdit;
        fullSeriesDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );
        fullSeriesDescriptionBox->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );

        fullSeriesLabel = new QLabel( "Full Series" );
        fullSeriesLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        fullSeriesTable = new QTableWidget( 3, 3 );

        fullSeriesTable->setMinimumWidth( 400 );
        fullSeriesTable->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Minimum );
        fullSeriesTable->setHorizontalHeaderLabels(QString("First;Last;Stride").split(";"));
        fullSeriesTable->setVerticalHeaderLabels( QString("Simulation Time;Simulation T.S.;Output T.S.").split(";"));
        fullSeriesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

        for (int i = 0; i < fullSeriesTable->rowCount(); ++i ) {
            for (int j = 0; j < fullSeriesTable->columnCount(); ++j ) {
                fullSeriesTable->setItem( i, j, new QTableWidgetItem );
                QFlags< Qt::ItemFlag > flags = fullSeriesTable->item( i, j )->flags();
                fullSeriesTable->item( i, j )->setFlags( flags &= ~Qt::ItemIsEditable );
            }
        }

        QHBoxLayout * kjncewionoicejnoinw1e = new QHBoxLayout;
        kjncewionoicejnoinw1e->addWidget( fullSeriesDescriptionLabel );
        kjncewionoicejnoinw1e->addStretch();

        fullSeriesDescriptionLayout->addLayout( kjncewionoicejnoinw1e );
        fullSeriesDescriptionLayout->addWidget( fullSeriesDescriptionBox );

        fullSeriesLayout = new QVBoxLayout;

        fullSeriesLayout->addWidget( fullSeriesLabel );
        fullSeriesLayout->addWidget( fullSeriesTable );
        fullSeriesLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );
        fullSeriesLayout->addLayout( fullSeriesDescriptionLayout );

        //////

        // Window

        seriesLabel = new QLabel( "Filtering Window" );
        seriesLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        seriesLayout = new QVBoxLayout;

        seriesTable = new QTableWidget( 3, 3 );
        seriesTable->setMinimumWidth( 420 );
        seriesTable->setHorizontalHeaderLabels(QString("First;Last;Stride").split(";"));
        seriesTable->setVerticalHeaderLabels(QString("Simulation Time;Simulation T.S.;Output T.S.").split(";"));
        seriesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        seriesTable->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Minimum );

        for (int i = 0; i < fullSeriesTable->rowCount(); ++i ) {
            for (int j = 0; j < fullSeriesTable->columnCount(); ++j ) {
                seriesTable->setItem( i, j, new QTableWidgetItem );
                if( i == 1 || i == 2 )
                {
                    seriesTable->item( i, j )->setBackgroundColor( Qt::white );
                }
            }
        }

        seriesDescriptionLayout = new QVBoxLayout;
        seriesDescriptionLabel = new QLabel( "Filtering Window Description" );
        seriesDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        seriesDescriptionBox = new QTextEdit;
        seriesDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );
        seriesDescriptionBox->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );

        QHBoxLayout * kjncewiojnoinwe = new QHBoxLayout;
        kjncewiojnoinwe->addWidget( seriesDescriptionLabel );
        kjncewiojnoinwe->addStretch();

        seriesDescriptionLayout->addLayout( kjncewiojnoinwe );
        seriesDescriptionLayout->addWidget( seriesDescriptionBox );

        connect( seriesTable, SIGNAL(cellClicked(int,int)), this, SLOT( setActiveCell( int, int ) ) );
        connect( seriesTable, SIGNAL(cellChanged(int,int)), this, SLOT( seriesEdited( int, int ) ) );

        // series layout

        seriesLayout->addWidget( seriesLabel );
        seriesLayout->addWidget( seriesTable );
        seriesLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );
        seriesLayout->addLayout( seriesDescriptionLayout );

        /////////////////////////////////////////////////////////////////////////////

        // Description

        filterDescriptionLayout = new QVBoxLayout;
        filterDescriptionLabel = new QLabel( "Filter Description" );
        filterDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        filterDescriptionBox = new QTextEdit;
        filterDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );

        QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
        kjncewionoicejnoinwe->addWidget( filterDescriptionLabel );
        kjncewionoicejnoinwe->addStretch();

        filterDescriptionLayout->addLayout( kjncewionoicejnoinwe );
        filterDescriptionLayout->addWidget( filterDescriptionBox );

        // main layout

        m_mainLayout->addLayout( filterExpressionContainer );

        QHBoxLayout * windowLayout = new QHBoxLayout;

        windowLayout->addLayout( fullSeriesLayout );
        windowLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );
        windowLayout->addLayout( seriesLayout );

        m_mainLayout->addLayout( windowLayout );
        m_mainLayout->addLayout( filterDescriptionLayout );

        m_mainLayout->addWidget( errorMessageLabel );

        QHBoxLayout *dll = new QHBoxLayout;
        dll->addWidget( filterNameingLabel );
        dll->addWidget( filterNameingLineEdit );

        dll->addWidget( cancelButton );
        dll->addWidget( acceptButton );
        m_mainLayout->addLayout( dll );
    }
}

