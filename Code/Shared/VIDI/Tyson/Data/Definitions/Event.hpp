#ifndef TN_EVENT_HPP
#define TN_EVENT_HPP

#include <string>

namespace TN
{

struct GobalEventDefinition
{
    std::size_t timeIndex;
    std::string name;
    std::string description;
};

}

#endif // TN_EVENT_HPP
