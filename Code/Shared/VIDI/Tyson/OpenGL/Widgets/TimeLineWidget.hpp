


#ifndef TIMELINEWIDGET_HPP
#define TIMELINEWIDGET_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/Plot.hpp"
#include "Types/Vec.hpp"
#include "OpenGL/Widgets/SliderWidget.hpp"
#include "OpenGL/Widgets/ComboWidget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"

namespace TN
{

class TimeLineWidget : public Plot
{

public:

    const int MARGIN_LEFT   = 70;
    const int MARGIN_RIGHT  = 34;
    const int MARGIN_TOP    = 74;
    const int MARGIN_BOTTOM = 45;

private:

    ////

    std::vector< Vec2< float > > m_background;
    std::vector< Vec2< float > > m_axis;
    std::vector< Vec2< float > > m_tics;
    std::vector< Vec2< float > > m_grid;

    Vec3< float > m_axisColor;
    Vec3< float > m_ticColor;
    Vec3< float > m_gridColor;
    Vec3< float > m_backgroundColor;
    Vec3< float > m_plotBackgroundColor;

    Vec2< float > m_xRange;
    Vec2< float > m_yRange;

    std::vector< float > m_xTicValues;
    std::vector< float > m_yTicValues;

    Vec2< float > m_minTicDist; // pixels, override users preference if tics would be too close
    Vec2< float > m_ticSpacing; //used when user doesn't give predefined tic value

    int m_gridRows;
    int m_gridCols;

    std::string m_xLabel;
    std::string m_yLabel;

    std::string m_title;

    int m_currentTimeStep;

    // data

    std::vector< float > m_y;
    std::vector< float > m_x;

    //

public:

    PressButton generateButton;
    ComboWidget timelineCombo;

    void make()
    {

        // background
        m_background.resize( 6 );
        m_background[0] = Vec2< float > ( -1, -1 );
        m_background[1] = Vec2< float > ( -1,  1 );
        m_background[2] = Vec2< float > (  1, -1 );
        m_background[3] = m_background[1];
        m_background[4] = Vec2< float > ( 1, 1 );
        m_background[5] = m_background[2];
        m_backgroundColor = Vec3< float >( 0.4f, .98f, .98f );

        // axis
        m_axis.resize( 4 );
        m_axis[0] = Vec2< float >( -1,  1 );
        m_axis[1] = Vec2< float >( -1, -1 );
        m_axis[2] = Vec2< float >( -1, -1 );
        m_axis[3] = Vec2< float >(  1, -1 );

        // tiks
        if( plotSize().x() <= 0 || plotSize().x() > 9999 || plotSize().y() <= 0 || plotSize().y() > 9999 )
        {
            return;
        }
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
        make();
    }

    void initialize()
    {
        make();
        m_yRange = Vec2< float >( -1, 1 );
        generateButton.setPressed( false );
        timelineCombo.addItem( "time vs selection sz" );
    }

    TimeLineWidget()
    {
        initialize();
    }

    const std::vector< Vec2< float > > & axis() const
    {
        return m_axis;
    }
    const std::vector< Vec2< float > > & background() const
    {
        return m_background;
    }
    const Vec3< float > & backgroundColor() const
    {
        return m_backgroundColor;
    }
    const Vec3< float > & plotBackgroundColor() const
    {
        return m_plotBackgroundColor;
    }
    const std::vector< Vec2< float > > & grid() const
    {
        return m_grid;
    }
    const std::string & title() const
    {
        return m_title;
    }

    Vec2< float > plotPosition() const
    {
        return position() + Vec2< float >( MARGIN_LEFT, MARGIN_BOTTOM );
    }
    Vec2< float > plotSize() const
    {
        return size() - Vec2< float >( MARGIN_LEFT + MARGIN_RIGHT, MARGIN_TOP + MARGIN_BOTTOM );
    }
    float plotTop()    const
    {
        return    MARGIN_TOP;
    }
    float plotBottom() const
    {
        return MARGIN_BOTTOM;
    }
    float plotLeft()   const
    {
        return   MARGIN_LEFT;
    }
    float plotRight()  const
    {
        return  MARGIN_RIGHT;
    }

    void setGrid( const std::vector< float > & _x )
    {
        //grid
        int NC = std::min( (int)_x.size() - 1, 400 );
        int NR = 10;

        m_gridRows = NR;
        m_gridCols = NC;

        m_grid.resize( ( NR + NC )*2 );

        float dc = 2.f / NC;
        for( int c = 0; c < NC; ++c )
        {
            m_grid[ c * 2 +  0 ] = Vec2< float >( -1 + (c+1)*dc, -1 );
            m_grid[ c * 2 +  1 ] = Vec2< float >( -1 + (c+1)*dc,  1 );
        }
        float dr = 2.f / NR;
        for( int r = NC; r < NR+NC; ++r )
        {
            m_grid[ r * 2 +  0 ] = Vec2< float >( -1, -1 + (r-NC+1)*dr );
            m_grid[ r * 2 +  1 ] = Vec2< float >(  1, -1 + (r-NC+1)*dr );
        }

        m_xRange = Vec2< float >( 0, _x.size()-1 );
    }

    void update( const std::vector< float > & _x, const std::vector< float > & _y, const Vec2< float > & _range )
    {
        if( _x.size() < 2 )
        {
            return;
        }

        if( _x.size() != m_x.size() )
        {
            setGrid( _x );
        }

        m_y = _y;
        m_x = _x;
        m_yRange = _range;
        m_xRange = Vec2< float >( _x[ 0 ], _x.back() );
    }

    const std::vector< float > x() const
    {
        return m_x;
    }

    const std::vector< float > y() const
    {
        return m_y;
    }

    Vec2< float > xRange()
    {
        return m_xRange;
    }

    Vec2< float > yRange()
    {
        return m_yRange;
    }

    void clear()
    {
        m_x.clear();
        m_y.clear();
    }

    void setTimeStep( int t )
    {
        m_currentTimeStep = t;
    }

    int currentTimeStep()
    {
        return m_currentTimeStep;
    }
};

}

#endif // TIMELINEWIDGET_HPP

