#ifndef GTSCONFIGURATION_CPP
#define GTSCONFIGURATION_CPP

#include "BaseConfiguration.hpp"

namespace TN {

struct DistributionBasedConfiguration : public BaseConfiguration
{
    std::map< std::string, std::map< std::string, PredefinedHistogram > >
    m_predefinedHistograms;

// TODO
//    std::map< std::string, std::map< std::string, GridPointFilter > >
//    m_gridPointFilters;

//    std::map< std::string, std::map< std::string, GridPlot > >
//    m_gridPlots;

//    std::map< std::string, std::map< std::string, m_gridTimePlot > >
//    m_gridTimePlot;

// derivations from grid point scalars?
// from distributions themselves ??
// just general calculations from what has been defined ??

    void setParticles( const std::set< std::string > & ptypes )
    {
        m_particleTypes = ptypes;
        for( auto & p : ptypes )
        {
            m_timeSeriesDefinitions.insert( { p, std::map< std::string, TimeSeriesDefinition >() } );
            m_spacePartitioning.insert( { p, SpacePartitioningDefinition() } );
        }
    }

    void setDistributions( const std::map< std::string, std::map< std::string, PredefinedHistogram > > & distributions )
    {
        m_predefinedHistograms = distributions;
    }

    QJsonObject toJSON() const
    {
        QJsonObject obj = BaseConfiguration::toJSON();
        QJsonObject histObj;
        for( auto p : m_particleTypes )
        {
            QJsonArray ar;
            auto it = m_predefinedHistograms.find( p );
            if( it != m_predefinedHistograms.end() )
            {
                for( auto it2 : it->second )
                {
                    ar.append( it2.second.toJSON() );
                }
            }
            histObj.insert( p.c_str(), ar );
        }
        obj.insert( "histograms", histObj );
        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        BaseConfiguration::fromJSON( obj );

        m_predefinedHistograms.clear();
        for( auto p : m_particleTypes )
        {
            m_predefinedHistograms.insert( { p, std::map< std::string, PredefinedHistogram >() } );
            foreach ( QJsonValue tsv, obj[ "histograms" ].toObject()[ p.c_str() ].toArray() )
            {
                QJsonObject tsObj = tsv.toObject();
                PredefinedHistogram tsd;
                tsd.fromJSON( tsObj );
                m_predefinedHistograms.at( p ).insert( { tsObj[ "name" ].toString().toStdString(), tsd } );
            }
        }
    }

    void clear()
    {
        BaseConfiguration::clear();
        m_predefinedHistograms.clear();
        m_spacePartitioning.clear();
    }

    ~DistributionBasedConfiguration() {}
};

}

#endif // GTSCONFIGURATION_CPP
