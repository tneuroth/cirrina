#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform sampler1D tf;

uniform float mx;
uniform float mn;

uniform bool fade;

void main(){

    color = vec4( texture( tf, texture( tex, uv.st ).r ).rgb, 1 );
}
