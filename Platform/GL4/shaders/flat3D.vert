#version 130



in vec3 posAttr;

uniform mat4 MVP;

void main(void)
{
    gl_Position = MVP*vec4( posAttr, 1 );
}
