#version 400




in float xAttr;
in float yAttr;
in int c;

uniform bool combo;
uniform mat4 MVP;

out vData
{
    flat int select;
} vertex;

void main(void)
{
    if( combo )
    {
        vertex.select = int( ( ( ( c & 1 ) != 0 ) && ( ( c & 2 ) != 0 ) ) );
    }
    else
    {
        vertex.select = int( ( ( ( c & 1 ) != 0  ) ) );
    }

    gl_Position = MVP * vec4(
         xAttr,
         yAttr,
         0.0,
         1.0
    );
}
