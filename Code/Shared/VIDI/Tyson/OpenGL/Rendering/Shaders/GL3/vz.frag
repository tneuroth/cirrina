#version 130



in  vec3 col;
in float op;
out vec4 fragColor;
uniform float opacity;

uniform bool rga;

void main(void)
{
    if ( rga ) {
        fragColor = vec4( col.x, col.y, col.z, op * opacity );
    }
    else {
        fragColor = vec4( col, opacity );
    }
}
