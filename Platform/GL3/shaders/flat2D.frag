#version 130



uniform vec4 color;

in  vec3 col;
out vec4 fragColor;

void main(void)
{
    if ( color.x >= 0 ) {
        fragColor = vec4( color );
    }
    else {
        fragColor = vec4( col, color.y );
    }
}
