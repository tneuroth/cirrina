#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform sampler1D tf;

uniform float mx;
uniform float mn;

void main(){

    float f = texture( tex, uv.st ).r;
    float r = ( f - mn ) / ( mx - mn );
    vec3 cl = texture( tf, 1.0 - r ).rgb;

//    if( r > ( mx - mn ) / 100 )
//    {
//        cl = vec3( 0, 0, 0 );
//    }
//    else
//    {
//        cl = vec3( 1, 1, 1 );
//    }

    color = vec4( cl.r, cl.g, cl.b, 1.0 );
}
