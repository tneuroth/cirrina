
#ifndef TN_SUBSET_DEFINITION
#define TN_SUBSET_DEFINITION

#include "Types/Vec.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"

#include <string>

// Currently just a copy of the particle filter
// Should also support loading flags
// Particle Data Manager Should be responsible for managing/flagging them in memory

namespace TN {

struct SelectionPlugin
{
    std::string name;
    std::string description;
    std::string module_key;

    enum Mode
    {
        STATIC,
        VARIANT
    } mode;
};

struct StoredSelection
{
    std::string name;
    std::string description;
    std::string filePath;

    enum Mode
    {
        STATIC,
        VARIANT
    } mode;
};

struct StaticSelector
{
    enum Mode
    {
        ALL_IF_ANY_IN_WINDOW,
        ALL_IF_ALL_IN_WINDOW,
        WINDOW_IF_ANY_IN_WINDOW,
        WINDOW_IF_ALL_IN_WINDOW,
        BEFORE_FIRST_IN_WINDOW,
        AFTER_FIRST_IN_WINDOW,
        BEFORE_LAST_IN_WINDOW,
        AFTER_LAST_IN_WINDOW
    } mode;

    std::string expression;
    std::string name;
    std::string description;

    TimeSeriesDefinition window;
};

struct VariantSelector
{
    enum Mode
    {
        WHEN_EXPR
    } mode;
};

struct SubsetDefinition
{
    std::string name;

    enum Source
    {
        EXPR,
        FILE,
        FUNC
    } source;

    enum Mode
    {
        WHEN,
        IF,
        AFTER_EXPR,
        BEFORE_EXPR,
        AFTER_GLOBAL_EVENT,
        BEFORE_GLOBAL_EVENT
    };

    std::string description;
    std::string expression;
    TimeSeriesDefinition window;
    bool fromFile;

    SubsetDefinition() { name = "None"; source = EXPR; }

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "name", name.c_str() );
        obj.insert( "description", description.c_str() );
        obj.insert( "expression", expression.c_str() );
        obj.insert( "window", window.toJSON() );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        name = obj[ "name" ].toString().toStdString();
        description = obj[ "description" ].toString().toStdString();
        expression = obj[ "expression" ].toString().toStdString();

        qDebug() << "before window from json";

        window.fromJSON( obj[ "window" ].toObject() );
    }
};

}
#endif

