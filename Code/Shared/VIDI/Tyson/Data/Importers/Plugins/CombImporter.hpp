
#ifndef COMB_IMPORTER_HPP
#define COMB_IMPORTER_HPP

#include "Vec.hpp"
#include "FieldInterpolator.h"
#include "../ParticleDataImporter.hpp"
#include "../../Importer/MetaData.hpp"
#include "../../Managers/VisualContextModel.hpp"

#include <QDebug>

#include <fstream>
#include <map>
#include <string>
#include <vector>
#include <memory>
#include <limits>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The CombImporter class
 */
class CombImporter : public ParticleDataImporter< TargetFloatType >
{

    typedef float OrigionalFloatType;

    std::vector< OrigionalFloatType > m_swap;
    std::vector< std::int32_t > m_simulationTimeSteps;
    std::int32_t m_numParticles;

public :

    /**
     * @brief init
     * @param dataSetMetaData
     * @return
     */
    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        this->m_basicDataInfo = basicDataInfo;
        this->m_initialized = true;
        return this->m_initialized;
    }

    virtual void loadTimeStepsMasks( std::vector< std::int32_t > & masks, size_t t )
    {
        masks = std::vector< std::int32_t >( m_numParticles.at( "ions" ) );
        for( int i = 0; i < m_numParticles.at( "ions" ); ++i )
        {
            masks[ i ] = 1;
        }
    }

    /**
     * @brief loadTemporallyStaticAttributes
     * @param particleVariantAttributes
     * @param constants
     * @param simulationTimeSteps
     * @param realTime
     */
    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes,
        std::map< std::string, std::map< std::string, Vec2< double > > > & variantAttributeRanges,
        std::map< std::string, std::vector< TargetFloatType > >  & particleVariantAttributes,
        std::map< std::string, BaseConstant > & constants,
        std::map< std::string, std::map< std::string, DerivedVariable > > & derivedVariables,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        std::int32_t & numTimeSteps,
        std::int32_t & numParticles )
    {
//            qDebug() << "loading static attributes for XGC particle data from: " << this->m_dataSetMetaData.filePath.c_str();

        std::ifstream inFile( this->m_dataSetMetaData.filePath + "/meta.txt" );
        if( ! inFile.is_open() )
        {
            qDebug() << "couln't open meta file";
        }

        std::string line;
        size_t ts_first;
        size_t ts_last;
        size_t ts_stride;
        std::string ptype;
        std::string attribute;

        while( std::getline( inFile, line ) )
        {
            if( line[ 0 ] == 't' && line[ 1 ] == 's' && line.size() == 2 )
            {
                //qDebug() << "reading time steps";

                std::getline( inFile, line );
                std::istringstream ss( line );
                ss >> ts_first >> ts_last >> ts_stride;
                numTimeSteps = ( ts_last - ts_first + ts_stride ) / ts_stride;

                //qDebug() << ts_first <<  " " << ts_last << " " << ts_stride;
                continue;
            }
            else if ( line[ 0 ] == 'o' && line.size() == 1 )
            {
                std::getline( inFile, line );
                std::istringstream ss( line );
                ss >> ptype >> numParticles;

                particleAndTimeVariantAttributes.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                            ptype,
                            std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > () ) );

                variantAttributeRanges.insert( std::pair< std::string, std::map< std::string, Vec2< double > > > (
                                                   ptype,
                                                   std::map< std::string, Vec2< double > >( ) ) );
                continue;
            }
            else if ( line[ 0 ] == 'v' && line.size() == 1 )
            {
                std::getline( inFile, line );
                std::istringstream ss( line );
                int num;
                ss >> num;

                for( int i = 0; i < num; ++i )
                {
                    std::getline( inFile, line );
                    std::istringstream ss1( line );
                    OrigionalFloatType a,b;
                    ss1 >> attribute >> a >> b;

                    particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                                attribute,
                                std::vector< std::unique_ptr< std::vector< TargetFloatType > > > () ) );
                    for( int t = 0; t < numTimeSteps; ++t )
                    {
                        particleAndTimeVariantAttributes.find( ptype)->second.find( attribute )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
                    }


                    variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
                                attribute,
                                Vec2< double > ( a, b ) ) );
                }
                continue;
            }
        }
        inFile.close();

        for( int t = ts_first; t <= ts_last; t += ts_stride )
        {
            simulationTimeSteps.push_back( t );
        }
        m_simulationTimeSteps = simulationTimeSteps;

        //qDebug() << "there are " << simulationTimeSteps.size() << " time steps";

        visualContextModels.insert( std::pair< std::string, VisualContextModel< TargetFloatType > >(
                                        "separatrix",
                                        VisualContextModel< TargetFloatType >() ) );

        visualContextModels.insert( std::pair< std::string, VisualContextModel< TargetFloatType > >(
                                        "device_walls",
                                        VisualContextModel< TargetFloatType >() ) );

        visualContextModels.find( "separatrix" )->second.color = Vec4( .6, .4, .8, 1 );
        visualContextModels.find( "device_walls" )->second.color = Vec4( .2, .2, .2, 1 );

        visualContextModels.find( "separatrix" )->second.primitiveType = GL_LINES;
        visualContextModels.find( "device_walls" )->second.primitiveType = GL_LINES;

        visualContextModels.find( "separatrix" )->second.coordinates.insert( { "r", std::vector< TargetFloatType >() } );
        visualContextModels.find( "separatrix" )->second.coordinates.insert( { "z", std::vector< TargetFloatType >() } );

        visualContextModels.find( "device_walls" )->second.coordinates.insert( { "r", std::vector< TargetFloatType >() } );
        visualContextModels.find( "device_walls" )->second.coordinates.insert( { "z", std::vector< TargetFloatType >() } );

        visualContextModels.find( "separatrix" )->second.boundingBox = std::map< std::string, Vec2< TargetFloatType > > (
        {
            {  "r", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) },
            {  "z", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) }
        } );

        visualContextModels.find( "device_walls" )->second.boundingBox = std::map< std::string, Vec2< TargetFloatType > > (
        {
            {  "r", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) },
            {  "z", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) }
        } );

        inFile.open( this->m_dataSetMetaData.filePath + "/Grid/geometry_fixed/separatrix.txt" );
        if( !inFile.is_open() )
        {
            qDebug() << "couln't open separatrix file";
        }

        std::vector< TargetFloatType > & stxRCoords = visualContextModels.find( "separatrix" )->second.coordinates.find( "r" )->second;
        std::vector< TargetFloatType > & stxZCoords = visualContextModels.find( "separatrix" )->second.coordinates.find( "z" )->second;
        Vec2< TargetFloatType > & stxRRange = visualContextModels.find( "separatrix" )->second.boundingBox.find( "r" )->second;
        Vec2< TargetFloatType > & stxZRange = visualContextModels.find( "separatrix" )->second.boundingBox.find( "z" )->second;

        std::getline( inFile, line );
        int jk = 0;
        while( inFile )
        {
            if ( ! std::getline( inFile, line ) ) break;
            std::istringstream ss( line );
            TargetFloatType r,z;
            ss >> r >> z;
            ++jk;

            stxRCoords.push_back( r );
            stxZCoords.push_back( z );

            stxRRange.a( std::min( stxRRange.a(), r ) );
            stxRRange.b( std::min( stxRRange.b(), r ) );

            stxZRange.a( std::min( stxZRange.a(), z ) );
            stxZRange.b( std::min( stxZRange.b(), z ) );

            if ( jk > 1)
            {
                stxRCoords.push_back( r );
                stxZCoords.push_back( z );
            }
        }
        inFile.close();

        inFile.open( this->m_dataSetMetaData.filePath + "/Grid/geometry_fixed/walls.txt" );
        if( !inFile.is_open() )
        {
            qDebug() << "couln't device wall file";
        }

        std::vector< TargetFloatType > & wallsRCoords = visualContextModels.find( "device_walls" )->second.coordinates.find( "r" )->second;
        std::vector< TargetFloatType > & wallsZCoords = visualContextModels.find( "device_walls" )->second.coordinates.find( "z" )->second;
        Vec2< TargetFloatType > & wallsRRange = visualContextModels.find( "device_walls" )->second.boundingBox.find( "r" )->second;
        Vec2< TargetFloatType > & wallsZRange = visualContextModels.find( "device_walls" )->second.boundingBox.find( "z" )->second;

        std::getline( inFile, line );
        jk = 0;
        while( inFile )
        {
            if ( ! std::getline( inFile, line ) ) break;
            std::istringstream ss( line );
            TargetFloatType r,z;
            ss >> r >> z;
            ++jk;

            wallsRCoords.push_back( r );
            wallsZCoords.push_back( z );

            wallsRRange.a( std::min( wallsRRange.a(), r ) );
            wallsRRange.b( std::min( wallsRRange.b(), r ) );

            wallsZRange.a( std::min( wallsZRange.a(), z ) );
            wallsZRange.b( std::min( wallsZRange.b(), z ) );

            if ( jk > 1)
            {
                wallsRCoords.push_back( r );
                wallsZCoords.push_back( z );
            }
        }
        inFile.close();

        inFile.open( this->m_dataSetMetaData.filePath + "/units.m" );
        while( inFile )
        {
            if ( ! std::getline( inFile, line ) ) break;
            std::istringstream ss( line );
            TargetFloatType value;
            std::string name;
            std::string valueStr;
            ss >> name >> valueStr;

            if( valueStr == "=" )
            {
                ss >> value;
            }
            else if( valueStr[ 0 ] == '=' )
            {
                valueStr.erase( valueStr.begin() );
                value = std::stod( valueStr );
            }
            else
            {
                value = std::stod( valueStr );
            }

            if( name.back() == '=' )
            {
                name.pop_back();
            }

            BaseConstant bc;
            bc.value( value );
            bc.name( name );
            constants.insert( { name, bc } );
        }
        inFile.close();

        m_swap.resize( numParticles );
        m_numParticles = numParticles;
    }

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param particleAndTimeVariantAttributes
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes )
    {
        //qDebug() << "loading time step " << timeStep << ", " << m_simulationTimeSteps.size() << " available";

        int t = m_simulationTimeSteps[ timeStep ];

        const std::string PRECISION = sizeof( OrigionalFloatType ) == 8 ? "DoublePrecision" : "SinglePrecision";
        std::string leadingZeros =   t < 10   ? "0000"
                                     : t < 100  ?  "000"
                                     : t < 1000 ?   "00"
                                     : t < 1000 ?    "0"
                                     : "";

        for( const auto & partType : needToLoad )
        {
            for( const auto & attr : partType.second  )
            {

                //qDebug() << "loading " << timeStep << " " << partType.first.c_str() << " " << attr.c_str();

                std::ifstream inFile( this->m_dataSetMetaData.filePath + "/Particles/Binary/" + PRECISION + "/" + partType.first + "/" + leadingZeros + std::to_string( t ) +  "." + attr + ".bin" );
                if( ! inFile.is_open() )
                {
                    //qDebug() << "failed to load file: " << ( this->m_dataSetMetaData.filePath + "/Particles/Binary/" + PRECISION + "/" + partType.first + "/" + leadingZeros + std::to_string( t ) +  "." + attr + ".bin" ).c_str();
                }

                //qDebug() << "file opened " << partType.first.c_str() << " " << attr.c_str();

                if( ! std::is_same< OrigionalFloatType, TargetFloatType >::value )
                {
                    //qDebug() << "types on disk and in memory differ";
                    inFile.read( (char*) m_swap.data(), m_numParticles*sizeof( OrigionalFloatType ) );
                    particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ].reset( new std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) );
                }
                else
                {
                    //qDebug() << "types on disk and in memory are the same";
                    particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ].reset( new std::vector< TargetFloatType >( m_numParticles ) );
                    inFile.read( (char*) particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ]->data(), m_numParticles*sizeof( OrigionalFloatType ) );
                }

                //qDebug() << "successfully loaded "  << partType.first.c_str() << " " << attr.c_str() << " closing file";
                inFile.close();
            }
        }
    }

    /**
     * @brief ~CombImporter
     */
    virtual ~CombImporter() {}
};

}

#endif // COMB_IMPORTER_HPP
