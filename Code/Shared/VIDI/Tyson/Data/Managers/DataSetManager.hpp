
#ifndef TN_DATASET_MANAGER_HPP
#define TN_DATASET_MANAGER_HPP

#include "Data/Importers/DataImporter.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"
#include "Data/Managers/DistributionGridManager.hpp"
#include "Data/Managers/ParticleDataManager.hpp"
#include "Data/Definitions/VisualContextModel.hpp"

#include "Data/Importers/Plugins/GTSImporter.hpp"
#include "Data/Importers/Plugins/XGCImporter.hpp"
#include "Data/Importers/Plugins/SLACImporter.hpp"

#include <set>
#include <string>
#include <map>
#include <memory>

namespace TN {

template < typename TargetFloatType >
/**
 * @brief The DataSetManager class
 */
class DataSetManager
{
    /**
     * @brief m_dataSetImporterKey
     */
    std::string m_dataSetImporterKey;

    /**
     * @brief m_supportedFormats
     */
    std::set< std::string > m_supportedFormats;

    /**
     * @brief m_availableDataTypes
     */
    std::string m_dataType;

    /**
     * @brief m_distributionDataManager
     */
    DistributionGridManager< TargetFloatType > m_distributionDataManager;

    /**
     * @brief m_particleDataManager
     */
    ParticleDataManager< TargetFloatType > m_particleDataManager;

    /**
     * @brief m_importers
     */
    std::map< std::string, std::unique_ptr< DataImporter< TargetFloatType > > > m_importers;

    std::string m_name;

public:

    /**
     * @brief DataSetManager
     */
    DataSetManager()
    {
        m_supportedFormats = { "XGC_Particles", "SLAC_Particles", "GTS_Distributions" };

        m_importers.insert(
            std::pair< std::string, std::unique_ptr< DataImporter< TargetFloatType > > > (
               "XGC_Particles", std::unique_ptr< DataImporter< TargetFloatType > >( new XGCImporter< TargetFloatType > ) ) );

        m_importers.insert(
            std::pair< std::string, std::unique_ptr< DataImporter< TargetFloatType > > > (
               "GTS_Distributions", std::unique_ptr< DataImporter< TargetFloatType > >( new GTSImporter< TargetFloatType > ) ) );

        m_importers.insert(
            std::pair< std::string, std::unique_ptr< DataImporter< TargetFloatType > > > (
               "SLAC_Particles", std::unique_ptr< DataImporter< TargetFloatType > >( new SLACImporter< TargetFloatType > ) ) );
    }

    /**
     * @brief distributionManager
     * @return
     */
    const DistributionGridManager< TargetFloatType > & distributionManager() const { return m_distributionDataManager; }
    DistributionGridManager< TargetFloatType > & distributionManager() { return m_distributionDataManager; }

    /**
     * @brief particleDataManager
     * @return
     */
    const ParticleDataManager< TargetFloatType > & particleDataManager() const { return m_particleDataManager; }
    ParticleDataManager< TargetFloatType > & particleDataManager() { return m_particleDataManager; }

    /**
     * @brief availableDataTypes
     * @return
     */
    const std::string dataType() const { return m_dataType; }

    /**
     * @brief setDataSet
     * @param dataSetMetaData
     * @return
     */
    bool setDataSet( const BasicDataInfo & basicDataInfo )
    {
        if( m_supportedFormats.count( basicDataInfo.format ) )
        {
            m_dataSetImporterKey = basicDataInfo.format;

            if( m_dataSetImporterKey == "XGC_Particles" )
            {
                m_particleDataManager.reset( basicDataInfo, ( ParticleDataImporter< TargetFloatType > * )( m_importers.find( "XGC_Particles" )->second.get() ) );
                m_dataType = "particle";
                m_name = "ITER";
            }
            else if( m_dataSetImporterKey == "GTS_Distributions" )
            {
                m_distributionDataManager.reset( basicDataInfo, ( DistributionDataImporter< TargetFloatType > * )( m_importers.find( "GTS_Distributions" )->second.get() ) );
                m_dataType = "distribution";
                m_name = "GTS 0";
            }
            else if( m_dataSetImporterKey == "SLAC_Particles" )
            {
                m_particleDataManager.reset( basicDataInfo, ( ParticleDataImporter< TargetFloatType > * )( m_importers.find( "SLAC_Particles" )->second.get() ) );
                m_dataType = "particle";
                m_name = "SLAC Job 4";
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    const std::string & name() const { return m_name; }
    void name( const std::string & nm ) { m_name = nm; }
};

}

#endif // DATASETMANAGER_HPP

