#version 130

uniform mat4 MVP;
in float xAttr;
in float yAttr;

void main(void)
{
    gl_Position = MVP*vec4( xAttr, yAttr, 0, 1.0 );
}
