
#ifndef META_DATA_HPP
#define META_DATA_HPP

#include <string>

namespace TN
{
struct DataSetMetaData
{
    std::string format;
    std::string filePath;
};

struct ProjectMetaData
{
    DataSetMetaData dataset;
};

}

#endif // META_DATA_HPP

