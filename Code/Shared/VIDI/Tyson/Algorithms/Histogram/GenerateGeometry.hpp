
#ifndef TN_GENERATE_GEOMETRY
#define TN_GENERATE_GEOMETRY

#include "Types/Vec.hpp"

#include <unordered_map>

namespace TN
{

namespace HIST
{

inline void appendModelTo2D(
    const Vec3< float > & cl1,
    const Vec3< float > & cl2,
    const Vec3< float > & cl3,
    const Vec3< float > & cl4,
    const Vec3< float > & cl5,
    const Vec3< float > & ncl2,
    const Vec3< float > & ncl3,
    const Vec3< float > & ncl4,
    const Vec3< float > & ncl5,
    std::vector< Vec2< float > > & vertices,
    std::vector< Vec3< float > > & cell_colors,
    Vec2< float > c1,
    Vec2< float > c2,
    Vec2< float > c3,
    Vec2< float > c4,
    const I2 RESOLUTION,
    const std::unordered_map< int, double > & map,
    double NORMALIZE,
    bool stretch = false,
    bool center  = false )
{
    if ( NORMALIZE == 0 )
    {
        return;
    }

    const int COLS = RESOLUTION.a();
    const int ROWS = RESOLUTION.b();

    float dx = std::abs( c1.x() - c3.x() ) / COLS;
    float dy = std::abs( c1.y() - c2.y() ) / ROWS;

    Vec2< float > offset( c3.x(), c4.y() );

    if(  ! stretch )
    {
        const int DIM = std::max( COLS, ROWS );
        dx = std::min( dx, dy );
        dy = dx;
        offset = offset + Vec2< float >(  dx* ( DIM - COLS ) / 2.0, 0 );
        if ( center )
        {
            offset = offset + Vec2< float >( 0, dx * ( DIM - ROWS ) / 2.0 );
        }
    }

    for ( auto &e : map )
    {
        int binId = e.first;
        float frequency = e.second;

        Vec3< float > c;

        float r = std::abs( frequency / NORMALIZE );

        int row = binId / COLS;
        int col = binId % COLS;

        if( frequency >= 0 )
        {
            if ( r <= .25 )
            {
                float p = r / .25;
                c = cl1 * ( 1.0 - p ) + cl2 * p;
            }
            else if ( r <= .5)
            {
                float p = ( r - .25 ) / .25;
                c = cl2 * ( 1.0 - p ) + cl3 * p;
            }
            else if ( r <= .75 )
            {
                float p = ( r - .5 ) / .25;
                c = cl3 * ( 1.0 - p ) + cl4 * p;
            }
            else if ( r <= 1.f )
            {
                float p = ( r - .75 ) / .25;
                c = cl4 * ( 1.0 - p  ) + cl5 * p;
            }
            else{
                c = cl5;
            }
        }
        else
        {
            if ( r <= .25 )
            {
                float p = r / .25;
                c = cl1 * ( 1.0 - p ) + ncl2 * p;
            }
            else if ( r <= .5)
            {
                float p = ( r - .25 ) / .25;
                c = ncl2 * ( 1.0 - p ) + ncl3 * p;
            }
            else if ( r <= .75 )
            {
                float p = ( r - .5 ) / .25;
                c = ncl3 * ( 1.0 - p ) + ncl4 * p;
            }
            else if ( r <= 1.f )
            {
                float p = ( r - .75 ) / .25;
                c = ncl4 * ( 1.0 - p  ) + ncl5 * p;
            }
            else{
                c = ncl5;
            }
        }

        for ( int i = 0; i < 6; ++i )
        {
            cell_colors.push_back( Vec3< float >( c.x(), c.y(), c.z() ) );
        }

        // upper triangle /////////////////
        // * *
        // *
        vertices.push_back(
            Vec2< float >( col * dx, row * dy ) + offset
        );

        vertices.push_back(
            Vec2< float >(
                ( col + 1 ) * dx,
                row * dy ) + offset );

        vertices.push_back(
            Vec2< float >(
                col * dx,
                ( row + 1 ) * dy ) + offset );

        //lower triangle
        //   *
        // * *
        vertices.push_back(
            Vec2< float >(
                ( col + 1 ) * dx,
                row * dy ) + offset );

        vertices.push_back(
            Vec2< float >(
                ( col + 1 ) * dx,
                ( row + 1 ) * dy ) + offset );

        vertices.push_back(
            Vec2< float >(
                col * dx,
                ( row + 1 ) * dy ) + offset );

    }
}

inline void appendCellOutline2D(
    int id,
    const Vec2< float > c1,
    const Vec2< float > c2,
    const Vec2< float > c3,
    const Vec2< float > c4,
    const I2 RESOLUTION,
    std::vector< Vec2< float > > & vertices )
{
    const int COLS = RESOLUTION.a();
    const int ROWS = RESOLUTION.b();

    const float dx = std::abs( c1.x() - c3.x() ) / COLS;
    const float dy = std::abs( c1.y() - c2.y() ) / ROWS;

    int row = id / COLS;
    int col = id % COLS;

    // upper triangle /////////////////
    // * -
    // * -
    vertices.push_back(
        Vec2< float >( c3.x() + col * dx,
                       c4.y() + row * dy ) );

    vertices.push_back(
        Vec2< float >(
            c3.x() + col * dx,
            c4.y() + ( row + 1 ) * dy ) );

    // upper triangle /////////////////
    // * *
    // - -
    vertices.push_back(
        Vec2< float >( c3.x() + col * dx,
                       c4.y() + row * dy ) );

    vertices.push_back(
        Vec2< float >(
            c3.x() + ( col + 1 ) * dx,
            c4.y() + row * dy ) );

    //lower triangle
    //  - *
    //  - *

    vertices.push_back(
        Vec2< float >(
            c3.x() + ( col + 1 ) * dx,
            c4.y() + ( row ) * dy ) );

    vertices.push_back(
        Vec2< float >(
            c3.x() + ( col + 1 ) * dx,
            c4.y() + ( row + 1 ) * dy ) );

    //lower triangle
    // - -
    // * *
    vertices.push_back(
        Vec2< float >(
            c3.x() + ( col + 1 ) * dx,
            c4.y() + ( row + 1 ) * dy ) );

    vertices.push_back(
        Vec2< float >(
            c3.x() + col * dx,
            c4.y() + ( row + 1 ) * dy ) );
}

inline void appendGridHistModelTo2D(
    const Vec3< float > & cl1,
    const Vec3< float > & cl2,
    const Vec3< float > & cl3,
    const Vec3< float > & cl4,
    const Vec3< float > & cl5,
    const Vec3< float > & ncl2,
    const Vec3< float > & ncl3,
    const Vec3< float > & ncl4,
    const Vec3< float > & ncl5,
    std::vector< Vec2< float > > & vertices,
    std::vector< Vec3< float > > & cell_colors,
    const Vec2< float > c1,
    const Vec2< float > c2,
    const Vec2< float > c3,
    const Vec2< float > c4,
    const I2 RESOLUTION,
    const std::vector< int >  & gridMap,
    const std::vector< float > & gridCellVolumes,
    const std::vector< float > & distrobutionValues,
    const double NORM_FACTOR )
{
    const int COLS = RESOLUTION.a();
    const int ROWS = RESOLUTION.b();

    float dx = std::abs( c1.x() - c3.x() ) / COLS;
    float dy = std::abs( c1.y() - c2.y() ) / COLS;

    const int SZ = gridMap.size();

    double volumeSum = 0;
    #pragma omp parallel for reduction(+:volumeSum)
    for ( int i = 0; i < SZ; ++i )
    {
        volumeSum += gridCellVolumes[ gridMap[i] ];
    }

    if( volumeSum == 0 )
    {
        return;
    }

    double maxF = 0;
    double minF = 0;

    std::vector< std::vector< double > > mergeResult( ROWS, std::vector< double >( COLS, 0 ) );

    const double FACTOR = NORM_FACTOR / volumeSum;

    for ( int col = 0; col < COLS; ++col )
    {
        for ( int row = 0; row < ROWS; ++row )
        {
            for ( int i = 0; i < SZ; ++i )
            {
                mergeResult[ row ][ col ] += distrobutionValues[ col + COLS * ( row + ROWS * gridMap[ i ] ) ];
            }
            mergeResult[ row ][ col ] *= FACTOR;
            maxF = std::max( mergeResult[ row ][ col ], maxF  );
            minF = std::min( mergeResult[ row ][ col ], minF  );
        }
    }

    double NORMALIZE = std::max( std::abs( minF ), std::abs( maxF ) );
    if( NORMALIZE == 0 )
    {
        return;
    }

    const float Y_START = c4.y() + ( COLS - ROWS ) * dy;

    for( int row = 0; row < ROWS; ++row )
    {
        for ( int col = 0; col < COLS; ++col )
        {
            Vec3< float > c;
            double frequency = mergeResult[ row ][ col ];
            double r = std::abs( frequency / NORMALIZE );

            if( frequency >= 0 )
            {
                if ( r <= .25 )
                {
                    float p = r / .25;
                    c = cl1 * ( 1.0 - p ) + cl2 * p;
                }
                else if ( r <= .5)
                {
                    float p = ( r - .25 ) / .25;
                    c = cl2 * ( 1.0 - p ) + cl3 * p;
                }
                else if ( r <= .75 )
                {
                    float p = ( r - .5 ) / .25;
                    c = cl3 * ( 1.0 - p ) + cl4 * p;
                }
                else if ( r <= 1.f )
                {
                    float p = ( r - .75 ) / .25;
                    c = cl4 * ( 1.0 - p  ) + cl5 * p;
                }
                else {
                   // qDebug() << r << " out of range";
                }
            }
            else
            {
                if ( r <= .25 )
                {
                    float p = r / .25;
                    c = cl1 * ( 1.0 - p ) + ncl2 * p;
                }
                else if ( r <= .5)
                {
                    float p = ( r - .25 ) / .25;
                    c = ncl2 * ( 1.0 - p ) + ncl3 * p;
                }
                else if ( r <= .75 )
                {
                    float p = ( r - .5 ) / .25;
                    c = ncl3 * ( 1.0 - p ) + ncl4 * p;
                }
                else if ( r <= 1.f )
                {
                    float p = ( r - .75 ) / .25;
                    c = ncl4 * ( 1.0 - p  ) + ncl5 * p;
                }

                else {
                   // qDebug() << r << " out of range";
                }
            }

            for ( int i = 0; i < 6; ++i )
            {
                cell_colors.push_back( Vec3< float >( c.x(), c.y(), c.z() ) );
            }

            // upper triangle /////////////////
            // * *
            // *
            vertices.push_back(
                Vec2< float >(
                    c3.x() + col * dx,
                    Y_START + row * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + ( col + 1 ) * dx,
                    Y_START + row * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + col * dx,
                    Y_START + ( row + 1 ) * dy ) );

            //lower triangle
            //   *
            // * *
            vertices.push_back(
                Vec2< float >(
                    c3.x() + ( col + 1 ) * dx,
                    Y_START + row * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + ( col + 1 ) * dx,
                    Y_START + ( row + 1 ) * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + col * dx,
                    Y_START + ( row + 1 ) * dy ) );
        }
    }
}

inline void appendGridHistModelTo2D(
    const Vec3< float > & cl1,
    const Vec3< float > & cl2,
    const Vec3< float > & cl3,
    const Vec3< float > & cl4,
    const Vec3< float > & cl5,
    const Vec3< float > & ncl2,
    const Vec3< float > & ncl3,
    const Vec3< float > & ncl4,
    const Vec3< float > & ncl5,
    std::vector< Vec2< float > > & vertices,
    std::vector< Vec3< float > > & cell_colors,
    const Vec2< float > c1,
    const Vec2< float > c2,
    const Vec2< float > c3,
    const Vec2< float > c4,
    const I2 RESOLUTION,
    const std::vector< double >  & histValues,
    double NORM_FACTOR )
{
    const int COLS = RESOLUTION.a();
    const int ROWS = RESOLUTION.b();

    float dx = std::abs( c1.x() - c3.x() ) / COLS;
    float dy = std::abs( c1.y() - c2.y() ) / COLS;

    const float Y_START = c4.y() + ( COLS - ROWS ) * dy;

    for( int row = 0; row < ROWS; ++row )
    {
        for ( int col = 0; col < COLS; ++col )
        {
            Vec3< float > c;
            double frequency = histValues[ row * COLS + col ];
            double r = std::min( std::abs( frequency / NORM_FACTOR ), 1.0 );

            if( frequency >= 0 )
            {
                if ( r <= .25 )
                {
                    float p = r / .25;
                    c = cl1 * ( 1.0 - p ) + cl2 * p;
                }
                else if ( r <= .5)
                {
                    float p = ( r - .25 ) / .25;
                    c = cl2 * ( 1.0 - p ) + cl3 * p;
                }
                else if ( r <= .75 )
                {
                    float p = ( r - .5 ) / .25;
                    c = cl3 * ( 1.0 - p ) + cl4 * p;
                }
                else if ( r <= 1.f )
                {
                    float p = ( r - .75 ) / .25;
                    c = cl4 * ( 1.0 - p  ) + cl5 * p;
                }
                else {
                   // qDebug() << r << " out of range";
                }
            }
            else
            {
                if ( r <= .25 )
                {
                    float p = r / .25;
                    c = cl1 * ( 1.0 - p ) + ncl2 * p;
                }
                else if ( r <= .5)
                {
                    float p = ( r - .25 ) / .25;
                    c = ncl2 * ( 1.0 - p ) + ncl3 * p;
                }
                else if ( r <= .75 )
                {
                    float p = ( r - .5 ) / .25;
                    c = ncl3 * ( 1.0 - p ) + ncl4 * p;
                }
                else if ( r <= 1.f )
                {
                    float p = ( r - .75 ) / .25;
                    c = ncl4 * ( 1.0 - p  ) + ncl5 * p;
                }

                else {
                   // qDebug() << r << " out of range";
                }
            }

            for ( int i = 0; i < 6; ++i )
            {
                cell_colors.push_back( Vec3< float >( c.x(), c.y(), c.z() ) );
            }

            // upper triangle /////////////////
            // * *
            // *
            vertices.push_back(
                Vec2< float >(
                    c3.x() + col * dx,
                    Y_START + row * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + ( col + 1 ) * dx,
                    Y_START + row * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + col * dx,
                    Y_START + ( row + 1 ) * dy ) );

            //lower triangle
            //   *
            // * *
            vertices.push_back(
                Vec2< float >(
                    c3.x() + ( col + 1 ) * dx,
                    Y_START + row * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + ( col + 1 ) * dx,
                    Y_START + ( row + 1 ) * dy ) );

            vertices.push_back(
                Vec2< float >(
                    c3.x() + col * dx,
                    Y_START + ( row + 1 ) * dy ) );
        }
    }
}

inline void appendCartesianOutlineTo(
    std::vector< Vec2< float > > & outline,
    Vec2< float > c1, Vec2< float > c2, Vec2< float > c3, Vec2< float > c4
)
{
    // c3      c1
    //
    // c4      c2

    outline.push_back( c3 );
    outline.push_back( c4 );
    outline.push_back( c4 );
    outline.push_back( c2 );
    outline.push_back( c2 );
    outline.push_back( c1 );
    outline.push_back( c1 );
    outline.push_back( c3 );
}

} // namespace HIST

} // namespace TN

#endif // TN_GENERATE_GEOMETRY
