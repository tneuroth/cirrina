
#ifndef TIMEPLOTDEFINITIONDIALOGUE
#define TIMEPLOTDEFINITIONDIALOGUE

#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/LinkedViewDefinition.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"

namespace TN {

class TimePlotDefinitionDialogue : public BaseDataDialogue
{
    Q_OBJECT

    std::map< std::string, TimePlotDefinition > definedTimePlots;

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    QVBoxLayout * PlotAttributeSection;

    /////////////////////////////////////////////////////////////////////

    // Sampling Space

    QLabel    * spaceAttrLabel;
    QComboBox * spaceAttrCombo1;

    QVBoxLayout * spacec2;
    QVBoxLayout * spacec3;

    QHBoxLayout * spaceLayout;

    // plot Nameing

    QLabel    * plotNameingLabel;
    QLineEdit * plotNameingLineEdit;

    ///////////////////////////////////////////////////////////////////////

    // plot description

    QVBoxLayout * plotDescriptionLayout;
    QLabel    * plotDescriptionLabel;
    QTextEdit * plotDescriptionBox;

    /////////////////////////////////////////////////////////////////////

public:

    ~TimePlotDefinitionDialogue();

signals:

    void cancelDefinition();

    void finalizeDefinition(
        const std::string &,
        const std::map< std::string, TimePlotDefinition > &,
        const std::map< std::string, BaseVariable > &,
        const std::map< std::string, DerivedVariable > &,
        const std::map< std::string, BaseConstant > &,
        const std::map< std::string, CustomConstant > & );

private slots:

    void plotDescriptionChanged();
    void finalize();
    void cancel();
    virtual void attemptDeriveVariable( ) override;

public:

    void update(
        const std::map< std::string, TimePlotDefinition > & _histMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap );

    void fromDefinition( const TimePlotDefinition & def );
    void setStatus( const std::string & status );

    TimePlotDefinitionDialogue( std::string _key, QWindow *parentWindow );
};

}

#endif // TIMEPLOTDEFINITIONDIALOGUE

