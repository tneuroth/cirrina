
#include "BALEEN.hpp"


#include "Dialogues/PhasePlotDefinitionDialogue.hpp"
#include "Dialogues/TimePlotDefinitionDialogue.hpp"
#include "Dialogues/TimeSeriesDefinitionDialogue.hpp"
#include "Dialogues/HistogramDefinitionDialogue.hpp"
#include "Dialogues/ParticleFilterDialogue.hpp"
#include "Dialogues/SubsetDialog.hpp"
#include "Dialogues/EditGridProjectionDialogue.hpp"
#include "Algorithms/IsoContour/Contour.hpp"

#include "Algorithms/Histogram/GenerateGeometry.hpp"
#include "Data/MetadataParsers/ProjectParser.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Algorithms/Standard/Util.hpp"
#include "Data/Configuration/ParticleBasedConfiguration.hpp"
#include "Data/Configuration/DistributionBasedConfiguration.hpp"

#include "Data/Managers/DataSetManager.hpp"
//#include "Algorithms/IsoContour/CubeMarcher.hpp"
#include "OpenGL/Rendering/Renderer.hpp"
#include "Algorithms/Standard/MyAlgorithms.hpp"

#include "Expressions/GenerateBooleanExpressionProgram.hpp"

#include <QInputDialog>
#include <QGLFramebufferObjectFormat>
#include <QOpenGLFramebufferObject>
#include <QApplication>
#include <QDesktopServices>
#include <QCoreApplication>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QWheelEvent>
#include <QDir>

#include "OpenGL/MyGLBuffer.hpp"
#include "Repo/OpenGL/Utils/TNTexture.hpp"

#include <omp.h>
#include <thread>
#include <atomic>
#include <set>
#include <chrono>
#include <ctime>
#include <cassert>

namespace TN
{

BALEEN::BALEEN() :
    RenderWindow(),
    m_contextColor( 0.1, .1, .1 ),
    m_haveInitializedOpenGL( false ),
    m_haveInitializedData( false ),
    m_selectedTimeStep( 0 ),
    m_dividerColor( .87, .87, .87, 1 ),
    m_textColor( 0.0, 0.0, 0.0 ),
    m_uiLayoutEdited( false ),
    m_uiLayoutInitialized( false ),
    m_dataInitializedFirst( false ),
    m_timePlotDialogOpen( false ),
    m_phasePlotDialogOpen( false ),
    m_timeSeriesDialogOpen( false ),
    m_filterDialogOpen( false ),
    m_subsetDialogOpen( false ),
    m_filterEdited( false ),
    m_datasetSelected( false ),
    m_presetSelected( false ),
    m_sessionInitiated( false ),
    m_timeSeriesDefined( false ),
    m_mouseIsPressed( false ),
    m_phasePlotsValid( false  ),
    m_recomputeSubsets( true ),
    m_selectedCombination( -1 ),
    m_hoverRadius( .05 ),
    m_updateSelection( false ),
    m_selectedParticleColor( 0.1, 0.1, 0.1, 1 ),
    m_statsComputer(),
    m_addNewSubset( false ),
    m_updateBins( true )
{
    m_subsetColorCodes =
    {
//        Vec3< float >( 0.074817377931564788, 0.37325643983083429, 0.65520953479430988 ),
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 80,80,80 )    / 255.f,
        Vec3< float >( 178,223,138 ) / 255.f,
        Vec3< float >( 251,154,153 ) / 255.f,
        Vec3< float >( 253,191,111 ) / 255.f,
        Vec3< float >( 202,178,214 ) / 255.f,
        Vec3< float >( 255,255,153 ) / 255.f,
        Vec3< float >(  31,120,180 ) / 255.f,
        Vec3< float >(  51,160, 44 ) / 255.f,
        Vec3< float >( 227, 26, 28 ) / 255.f,
        Vec3< float >( 255,127,  0 ) / 255.f,
        Vec3< float >( 106, 61,154 ) / 255.f,
        Vec3< float >( 177, 89, 40 ) / 255.f
    };

    m_subsetSymbols =
    {
        "$", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
        "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    };
//      m_emissionEventColor = { 253 / 255.f, 202 / 255.f, 242 / 255.f, 0.9 };
    m_absorptionEventColor = {  106 / 255.0, 61 / 255.0, 154 / 255.0, 0.9 };

    m_emissionEventColor = { 20 / 250.0, 160/ 250.0, 105/ 250.0, 0.9 };
    //m_absorptionEventColor = { 20 / 250.0, 160/ 250.0, 105/ 250.0, 0.9 };

//    m_emissionEventColor = { 1, 1, .5, 0.75 };
//    m_absorptionEventColor = { 239 / 255.0, 225 / 255.0, 201 / 255.0, 0.85 };
   // m_combinationColor = { 0.074817377931564788 * 1.4, 0.37325643983083429 * 1.4, 0.65520953479430988 * 1.4, 1 };

    m_combinationColor = { 178 / 255.f, 64 / 255.f, 100 / 255.f, 1 };

    connect( & m_savePresetDialog,
             SIGNAL( saveProject( bool ) ),
             this,
             SLOT( saveProject(bool) ) );

    m_renderer.reset( new Renderer );

    /////////////////////////////////////////

    // Filtering

    m_filterCombo.addItem( "None" );
    m_previousFilter = m_filterCombo.selectedItemText();

    /////////////////////////////////////////

    m_fbo = 0;

    Vec4 cl( 1, 1, 1, 1.0 );
//    Vec4 cl( .8, .8, .8, 1.0 );

    m_plotGridView.viewPort().bkgColor( cl );
    m_selectionView.viewPort().bkgColor( cl );
    //m_statsView.viewPort().bkgColor( cl );

    m_timeLineWidget.viewPort().bkgColor( cl );
    m_combinationExpression.viewPort().bkgColor( Vec4( 1, 1, 1, 1 ) );

    m_selectFromTrajectoriesButton.setPressed( true );

    m_rangeView.viewPort().bkgColor(      cl );
    m_trajectoryView.viewPort().bkgColor( cl );
    m_recurrenceView.viewPort().bkgColor( cl );

    m_timePlotModeCombo.addItem( "Mean" );
    m_timePlotModeCombo.addItem( "Root Mean Squared" );
    m_timePlotModeCombo.addItem( "Mean Absolute Dev." );

    m_phasePlotModeCombo.addItem( "Trajectories" );
    m_phasePlotModeCombo.addItem( "Points"       );

    m_errorRemovalCombo.addItem("NaN,Inf");
    m_errorRemovalCombo.addItem("NaN,Inf,Unexp.");

    m_phasePlotBlendingCombo.addItem( "Number Density" );
    m_phasePlotBlendingCombo.addItem( "Path Density" );
    m_phasePlotBlendingCombo.addItem( "Weighted Density" );
    m_phasePlotBlendingCombo.addItem( "Mean" );
    m_phasePlotBlendingCombo.addItem( "Max" );
    m_phasePlotBlendingCombo.addItem( "Min" );
    m_phasePlotBlendingCombo.addItem( "Range" );
    m_phasePlotBlendingCombo.addItem( "Variance" );
    m_phasePlotBlendingCombo.addItem( "Recurrence Density" );
    m_phasePlotBlendingCombo.addItem( "Local Statistical Coherency" );
    m_phasePlotBlendingCombo.addItem( "Run Test" );

    m_phasePlotInterpolationCombo.addItem( "None" );
    m_phasePlotInterpolationCombo.addItem( "Linear" );

//    m_phasePlotSelectionModeCombo.addItem( "Radius", "Box" );
//    m_phasePlotSelectionModeCombo.addItem( "Radius", "Box" );

    m_recurrenceView.modeCombo.addItem( "include tang. mot." );
    m_recurrenceView.modeCombo.addItem( "exclude tang. mot." );

    m_toolCombo.addItem( "Probe"    );
    m_toolCombo.addItem( "Box"      );
    m_toolCombo.addItem( "lasso"    );
    m_toolCombo.addItem( "line"     );
    m_toolCombo.addItem( "curve"    );
    m_toolCombo.addItem( "localize" );
    m_toolCombo.addItem( "declutter" );
    m_toolCombo.addItem( "bkg2front" );
    m_toolCombo.addItem( "flow dir." );
    m_toolCombo.addItem( "measure"  );

    m_comboRefs.push_back( & m_toolCombo );
    m_comboRefs.push_back( &m_phasePlotBlendingCombo );
    m_comboRefs.push_back( &m_phasePlotInterpolationCombo );
    m_comboRefs.push_back( &m_datasetCombo );
//    m_comboRefs.push_back( &m_phasePlotCombo );
    m_comboRefs.push_back( &m_phasePlotModeCombo );
    m_comboRefs.push_back( &m_timeSeriesCombo );
    m_comboRefs.push_back( &m_timePlotModeCombo );
    m_comboRefs.push_back( &m_timePlotCombo );
    m_comboRefs.push_back( &m_filterCombo );
    //m_comboRefs.push_back( &m_subsetCombo );
    m_comboRefs.push_back( &m_errorRemovalCombo );
    m_comboRefs.push_back( &m_particleCombo );
    m_comboRefs.push_back( &m_configurationCombo );
    //m_comboRefs.push_back( & m_recurrenceView.modeCombo );

//    m_comboRefs( & m_phasePlotSelectionModeCombo );
//    m_comboRefs(    & m_phasePlotRenderModeCombo );

    Vec3< float > immediateFlashColor( 1, .8, .5 );
    Vec3< float > optionalFlashColor( .8, .9, 1 );

    m_initiateSessionButton.flash( true );
    m_initiateSessionButton.flashColor( immediateFlashColor );

    m_datasetCombo.flashColor( immediateFlashColor );
    m_configurationCombo.flashColor( immediateFlashColor );

    m_defineTimeSeriesButton.flashColor( immediateFlashColor );

    m_defineFilterButton.flashColor( optionalFlashColor );
    //m_definePhasePlotButton.flashColor( optionalFlashColor );
    m_definePlotButton.flashColor( optionalFlashColor );

    m_timeSubsetStatsSelector.setPressed( false );
    m_timeSubsetCombStatsSelector.setPressed (true );
    m_timeTextureSelector.setPressed( true );
    m_selectTintButton.setPressed( false );
    m_selectFocusButton.setPressed( true );

    m_datasetCombo.flash( true );

    m_helpButtonEvent.text( "Events" );
    m_helpButtonPlotGrid.text( "Phase Plots " );
    m_helpButtonData.text( "Data" );
    m_helpButtonTimeline.text( "Temporal Plotting & T.S. Selection" );
//    m_helpButtonStatsView.text( "Statistics" );
    m_helpButtonSubsetting.text( "Subsets" );
    m_helpButtonSelection.text( "Combinations" );
//    m_helpButtonReccu.text( "Recurrence" );
    m_helpButtonTraje.text( "Trajectories" );
    m_helpButtonRange.text( "1D Distributions" );

    m_helpButtonPlotGrid.htmlReference( "#Binning" );
    m_helpButtonData.htmlReference( "#Data" );
    m_helpButtonTimeline.htmlReference( "#Timeline" );
    //m_helpButtonStatsView.htmlReference( "#LinkedPlot" );
    m_helpButtonSubsetting.htmlReference( "#Subsetting" );
    m_helpButtonSelection.htmlReference( "#Selection" );

    m_helpLabelWidgetRefs.push_back( & m_helpButtonEvent );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonPlotGrid );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonData );
    //m_helpLabelWidgetRefs.push_back( & m_helpButtonStatsView );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonTimeline );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonSubsetting );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonSelection );
//    m_helpLabelWidgetRefs.push_back( & m_helpButtonReccu );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonTraje );
    m_helpLabelWidgetRefs.push_back( & m_helpButtonRange );

    m_thresholdSlider.setSliderPos( 1.0 );
    m_resolutionSlider.setSliderPos( 1.0 );

    queryConfigurations();
}

void BALEEN::datasetSwitched()
{
    clearProject();

    //////////////////////////qDebug()  << "project cleared";

    m_presetSelected = false;
    m_timeSeriesCombo.flash( false );

    afterSelectedDataset();
}

void BALEEN::presetSwitched()
{

}

void BALEEN::afterSelectedPreset()
{
    std::string selectedCF = m_configurationCombo.selectedItemText();

    //////////////////////////qDebug()  << "configuring";

    configure( m_datasetCombo.selectedItemText(), m_configurationCombo.selectedItemText() );

    std::vector< std::string > cfs = m_configurationCombo.items();
    m_configurationCombo.clear();

    int idx = 0;
    int i = 0;
    for( auto & s : cfs )
    {
        if( s == selectedCF )
        {
            idx = i;
        }
        if( s != "select preset" )
        {
            m_configurationCombo.addItem( s );
            ++i;
        }
    }

    m_configurationCombo.selectItem( idx );

    m_presetSelected = true;
    m_configurationCombo.flash( false );

    //////////////////////////qDebug()  << "after selected preset done";
}

void BALEEN::afterSelectedDataset()
{
    std::string selectedDS = m_datasetCombo.selectedItemText();
    std::vector< std::string > dsets = m_datasetCombo.items();
    m_datasetCombo.clear();

    int idx = 0;
    int i = 0;
    for( auto & s : dsets )
    {
        if( s == selectedDS )
        {
            idx = i;
        }
        if( s != "select dataset" )
        {
            m_datasetCombo.addItem( s );
            ++i;
        }
    }

    m_datasetCombo.selectItem( idx );

    m_configurationCombo.clear();
    m_configurationCombo.addItem( "select preset" );

    for( auto & dfconf : m_savedConfigurations )
    {
        if( dfconf.first == selectedDS )
        {
            for( auto & config : dfconf.second )
            {
                m_configurationCombo.addItem( config.first );
            }
        }
    }

    m_datasetSelected = true;
    m_presetSelected = false;
    m_sessionInitiated = false;

    m_datasetCombo.flash( false );
    m_configurationCombo.flash( true );

    //////////////////////////qDebug()  << "selected dataset";
}

void BALEEN::queryConfigurations()
{
    // go into data folder and query datasets and configurations
    std::string dirPath = RELATIVE_PATH_PREFIX + "/Configuration/DATA/";
    QStringList datasets = QDir( QString( dirPath.c_str() ) ).entryList( QDir::Dirs | QDir::NoDotAndDotDot, QDir::NoSort );

    m_datasetCombo.clear();
    m_datasetCombo.addItem( "select dataset" );

    // parse the base configurations for all data types

    for( auto & ds : datasets )
    {
        m_datasetCombo.addItem( ds.toStdString() );
        std::string configPath = dirPath + "/" + ds.toStdString() + "/config/";
        QDir configDir( QString( configPath.c_str() ) );
        configDir.setNameFilters( QStringList() << "*.json" );
        QStringList configs = configDir.entryList( QDir::Files, QDir::NoSort );

        m_savedConfigurations.insert( { ds.toStdString(), { } } );

        // read dataset to find out it's type
        std::string dataInfoPath = RELATIVE_PATH_PREFIX + "/Configuration//DATA//" + ds.toStdString() + "//data.json";
        QString valD;
        QFile fileD;
        fileD.setFileName( dataInfoPath.c_str());
        fileD.open(QIODevice::ReadOnly | QIODevice::Text);
        valD = fileD.readAll();
        fileD.close();
        QJsonDocument dataDoc = QJsonDocument::fromJson(valD.toUtf8());
        QJsonObject objD = dataDoc.object();

        std::string type = objD[ "type" ].toString().toStdString();

        std::string basePresetPath = RELATIVE_PATH_PREFIX + "/Configuration//GLOBAL_BASE_DEFAULT//";
        if( type == "particle" )
        {
            basePresetPath += "particle.base.default.json";
        }
        else
        {
            basePresetPath += "grid.base.default.json";
        }

        // add base configuration ...
        QString valP;
        QFile fileP;
        fileP.setFileName( basePresetPath.c_str() );
        fileP.open(QIODevice::ReadOnly | QIODevice::Text);
        valP = fileP.readAll();
        fileP.close();
        QJsonDocument dP = QJsonDocument::fromJson(valP.toUtf8());
        QJsonObject objP = dP.object();
        ProjectConfiguration baseConfig;
        baseConfig.fromJSON( objP );
        m_savedConfigurations.at( ds.toStdString() ).insert( { "base", baseConfig } );

        //////////////////////////qDebug()  << "before load configs";

        // load saved configurations
        for( auto & cf : configs )
        {
            std::string presetName = cf.left( cf.lastIndexOf(".") ).toStdString();
            std::string presetPath = RELATIVE_PATH_PREFIX + "Configuration/DATA/" + ds.toStdString() + "/config/" + presetName + ".json";
            QString val;
            QFile file;
            file.setFileName(presetPath.c_str());
            file.open(QIODevice::ReadOnly | QIODevice::Text);
            val = file.readAll();
            file.close();
            QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject obj = d.object();

            ProjectConfiguration config;

            //////////////////////////qDebug()  << "calling from JSON";
            //////////////////////////qDebug()  << obj;
            config.fromJSON( obj );
            //////////////////////////qDebug()  << "done with from JSON";

            m_savedConfigurations.at( ds.toStdString() ).insert( { presetName, config } );
        }
        //////////////////////////qDebug()  << "after load configs";
    }
    m_datasetCombo.selectItem( 0 );
}

void BALEEN::loadTF( const std::string & path, TNR::Texture1D3 & tex )
{
    std::ifstream inFile( path );
    std::string line;

    std::vector< Vec3< float > > colors;
    while( std::getline( inFile, line ) )
    {
        std::string rgb[ 3 ];
        for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
        {
            if( line[i] == ',' )
            {
                ++cr;
                continue;
            }
            else if( line[ i ] == ')' )
            {
                break;
            }
            else if( line[ i ] == ' ' )
            {
                continue;
            }
            else
            {
                rgb[ cr ].push_back( line[ i ] );
            }
        }

        colors.push_back( Vec3< float >(
                              std::stof( rgb[ 0 ] ),
                              std::stof( rgb[ 1 ] ),
                              std::stof( rgb[ 2 ] ) ) );

        tex.load( colors );
    }
}


void BALEEN::computeTF( const std::vector< Vec3< float > > & colorTics, TNR::Texture1D3 & tex )
{
    std::vector< Vec3< float > > buffer( TF_SIZE  );
    int chunkWidth = TF_SIZE / ( colorTics.size() - 1 );

    for( int i = 0; i < TF_SIZE; ++i )
    {
        int c1Id = std::floor( i / ( double ) chunkWidth );
        int c2Id = std::ceil(  i / ( double ) chunkWidth );

        float r = ( c2Id * chunkWidth - i ) / (double) chunkWidth;
        buffer[ i ] = colorTics[ c1Id ] * r + colorTics[ c2Id ] * ( 1.0 - r );
    }

    tex.load( buffer );
}

void BALEEN::resetSamplerCam()
{
    float cX = ( m_contextBoundingBox.second.a() + m_contextBoundingBox.first.a() ) / 2.0;
    float cY = ( m_contextBoundingBox.second.b() + m_contextBoundingBox.first.b() ) / 2.0;

    camera.setTranslation( Vec2< float >( -cX, -cY ) );
    float xWidth = m_contextBoundingBox.second.a() - m_contextBoundingBox.first.a();
    float yWidth = m_contextBoundingBox.second.b() - m_contextBoundingBox.first.b();

    float dataAspect = xWidth / yWidth;
    float viewAspect = m_plotGridView.viewPort().width() / ( double ) m_plotGridView.viewPort().height();

    float zoom;

    if( dataAspect >= viewAspect )
    {
        zoom = xWidth / ( scaledWidth() );
    }
    else
    {
        zoom = yWidth / ( scaledHeight() );
    }

    camera.setZoom( zoom );

    QVector3D c1 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1, 1, 0 );
    QVector3D c2 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1, 1, 0 );
    QVector3D c3 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1,-1, 0 );
    QVector3D c4 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1,-1, 0 );

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    renderLater();
}

void BALEEN::clearSystem()
{
//    ////////////////////////qDebug()  << "clearing system";
    m_dataSetManager.reset( new DataSetManager< float > );

    m_selectedTimeStep = 0;

    camera.reset();
    camera.setZoom( 0.007 );
    camera.dragLeftButton( Vec2< float >( -8.1f, 2.0f ) );

    m_dataInitializedFirst = false;
    m_recalculateFullTimePlot = true;
}

void BALEEN::cleanDialogues()
{
    m_phasePlotDefinitionDialogues.clear();

//    for( auto & mp : m_gridProjectionDialogues )
//    {
//        for( auto & e : mp.second )
//        {
//            e.second->deleteLater();
//        }
//    }
//    m_gridProjectionDialogues.clear();

    for( auto & mp : m_timePlotDefinitionDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_timePlotDefinitionDialogues.clear();

    for( auto & mp : m_timeSeriesDefinitionDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_timeSeriesDefinitionDialogues.clear();

    for( auto & mp : m_particleFilterDialogues )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_particleFilterDialogues.clear();

    for( auto & mp : m_subsetDialogs )
    {
        for( auto & e : mp.second )
        {
            e.second->deleteLater();
        }
    }
    m_subsetDialogs.clear();
}

BALEEN::~BALEEN()
{
    cleanDialogues();
    delete m_fbo;
}

void BALEEN::clearProject()
{
    m_particleCombo.clear();
    m_timePlotModeCombo.clear();
    m_phasePlotCombo.clear();
    m_filterCombo.clear();
    m_timeSeriesCombo.clear();

    cleanDialogues();

    m_currentConfigurations.distributionBasedConfiguration.clear();
    m_currentConfigurations.particleBasedConfiguration.clear();

    m_dataInitializedFirst = false;
    m_timeSeriesDefined = false;
    m_sessionInitiated = false;

    m_datasetSelected = false;
    m_presetSelected = false;

    m_haveInitializedData = false;
    m_recalculateFullTimePlot = true;

    m_uiLayoutEdited = false;
    m_uiLayoutInitialized = false;
    m_filterEdited = false;

    m_timePlotDialogOpen = false;
    m_phasePlotDialogOpen = false;
    m_timeSeriesDialogOpen = false;
    m_filterDialogOpen = false;
    m_subsetDialogOpen = false;

    m_dataSetManager->particleDataManager().clear();
    m_dataSetManager->distributionManager().clear();
}

bool BALEEN::parseBasicDataInfo( const std::string & dataset, BasicDataInfo & result )
{

    const std::string path = RELATIVE_PATH_PREFIX + "Configuration//DATA//" + dataset + "//data.json";
    QFile loadFile( path.c_str() );

    if ( ! loadFile.open( QIODevice::ReadOnly ) )
    {
        return false;
    }

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc( QJsonDocument::fromJson( saveData ) );

    result.format = loadDoc.object()[ "format" ].toString().toStdString();
    result.location = loadDoc.object()[ "location" ].toString().toStdString();
    result.type = loadDoc.object()[ "type" ].toString().toStdString();
    result.description = loadDoc.object()[ "description" ].toString().toStdString();
    result.name = loadDoc.object()[ "name" ].toString().toStdString();

    return true;
}

void BALEEN::saveProject( bool asDefault )
{
    //////////////////////////qDebug()  << "saving project";

    m_currentConfigurations.phasePlotMode = m_phasePlotModeCombo.selectedItemText();
    m_currentConfigurations.timePlotMode = m_timePlotModeCombo.selectedItemText();
    m_currentConfigurations.timePlotRegion = m_timeSubsetStatsSelector.isPressed() ? "hist" : "all";

    m_currentConfigurations.scaledScreenResolutionX = scaledWidth();
    m_currentConfigurations.scaledScreenResolutionY = scaledHeight();

    m_currentConfigurations.uiOffsetA = m_resizeWidgetA.position().x() - DIVIDER_WIDTH;
    m_currentConfigurations.uiOffsetB = m_resizeWidgetB.position().x();
    m_currentConfigurations.uiOffsetD = m_resizeWidgetD.position().y();

    //////////////////////////qDebug()  << "getting camera toJSON";
    m_currentConfigurations.samplingCamera = camera.toJSON();
    //////////////////////////qDebug()  << "got camera toJSON";

    if( m_timeSeriesDefined )
    {
        m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries = m_timeSeriesCombo.selectedItemText();
    }
    else
    {
        m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries = "";
    }

    // it's flashing if nothing is defined
    // TODO use proper state variables
    if( m_timePlotCombo.flash() != true )
    {
        m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot = m_timePlotCombo.selectedItemText();
    }
    else
    {
        m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot = "";
    }

    m_currentConfigurations.particleBasedConfiguration.m_selectedFilter = m_filterCombo.selectedItemText();
    m_currentConfigurations.particleBasedConfiguration.m_selectedParticleType = m_particleCombo.selectedItemText();

    m_currentConfigurations.particleBasedConfiguration.m_selectedTimeStep = m_selectedTimeStep;
    m_currentConfigurations.errorRemovalOption = m_errorRemovalCombo.selectedItemText();

    auto & mp = m_savedConfigurations.at( m_datasetCombo.selectedItemText() );
    std::string name = m_populationDataTypeEnum == PopulationDataType::PARTICLES
                       ? m_currentConfigurations.particleBasedConfiguration.m_name
                       : m_currentConfigurations.distributionBasedConfiguration.m_name;

    if( mp.count( name ) )
    {
        mp.at( name ) = m_currentConfigurations;
        m_configurationCombo.selectByText( name );
    }
    else
    {
        mp.insert( { name, m_currentConfigurations } );
        m_configurationCombo.addItem( name );
        m_configurationCombo.selectByText( name );
    }

    m_currentConfigurations.write( asDefault, RELATIVE_PATH_PREFIX );
    renderLater();
}

bool BALEEN::configure( const std::string & dataset, const std::string & preset )
{
    //////////////////////////qDebug()  << "loading dataset" << dataset.c_str();

    clearSystem();

    ////////qDebug()  << "parsed basic data info";

    if( parseBasicDataInfo( dataset, m_basicDataInfo ) )
    {
        ////////qDebug()  << "parsed basic data info sucessfully";

        m_currentConfigurations = m_savedConfigurations.at( dataset ).at( preset );
        m_currentConfigurations.activeType = m_basicDataInfo.type;
        m_dataSetManager->setDataSet( m_basicDataInfo );
        m_populationDataTypeEnum = m_basicDataInfo.type == "particle" ? PopulationDataType::PARTICLES : PopulationDataType::GRID_POINTS;
        m_savePresetDialog.setConfig( & m_currentConfigurations );
        m_savePresetDialog.setSavedConfigurations( & m_savedConfigurations.at( dataset ) );

        ////////qDebug()  << "retrieved configurations";

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            ////////qDebug()  << "updating custom constsants";
            m_dataSetManager->particleDataManager().updateCustomConstants( m_currentConfigurations.particleBasedConfiguration.m_customConstants );

            // set particle only based ui settin
            m_timePlotModeCombo.addItem( "mean" );
            m_timePlotModeCombo.addItem( "mean-squared" );

            m_currentConfigurations.particleBasedConfiguration.m_datasetInfo = m_basicDataInfo;
            std::set< std::string > ptypes = m_dataSetManager->particleDataManager().particleTypes();
            m_currentConfigurations.particleBasedConfiguration.setParticles( ptypes );

            for( auto & pt : ptypes )
            {
                ////////qDebug()  << "mapping " << pt.c_str();
            }

            ////////qDebug()  << "loading base variables";
            for( auto & e : m_dataSetManager->particleDataManager().baseVariables() )
            {
                ////////qDebug()  << "adding " << e.first.c_str();

                m_particleCombo.addItem( e.first );
                m_phasePlotDefinitionDialogues.insert( { e.first, std::map< std::string, PhasePlotDefinitionDialogue* >() } );
                m_timePlotDefinitionDialogues.insert( { e.first, std::map< std::string, TimePlotDefinitionDialogue* >() } );
                m_timeSeriesDefinitionDialogues.insert( { e.first, std::map< std::string, TimeSeriesDefinitionDialogue* >() } );
                m_particleFilterDialogues.insert( { e.first, std::map< std::string, ParticleFilterDialogue* >() } );
                m_subsetDialogs.insert( { e.first, std::map< std::string, SubsetDialog* >() } );

                ////////qDebug()  << "updating derivations";
                m_dataSetManager->particleDataManager().updateDerivations( e.first, m_currentConfigurations.particleBasedConfiguration.m_derivedVariables.at(  e.first ) );

                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( e.first ) )
                {
                    ////////qDebug()  << "adding time plot";

                    auto it = m_timePlotDefinitionDialogues.find( e.first );
                    TimePlotDefinitionDialogue *dialog = new TimePlotDefinitionDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimePlotDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeTimePlotDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants() );

                    inserted.first->second->fromDefinition( defs.second );
                }

                ////////qDebug()  << "time series definitions";
                for( auto & defs : m_currentConfigurations.timeSeriesDefinitions().at( e.first ) )
                {
                    auto it = m_timeSeriesDefinitionDialogues.find( e.first );
                    TimeSeriesDefinitionDialogue *dialog = new TimeSeriesDefinitionDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimeSeriesDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeTimeSeriesDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.timeSeriesDefinitions().find( e.first )->second,
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().fullTimeSeries()
                        :  m_dataSetManager->distributionManager().fullTimeSeries(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().realTime()
                        :  m_dataSetManager->distributionManager().realTime(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().simTime()
                        :  m_dataSetManager->distributionManager().simTime() );

                    inserted.first->second->fromDefinition( defs.second );
                }

                ////////qDebug()  << "adding filters";
                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_particleFilters.at( e.first ) )
                {
                    auto it = m_particleFilterDialogues.find( e.first );
                    ParticleFilterDialogue *dialog = new ParticleFilterDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelParticleFilterDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, ParticleFilter > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeParticleFilterDefinition(
                                     const std::string &,
                                     const std::map< std::string, ParticleFilter > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants(),
                        m_dataSetManager->particleDataManager().fullTimeSeries(),
                        m_dataSetManager->particleDataManager().realTime(),
                        m_dataSetManager->particleDataManager().simTime() );

                    inserted.first->second->fromDefinition( defs.second );
                }

                ////////qDebug()  << "adding subset definitions";
                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_subsetDefinitions.at( e.first ) )
                {
                    auto it = m_subsetDialogs.find( e.first );
                    SubsetDialog *dialog = new SubsetDialog( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT( cancelSubsetDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, ParticleFilter > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeParticleSubsetDefinition(
                                     const std::string &,
                                     const std::map< std::string, ParticleFilter > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );

                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.particleBasedConfiguration.m_subsetDefinitions.find( e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants(),
                        m_dataSetManager->particleDataManager().fullTimeSeries(),
                        m_dataSetManager->particleDataManager().realTime(),
                        m_dataSetManager->particleDataManager().simTime() );

                    inserted.first->second->fromDefinition( defs.second );
                }

                ////////qDebug()  << "adding phase plot definitions";
                for( auto & defs : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( e.first ) )
                {
                    auto it = m_phasePlotDefinitionDialogues.find( e.first );
                    PhasePlotDefinitionDialogue *dialog = new PhasePlotDefinitionDialogue( defs.first, this );
                    auto inserted = it->second.insert( std::make_pair( defs.first, dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelPhasePlotDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, PhasePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizePhasePlotDefinition(
                                     const std::string &,
                                     const std::map< std::string, PhasePlotDefinition > &,
                                     const std::map< std::string, BaseVariable > &,
                                     const std::map< std::string, DerivedVariable > &,
                                     const std::map< std::string, BaseConstant > &,
                                     const std::map< std::string, CustomConstant > &
                                 ) ) );
                    connect( inserted.first->second,
                             SIGNAL( updateAttributes(
                                         const std::map<std::string, BaseVariable> &,
                                         const std::map<std::string, DerivedVariable> &,
                                         const std::map<std::string, BaseConstant> &,
                                         const std::map<std::string, CustomConstant> & ) ),
                             this,
                             SLOT( updateAttributes(
                                       const std::map<std::string, BaseVariable> &,
                                       const std::map<std::string, DerivedVariable> &,
                                       const std::map<std::string, BaseConstant> &,
                                       const std::map<std::string, CustomConstant> & ) ) );
                    connect( inserted.first->second,
                             SIGNAL( updateUserDataDescription(
                                         const std::string & ) ),
                             this,
                             SLOT( updateUserDataDescription(
                                       const std::string & ) ) );

                    inserted.first->second->update(
                        m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( e.first )->second,
                        m_dataSetManager->particleDataManager().baseVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().derivedVariables().find( e.first )->second,
                        m_dataSetManager->particleDataManager().customConstants(),
                        m_dataSetManager->particleDataManager().baseConstants() );

                    inserted.first->second->fromDefinition( defs.second );
                }
            }

            ////////qDebug()  << "updating part type";
            updatePartType();

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedParticleType != "" )
            {
                m_particleCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedParticleType );
            }

            m_numTimeSteps = m_dataSetManager->particleDataManager().numTimeSteps();

            // set ui elements

            if( m_currentConfigurations.errorRemovalOption != "" )
            {
                m_errorRemovalCombo.selectByText( m_currentConfigurations.errorRemovalOption );
            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot != "" )
            {
                m_timePlotCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedTimePlot );
            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedFilter != "" )
            {
                m_filterCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedFilter );
            }

            if( m_currentConfigurations.timePlotMode != "" )
            {
                m_timePlotModeCombo.selectByText( m_currentConfigurations.timePlotMode );
            }

//            if( m_currentConfigurations.timePlotRegion == "all" )
//            {
//                m_timeSubsetCombStatsSelector.setPressed( true );
//                m_timeTextureSelector.setPressed( false );
//                m_timeSubsetStatsSelector.setPressed( false );
//            }
//            if( m_currentConfigurations.timePlotRegion == "hist" )
//            {
//                m_timeSubsetCombStatsSelector.setPressed( false );
//                m_timeTextureSelector.setPressed( false );
//                m_timeSubsetStatsSelector.setPressed( true );
//            }
//            else if( m_currentConfigurations.timePlotRegion == "both" )
//            {
//                m_timeSubsetCombStatsSelector.setPressed( false );
//                m_timeTextureSelector.setPressed( true );
//                m_timeSubsetStatsSelector.setPressed( false );
//            }

            if( m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries != "" )
            {
                m_timeSeriesCombo.selectByText( m_currentConfigurations.particleBasedConfiguration.m_selectedTimeSeries );
            }

            ////////qDebug()  << "done with particle specific configuration";
        }

        camera.setSize( scaledWidth(), scaledHeight() );

        if( ( int( m_currentConfigurations.scaledScreenResolutionX ) == int( scaledWidth() ) )
                && ( int( m_currentConfigurations.scaledScreenResolutionY ) == int( scaledHeight() ) ) )
        {
            float offsetA = m_currentConfigurations.uiOffsetA;
            float offsetB = m_currentConfigurations.uiOffsetB;
            float offsetC = m_currentConfigurations.uiOffsetC;
            float offsetD = m_currentConfigurations.uiOffsetD;

            //m_resizeWidgetA.setPosition( DIVIDER_WIDTH + offsetA, 0 );
            m_resizeWidgetA.setSize( DIVIDER_WIDTH, scaledHeight() - MENU_HEIGHT );

            m_resizeWidgetB.setPosition( offsetB, offsetC+DIVIDER_WIDTH );
            m_resizeWidgetB.setSize( DIVIDER_WIDTH, scaledHeight() - MENU_HEIGHT - offsetC - DIVIDER_WIDTH );

            m_resizeWidgetD.setPosition( offsetA + DIVIDER_WIDTH*2, offsetD );
            m_resizeWidgetD.setSize(scaledWidth() - offsetA - DIVIDER_WIDTH * 2, DIVIDER_WIDTH );

            m_uiLayoutInitialized = true;
        }

        m_phasePlotInterpolationCombo.selectByText( "Linear" );
        recalculateUiLayout();

        ////////qDebug()  << "done configuring";

        return true;
    }

    return false;
}

void BALEEN::wheelEvent(QWheelEvent *e)
{
    double numDegrees = e->angleDelta().y() / -9000000.0;
    Vec2< float > pos( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() );

    for( auto & rw : m_rangeView.rangeWidgets )
    {
        if( rw.first.pointInViewPort( pos ) )
        {
            rw.first.mouseWheel( numDegrees );
            rw.second.mouseWheel( numDegrees );
        }
        if( rw.second.pointInViewPort( pos ) )
        {
            rw.first.mouseWheel( numDegrees );
            rw.second.mouseWheel( numDegrees );
        }
    }

    if( m_plotGridView.pointInViewPort( pos )
            || ( m_timeLineWidget.pointInViewPort( pos + Vec2< float >( -m_timeLineWidget.MARGIN_LEFT,  0 ) )
                 && m_timeLineWidget.pointInViewPort( pos + Vec2< float >(  m_timeLineWidget.MARGIN_RIGHT, 0 ) ) ) )
    {
        m_hoverRadius -= numDegrees * 30;
        m_hoverRadius = std::max( m_hoverRadius, 0.0005f );
        m_hoverRadius = std::min( m_hoverRadius, 1.f );
    }

    QVector3D c1;
    QVector3D c2;
    QVector3D c3;
    QVector3D c4;

    c1.setX( -1.0 );
    c1.setY(  1.0 );
    c1.setZ(  0.0 );

    c2.setX( 1.0 );
    c2.setY( 1.0 );
    c2.setZ( 0.0 );

    c3.setX(  1.0 );
    c3.setY( -1.0 );
    c3.setZ(  0.0 );

    c4.setX( -1.0 );
    c4.setY( -1.0 );
    c4.setZ(  0.0 );

    c1 = ( camera.proj()* camera.view() ).inverted() * c1;
    c2 = ( camera.proj()* camera.view() ).inverted() * c2;
    c3 = ( camera.proj()* camera.view() ).inverted() * c3;
    c4 = ( camera.proj()* camera.view() ).inverted() * c4;

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    renderLater();
}

void BALEEN::mouseDoubleClickEvent(QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos();

    QVector2D mouseDir =
        QVector2D((mouseCurr - m_previousMousePosition )
                  / (scaledHeight()));

    mouseDir.setY(-mouseDir.y());
    m_previousMousePosition = mouseCurr;

    Vec2< float > pos( mouseCurr.x(), scaledHeight() - mouseCurr.y() );
    if( m_plotGridView.pointInViewPort( pos ) || ( m_timeLineWidget.pointInViewPort( pos + Vec2< float >( -m_timeLineWidget.MARGIN_LEFT,  0 ) )
            && m_timeLineWidget.pointInViewPort( pos + Vec2< float >(  m_timeLineWidget.MARGIN_RIGHT, 0 ) ) ) )
    {
        m_selectedParticle = -1;
        ////////qDebug()  << "selected plot set to empty 2";
        m_selectedPlot = "";
        m_updateSelection = false;

        for( auto & plt : m_phasePlotPosSz )
        {
            ViewPort vp;
            vp.setSize( plt.second.second.x(), plt.second.second.y() );
            vp.setPosition( plt.second.first.x(), plt.second.first.y() );

            Vec2< float > pos( mouseCurr.x(), scaledHeight() - mouseCurr.y() );

            if( plt.first == "timeline" )
            {
                ////////qDebug()  << "timeline ? is at " << plt.second.first.x() << " " << plt.second.first.y() <<  " " << plt.second.second.x() << " " << plt.second.second.y();
                ////////qDebug()  << "click is at " << pos.x() << " " << pos.y();
            }

            if( vp.pointInViewPort( pos ) )
            {
                m_selectionRadius = m_hoverRadius;
                m_selectionPosition = getRelativeSelectionCenter( pos, plt.second.first, plt.second.second );
                m_selectedPlot = plt.first;

                m_updateSelection = true;

                ////////qDebug()  << "Selected " << plt.first.c_str();

                break;
            }
        }
    }
}

void BALEEN::mousePressEvent(QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos() * devicePixelRatio();
    Vec2< float > pos( mouseCurr.x(), scaledHeight() - mouseCurr.y() );

    m_mouseIsPressed = true;

    // Combos

    bool comboPressed = false;
    for( auto comboPtr : m_comboRefs )
    {
        if( (*comboPtr).pointInViewPort( pos ) )
        {
            if( ! (*comboPtr).isPressed() )
            {
                (*comboPtr).setPressed( true );
            }
            else
            {
                (*comboPtr).pressed( pos );
                (*comboPtr).setPressed( false );
            }
            comboPressed = true;
        }
        else
        {
            (*comboPtr).setPressed( false );
        }
    }

    if( comboPressed )
    {
        renderLater();
        return;
    }

    if( m_recurrenceSlider.pointInViewPort( pos ) )
    {
        m_recurrenceSlider.setPressed( true );
    }

    if( m_resolutionSlider.pointInViewPort( pos ) )
    {
        m_resolutionSlider.setPressed( true );
    }

    if( m_thresholdSlider.pointInViewPort( pos ) )
    {
        m_thresholdSlider.setPressed( true );
    }

    /////

    if( m_recurrenceView.epsilonSlider.pointInViewPort( pos ) )
    {
        m_recurrenceView.epsilonSlider.setPressed( true );
    }

    ViewPort vpTT;
    vpTT.setPosition( m_trajectoryView.position().x(), m_trajectoryView.position().y() );
    vpTT.setSize( m_trajectoryView.size().x() - m_trajectoryView.SCROLL_BAR_HEIGHT, m_trajectoryView.size().y() - 60 + BUTTON_HEIGHT );

    if( m_selectedPlot !=  "" )
    {
        if( vpTT.pointInViewPort( pos ) )
        {
            float xRel = ( pos.x() - vpTT.offsetX() ) / vpTT.width();
            float yRel = ( vpTT.offsetY() + vpTT.height() - pos.y() ) / vpTT.height();

            int col = std::min( std::floor( xRel * m_trajectoryViewNCols ), m_trajectoryViewNCols - 1.f );
            int row = std::min( std::floor( yRel * m_trajectoryViewNRows ), m_trajectoryViewNRows - 1.f );

            int idx = row * m_trajectoryViewNCols + col;
            if( idx < m_particleSelectionSample.size() )
            {
                m_selectedParticle = m_particleSelectionSample[ idx ];
                //////////qDebug()  << "in mouse press, m_selectedParticle = " << m_selectedParticle;
            }

            std::fill( m_particleSelectionSubSampleCounts.begin(), m_particleSelectionSubSampleCounts.end(), 0 );
            m_particleSelectionSubSampleCounts[ m_selectedParticle ] = m_particleSelectionCounts[ m_selectedParticle ];
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if( m_timeLineWidget.pointInViewPort( pos + Vec2< float >( -m_timeLineWidget.MARGIN_LEFT,  0 ) )
            && m_timeLineWidget.pointInViewPort( pos + Vec2< float >(  m_timeLineWidget.MARGIN_RIGHT, 0 ) ) )
    {
//        int offX = pos.x() - ( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT );

//        TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
//                                    : m_dataSetManager->distributionManager().currentTimeSeries();
//        double relative = ( offX / ( double ) m_timeLineWidget.plotSize().x() );
//        int intTStep = ( std::max( std::min( static_cast< int32_t >( std::round( tsrs.firstIdx + relative * ( tsrs.lastIdx - tsrs.firstIdx ) ) ), tsrs.lastIdx ), tsrs.firstIdx ) );
//        int diffStride = ( intTStep - tsrs.firstIdx ) % tsrs.idxStride;
//        if( diffStride != 0 )
//        {
//            if( diffStride > tsrs.idxStride / 2.0 )
//            {
//                intTStep += tsrs.idxStride - diffStride;
//            }
//            else
//            {
//                intTStep -= diffStride;
//            }
//        }

//        m_selectedTimeStep = intTStep;
//        m_timeLineWidget.setTimeStep( m_selectedTimeStep );

//        //////////////////////////////qDebug()  << "set time step to " << m_selectedTimeStep;

//        renderLater();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // resize widgets

    if( m_resizeWidgetA.pointInViewPort( pos ) )
    {
        m_resizeWidgetA.setPressed( true );
    }
    else if( m_resizeWidgetB.pointInViewPort( pos ) )
    {
        m_resizeWidgetB.setPressed( true );
    }
    else if( m_resizeWidgetD.pointInViewPort( pos ) )
    {
        m_resizeWidgetD.setPressed( true );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    m_plotGridView.scrollBar().mousePress( pos );
    m_selectionView.scrollBar().mousePress( pos );

    // Buttons

    int bidx = 0;
    for( auto & b : m_rangeView.varSelectButtons )
    {
        if( b.pointInViewPort( pos ) )
        {
            b.setPressed( ! b.isPressed() );
            std::string varName = m_rangeView.rangeWidgets[ bidx ].first.id;

            if( ! b.isPressed() )
            {
                m_recurrenceSpace.erase( varName );
            }
            else
            {
                m_recurrenceSpace.insert( varName );
            }
        }
        ++bidx;
    }

    int selectedC = -1;
    for( int i = 0; i < m_combinationSelectorList.size(); ++i )
    {
        if( m_combinationSelectorList[ i ].pointInViewPort( pos ) )
        {
            m_combinationSelectorList[ i ].setPressed( true );
            selectedC = i;
        }
    }
    if( selectedC >= 0 )
    {
        m_selectedCombination = selectedC;
        //////////////qDebug()  << "selected combination " << m_selectedCombination;
        for( int i = 0; i < m_combinationSelectorList.size(); ++i )
        {
            if( i != selectedC )
            {
                m_combinationSelectorList[ i ].setPressed( false );
            }
        }
    }

    for( int i = 0; i < m_editSubsetButtons.size(); ++i )
    {
        if( m_editSubsetButtons[ i ].pointInViewPort( pos ) )
        {
            m_editSubsetButtons[ i ].setPressed( true );
        }
    }

    for( int i = 0; i < m_removeSubsetButtons.size(); ++i )
    {
        if( m_removeSubsetButtons[ i ].pointInViewPort( pos ) )
        {
            m_removeSubsetButtons[ i ].setPressed( true );
        }
    }

    ////////////

    if( m_absorptionEventButton.pointInViewPort( pos ) )
    {
        m_absorptionEventButton.setPressed( ! m_absorptionEventButton.isPressed() );
    }
    if( m_emissionEventButton.pointInViewPort( pos ) )
    {
        m_emissionEventButton.setPressed( ! m_emissionEventButton.isPressed() );
    }
    if( m_plotAllSampleButton.pointInViewPort( pos ) )
    {
        m_plotAllSampleButton.setPressed( ! m_plotAllSampleButton.isPressed() );
    }

    if( m_selectionToSubsetButton.pointInViewPort( pos ) )
    {
        m_selectionToSubsetButton.setPressed( true );
    }

    if( m_thresholdRecurrenceButton.pointInViewPort( pos ) )
    {
        m_thresholdRecurrenceButton.setPressed( ! m_thresholdRecurrenceButton.isPressed() );
    }
    if( m_showRecurrenceButton.pointInViewPort( pos ) )
    {
        m_showRecurrenceButton.setPressed( ! m_showRecurrenceButton.isPressed() );
    }
    if( m_recurrenceSettingsButton.pointInViewPort( pos ) )
    {
        m_recurrenceSettingsButton.setPressed( true );
    }

    if( m_selectTintButton.pointInViewPort( pos ) )
    {
        m_selectTintButton.setPressed( true );
        m_selectFocusButton.setPressed( false );
    }
    else if( m_selectFocusButton.pointInViewPort( pos ) )
    {
        m_selectTintButton.setPressed( false );
        m_selectFocusButton.setPressed( true );
    }

    if( m_selectFromEventsButton.pointInViewPort( pos ) )
    {
        m_selectFromEventsButton.setPressed( true );
        m_selectFromTrajectoriesButton.setPressed( false );
    }
    else if( m_selectFromTrajectoriesButton.pointInViewPort( pos ) )
    {
        m_selectFromEventsButton.setPressed( false );
        m_selectFromTrajectoriesButton.setPressed( true );
    }
    if( m_selectionRefinementButton.pointInViewPort( pos ) )
    {
        m_selectionRefinementButton.setPressed( ! m_selectionRefinementButton.isPressed() );
    }

    ///////////////

    if( m_timeTextureSelector.pointInViewPort( pos ) )
    {
        m_timeTextureSelector.setPressed( ! m_timeTextureSelector.isPressed() );
    }
    else if( m_timeSubsetStatsSelector.pointInViewPort( pos ) )
    {
        m_timeSubsetStatsSelector.setPressed( ! m_timeSubsetStatsSelector.isPressed() );
    }
    else if( m_timeSubsetCombStatsSelector.pointInViewPort( pos ) )
    {
        m_timeSubsetCombStatsSelector.setPressed( ! m_timeSubsetCombStatsSelector.isPressed() );
    }

    //////////////

    if( m_saveCombinationButton.pointInViewPort( pos ) )
    {
        m_saveCombinationButton.setPressed( true );
    }

    if( m_gridOverlayButton.pointInViewPort( pos ) )
    {
        m_gridOverlayButton.setPressed( ! m_gridOverlayButton.isPressed() );
    }

    if( m_zoomOutPhaseViewButton.pointInViewPort( pos ) )
    {
        m_zoomOutPhaseViewButton.setPressed( true );
    }

    if( m_zoomInPhaseViewButton.pointInViewPort( pos ) )
    {
        m_zoomInPhaseViewButton.setPressed( true );
    }

    if( m_saveButton.pointInViewPort( pos ) )
    {
        m_saveButton.setPressed( true );
    }

    if( m_loadProjectFileButton.pointInViewPort( pos ) )
    {
        m_loadProjectFileButton.setPressed( true );
    }

    if( m_inspectWarningsButton.pointInViewPort( pos ) )
    {
        m_inspectWarningsButton.setPressed( true );
    }
    if( m_saveProjectButton.pointInViewPort( pos ) )
    {
        m_saveProjectButton.setPressed( true );
    }

    if( m_initiateSessionButton.pointInViewPort( pos ) )
    {
        m_initiateSessionButton.setPressed( true );
    }

    if( m_definePlotButton.pointInViewPort( pos ) )
    {
        m_definePlotButton.setPressed( true );
    }

    if( m_defineTimeSeriesButton.pointInViewPort( pos ) )
    {
        m_defineTimeSeriesButton.setPressed( true );
    }

    if( m_deletePhasePlotButton.pointInViewPort( pos ) )
    {
        m_deletePhasePlotButton.setPressed( true );
    }

    if( m_definePhasePlotButton.pointInViewPort( pos ) )
    {
        m_definePhasePlotButton.setPressed( true );
    }

    if( m_editPhasePlotButton.pointInViewPort( pos ) )
    {
        m_editPhasePlotButton.setPressed( true );
    }

    if( m_editTimePlotButton.pointInViewPort( pos ) )
    {
        m_editTimePlotButton.setPressed( true );
    }

    if( m_editTimeSeriesButton.pointInViewPort( pos ) )
    {
        m_editTimeSeriesButton.setPressed( true );
    }

    if( m_defineFilterButton.pointInViewPort( pos ) )
    {
        m_defineFilterButton.setPressed( true );
    }

    if( m_defineSubsetButton.pointInViewPort( pos ) )
    {
        m_defineSubsetButton.setPressed( true );
    }


    if( m_editFilterButton.pointInViewPort( pos ) )
    {
        m_editFilterButton.setPressed( true );
    }

    if( m_combinationExpression.pointInViewPort( pos ) )
    {
        m_combinationExpression.setFocused( true );
    }
    else
    {
        m_combinationExpression.setFocused( false );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // help label buttons

    for( HelpLabelWidget * hl : m_helpLabelWidgetRefs )
    {
        if( hl->pointInViewPort( pos ) )
        {
            hl->setPressed( true );
        }
    }

    ////////////////////////////////////???????/////////////////////////////////////////////////////////////////////////

    renderLater();
}

void BALEEN::cancelTimeSeriesDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_timeSeriesDefinitionDialogues.find( ptype ) == m_timeSeriesDefinitionDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_timeSeriesDefinitionDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_timeSeriesDialogOpen = false;
}



void BALEEN::cancelTimePlotDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_timePlotDefinitionDialogues.find( ptype ) == m_timePlotDefinitionDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_timePlotDefinitionDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_timePlotDialogOpen = false;
}

void BALEEN::cancelParticleSubsetDefinition()
{
    //////////////////////////////qDebug()  << "canceling particle filter";

    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_subsetDialogs.find( ptype ) == m_subsetDialogs.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_subsetDialogs.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_subsetDialogOpen = false;
}

void BALEEN::cancelParticleFilterDefinition()
{
    //////////////////////////////qDebug()  << "canceling particle filter";

    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_particleFilterDialogues.find( ptype ) == m_particleFilterDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_particleFilterDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_filterDialogOpen = false;
}

void BALEEN::updateDerivations( const std::string & ptype, const std::map< std::string, DerivedVariable > & derived )
{
    m_currentConfigurations.particleBasedConfiguration.m_derivedVariables.at( ptype ) = derived;
    m_dataSetManager->particleDataManager().updateDerivations( ptype, derived );
}

void BALEEN::updateCustomConstants( const std::map< std::string, CustomConstant > & cc )
{
    m_dataSetManager->particleDataManager().updateCustomConstants( cc );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_customConstants = cc;
    }
    else
    {
        m_currentConfigurations.distributionBasedConfiguration.m_customConstants = cc;
    }
}

void BALEEN::updateBaseConstants( const std::map< std::string, BaseConstant > & cc )
{
    m_dataSetManager->particleDataManager().updateBaseConstants( cc );

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_baseConstants = cc;
    }
    else
    {
        m_currentConfigurations.distributionBasedConfiguration.m_baseConstants = cc;
    }
}

void BALEEN::updateBaseVariables( const std::string & ptype, const std::map< std::string, BaseVariable > & variables )
{
    //////////////////////////qDebug()  << "update config base vars";
    m_currentConfigurations.particleBasedConfiguration.m_baseVariables.find( ptype )->second = variables;

    //////////////////////////qDebug()  << "update part maneger base vars " << ptype.c_str();
    m_dataSetManager->particleDataManager().updateBaseVariables( ptype, variables );

    //////////////////////////qDebug()  << "done updating data manager base vars";
}

void BALEEN::finalizeParticleSubsetDefinition
(
    const std::string & key,
    const std::map< std::string, SubsetDefinition > & subsetDefinitions,
    const std::map< std::string, BaseVariable >     & baseVars,
    const std::map< std::string, DerivedVariable >  & derivedVars,
    const std::map< std::string, BaseConstant  >    & baseConsts,
    const std::map< std::string, CustomConstant   > & custConsts )
{
    //////////////qDebug()  << "finalizing particle subset";

    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_subsetDialogs.find( ptype )->second );

    //////////////qDebug()  << "grabbed particle subset dialogs";

    if( currentDialogsForCurrentParticle.find( key ) == currentDialogsForCurrentParticle.end() )
    {
        //////////////qDebug()  << "key not found, removing temp";

        SubsetDialog *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { key, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    //////////////qDebug()  << "getting definitions";

    m_currentConfigurations.particleBasedConfiguration.m_subsetDefinitions.find( ptype )->second = subsetDefinitions;

    //////////////qDebug()  << "updating variable";

    updateBaseVariables( ptype, baseVars );
    updateDerivations( ptype, derivedVars );
    updateCustomConstants( custConsts );
    updateBaseConstants( baseConsts );

    m_subsetDefinitions.clear();
    m_subsetCombo.clear();
    m_subsetCombo.addItem( "All" );

    //////////////qDebug()  << "updating combo";

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_subsetDefinitions.find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_subsetCombo.addItem( h.second.name );
        if( h.second.name == key )
        {
            comboIdx = idx;
        }
        m_subsetDefinitions.push_back( h.second );
    }
    if( m_subsetCombo.numItems() )
    {
        m_subsetCombo.selectItem( comboIdx );
    }

    m_subsetDialogOpen = false;
    m_recomputeSubsets = true;
}

void BALEEN::finalizeParticleFilterDefinition
(
    const std::string & key,
    const std::map< std::string, ParticleFilter >   & particleFilters,
    const std::map< std::string, BaseVariable >     & baseVars,
    const std::map< std::string, DerivedVariable >  & derivedVars,
    const std::map< std::string, BaseConstant  >    & baseConsts,
    const std::map< std::string, CustomConstant   > & custConsts )
{
    //////////////////////////////qDebug()  << "finalizing particle filter";

    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_particleFilterDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( key ) == currentDialogsForCurrentParticle.end() )
    {
        ParticleFilterDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { key, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( ptype )->second = particleFilters;

    updateBaseVariables( ptype, baseVars );
    updateDerivations( ptype, derivedVars );
    updateCustomConstants( custConsts );
    updateBaseConstants( baseConsts );

    m_filterCombo.clear();
    m_filterCombo.addItem( "None" );

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_filterCombo.addItem( h.second.name );
        if( h.second.name == key )
        {
            comboIdx = idx;
        }
    }
    if( m_filterCombo.numItems() )
    {
        m_filterCombo.selectItem( comboIdx );
    }

    m_filterDialogOpen = false;
    m_filterEdited = true;
}

void BALEEN::updateUserDataDescription( const std::string & desc )
{
    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        m_currentConfigurations.particleBasedConfiguration.m_userDataDescription = desc;
    }
    else
    {
        m_currentConfigurations.distributionBasedConfiguration.m_userDataDescription = desc;
    }
}

void BALEEN::updateAttributes(
    const std::map<std::string, BaseVariable> & bv,
    const std::map<std::string, DerivedVariable> & dv,
    const std::map<std::string, BaseConstant> & bc,
    const std::map<std::string, CustomConstant> & cc
)
{
    updateBaseConstants( bc );
    updateCustomConstants( cc );
    updateBaseVariables( m_particleCombo.selectedItemText(), bv );
    updateDerivations( m_particleCombo.selectedItemText(), dv );
}

void BALEEN::mouseReleaseEvent( QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos();
    Vec2< float > pos( mouseCurr.x(), scaledHeight() - mouseCurr.y() );

    m_mouseIsPressed = false;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    m_plotGridView.scrollBar().release();
    m_selectionView.scrollBar().release();

    // Buttons

    for( int i = 0; i < m_removeSubsetButtons.size(); ++i )
    {
        if( m_removeSubsetButtons[ i ].isPressed() )
        {
            m_removeSubsetButtons[ i ].setPressed( false );
            if( m_removeSubsetButtons[ i ].pointInViewPort( pos ) )
            {
                // remove it
            }
        }
    }
    if( m_selectionToSubsetButton.isPressed() )
    {
        m_selectionToSubsetButton.setPressed( false );
        if( m_selectionToSubsetButton.pointInViewPort( pos ) )
        {
            bool ok;
            QString text = QInputDialog::getText( 0, "",
                                                  "name",
                                                  QLineEdit::Normal,
                                                  "",
                                                  & ok );

            m_subsetCombo.addItem( text.toStdString() );

            m_addNewSubset = true;
        }
    }
    if( m_saveProjectButton.isPressed() )
    {
        m_saveProjectButton.setPressed( false );
        if( m_saveProjectButton.pointInViewPort( pos ) )
        {

        }
    }
    if( m_loadProjectFileButton.isPressed() )
    {
        m_loadProjectFileButton.setPressed( false );
        if( m_loadProjectFileButton.pointInViewPort( pos ) )
        {

        }
    }
    if( m_recurrenceSettingsButton.isPressed() )
    {
        m_recurrenceSettingsButton.setPressed( false );
        if( m_recurrenceSettingsButton.pointInViewPort( pos ) )
        {

        }
    }
    if( m_saveButton.isPressed() )
    {
        m_saveButton.setPressed( false );
        if( m_saveButton.pointInViewPort( pos ) )
        {
            if( m_datasetSelected && m_presetSelected )
            {
                if( m_configurationCombo.selectedItemText() != "base" )
                {
                    std::string name = m_configurationCombo.selectedItemText();
                    std::string author = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
                                         m_currentConfigurations.particleBasedConfiguration.m_author
                                         : m_currentConfigurations.distributionBasedConfiguration.m_author;

                    std::string desc = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?
                                       m_currentConfigurations.particleBasedConfiguration.m_description
                                       : m_currentConfigurations.distributionBasedConfiguration.m_description;

                    m_savePresetDialog.update( name, author, desc );
                }
                m_savePresetDialog.show();
            }
        }
    }
    if( m_initiateSessionButton.isPressed() )
    {
        m_initiateSessionButton.setPressed( false );
        if( m_initiateSessionButton.pointInViewPort( pos ) )
        {
            if( m_presetSelected && m_datasetSelected && ! m_sessionInitiated && m_timeSeriesDefined )
            {
                m_sessionInitiated = true;
            }
        }
    }
    if( m_zoomOutPhaseViewButton.isPressed() )
    {
        m_zoomOutPhaseViewButton.setPressed( false );
        if( m_zoomOutPhaseViewButton.pointInViewPort( pos ) )
        {
            m_plotGridView.zoomOut();
        }
    }
    if( m_zoomInPhaseViewButton.isPressed() )
    {
        m_zoomInPhaseViewButton.setPressed( false );
        if( m_zoomInPhaseViewButton.pointInViewPort( pos ) )
        {
            m_plotGridView.zoomIn();
        }
    }

    if( m_saveCombinationButton.isPressed() )
    {
        m_saveCombinationButton.setPressed( false );
        if( m_saveCombinationButton.pointInViewPort( pos ) )
        {
            m_combinationSelectorList.push_back( PressButton() );
            m_combinationList.push_back( m_combinationExpression.text() );
        }
    }

    if( m_closeButton.isPressed() )
    {
        m_closeButton.setPressed( false );
        if( m_closeButton.pointInViewPort( pos ) )
        {
            QApplication::quit();
        }
    }
    if( m_defineFilterButton.isPressed() )
    {
        m_defineFilterButton.setPressed( false );
        if( m_defineFilterButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_filterDialogOpen )
            {
                return;
            }
            m_filterDialogOpen = true;

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_particleCombo.numItems() > 0 )
                {
                    std::string currentParticleType = m_particleCombo.selectedItemText();

                    auto it = m_particleFilterDialogues.find( currentParticleType );
                    if( it == m_particleFilterDialogues.end() )
                    {
                        //////////////////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped define hist button";
                    }
                    else
                    {
                        ParticleFilterDialogue *dialog = new ParticleFilterDialogue( "temp", this );
                        auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                        connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelParticleFilterDefinition() ) );
                        connect( inserted.first->second,
                                 SIGNAL(
                                     finalizeDefinition(
                                         const std::string &,
                                         const std::map< std::string, ParticleFilter > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     )
                                 ),
                                 this,
                                 SLOT(
                                     finalizeParticleFilterDefinition(
                                         const std::string &,
                                         const std::map< std::string, ParticleFilter > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateAttributes(
                                             const std::map<std::string, BaseVariable> &,
                                             const std::map<std::string, DerivedVariable> &,
                                             const std::map<std::string, BaseConstant> &,
                                             const std::map<std::string, CustomConstant> & ) ),
                                 this,
                                 SLOT( updateAttributes(
                                           const std::map<std::string, BaseVariable> &,
                                           const std::map<std::string, DerivedVariable> &,
                                           const std::map<std::string, BaseConstant> &,
                                           const std::map<std::string, CustomConstant> & ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateUserDataDescription(
                                             const std::string & ) ),
                                 this,
                                 SLOT( updateUserDataDescription(
                                           const std::string & ) ) );

                        inserted.first->second->update(
                            m_currentConfigurations.particleBasedConfiguration.m_particleFilters.find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().customConstants(),
                            m_dataSetManager->particleDataManager().baseConstants(),
                            m_dataSetManager->particleDataManager().fullTimeSeries(),
                            m_dataSetManager->particleDataManager().realTime(),
                            m_dataSetManager->particleDataManager().simTime() );
                        inserted.first->second->show();
                    }
                }
            }
        }
    }
    if( m_editFilterButton.isPressed() )
    {
        m_editFilterButton.setPressed( false );

        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( m_editFilterButton.pointInViewPort( pos ) )
            {
                if( m_datasetCombo.selectedItemText() == "" )
                {
                    return;
                }

                auto it = m_particleFilterDialogues.find( m_particleCombo.selectedItemText() );
                if( it == m_particleFilterDialogues.end() )
                {
                    //////////////////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                }
                else
                {
                    auto it2 = it->second.find( m_filterCombo.selectedItemText() );
                    if( it2 == it->second.end() )
                    {
                        //////////////////////////////qDebug()  << "error pdf is not mapped";
                    }
                    else
                    {
                        it2->second->show();
                    }
                }
            }
        }
    }
    if( m_defineSubsetButton.isPressed() )
    {
        m_defineSubsetButton.setPressed( false );
        if( m_defineSubsetButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_subsetDialogOpen )
            {
                return;
            }
            m_subsetDialogOpen = true;

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_particleCombo.numItems() > 0 )
                {
                    std::string currentParticleType = m_particleCombo.selectedItemText();

                    auto it = m_subsetDialogs.find( currentParticleType );
                    if( it == m_subsetDialogs.end() )
                    {
                    }
                    else
                    {
                        SubsetDialog *dialog = new SubsetDialog( "temp", this );
                        auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                        connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelParticleSubsetDefinition() ) );
                        connect( inserted.first->second,
                                 SIGNAL(
                                     finalizeDefinition(
                                         const std::string &,
                                         const std::map< std::string, SubsetDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     )
                                 ),
                                 this,
                                 SLOT(
                                     finalizeParticleSubsetDefinition(
                                         const std::string &,
                                         const std::map< std::string, SubsetDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateAttributes(
                                             const std::map<std::string, BaseVariable> &,
                                             const std::map<std::string, DerivedVariable> &,
                                             const std::map<std::string, BaseConstant> &,
                                             const std::map<std::string, CustomConstant> & ) ),
                                 this,
                                 SLOT( updateAttributes(
                                           const std::map<std::string, BaseVariable> &,
                                           const std::map<std::string, DerivedVariable> &,
                                           const std::map<std::string, BaseConstant> &,
                                           const std::map<std::string, CustomConstant> & ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateUserDataDescription(
                                             const std::string & ) ),
                                 this,
                                 SLOT( updateUserDataDescription(
                                           const std::string & ) ) );

                        inserted.first->second->update(
                            m_currentConfigurations.particleBasedConfiguration.m_subsetDefinitions.find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().customConstants(),
                            m_dataSetManager->particleDataManager().baseConstants(),
                            m_dataSetManager->particleDataManager().fullTimeSeries(),
                            m_dataSetManager->particleDataManager().realTime(),
                            m_dataSetManager->particleDataManager().simTime() );
                        inserted.first->second->show();
                    }
                }
            }
        }
    }

    for( int i = 0; i < m_editSubsetButtons.size(); ++i )
    {
        if( m_editSubsetButtons[ i ].isPressed() )
        {
            m_editSubsetButtons[ i ].setPressed( false );

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_editSubsetButtons[ i ].pointInViewPort( pos ) )
                {
                    if( m_datasetCombo.selectedItemText() == "" )
                    {
                        return;
                    }

                    auto it = m_subsetDialogs.find( m_particleCombo.selectedItemText() );
                    if( it == m_subsetDialogs.end() )
                    {

                    }
                    else
                    {
                        auto it2 = it->second.find( m_subsetCombo.items()[ i ] );
                        if( it2 == it->second.end() )
                        {

                        }
                        else
                        {
                            it2->second->show();
                        }
                    }
                }
            }
        }
    }
    if( m_definePlotButton.isPressed() )
    {
        m_definePlotButton.setPressed( false );
        if( m_definePlotButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_timePlotDialogOpen )
            {
                return;
            }
            m_timePlotDialogOpen = true;

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_particleCombo.numItems() > 0 )
                {
                    std::string currentParticleType = m_particleCombo.selectedItemText();

                    auto it = m_timePlotDefinitionDialogues.find( currentParticleType );
                    if( it == m_timePlotDefinitionDialogues.end() )
                    {
                        //////////////////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped define phase plot button";
                    }
                    else
                    {
                        TimePlotDefinitionDialogue *dialog = new TimePlotDefinitionDialogue( "temp", this );
                        auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                        connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimePlotDefinition() ) );
                        connect( inserted.first->second,
                                 SIGNAL(
                                     finalizeDefinition(
                                         const std::string &,
                                         const std::map< std::string, TimePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     )
                                 ),
                                 this,
                                 SLOT(
                                     finalizeTimePlotDefinition(
                                         const std::string &,
                                         const std::map< std::string, TimePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     ) ) );

                        connect( inserted.first->second,
                                 SIGNAL( updateAttributes(
                                             const std::map<std::string, BaseVariable> &,
                                             const std::map<std::string, DerivedVariable> &,
                                             const std::map<std::string, BaseConstant> &,
                                             const std::map<std::string, CustomConstant> & ) ),
                                 this,
                                 SLOT( updateAttributes(
                                           const std::map<std::string, BaseVariable> &,
                                           const std::map<std::string, DerivedVariable> &,
                                           const std::map<std::string, BaseConstant> &,
                                           const std::map<std::string, CustomConstant> & ) ) );

                        connect( inserted.first->second,
                                 SIGNAL( updateUserDataDescription(
                                             const std::string & ) ),
                                 this,
                                 SLOT( updateUserDataDescription(
                                           const std::string & ) ) );

                        inserted.first->second->update(
                            m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().customConstants(),
                            m_dataSetManager->particleDataManager().baseConstants() );
                        inserted.first->second->show();
                    }
                }
            }
        }
    }

    if( m_defineTimeSeriesButton.isPressed() )
    {
        m_defineTimeSeriesButton.setPressed( false );
        if( m_defineTimeSeriesButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_timeSeriesDialogOpen )
            {
                return;
            }
            m_timeSeriesDialogOpen = true;

            if( m_particleCombo.numItems() > 0 )
            {
                std::string currentParticleType = m_particleCombo.selectedItemText();

                auto it = m_timeSeriesDefinitionDialogues.find( currentParticleType );
                if( it == m_timeSeriesDefinitionDialogues.end() )
                {
                    //////////////////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped define time series button";
                }
                else
                {
                    TimeSeriesDefinitionDialogue *dialog = new TimeSeriesDefinitionDialogue( "temp", this );
                    auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                    connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelTimeSeriesDefinition() ) );
                    connect( inserted.first->second,
                             SIGNAL(
                                 finalizeDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 )
                             ),
                             this,
                             SLOT(
                                 finalizeTimeSeriesDefinition(
                                     const std::string &,
                                     const std::map< std::string, TimeSeriesDefinition > &
                                 ) ) );
                    //
                    // need full time series, real time and sim time

                    inserted.first->second->update(
                        m_currentConfigurations.timeSeriesDefinitions().find( currentParticleType )->second,
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().fullTimeSeries()
                        :  m_dataSetManager->distributionManager().fullTimeSeries(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().realTime()
                        :  m_dataSetManager->distributionManager().realTime(),
                        m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().simTime()
                        :  m_dataSetManager->distributionManager().simTime() );
                    inserted.first->second->show();
                }
            }
        }
    }

    if( m_deletePhasePlotButton.isPressed() )
    {
        m_deletePhasePlotButton.setPressed( false );
        if( m_deletePhasePlotButton.pointInViewPort( pos ) )
        {

        }
    }

    if( m_definePhasePlotButton.isPressed() )
    {
        m_definePhasePlotButton.setPressed( false );
        if( m_definePhasePlotButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            if( m_phasePlotDialogOpen )
            {
                return;
            }
            m_phasePlotDialogOpen = true;

            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                if( m_particleCombo.numItems() > 0 )
                {
                    std::string currentParticleType = m_particleCombo.selectedItemText();

                    auto it = m_phasePlotDefinitionDialogues.find( currentParticleType );
                    if( it == m_phasePlotDefinitionDialogues.end() )
                    {
                        //////////////////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped define phase plot button";
                    }
                    else
                    {
                        PhasePlotDefinitionDialogue *dialog = new PhasePlotDefinitionDialogue( "temp", this );
                        auto inserted = it->second.insert( std::make_pair( "temp", dialog ) );
                        connect( inserted.first->second, SIGNAL( cancelDefinition() ), this, SLOT(cancelPhasePlotDefinition() ) );
                        connect( inserted.first->second,
                                 SIGNAL(
                                     finalizeDefinition(
                                         const std::string &,
                                         const std::map< std::string, PhasePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     )
                                 ),
                                 this,
                                 SLOT(
                                     finalizePhasePlotDefinition(
                                         const std::string &,
                                         const std::map< std::string, PhasePlotDefinition > &,
                                         const std::map< std::string, BaseVariable > &,
                                         const std::map< std::string, DerivedVariable > &,
                                         const std::map< std::string, BaseConstant > &,
                                         const std::map< std::string, CustomConstant > &
                                     ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateAttributes(
                                             const std::map<std::string, BaseVariable> &,
                                             const std::map<std::string, DerivedVariable> &,
                                             const std::map<std::string, BaseConstant> &,
                                             const std::map<std::string, CustomConstant> & ) ),
                                 this,
                                 SLOT( updateAttributes(
                                           const std::map<std::string, BaseVariable> &,
                                           const std::map<std::string, DerivedVariable> &,
                                           const std::map<std::string, BaseConstant> &,
                                           const std::map<std::string, CustomConstant> & ) ) );
                        connect( inserted.first->second,
                                 SIGNAL( updateUserDataDescription(
                                             const std::string & ) ),
                                 this,
                                 SLOT( updateUserDataDescription(
                                           const std::string & ) ) );

                        inserted.first->second->update(
                            m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().baseVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().derivedVariables().find( currentParticleType )->second,
                            m_dataSetManager->particleDataManager().customConstants(),
                            m_dataSetManager->particleDataManager().baseConstants() );
                        inserted.first->second->show();
                    }
                }
            }
        }
    }
    if( m_inspectWarningsButton.isPressed() )
    {
        m_inspectWarningsButton.setPressed( false );
        if( m_inspectWarningsButton.pointInViewPort( pos ) )
        {

        }
    }
    if( m_editPhasePlotButton.isPressed() )
    {
        m_editPhasePlotButton.setPressed( false );
        m_editPhasePlotButton.setPressed( false );
        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( m_editPhasePlotButton.pointInViewPort( pos ) )
            {
                if( m_datasetCombo.selectedItemText() == "" )
                {
                    return;
                }

                auto it = m_phasePlotDefinitionDialogues.find( m_particleCombo.selectedItemText() );
                if( it == m_phasePlotDefinitionDialogues.end() )
                {
                    //////////////////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                }
                else
                {
                    auto it2 = it->second.find( m_phasePlotCombo.selectedItemText() );
                    if( it2 == it->second.end() )
                    {
                        //////////////////////////////qDebug()  << "error pdf is not mapped";
                    }
                    else
                    {
                        it2->second->show();
                    }
                }
            }
        }
    }
    if( m_editTimePlotButton.isPressed() )
    {
        m_editTimePlotButton.setPressed( false );
        if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
        {
            if( m_editTimePlotButton.pointInViewPort( pos ) )
            {
                if( m_datasetCombo.selectedItemText() == "" )
                {
                    return;
                }

                auto it = m_timePlotDefinitionDialogues.find( m_particleCombo.selectedItemText() );
                if( it == m_timePlotDefinitionDialogues.end() )
                {
                    //////////////////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
                }
                else
                {
                    auto it2 = it->second.find( m_timePlotCombo.selectedItemText() );
                    if( it2 == it->second.end() )
                    {
                        //////////////////////////////qDebug()  << "error pdf is not mapped";
                    }
                    else
                    {
                        it2->second->show();
                    }
                }
            }
        }
    }
    if( m_editTimeSeriesButton.isPressed() )
    {
        m_editTimeSeriesButton.setPressed( false );
        if( m_editTimeSeriesButton.pointInViewPort( pos ) )
        {
            if( m_datasetCombo.selectedItemText() == "" )
            {
                return;
            }

            auto it = m_timeSeriesDefinitionDialogues.find( m_particleCombo.selectedItemText() );
            if( it == m_timeSeriesDefinitionDialogues.end() )
            {
                ////////////////qDebug()  << "something is wrong, particle data is listed, but no dialogue is mapped, edit hist button, distribution";
            }
            else
            {
                auto it2 = it->second.find( m_timeSeriesCombo.selectedItemText() );
                if( it2 == it->second.end() )
                {
                    ////////////////qDebug()  << "error pdf is not mapped";
                }
                else
                {
                    it2->second->show();
                }
            }
        }
    }

    if( m_resolutionSlider.isPressed() )
    {
        m_resolutionSlider.setPressed( false );
        m_phasePlotsValid = false;
        renderLater();
    }

    if( m_thresholdSlider.isPressed() )
    {
        m_thresholdSlider.setPressed( false );
        renderLater();
    }

    if( m_recurrenceSlider.isPressed() )
    {
        m_recurrenceSlider.setPressed( false );

        // update the recurrence plot
    }

    if( m_recurrenceView.epsilonSlider.isPressed() )
    {
        m_recurrenceView.epsilonSlider.setPressed( false );

        // update the recurrence plot
    }

    // resize widgets

    m_resizeWidgetA.setPressed( false );
    m_resizeWidgetB.setPressed( false );
    m_resizeWidgetD.setPressed( false );

    /////////////////////////////////////////////

    // help button widgets

    for( HelpLabelWidget * hl : m_helpLabelWidgetRefs )
    {
        if( hl->pointInViewPort( pos ) )
        {
            if( hl->isPressed() )
            {
                QDir dir( QCoreApplication::applicationDirPath() );
                QString link = dir.absoluteFilePath( ( RELATIVE_PATH_PREFIX + "UserGuide/index.html" ).c_str() );// + hl->htmlReference();
                link.replace( " ", "\ " );
              QUrl url = QUrl( "file:///" + link + hl->htmlReference().c_str(), QUrl::TolerantMode );
//                url.setFragment( hl->htmlReference().c_str(), QUrl::TolerantMode );
                              QDesktopServices::openUrl( url );
            }
        }
        hl->setPressed( false );
    }

    renderLater();
}

void BALEEN::cancelPhasePlotDefinition()
{
    std::string ptype = m_particleCombo.selectedItemText();
    if( ptype == "" )
    {
        return;
    }

    if( m_phasePlotDefinitionDialogues.find( ptype ) == m_phasePlotDefinitionDialogues.end() )
    {
        return;
    }
    auto & currentDialogsForCurrentParticle = m_phasePlotDefinitionDialogues.find( ptype )->second;
    if( currentDialogsForCurrentParticle.find( "temp" ) == currentDialogsForCurrentParticle.end() )
    {
        return;
    }
    else
    {
        currentDialogsForCurrentParticle.find( "temp" )->second->deleteLater();
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_phasePlotDialogOpen = false;
}

void BALEEN::finalizePhasePlotDefinition(
    const std::string & plotKey,
    const std::map<std::string, PhasePlotDefinition > & phasePlotDefinitions,
    const std::map<std::string, BaseVariable> & baseVars,
    const std::map<std::string, DerivedVariable> & derivedVars,
    const std::map<std::string, BaseConstant> & baseConsts,
    const std::map<std::string, CustomConstant> & custConsts )
{
    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_phasePlotDefinitionDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( plotKey ) == currentDialogsForCurrentParticle.end() )
    {
        PhasePlotDefinitionDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { plotKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype )->second = phasePlotDefinitions;

    updateBaseVariables( ptype, baseVars );
    updateDerivations( ptype, derivedVars );
    updateCustomConstants( custConsts );
    updateBaseConstants( baseConsts );

    m_phasePlotCombo.clear();

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype )->second )
    {
        int idx = m_phasePlotCombo.addItem( h.second.name );
        if( h.second.name == plotKey )
        {
            comboIdx = idx;
        }
    }
    if( m_phasePlotCombo.numItems() )
    {
        m_phasePlotCombo.selectItem( comboIdx );
    }

//    PhasePlotDefinition & def = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype )->second.find( plotKey )->second;

    m_phasePlotDialogOpen = false;
}

void BALEEN::finalizeTimeSeriesDefinition(
    const std::string & seriesKey,
    const std::map<std::string, TimeSeriesDefinition > & timeSeriesDefinitions )
{
    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_timeSeriesDefinitionDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( seriesKey ) == currentDialogsForCurrentParticle.end() )
    {
        TimeSeriesDefinitionDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { seriesKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.timeSeriesDefinitions().find( ptype )->second = timeSeriesDefinitions;

    //need a combo for the plot separate from the 'mode'
    m_timeSeriesCombo.clear();

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.timeSeriesDefinitions().find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_timeSeriesCombo.addItem( h.second.name );
        if( h.second.name == seriesKey )
        {
            comboIdx = idx;
        }
    }
    if( m_timeSeriesCombo.numItems() )
    {
        m_timeSeriesCombo.selectItem( comboIdx );
    }

    m_recalculateFullTimePlot = true;
    m_timeSeriesDialogOpen = false;
    renderLater();
}

void BALEEN::finalizeTimePlotDefinition(
    const std::string & plotKey,
    const std::map<std::string, TimePlotDefinition > & timePlotDefinitions,
    const std::map<std::string, BaseVariable> & baseVars,
    const std::map<std::string, DerivedVariable> & derivedVars,
    const std::map<std::string, BaseConstant> & baseConsts,
    const std::map<std::string, CustomConstant> & custConsts )
{
    const std::string & ptype = m_particleCombo.selectedItemText();

    auto & currentDialogsForCurrentParticle = ( m_timePlotDefinitionDialogues.find( ptype )->second );
    if( currentDialogsForCurrentParticle.find( plotKey ) == currentDialogsForCurrentParticle.end() )
    {
        TimePlotDefinitionDialogue *d = currentDialogsForCurrentParticle.find( "temp" )->second;
        currentDialogsForCurrentParticle.insert( { plotKey, d } );
        currentDialogsForCurrentParticle.erase( "temp" );
    }

    m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( ptype )->second =  timePlotDefinitions;

    updateBaseVariables( ptype, baseVars );
    updateDerivations( ptype, derivedVars );
    updateCustomConstants( custConsts );
    updateBaseConstants( baseConsts );

    m_timePlotCombo.clear();

    int comboIdx = 0;
    for( auto & h : m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( m_particleCombo.selectedItemText() )->second )
    {
        int idx = m_timePlotCombo.addItem( h.second.name );
        if( h.second.name == plotKey )
        {
            comboIdx = idx;
        }
    }
    if( m_timePlotCombo.numItems() )
    {
        m_timePlotCombo.selectItem( comboIdx );
    }

    m_recalculateFullTimePlot = true;
    m_timePlotDialogOpen = false;
}

void BALEEN::finalizeProjection( std::vector< std::string > projection, bool preserveAspect )
{
    SpacePartitioningDefinition * sp;
    const std::string & ptype = m_particleCombo.selectedItemText();

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        sp = & ( m_currentConfigurations.particleBasedConfiguration.m_spacePartitioning.find( ptype )->second );
    }
    else
    {
        sp = & ( m_currentConfigurations.distributionBasedConfiguration.m_spacePartitioning.find( ptype )->second );
    }

    sp->attributes.clear();
    sp->scaling.clear();

    for( const auto & vKey : projection )
    {
        sp->attributes.push_back( vKey );
        sp->scaling.push_back( 1.0 );
    }

    sp->preserveAspectRatio = preserveAspect;
}

void BALEEN::mouseMoveEvent(QMouseEvent *e)
{
    QPointF mouseCurr = e->localPos() * devicePixelRatio();
    Vec2< float > pos1( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y()  );

    QVector2D mouseDir =
        QVector2D((mouseCurr - m_previousMousePosition )
                  / ( height()));

    mouseDir.setY(-mouseDir.y());

    m_previousMousePosition = mouseCurr;

//    ViewPort vpTT;
//    vpTT.setPosition( m_trajectoryView.position().x(), m_trajectoryView.position().y() );
//    vpTT.setSize( m_trajectoryView.size().x() - m_trajectoryView.SCROLL_BAR_HEIGHT, m_trajectoryView.size().y() - 60 + BUTTON_HEIGHT );

//    if( m_selectedPlot !=  "" )
//    {
//        if( vpTT.pointInViewPort( pos1 ) )
//        {
//            float xRel = ( pos1.x() - vpTT.offsetX() ) / vpTT.width();
//            float yRel = ( vpTT.offsetY() + vpTT.height() - pos1.y() ) / vpTT.height();

//            int col = std::min( std::floor( xRel * m_trajectoryViewNCols ), m_trajectoryViewNCols - 1.f );
//            int row = std::min( std::floor( yRel * m_trajectoryViewNRows ), m_trajectoryViewNRows - 1.f );

//            int idx = row * m_trajectoryViewNCols + col;
//            if( idx < m_particleSelectionSample.size() )
//            {
//                m_selectedParticle = m_particleSelectionSample[ idx ];
//                //////////qDebug()  << "in mouse move, m_selectedParticle = " << m_selectedParticle;
//            }

//            std::fill( m_particleSelectionSubSampleCounts.begin(), m_particleSelectionSubSampleCounts.end(), 0 );
//            m_particleSelectionSubSampleCounts[ m_selectedParticle ] = getTimeSeries( m_particleCombo.selectedItemText() )->numSteps();
//        }
//    }

    // sliders

    bool sliderDown = false;

    if( m_recurrenceSlider.isPressed() )
    {
        sliderDown = true;
        m_recurrenceSlider.translate( pos1 );
    }

    if( m_thresholdSlider.isPressed() )
    {
        sliderDown = true;
        m_thresholdSlider.translate( pos1 );
    }

    if( m_resolutionSlider.isPressed() )
    {
        sliderDown = true;
        m_resolutionSlider.translate( pos1 );
    }

    if( m_recurrenceView.epsilonSlider.isPressed() )
    {
        sliderDown = true;
        m_recurrenceView.epsilonSlider.translate( pos1 );
    }

    const float viewSpaceWidth = scaledWidth();
    const float viewSpaceHeight = ( scaledHeight() - MENU_HEIGHT );

    QApplication::restoreOverrideCursor();

    if( m_plotGridView.scrollBar().update( pos1 ) )
    {
        renderLater();
        return;
    }

    if( m_selectionView.scrollBar().update( pos1 ) )
    {
        renderLater();
        return;
    }

    // resize widgets

    if(  m_resizeWidgetA.pointInViewPort( pos1 )
            || m_resizeWidgetB.pointInViewPort( pos1 ) )
    {
        QApplication::setOverrideCursor( Qt::SizeHorCursor );
    }
    else if(  m_resizeWidgetD.pointInViewPort( pos1 ) )
    {
        QApplication::setOverrideCursor( Qt::SizeVerCursor );
    }

    if( /*m_resizeWidgetA.isPressed()*/ false )
    {
        double detailViewMax = std::min( DETAIVIEW_MAX_WIDTH, scaledWidth()
                                         - SAMPLER_MIN_WIDTH - PHASEVIEW_MIN_WIDTH - DIVIDER_WIDTH * 2 - 2 );
        float rzOffst = std::min( std::max( (double) pos1.x(), DETAILVIEW_MIN_WIDTH ), detailViewMax );

        if( 1 )
        {
            m_resizeWidgetA.setPosition( rzOffst, m_resizeWidgetA.position().y() );
            m_resizeWidgetD.setPosition( rzOffst + DIVIDER_WIDTH, m_resizeWidgetD.position().y() );


            if( m_resizeWidgetB.position().x() - m_resizeWidgetA.position().x() < SAMPLER_MIN_WIDTH )
            {
                //////////////////////qDebug()  << "reset part B to " << m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH;
                m_resizeWidgetB.setPosition( m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH, m_resizeWidgetB.position().y() );
            }

            m_uiLayoutEdited = true;
            m_uiLayoutInitialized = true;
        }
    }
    else if ( m_resizeWidgetB.isPressed() )
    {
        float rzOffst = pos1.x();

        if( viewSpaceWidth - rzOffst < PHASEVIEW_MIN_WIDTH)
        {
            rzOffst = viewSpaceWidth - PHASEVIEW_MIN_WIDTH;
        }

        //////////////////////qDebug()  << "reset B to " << rzOffst;
        m_resizeWidgetB.setPosition( rzOffst, m_resizeWidgetB.position().y() );

        if( m_resizeWidgetB.position().x() - m_resizeWidgetA.position().x()  < SAMPLER_MIN_WIDTH )
        {
            //////////////////////qDebug()  << "set B to  " << m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH;
            m_resizeWidgetB.setPosition( m_resizeWidgetA.position().x() + SAMPLER_MIN_WIDTH, m_resizeWidgetB.position().y() );
        }

        m_uiLayoutEdited = true;
        m_uiLayoutInitialized = true;
    }
    else if ( m_resizeWidgetD.isPressed() )
    {
        float rzOffst = pos1.y();

//        if( scaledHeight() - rzOffst - m_histogramViewPort.height() - MENU_HEIGHT
//                - DIVIDER_WIDTH * 2 - m_sampler.CONTROL_PANEL_HEIGHT < MINIMAP_MIN_HEIGHT )
//        {
//            rzOffst = scaledHeight() - m_histogramViewPort.height()
//                      - MENU_HEIGHT - DIVIDER_WIDTH * 2 - m_sampler.CONTROL_PANEL_HEIGHT - MINIMAP_MIN_HEIGHT - 2;
//        }
        if( rzOffst < TIMELINE_MIN_HEIGHT )
        {
            rzOffst = TIMELINE_MIN_HEIGHT;
        }
        if( viewSpaceHeight - rzOffst < SAMPLER_MIN_HEIGHT + DIVIDER_WIDTH + ISOVIEW_MIN_HEIGHT )
        {
            rzOffst = viewSpaceHeight - ( SAMPLER_MIN_HEIGHT + DIVIDER_WIDTH + ISOVIEW_MIN_HEIGHT );
        }

        m_resizeWidgetD.setPosition( m_resizeWidgetD.position().x(), rzOffst );

        m_resizeWidgetB.setPosition( m_resizeWidgetB.position().x(), m_resizeWidgetD.position().y() + DIVIDER_WIDTH );
        m_resizeWidgetB.setSize( m_resizeWidgetB.size().x(), scaledHeight() - m_resizeWidgetD.position().y()
                                 - MENU_HEIGHT - DIVIDER_WIDTH );

        m_uiLayoutEdited = true;
        m_uiLayoutInitialized = true;
    }

    ///////////

    if (e->buttons() & Qt::RightButton)
    {

    }
    if (e->buttons() & Qt::LeftButton && sliderDown == false
            && ! ( m_resizeWidgetA.isPressed()
                   || m_resizeWidgetB.isPressed()
                   || m_resizeWidgetD.isPressed() ) )
    {
        if( m_timeLineWidget.pointInViewPort( pos1 + Vec2< float >( -m_timeLineWidget.MARGIN_LEFT,  0 ) )
                && m_timeLineWidget.pointInViewPort( pos1 + Vec2< float >(  m_timeLineWidget.MARGIN_RIGHT, 0 ) ) )
        {
            int offX = pos1.x() - ( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT );

            TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                        : m_dataSetManager->distributionManager().currentTimeSeries();
            double relative = ( offX / ( double ) m_timeLineWidget.plotSize().x() );
            int intTStep = ( std::max( std::min( static_cast< int32_t >( std::round( tsrs.firstIdx + relative * ( tsrs.lastIdx - tsrs.firstIdx ) ) ), tsrs.lastIdx ), tsrs.firstIdx ) );
            int diffStride = ( intTStep - tsrs.firstIdx ) % tsrs.idxStride;
            if( diffStride != 0 )
            {
                if( diffStride > tsrs.idxStride / 2.0 )
                {
                    intTStep += tsrs.idxStride - diffStride;
                }
                else
                {
                    intTStep -= diffStride;
                }
            }

            m_selectedTimeStep = intTStep;
            m_timeLineWidget.setTimeStep( m_selectedTimeStep );
            renderLater();
        }

        for( auto & rw : m_rangeView.rangeWidgets )
        {
            if( rw.first.pointInViewPort( pos1 ) )
            {
                rw.first.mouseDrag( pos1 );
                rw.second.mouseDrag( pos1 );
            }
            if( rw.second.pointInViewPort( pos1 ) )
            {
                rw.first.mouseDrag( pos1 );
                rw.second.mouseDrag( pos1 );
            }
        }
    }

    QVector3D c1 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1, 1, 0 );
    QVector3D c2 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1, 1, 0 );
    QVector3D c3 = ( camera.proj()* camera.view() ).inverted() * QVector3D(  1,-1, 0 );
    QVector3D c4 = ( camera.proj()* camera.view() ).inverted() * QVector3D( -1,-1, 0 );

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    QVector3D pos;

    float w = scaledWidth();
    float h = scaledHeight();

    pos.setX( (     mouseCurr.x() - w / 2 )   / ( w / 2 ) );
    pos.setY( ( - ( mouseCurr.y() - h / 2 ) ) / ( h / 2 ) );
    pos.setZ( 0 );
    pos = ( camera.proj() * camera.view() ).inverted() * pos;

    //////////////////////////////////////////////////////////////////////////////////////////////

    // Combos

    for( auto comboPtr : m_comboRefs )
    {
        if( (*comboPtr).isPressed() )
        {
            int id = (*comboPtr).mousedOver( pos1 );
            if( id == -1 )
            {
                (*comboPtr).setPressed( false );
            }
            renderLater();
            return;
        }
    }

    renderLater();
}

void BALEEN::regenerateCombinationShader( const std::string & expression )
{
    const std::string SHADER_PATH =
        std::string( "../Platform/" ) +
        ( VIDI_GL_MAJOR_VERSION == 3 ? "GL3" : "GL4" ) +
        std::string( "/shaders" );

    auto start = std::chrono::system_clock::now();

    m_densityProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_densityProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS.vert" ).c_str() );

    m_densityProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMap2.frag" ).c_str() );

    m_densityProgram->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/LineMap.geom" ).c_str() );

    if( ! m_densityProgram->link())
    {
        ////qDebug()  << "lineMap 1 Program linkage failed";
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "compile shader time: "  << elapsed_seconds.count() << "s\n";

    ///////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////

    m_varianceProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_varianceProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS2.vert" ).c_str() );

    m_varianceProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMapVariance.frag" ).c_str() );

    m_varianceProgram->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/LineMapVariance.geom" ).c_str() );

    if( ! m_varianceProgram->link())
    {
        ////qDebug()  << "lineMap 2 Program linkage failed";
    }

    ////////////////////////////////////////////////

    m_summationProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_summationProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS2.vert" ).c_str() );

    m_summationProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMapSummation.frag" ).c_str() );

    m_summationProgram->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/LineMap2W.geom" ).c_str() );

    if( ! m_summationProgram->link())
    {
        ////qDebug()  << "lineMap 3 Program linkage failed";
    }

    ////////////////////////////////////////////////

    m_densityProgramWeighted = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_densityProgramWeighted->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS2.vert" ).c_str() );

    m_densityProgramWeighted->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMapSum2.frag" ).c_str() );

    m_densityProgramWeighted->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/LineMap2W2.geom" ).c_str() );

    if( ! m_densityProgramWeighted->link())
    {
        ////qDebug()  << "lineMap 4 Program linkage failed";
    }

    ////////////////////

    m_minMaxProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_minMaxProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/MinMaxLineMap.vert" ).c_str() );

    m_minMaxProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/MinMaxLineMap.frag" ).c_str() );

    m_minMaxProgram->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/MinMaxLineMap.geom" ).c_str() );

    if( ! m_minMaxProgram->link())
    {
        ////qDebug()  << "lineMap 5 Program linkage failed";
    }

    /////////////////////////////

    m_densityProgramWrapAround = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_densityProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS.vert" ).c_str() );

    m_densityProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMap2.frag" ).c_str() );

    m_densityProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/WrapLine1.geom" ).c_str() );

    if( ! m_densityProgramWrapAround->link())
    {
        ////qDebug()  << "lineMap 5 Program linkage failed";
    }

    ////////////////////

    m_densityProgramWeightedWrapAround = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_densityProgramWeightedWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS2.vert" ).c_str() );

    m_densityProgramWeightedWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMapSum2.frag" ).c_str() );

    m_densityProgramWeightedWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/WrapLine2.geom" ).c_str() );

    if( ! m_densityProgramWeightedWrapAround->link())
    {
        ////qDebug()  << "lineMap 5 Program linkage failed";
    }

//    ////////////////////

    m_summationProgramWrapAround = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_summationProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS2.vert" ).c_str() );

    m_summationProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMapSummation.frag" ).c_str() );

    m_summationProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/WrapLine3.geom" ).c_str() );

    if( ! m_summationProgramWrapAround->link())
    {
        ////qDebug()  << "lineMap 5 Program linkage failed";
    }

    //////////////////////////////////

    m_minMaxProgramWrapAround = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    m_minMaxProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/MinMaxLineMap.vert" ).c_str() );

    m_minMaxProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/MinMaxLineMap.frag" ).c_str() );

    m_minMaxProgramWrapAround->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/WrapLine4.geom" ).c_str() );

    if( ! m_minMaxProgramWrapAround->link())
    {
        ////qDebug()  << "lineMap 5 Program linkage failed";
    }

    /////////////////

    wrapProgramV = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram( this ) );

    wrapProgramV->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/CombVS2.vert" ).c_str() );

    wrapProgramV->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/LineMapVariance.frag" ).c_str() );

    wrapProgramV->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/WrapLineV.geom" ).c_str() );

    if( ! wrapProgramV->link())
    {
        ////qDebug()  << "lineMap 2 Program linkage failed";
    }
}

void BALEEN::compileShaders()
{
    const std::string SHADER_PATH =
        std::string( "../Platform/" ) +
        ( VIDI_GL_MAJOR_VERSION == 3 ? "GL3" : "GL4" ) +
        std::string( "/shaders" );

    timeStackProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    timeStackProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/timeStack.vert" ).c_str() );

    timeStackProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/timeStack.frag" ).c_str() );

    if(!timeStackProg->link())
    {
        //////////////////////////////qDebug()  << "timeStack Program linkage failed";
    }

    ///////////////////////////////////////////////////////////

    cartesianProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    cartesianProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2D.vert" ).c_str() );

    cartesianProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!cartesianProg->link())
    {
        //////////////////////////////qDebug()  << "flat2D Program linkage failed";
    }

    cartesianProg3 = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    cartesianProg3->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2D3.vert" ).c_str() );

    cartesianProg3->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!cartesianProg3->link())
    {
        //////////////////////////////qDebug()  << "flat2D3 Program linkage failed";
    }

    colorMapProg3 = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    colorMapProg3->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/vz.vert" ).c_str() );

    colorMapProg3->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/vz.frag" ).c_str() );

    if(!colorMapProg3->link())
    {
        //////////////////////////////qDebug()  << "vz Program linkage failed";
    }

    points3DProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    points3DProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/points3D.vert" ).c_str() );

    points3DProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/points3D.frag" ).c_str() );

    if(!points3DProg->link())
    {
        //////////////////////////////qDebug()  << "points3D Program linkage failed";
    }

    if( VIDI_GL_MAJOR_VERSION == 4 )
    {
        tubePathProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Vertex,
            ( SHADER_PATH + "/pathtube.vert" ).c_str() );

        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Fragment,
            ( SHADER_PATH + "/pathtube.frag" ).c_str() );

        tubePathProgram->addShaderFromSourceFile(
            QOpenGLShader::Geometry,
            ( SHADER_PATH + "/pathtube.geom" ).c_str() );

        if(!tubePathProgram->link())
        {
            //////////////////////////////qDebug()  << "pathtube Program linkage failed";
        }
    }

    //

    simpleProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    simpleProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/simple.vert" ).c_str() );

    simpleProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2D.frag" ).c_str() );

    if(!simpleProg->link())
    {
        //////////////////////////////qDebug()  << "simple Program linkage failed";
    }

    //

    simpleColorProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    simpleColorProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/simpleColor.vert" ).c_str() );

    simpleColorProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/simpleColor.frag" ).c_str() );

    if(!simpleColorProg->link())
    {
        //////////////////////////////qDebug()  << "simpleColor Program linkage failed";
    }

    //

    prog3D = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    prog3D->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/mesh.vert" ).c_str() );

    prog3D->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/mesh.frag" ).c_str() );

    if(!simpleProg->link())
    {
        //////////////////////////////qDebug()  << "mesh Program linkage failed";
    }

    //

    flatProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat3D.vert" ).c_str() );

    flatProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat3D.frag" ).c_str() );

    if(!flatProg->link())
    {
        //////////////////////////////qDebug()  << "flat3D Program linkage failed";
    }

    //

    flatProgSerial = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatProgSerial->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2DSerial.vert" ).c_str() );

    flatProgSerial->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2DSerial.frag" ).c_str() );

    if(!flatProgSerial->link())
    {
        //////////////////////////////qDebug()  << "flat2DSerial Program linkage failed";
    }

    //

    trajectoryProgramNoWrap = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    trajectoryProgramNoWrap->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/TrajNoWrap.vert" ).c_str() );

    trajectoryProgramNoWrap->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/TrajNoWrap.frag" ).c_str() );

    if(!trajectoryProgramNoWrap->link())
    {
        //qDebug()  << "traj Program linkage failed";
    }

    //

    trajectoryProgramWraparound = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    trajectoryProgramWraparound->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flat2DSerialC.vert" ).c_str() );

    trajectoryProgramWraparound->addShaderFromSourceFile(
        QOpenGLShader::Geometry,
        ( SHADER_PATH + "/TrajectoryWrap.geom" ).c_str() );

    trajectoryProgramWraparound->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flat2DSerialC.frag" ).c_str() );

    if(!trajectoryProgramWraparound->link())
    {
        ////////qDebug()  << "traj Program linkage failed";
    }

    //

    textProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    textProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/text.vert" ).c_str() );

    textProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/text.frag" ).c_str() );

    if(!textProg->link())
    {
        //////////////////////////////qDebug()  << "text Program linkage failed";
    }

    //

    ucProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    ucProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/quadTex.vert" ).c_str() );

    ucProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/quadTex.frag" ).c_str() );

    if(!ucProg->link())
    {
        //////////////////////////////qDebug()  << "quadTex Program linkage failed";
    }

    //

    colorLegendProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    colorLegendProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/quadTex.vert" ).c_str() );

    colorLegendProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/ColorLegend.frag" ).c_str() );

    if(! colorLegendProg->link())
    {
        //////////////////////qDebug()  << "legend Program linkage failed";
    }

    //

    flatMeshProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    flatMeshProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/flatMesh.vert" ).c_str() );

    flatMeshProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/flatMesh.frag" ).c_str() );

    if(!flatMeshProg->link())
    {
        //////////////////////////////qDebug()  << "flatMesh Program linkage failed";
    }

    //

    heatMapGenProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    heatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/heatMapGen.vert" ).c_str() );

    heatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/heatMapGen.frag" ).c_str() );

    if(!heatMapGenProg->link())
    {
        //////////////////////////////qDebug()  << "heatmapgen Program linkage failed";
    }

    //

    focusContextProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    focusContextProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/heatMapRender.vert" ).c_str() );

    focusContextProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/heatMapRender2.frag" ).c_str() );

    if(!heatMapGenProg->link())
    {
        //////////////////////////////qDebug()  << "heatmapgen Program linkage failed";
    }


    //

    pointHeatMapGenProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    pointHeatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/pointHeatMapGen.vert" ).c_str() );

    pointHeatMapGenProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/pointHeatMapGen.frag" ).c_str() );

    if(!pointHeatMapGenProg->link())
    {
        //////////////////////////////qDebug()  << "heatmapgen Program linkage failed";
    }

    ////

    heatMapRenderProg = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    heatMapRenderProg->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/heatMapRender.vert" ).c_str() );

    heatMapRenderProg->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/heatMapRender.frag" ).c_str() );

    if(!heatMapRenderProg->link())
    {
        //////////////////////////////qDebug()  << "flatMesh Program linkage failed";
    }

    //////////////////////////////qDebug()  << "returning";

    binDiagnosticProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    binDiagnosticProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/binDiagnostic.vert" ).c_str() );

    binDiagnosticProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/binDiagnostic.frag" ).c_str() );

    if(!binDiagnosticProgram->link())
    {
        //////////////////////////////qDebug()  << "bin diagnostic Program linkage failed";
    }

    /////////

    textureDivideProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    textureDivideProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/quadTex.vert" ).c_str() );

    textureDivideProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/textureDivide.frag" ).c_str() );

    if(!textureDivideProgram->link())
    {
        //////////////////////////////qDebug()  << "bin diagnostic Program linkage failed";
    }

    ///////////////////

    differenceProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

    differenceProgram->addShaderFromSourceFile(
        QOpenGLShader::Vertex,
        ( SHADER_PATH + "/quadTex.vert" ).c_str() );

    differenceProgram->addShaderFromSourceFile(
        QOpenGLShader::Fragment,
        ( SHADER_PATH + "/difference.frag" ).c_str() );

    if(!differenceProgram->link())
    {
        //////////////////////////////qDebug()  << "bin diagnostic Program linkage failed";
    }


    ///////////////////

//    if( VIDI_GL_MAJOR_VERSION == 4 )
//    {
//        lineMapProgram = std::unique_ptr<QOpenGLShaderProgram>( new QOpenGLShaderProgram(this) );

//        lineMapProgram->addShaderFromSourceFile(
//            QOpenGLShader::Vertex,
//            ( SHADER_PATH + "/LineMap.vert" ).c_str() );


//        lineMapProgram->addShaderFromSourceFile(
//            QOpenGLShader::Fragment,
//            ( SHADER_PATH + "/LineMap.frag" ).c_str() );

//        lineMapProgram->addShaderFromSourceFile(
//            QOpenGLShader::Geometry,
//            ( SHADER_PATH + "/LineMap.geom" ).c_str() );

//        if( ! lineMapProgram->link())
//        {
//            //////////////////////////////qDebug()  << "lineMap Program linkage failed";
//        }
//    }

    if( m_selectedCombination >= 0 )
    {
        regenerateCombinationShader( m_combinationList[ m_selectedCombination ] );
    }
    else
    {
        regenerateCombinationShader( "$" );
    }
}

void BALEEN::initGL()
{
    //////////////////////////////qDebug()  << "initializing OpenGL";

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor( m_dividerColor.x(), m_dividerColor.y(), m_dividerColor.z(), 1.0 );
    glEnable( GL_BLEND );

    //////////////////////////////qDebug()  << "calling compiling the shaders";

    m_statsComputer.initRuntime();
    compileShaders();

    //////////////////////////////qDebug()  << "initializing renderer";

    m_renderer->init();

    //////////////////////////////qDebug()  << "computing pixel angle densities";

    if( VIDI_GL_MAJOR_VERSION == 4 )
    {
        m_renderer->computePixelDensityByAngle( binDiagnosticProgram, m_pixelDensityMap );
    }

    resizeEvent( NULL );

    ////////////////////////////////////////////////////////////////

    m_timeAggregateEquationDefinitions.setTexFromPNG( RELATIVE_PATH_PREFIX + "/textures/latex/description.png", RELATIVE_PATH_PREFIX + "/textures/latex/description.png" );
    m_timeAggregateEquationDefinitions.resizeByHeight( EQUATION_HEIGHT );

    m_timeAggregateEquationDefinitionsAll.setTexFromPNG( RELATIVE_PATH_PREFIX + "/textures/latex/descriptionAll.png", RELATIVE_PATH_PREFIX + "/textures/latex/descriptionAll.png" );
    m_timeAggregateEquationDefinitionsAll.resizeByHeight( EQUATION_HEIGHT );

    //

    const std::string ziP = RELATIVE_PATH_PREFIX + "/textures/zoomIn48.png";
    const std::string zoP = RELATIVE_PATH_PREFIX + "/textures/zoomOut48.png";
    const std::string ziPP = RELATIVE_PATH_PREFIX + "/textures/zoomIn48Press.png";
    const std::string zoPP = RELATIVE_PATH_PREFIX + "/textures/zoomOut48Press.png";
    const std::string gP = RELATIVE_PATH_PREFIX + "/textures/grid.png";
    const std::string gpp = RELATIVE_PATH_PREFIX + "/textures/gridPressed.png";
    const std::string df = RELATIVE_PATH_PREFIX + "/textures/addPlotIcon48.png";
    const std::string dfp = RELATIVE_PATH_PREFIX + "/textures/addPlotIconPressed48.png";

    m_zoomInPhaseViewButton.setTexFromPNG(  ziP, ziPP );
    m_zoomOutPhaseViewButton.setTexFromPNG( zoP, zoPP );
    m_gridOverlayButton.setTexFromPNG( gP, gpp );
    m_definePhasePlotButton.setTexFromPNG( df, dfp );

    //

    aggregator.init();
    aggregator.setPixelAngleTable( m_pixelDensityMap );

    m_phaseVAO = aggregator.getVAO().get();//std::unique_ptr<QOpenGLVertexArrayObject>(new QOpenGLVertexArrayObject());
    m_phaseVBO = aggregator.getVBO().get();//std::unique_ptr<TNR::TNGLBuffer>(new TNR::TNGLBuffer() );

    //m_phaseVBO->setUsagePattern( QOpenGLBuffer::DynamicCopy);
    //m_phaseVAO->create();
    //m_phaseVBO->create();

    /////////////////////////////////////////////////////////////////////////////////////////////

    m_texVAO = std::unique_ptr<QOpenGLVertexArrayObject>(new QOpenGLVertexArrayObject());
    m_texVBO = std::unique_ptr<TNR::TNGLBuffer>( new TNR::TNGLBuffer() );
    m_texVBO->setUsagePattern( QOpenGLBuffer::DynamicCopy);
    m_texVAO->create();
    m_texVBO->create();

    const float quad[] =
    {
        -1.f,  1.f,
        -1.f, -1.f,
        1.f, -1.f,
        -1.f,  1.f,
        1.f, -1.f,
        1.f,  1.f,
    };

    const float texcoords[] =
    {
        0.f, 1.f,
        0.f, 0.f,
        1.f, 0.f,
        0.f, 1.f,
        1.f, 0.f,
        1.f, 1.f
    };

    m_texVAO->bind();
    m_texVBO->bind();

    if ( static_cast< size_t >( m_texVBO->size() ) < 24 * sizeof( float ) )
    {
        m_texVBO->allocate( 24 * sizeof( float ) );
    }

    m_texVBO->write( 0, quad, 12 * sizeof( float ) );
    m_texVBO->write( 12*sizeof(float), texcoords, 12 * sizeof( float ) );

    m_texVBO->release();
    m_texVAO->release();

    /////////////////////////////////////////////////////////////////////////////////////

    m_phasePlotTFAll.create();
    m_phasePlotTFRegion.create();
    m_phasePlotTFRegionBin.create();
    m_phasePlotTFWeightBin.create();

    //loadTF( RELATIVE_PATH_PREFIX + "/TF//sequential2//binary.txt", m_phasePlotTFRegion );
    //loadTF( RELATIVE_PATH_PREFIX + "/TF//sequential//YlGnBu.txt", m_phasePlotTFRegion );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//diverging//Spectral.txt", m_phasePlotTFAll );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//diverging//PiYG.txt", m_phasePlotTFRegion );
    //loadTF( RELATIVE_PATH_PREFIX + "/TF//perceptual//magma.txt", m_phasePlotTFAll );
    loadTF( RELATIVE_PATH_PREFIX + "/TF//perceptual//magma.txt", m_phasePlotTFWeightBin );

    // for now juse use fo the angle to pixel density map
    m_phasePlotCache.initGL();
    m_phasePlotCache.setAngleToPixelDensityTexture( m_pixelDensityMap );
}

TimeSeriesDefinition * BALEEN::getTimeSeries( const std::string & ptype )
{
    std::string seriesKey = m_timeSeriesCombo.selectedItemText();

    const auto it = m_currentConfigurations.timeSeriesDefinitions().find( ptype );
    if( it == m_currentConfigurations.timeSeriesDefinitions().end() )
    {
        return nullptr;
    }

    const auto & d_it = it->second.find( seriesKey );
    if( d_it == it->second.end() )
    {
        return nullptr;
    }

    return & d_it->second;
}

TimePlotDefinition * BALEEN::getTimePlotDefinition( const std::string & ptype )
{
    const std::string & key = m_timePlotCombo.selectedItemText();
    auto it = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( ptype );
    if( it == m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.end() )
    {
        return nullptr;
    }
    auto it2 = it->second.find( key );
    if( it2 == it->second.end() )
    {
        return nullptr;
    }
    return & it2->second;
}

int BALEEN::getStepOffset()
{

    std::int32_t firstTStep = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries().firstIdx
                              : m_dataSetManager->distributionManager().currentTimeSeries().firstIdx;

    std::int32_t tsStride = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries().idxStride
                            : m_dataSetManager->distributionManager().currentTimeSeries().idxStride;

    return ( m_selectedTimeStep - firstTStep ) / tsStride;
}

void BALEEN::preparePhaseTrajectories(
    const std::string & ptype,
    const TimeSeriesDefinition & trsr,
    const PhasePlotDefinition & plotDef )
{
//    const int FIRST = trsr.firstIdx;
//    const int LAST  = trsr.lastIdx;
//    const int STRIDE = trsr.idxStride;
//    const int NT = trsr.numSteps();
//    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

//    m_x0Buffer.resize( NP*NT );
//    m_x1Buffer.resize( NP*NT );

//    const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 0 ] );
//    const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 1 ] );

//    #pragma omp parallel for
//    for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
//    {
//        int ti = ( t - FIRST ) / STRIDE;

//        const std::vector< float > & xPos = *( xAttr[ t ] );
//        const std::vector< float > & yPos = *( yAttr[ t ] );

//        for ( unsigned i = 0; i < NP; ++i )
//        {
//            m_x0Buffer[ i*NT + ti ] = xPos[ i ];
//            m_x1Buffer[ i*NT + ti ] = yPos[ i ];
//        }
//    }

//    m_phaseBufferFirsts.resize( NP );
//    m_phaseBufferCounts.resize( NP );

//    #pragma omp parallel
//    for ( int i = 0; i < NP; ++i )
//    {
//        m_phaseBufferFirsts[ i ] = NT * i;
//        m_phaseBufferCounts[ i ] = NT;
//    }

//    size_t N_BYTES = NP * NT * sizeof( float );

//    m_phaseVAO->bind();
//    m_phaseVBO->bind();

//    if ( static_cast< size_t >( m_phaseVBO->size() ) < N_BYTES*2 )
//    {
//        m_phaseVBO->allocate( N_BYTES*2 );
//    }
//    ////checkError( "allocate phase vbo" );

////    ////////////////////////qDebug()  << m_phaseVBO->size();

//    m_phaseVBO->write( 0, m_x0Buffer.data(), N_BYTES );
//    m_phaseVBO->write( N_BYTES, m_x1Buffer.data(), N_BYTES );

//    m_phaseVBO->release();
//    m_phaseVAO->release();
}

void BALEEN::renderPhasePrimitives(
    int viewMode,
    const std::string & interpolation,
    const std::string & ptype,
    const PhasePlotDefinition & plotDef,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    float w,
    float h,
    float x,
    float y,
    const QVector4D & color )
{
//    glLineWidth( 4.0 );
//    glPointSize( 4.0 );

//    glViewport( x, y, w, h );

//    QMatrix4x4 M;
//    M.setToIdentity();

//    Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 0 ] )->second;
//    Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 1 ]  )->second;

//    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

//    const float xW = ( rX.b() - rX.a() );
//    const float yW = ( rY.b() - rY.a() );

//    // scale to (-1,1)
//    M.scale( 2.0 / xW, 2.0 / yW );

//    // center at (0,0)
//    M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );

//    if( viewMode == PlotViewMode::TRAJECTORIES  )
//    {
//        flatProgSerial->bind();
//        ////checkError( "bind line map program" );

//        GLuint xAttr = flatProgSerial->attributeLocation( "xAttr" );
//        GLuint yAttr = flatProgSerial->attributeLocation( "yAttr" );
//        ////checkError( "get attr locations" );

//        m_phaseVAO->bind();
//        m_phaseVBO->bind();
//        ////checkError( "bind phase vao,vbo" );

//        flatProgSerial->setUniformValue( "color", color );
//        ////checkError( "set uniforms" );

//        flatProgSerial->setUniformValue( "MVP", M );
//        ////checkError( "set uniforms" );

//        flatProgSerial->enableAttributeArray( xAttr );
//        flatProgSerial->enableAttributeArray( yAttr );
//        ////checkError( "enable attributes" );

//        flatProgSerial->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
//        flatProgSerial->setAttributeBuffer( yAttr, GL_FLOAT, m_x0Buffer.size()*sizeof( float ), 1 );
//        ////checkError( "set attrs" );

//        if( interpolation != "None" )
//        {
//            glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
//        }
//        glMultiDrawArrays( GL_POINTS, firsts.data(), counts.data(), NP );

//        ////checkError( "draw multi arrays" );

//        flatProgSerial->disableAttributeArray( xAttr );
//        flatProgSerial->disableAttributeArray( yAttr );
//        ////checkError( "disable attr" );

//        m_phaseVBO->release();
//        m_phaseVAO->release();
//        ////checkError( "release vao, vbo" );

//        flatProgSerial->release();
//        ////checkError( "release program" );
//    }
//    else if( viewMode == PlotViewMode::SCATTER_PLOT )
//    {
////        const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 0 ] );
////        const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 1 ] );
////        glPointSize(3.0);
////        m_renderer->renderPointsAsHeatMap(
////           *( xAttr[ m_selectedTimeStep ] ),
////           *( yAttr[ m_selectedTimeStep ] ),
////           M,
////           pointHeatMapGenProg );
//    }
}

QMatrix4x4 BALEEN::computePhaseTexture(
    PhasePlotCache & cache,
    TNR::TextureLayer & result,
    ParticleDataManager< float > & dataManager,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    int mode )
{
    return QMatrix4x4();
}

// can now generalize this since I can pass the texture layer, as well as the counts array to use to switch between all, region, bin
QMatrix4x4 BALEEN::computePhaseTexture(
    PhasePlotCache & cache,
    TNR::TextureLayer & result,
    int viewMode,
    const std::string & ptype,
    const PhasePlotDefinition & plotDef,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const std::string & interpolation,
    const std::string & blending,
    const std::string & what,
    int cellSelection )
{
//    cache.setTexture( result );
//    ////checkError( "after set texture" );

//    cache.bindFrameBuffer();
//    ////checkError( "after bind frame buffer" );

//    glEnable( GL_BLEND );
//    ////checkError( "after enable blend" );

//    //glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
//    glBlendFunc(GL_ONE, GL_ONE);

//     ////checkError( "after set blend" );

//    glLineWidth( 2.0 );
//    glPointSize( 2.0 );

//    glClearColor( 0.0, 0.0, 0.0, 1.0 );
//    ////checkError( "after set clear color" );

//    glViewport( 0, 0, result.width, result.height );
//    ////checkError( ( "after viewport " + std::to_string( result.width ) + " " + std::to_string( result.height ) ).c_str() );

//    glClear( GL_COLOR_BUFFER_BIT );
//    ////checkError( "clear color bit" );

//    QMatrix4x4 M;
//    M.setToIdentity();

//    Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 0 ] )->second;
//    Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plotDef.attrs[ 1 ]  )->second;

//    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

//    const float xW = ( rX.b() - rX.a() );
//    const float yW = ( rY.b() - rY.a() );

//    // scale to (-1,1)
//    M.scale( 2.0 / xW, 2.0 / yW );

//    // center at (0,0)
//    M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );

//    if( viewMode == PlotViewMode::TRAJECTORIES && interpolation != "None" )
//    {
//        lineMapProgram->bind();
//        ////checkError( "bind line map program" );

//        GLuint xAttr = lineMapProgram->attributeLocation( "xAttr" );
//        GLuint yAttr = lineMapProgram->attributeLocation( "yAttr" );
//        ////checkError( "get attr locations" );

//        m_phaseVAO->bind();
//        m_phaseVBO->bind();
//        ////checkError( "bind phase vao,vbo" );

//        glActiveTexture( GL_TEXTURE0 );
//        cache.angleToPixelDensityTexture.bind();
//        ////checkError( "bind angle to density texture" );

//        lineMapProgram->setUniformValue( "angleToNormalization", 0 );
//        lineMapProgram->setUniformValue( "M", M );
//        lineMapProgram->setUniformValue( "width", static_cast< float >( result.width ) );
//        lineMapProgram->setUniformValue( "height", static_cast< float >( result.height ) );
//        lineMapProgram->setUniformValue( "distanceWeighted", blending == "Number Density" );

//        ////checkError( "set uniforms" );

//        lineMapProgram->enableAttributeArray( xAttr );
//        lineMapProgram->enableAttributeArray( yAttr );
//        ////checkError( "enable attributes" );

//        lineMapProgram->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
//        lineMapProgram->setAttributeBuffer( yAttr, GL_FLOAT, m_x0Buffer.size()*sizeof( float ), 1 );
//        ////checkError( "set attrs" );

//        glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
//        ////checkError( "draw multi arrays" );

//        lineMapProgram->disableAttributeArray( xAttr );
//        lineMapProgram->disableAttributeArray( yAttr );
//        ////checkError( "disable attr" );

//        m_phaseVBO->release();
//        m_phaseVAO->release();
//        ////checkError( "release vao, vbo" );

//        lineMapProgram->release();
//        ////checkError( "release program" );
//    }
//    else if( viewMode == PlotViewMode::TRAJECTORIES )
//    {
//        glEnable( GL_BLEND );
//        glBlendFunc(GL_ONE, GL_ONE);

//        glClearColor( 0.0, 0.0, 0.0, 1.0 );
//        glClear( GL_COLOR_BUFFER_BIT );

//        pointHeatMapGenProg->bind();

//        GLuint xAttr = pointHeatMapGenProg->attributeLocation( "xAttr" );
//        GLuint yAttr = pointHeatMapGenProg->attributeLocation( "yAttr" );

//        GLuint loc = pointHeatMapGenProg->uniformLocation( "MVP" );
//        pointHeatMapGenProg->setUniformValue( loc, M );

//        m_phaseVBO->bind();
//        m_phaseVBO->bind();

//        pointHeatMapGenProg->enableAttributeArray( xAttr );
//        pointHeatMapGenProg->enableAttributeArray( yAttr );

//        pointHeatMapGenProg->setAttributeBuffer( xAttr, GL_FLOAT, 0, 1 );
//        pointHeatMapGenProg->setAttributeBuffer( yAttr, GL_FLOAT, m_x0Buffer.size()*sizeof( float ), 1 );

//        glMultiDrawArrays( GL_POINTS, firsts.data(), counts.data(), counts.size() );

//        m_phaseVBO->release();
//        m_phaseVBO->release();

//        pointHeatMapGenProg->release();
//    }
//    else if( viewMode == PlotViewMode::SCATTER_PLOT )
//    {
//        const std::vector< std::unique_ptr< std::vector< float > > > & xAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 0 ] );
//        const std::vector< std::unique_ptr< std::vector< float > > > & yAttr = m_dataSetManager->particleDataManager().values( ptype, plotDef.attrs[ 1 ] );
//        glPointSize(3.0);

//        if( what == "all" )
//        {
//            m_renderer->renderPointsAsHeatMap(
//               *( xAttr[ m_selectedTimeStep ] ),
//               *( yAttr[ m_selectedTimeStep ] ),
//               M,
//               pointHeatMapGenProg );
//        }
//        else if( what == "hist" )
//        {
//            const std::vector< std::pair< int, unsigned int > > & ptclMap = m_sampler.particleMap();

//            static std::vector< float > sHistSelectX;
//            static std::vector< float > sHistSelectY;

//            sHistSelectX.clear();
//            sHistSelectY.clear();

//            for ( unsigned i = 0, end = ( *( xAttr[ 0 ] ) ).size(); i < end; ++i )
//            {
//                if( ptclMap[ i ].first != cellSelection )
//                {
//                    continue;
//                }

//                sHistSelectX.push_back( ( *( xAttr[ m_selectedTimeStep ] ) )[ i ] );
//                sHistSelectY.push_back( ( *( yAttr[ m_selectedTimeStep ] ) )[ i ] );
//            }

//            m_renderer->renderPointsAsHeatMap(
//               sHistSelectX,
//               sHistSelectY,
//               M,
//               pointHeatMapGenProg );
//        }
//        else if ( what == "bin" )
//        {
//            m_renderer->renderPointsAsHeatMap(
//               *( xAttr[ m_selectedTimeStep ] ),
//               *( yAttr[ m_selectedTimeStep ] ),
//               m_sampler.getHighlightIndices(),
//               M,
//               pointHeatMapGenProg );
//        }
//    }

//    result.computeRange();

//    cache.releaseFrameBuffer();
//    ////checkError( "after release frame buffer" );

//    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
//    glEnable( GL_POINT_SMOOTH );
//    glEnable( GL_LINE_SMOOTH );

//    glViewport( 0, 0, scaledWidth(), scaledHeight() );

//    return M;
}

void BALEEN::renderFocusContext(
    TNR::TextureLayer & layer1,
    TNR::TextureLayer & layer2,
    TNR::Texture1D3 & tf1,
    TNR::Texture1D3 & tf2,
    Vec2< float > & position,
    Vec2< float > & size,
    const Vec2< float > & scissor,
    int which,
    bool localize,
    const Vec2< float > & localCenter,
    const Vec2< float > & localRadius,
    const Vec2< float > &  localRange )
{
    glViewport( position.x(), position.y(), size.x(), size.y() );
    glScissor(  position.x() + scissor.a(), position.y(), size.x() - ( scissor.a() + scissor.b() ), size.y() );
    glEnable( GL_SCISSOR_TEST );

    localize = m_toolCombo.selectedItemText() == "localize" && m_mouseIsPressed;
    bool forground = m_toolCombo.selectedItemText() == "bkg2front" && m_mouseIsPressed;

    m_renderer->renderFocusContextTextures(
        layer1.tex,
        layer2.tex,
        tf1,
        tf2,
        focusContextProg,
        layer1.maxValue * m_thresholdSlider.sliderPosition()* m_thresholdSlider.sliderPosition(),
        layer2.maxValue * m_thresholdSlider.sliderPosition()* m_thresholdSlider.sliderPosition(),
        layer1.minValue * m_thresholdSlider.sliderPosition()* m_thresholdSlider.sliderPosition(),
        layer2.minValue * m_thresholdSlider.sliderPosition()* m_thresholdSlider.sliderPosition(),
        (float ) layer1.width,
        (float)  layer1.height,
        ! m_selectFocusButton.isPressed(),
        which, localize, localCenter, localRadius, localRange, forground );
}

//void BALEEN::renderPhaseTexture(
//    TNR::TextureLayer & layer,
//    TNR::Texture1D3 & tf,
//    bool fade,
//    Vec2< float > & position,
//    Vec2< float > & size,
//    bool outline,
//    bool onlyOutline,
//    const Vec2< float > & scissor )
//{
//    glViewport( position.x(), position.y(), size.x(), size.y() );
//    glScissor(  position.x() + scissor.a(), position.y(), size.x() - ( scissor.a() + scissor.b() ), size.y() );
//    glEnable( GL_SCISSOR_TEST );

//    m_renderer->renderHeatMapTexture( layer.tex, tf, heatMapRenderProg, layer.maxValue, fade, layer.width, layer.height, outline, onlyOutline );
//}

void BALEEN::renderPhasePlotGrid(
    const Vec2< float > & plotSize,
    const Vec2< float > & plotPosition,
    const Vec2< float > & scissor,
    const std::string & xLabel,
    const std::string & yLabel,
    const Vec2< double > & rX,
    const Vec2< double > & rY,
    bool withText,
    bool withClear )
{

    if( withText )
    {
        glViewport( 0, 0, scaledWidth(), scaledHeight() );

        m_renderer->renderText( true,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                xLabel,
                                plotPosition.x() + plotSize.x() / 2.0 - 25,
                                plotPosition.y() - 40,
                                1.0f,
                                m_textColor );

        m_renderer->renderText( true,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                yLabel,
                                plotPosition.x() - TEXT_ROW_HEIGHT - 6,
                                plotPosition.y() + plotSize.y() / 2.0 - 20,
                                1.0,
                                m_textColor ,
                                true );

        const int CHARWIDTH = 7.5;

        for( int c = 0; c <= 2; ++c )
        {
            float p = c / 2.f;
            float value = rX.a() + ( rX.b() - rX.a() ) * p;
            std::string text = to_string_with_precision( value, 3 );
            float offset = c == 2 ? text.length() * CHARWIDTH : c == 0 ? 0 : text.length() * CHARWIDTH / 2.0;

            m_renderer->renderText(
                false,
                textProg,
                scaledWidth(),
                scaledHeight(),
                text,
                plotPosition.x() + plotSize.x() * p - offset,
                plotPosition.y() - 26,
                1.0f,
                m_textColor );
        }

        for( int r = 0; r <= 2; ++r )
        {
            float p = r / 2.f;
            float value = rY.a() + ( rY.b() - rY.a() ) * p;
            std::string text = to_string_with_precision( value, 3 );
            float offset = r == 2.f ? text.length() * CHARWIDTH : r == 0 ? 0 : text.length() * CHARWIDTH / 2.0;

            m_renderer->renderText(
                false,
                textProg,
                scaledWidth(),
                scaledHeight(),
                text,
                plotPosition.x() - 16,
                plotPosition.y() + plotSize.y() * p - offset,
                1.0,
                m_textColor ,
                true );
        }
    }

    int gridSpacing = 12;
    glLineWidth( 1 );
    m_renderer->renderGrid(
        plotSize,
        plotPosition,
        scissor,
        gridSpacing,
        QVector4D( 0.8, 0.8, 0.8, 1 ),
        QVector4D( 1, 1, 1, 1 ),
        cartesianProg,
        simpleColorProg,
        withClear );

    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

void BALEEN::renderExpressionSelector(
    const std::string & expression,
    PressButton & button )
{
    m_renderer->renderPressButton( button, cartesianProg, simpleColorProg );

    if( button.isPressed() )
    {
        glViewport( button.position().x() + button.size().x() - button.size().y(), button.position().y(), button.size().y(), button.size().y() );
        m_renderer->clear( m_combinationColor, cartesianProg );
        m_renderer->renderPopSquare( simpleColorProg );
    }

    const int TEXT_PAD = 8;

    Vec3< float > opColor( 0.2, 0.2, 0.2 );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    std::vector< Vec3< float > > charColors( expression.size(), opColor );
    std::pair< std::vector< Vec2< float > >,
        std::vector< Vec2< float > > > posSize = m_renderer->renderTextMultiColor(
                    true,
                    textProg,
                    scaledWidth(),
                    scaledHeight(),
                    expression,
                    charColors,
                    button.position().x() + TEXT_PAD - 4,
                    button.position().y() + TEXT_PAD,
                    1.0 );
}

void BALEEN::renderExpressionEdit( ExpressionEdit & edit )
{
    glViewport(
        edit.position().x(),
        edit.position().y(),
        edit.size().x(),
        edit.size().y() );

    m_renderer->clearViewPort( edit.viewPort(), cartesianProg );
    m_renderer->renderViewPortBoxShadow( edit.viewPort(), simpleColorProg );

    const int TEXT_PAD = 8;

    Vec3< float > opColor( 0.2, 0.2, 0.2 );
    Vec3< float > setColor( 0.0, 0.0, 0.0 );

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    std::vector< Vec3< float > > charColors;
    for( auto & c : edit.text() )
    {
        bool isSet = false;
        for( auto & p : m_subsetSymbols )
        {
            if( c == p[ 0 ] )
            {
                isSet = true;
                charColors.push_back( setColor );
                break;
            }
        }
        if( ! isSet )
        {
            charColors.push_back( opColor );
        }
    }

    std::pair< std::vector< Vec2< float > >,
        std::vector< Vec2< float > > > posSize = m_renderer->renderTextMultiColor(
                    true,
                    textProg,
                    scaledWidth(),
                    scaledHeight(),
                    edit.text(),
                    charColors,
                    edit.position().x() + TEXT_PAD - 4,
                    edit.position().y() + TEXT_PAD,
                    1.0 );

    if( edit.hasFocus() )
    {
        if( edit.text().size() > 0 )
        {
            int cp = edit.cursorPos();
            float px = posSize.first[ cp ].x() - 3;

            if( edit.text()[ cp ] == ' ' && edit.text().size() > 1 )
            {
                px += 7;
            }

            glViewport(
                px,
                edit.position().y() + 2,
                9,
                edit.size().y() - 2
            );
        }
        else
        {
            glViewport(
                edit.position().x() + TEXT_PAD - 4,
                edit.position().y() + 2,
                6,
                edit.size().y() - 2
            );
        }

        std::vector< Vec2< float > > cur = { { 0, -1 }, { 0, 1 } };
        glLineWidth( 1 );
        m_renderer->renderPrimitivesFlat( cur, cartesianProg, QVector4D( 0.4, 0.4, 0.4, 1.0 ), QMatrix4x4(),  GL_LINES );
    }
}

float BALEEN::renderList(
    const std::vector< std::string >   & itemText,
    const std::vector< Vec3< float > > & itemColorCodes,
    const std::vector< std::string >   & itemSymbols,
    const Vec2< float > & pos,
    float width,
    float itemHeight )
{
    const float PADY = 6;

    int offset = 0;
    for( int i = 0; i < itemText.size(); ++i )
    {
        glViewport( pos.x(), pos.y() - itemHeight * offset, width, itemHeight );
        m_renderer->clear( QVector4D( 1, 1, 1, 1 ), cartesianProg );
        m_renderer->renderPopSquare( simpleColorProg );

        const float colorCodeSize = itemHeight;
        glViewport( pos.x() + width - colorCodeSize, pos.y() - itemHeight * offset, colorCodeSize, itemHeight );
        m_renderer->clear( QVector4D( itemColorCodes[ i ].r(), itemColorCodes[ i ].g(), itemColorCodes[ i ].b(), 1 ), cartesianProg );
        m_renderer->renderPopSquare( simpleColorProg );

        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText(
            true,
            textProg,
            scaledWidth(),
            scaledHeight(),
            itemSymbols[ i ],
            int( pos.x() + width - colorCodeSize + PADY ),
            int( pos.y() - itemHeight * offset + PADY ),
            1.0,
            Vec3< float >( 1, 1, 1 ) );

        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            itemText[ i ],
            int( pos.x() + 8 ),
            int( pos.y() - itemHeight * offset + PADY ),
            1.0,
            m_textColor );

        if( m_removeSubsetButtons.size() <= i )
        {
            m_removeSubsetButtons.push_back( PressButton() );
        }

        m_removeSubsetButtons[ i ].setSize( itemHeight, itemHeight );
        m_removeSubsetButtons[ i ].setPosition( int( pos.x() - itemHeight ), int( pos.y() - itemHeight * offset ) );
        m_renderer->renderPressButton( m_removeSubsetButtons[ i ], cartesianProg, simpleColorProg );

        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            " - ",
            m_removeSubsetButtons[ i ].position().x() + PADY-3,
            m_removeSubsetButtons[ i ].position().y() + PADY, 1.0,
            m_textColor );

        if( m_editSubsetButtons.size() <= i )
        {
            m_editSubsetButtons.push_back( PressButton() );
        }
        m_editSubsetButtons[ i ].setSize( 40, itemHeight );
        m_editSubsetButtons[ i ].setPosition( int( pos.x() + width ), int( pos.y() - itemHeight * offset ) );
        m_renderer->renderPressButton( m_editSubsetButtons[ i ], cartesianProg, simpleColorProg );

        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            " Edit ",
            m_editSubsetButtons[ i ].position().x() + PADY-4,
            m_editSubsetButtons[ i ].position().y() + PADY, 1.0,
            m_textColor );

        offset += 1;
    }

    return pos.y() - itemHeight * offset;
}

void BALEEN::renderComboItems( const ComboWidget & combo )
{
    const int PADY = 8;

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    if( ! combo.isPressed() && combo.numItems() > 0 )
    {
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                combo.items()[ combo.selectedItem() ],
                                combo.position().x() + 8,
                                combo.position().y() + PADY,
                                1.0,
                                m_textColor );
    }
    else if ( combo.numItems() > 0 )
    {
        m_renderer->renderText( false,
                                textProg,
                                scaledWidth(),
                                scaledHeight(),
                                combo.items()[ combo.selectedItem() ],
                                combo.position().x() + 8,
                                combo.position().y() + PADY,
                                1.0,
                                m_textColor );

        int offset = 1;

        for( int i = 0; i < combo.numItems(); ++i )
        {
            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    combo.items()[ i ],
                                    combo.position().x() + 8,
                                    combo.position().y() - combo.size().y() * offset + PADY,
                                    1.0,
                                    m_textColor );

            offset += 1;
        }
    }
}

void BALEEN::renderTimeSeriesInfo( const std::string & ptype )
{
    std::vector< Vec2< float > > lines;

    // MODIFY

    int allStart, allEnd, allStride, currentStart, currentEnd, currentStride;
    allStart = allEnd = allStride = currentStart = currentEnd = currentStride = 0;
    allStart = 0;
    allEnd = m_numTimeSteps;
    allStride = 1;

    TimeSeriesDefinition def;

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        def = m_dataSetManager->particleDataManager().currentTimeSeries();
    }
    else if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        def = m_dataSetManager->distributionManager().currentTimeSeries();
    }
    else
    {
        return;
    }

    currentStart  = def.firstIdx;
    currentEnd    = def.lastIdx;
    currentStride = def.idxStride;

    double allStartReal, allEndReal, allStrideReal, currentStartReal, currentEndReal, currentStrideReal;
    allStartReal = allEndReal = allStrideReal = currentStartReal = currentEndReal = currentStrideReal = 0;

    int allStartSim, allEndSim, allStrideSim, currentStartSim, currentEndSim, currentStrideSim;
    allStartSim = allEndSim = allStrideSim = currentStartSim = currentEndSim = currentStrideSim = 0;

    const std::vector< double > & realTime = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().realTime()
            :  m_dataSetManager->distributionManager().realTime();

    const std::vector< std::int32_t > & simTime = m_populationDataTypeEnum == PopulationDataType::PARTICLES ?  m_dataSetManager->particleDataManager().simTime()
            :  m_dataSetManager->distributionManager().simTime();
    bool rT=false, sT = false;
    if( realTime.size() >= 2 )
    {
        rT = true;
        allStartReal = realTime.front();
        allEndReal = realTime.back();

        // TODO: check if this is const
        allStrideReal = realTime[ 1 ] - realTime[ 0 ];

        currentStartReal  = realTime[ def.firstIdx ];
        currentEndReal    = realTime[ def.lastIdx ];

        // This too...
        currentStrideReal = realTime[ def.lastIdx ] - realTime[ def.lastIdx - def.idxStride ];
    }
    if( simTime.size() >= 2 )
    {
        sT = true;
        allStartSim = simTime.front();
        allEndSim = simTime.back();

        // TODO: check if this is const
        allStrideSim = simTime[ 1 ] - simTime[ 0 ];

        currentStartSim  = simTime[ def.firstIdx ];
        currentEndSim    = simTime[ def.lastIdx ];

        // This too ...
        currentStrideSim = simTime[ def.lastIdx ] - simTime[ def.lastIdx - def.idxStride ];
    }

    const int OFF_TLD = 0;

//    m_renderer->renderText( false,
//                            textProg,
//                            scaledWidth(),
//                            scaledHeight(),
//                            "Dataset",
//                            m_dataSetInfoWidget.position().x() + 10,
//                            m_dataSetInfoWidget.position().y() + m_dataSetInfoWidget.size().y() - 16,
//                            1.0f,
//                            Vec3< float >( .2, .2, .1 ) );

//    m_renderer->renderText( false,
//                            textProg,
//                            scaledWidth(),
//                            scaledHeight(),
//                            "                " + m_dataSetManager->name() + ( m_populationDataTypeEnum == PopulationDataType::PARTICLES && m_particleCombo.selectedItemText() != "" ?
//                                    ( ", " + std::to_string( m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() ) ) + "/"
//                                      + std::to_string(m_dataSetManager->particleDataManager().numParticles( ptype ) ) + " particles" )
//                                    : "" ),
//                            m_dataSetInfoWidget.position().x() + 10,
//                            m_dataSetInfoWidget.position().y() + m_dataSetInfoWidget.size().y() - 16,
//                            1.0f,
//                            Vec3< float >( 0, 0, 0 ) );

//    m_renderer->renderText( false,
//                            textProg,
//                            scaledWidth(),
//                            scaledHeight(),
//                            "Time Series",
//                            m_dataSetInfoWidget.position().x() + 10,
//                            m_dataSetInfoWidget.position().y() + m_dataSetInfoWidget.size().y() - 38,
//                            1.0f,
//                            Vec3< float >( .2, .2, .1 ) );

//    m_renderer->renderText( false,
//                            textProg,
//                            scaledWidth(),
//                            scaledHeight(),
//                            "                      (no post-hoc t.s.-integration)",
//                            m_dataSetInfoWidget.position().x() + 10,
//                            m_dataSetInfoWidget.position().y() + m_dataSetInfoWidget.size().y() - 38,
//                            1.0f,
//                            Vec3< float >( 0, 0, 0 ) );

    float dX = ( m_dataSetInfoWidget.size().x() - 80 ) / 3;
    float dY = 20;
    float offX = m_dataSetInfoWidget.position().x() + 94;
    float offY = m_dataSetInfoWidget.position().y() + m_dataSetInfoWidget.size().y() - OFF_TLD - 14 - dY;

//    lines.push_back( Vec2< float >( offX - 9,   offY - 10 + dY ) );
//    lines.push_back( Vec2< float >( offX - 9, ( offY - 9 - dY*3 ) ) );

//    lines.push_back( Vec2< float >( offX - 116, offY - 9 ) );
//    lines.push_back( Vec2< float >( offX - 9 + dX * 3 - 3, offY - 9 ) );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "first",
//        offX,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        rT ? to_string_with_precision( allStartReal ) : "NA",
//        offX,
//        offY-dY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        sT ? std::to_string( allStartSim ) : "NA",
//        offX,
//        offY-dY*2,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( allStart ),
//        offX,
//        offY-dY*3,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "last",
//        offX + dX,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        rT ? to_string_with_precision( allEndReal ) : "NA",
//        offX + dX,
//        offY-dY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        sT ? std::to_string( allEndSim ) : "NA",
//        offX + dX,
//        offY-dY*2,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( allEnd ),
//        offX + dX,
//        offY-dY*3,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "stride",
//        offX + dX*2,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        rT ? to_string_with_precision( allStrideReal ) : "NA",
//        offX + dX*2,
//        offY - dY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        sT ? std::to_string( allStrideSim ) : "NA",
//        offX + dX*2,
//        offY - dY*2,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( allStride ),
//        offX + dX*2,
//        offY - dY*3,
//        1.0,
//        m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Real Time",
//        m_dataSetInfoWidget.position().x() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Simulation TS",
//        m_dataSetInfoWidget.position().x() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Output TS",
//        m_dataSetInfoWidget.position().x() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    //

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Time",
                            m_dataSetInfoWidget.position().x() + 10,
                            m_dataSetInfoWidget.position().y() + m_dataSetInfoWidget.size().y() - OFF_TLD - dY,
                            1.0f,
                            Vec3< float >( 0, 0, 0 ) );

    offY = m_dataSetInfoWidget.position().y() + m_dataSetInfoWidget.size().y() - OFF_TLD - dY;

    lines.push_back( Vec2< float >( offX - 9,   offY - 10 + dY ) );
    lines.push_back( Vec2< float >( offX - 9, ( offY - 5 - dY*2 ) ) );

    lines.push_back( Vec2< float >( offX - 130, offY - 6 ) );
    lines.push_back( Vec2< float >( offX - 9 + dX * 3 - 6, offY - 6 ) );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "first",
                            offX,
                            offY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            rT ? to_string_with_precision( currentStartReal ) : "NA",
                            offX,
                            offY-dY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            sT ? std::to_string( currentStartSim ) + "/" + std::to_string( allStartSim ): "NA",
                            offX,
                            offY-dY*2,
                            1.0,
                            m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( currentStart ),
//        offX,
//        offY-dY*3,
//        1.0,
//        m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "last",
                            offX + dX,
                            offY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            rT ? to_string_with_precision( currentEndReal ) : "NA",
                            offX + dX,
                            offY - dY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            sT ? std::to_string( currentEndSim ) + "/" + std::to_string( allEndSim ) : "NA",
                            offX + dX,
                            offY - dY*2,
                            1.0,
                            m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( currentEnd ),
//        offX + dX,
//        offY - dY*3,
//        1.0,
//        m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "stride",
                            offX + dX*2,
                            offY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            rT ? to_string_with_precision( currentStrideReal ) : "NA",
                            offX + dX*2,
                            offY - dY,
                            1.0,
                            m_textColor );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            sT ? std::to_string( currentStrideSim ) + "/" + std::to_string( allStrideSim ): "NA",
                            offX + dX*2,
                            offY - dY*2,
                            1.0,
                            m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        std::to_string( currentStride ),
//        offX + dX*2,
//        offY - dY*3,
//        1.0,
//        m_textColor );

    offY -= dY;

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Real",
                            m_dataSetInfoWidget.position().x() + 10,
                            offY,
                            1.0,
                            m_textColor );

    offY -= dY;

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Simulation",
                            m_dataSetInfoWidget.position().x() + 10,
                            offY,
                            1.0,
                            m_textColor );

//    offY -= dY;

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Output TS",
//        m_dataSetInfoWidget.position().x() + 10,
//        offY,
//        1.0,
//        m_textColor );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "Selected : Real,Sim,Out",
//        m_dataSetInfoWidget.position().x() + 10,
//        m_dataSetInfoWidget.position().y() + 10,
//        1.0,
//        Vec3< float >( 0, 0, 0 ) );

//    m_renderer->renderText( false,
//        textProg,
//       scaledWidth(),
//       scaledHeight(),
//        "( "
//        + ( realTime.size() ? to_string_with_precision( realTime[ timeStep ] ) : "NA" )
//        + ", " + ( simTime.size() ? std::to_string( simTime[ timeStep ] ) : "NA" )
//        + ", " + std::to_string( timeStep )
//        + " )",
//        m_dataSetInfoWidget.position().x() + 160,
//        m_dataSetInfoWidget.position().y() + 10,
//        1.0f,
//        Vec3< float >( m_boxSelectionColor.x(), m_boxSelectionColor.y(), m_boxSelectionColor.z() ) );

    for( auto & p : lines )
    {
        p.x( 2 * p.x() / ( ( double )scaledWidth() ) - 1.0 );
        p.y( 2 * p.y() / ( ( double )scaledHeight() ) - 1.0 );
    }

    glLineWidth( 1 );
    m_renderer->renderPrimitivesFlat( lines, cartesianProg, QVector4D( 0.4, 0.4, 0.4, 1.0 ), QMatrix4x4(),  GL_LINES );
}

void BALEEN::renderTimeLineAnnotation()
{
    TimeSeriesDefinition def;

    const std::vector< std::int32_t > simulationTime =  m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().simTime()
            : m_dataSetManager->distributionManager().simTime();

    if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        def = m_dataSetManager->particleDataManager().currentTimeSeries();
    }
    else if( m_populationDataTypeEnum == PopulationDataType::GRID_POINTS )
    {
        def = m_dataSetManager->distributionManager().currentTimeSeries();
    }
    else
    {
        return;
    }

    if( ( def.lastIdx - def.firstIdx ) < 2 )
    {
        return;
    }

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    int NC = 10;

    float xS = m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft();
    float dX = ( float( 1 ) / ( NC - 1 ) ) * m_timeLineWidget.plotSize().x();
    float y =  m_timeLineWidget.position().y() + 16;

    for( int c = 0; c < NC; ++c )
    {
        float x = xS + c * dX;
        std::string valStr;

        int idx = def.firstIdx + ( c / ( double ) ( NC - 1 ) ) * ( def.lastIdx - def.firstIdx );

        valStr = std::to_string( simulationTime[ idx ] );

        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            valStr,
            x - valStr.size() * 7.0 / 2.0,
            y,
            1.0,
            m_textColor );
    }

    std::string tsString( std::to_string( simulationTime[ m_selectedTimeStep ] ) );

    m_renderer->renderText( false,
                            textProg,
                            scaledWidth(),
                            scaledHeight(),
                            tsString,
                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() + ( ( ( m_selectedTimeStep - def.firstIdx ) / ( double ) def.idxStride ) / ( double ) ( def.numSteps() - 1  ) )
                            * ( m_timeLineWidget.size().x() - m_timeLineWidget.plotLeft() - m_timeLineWidget.plotRight() )
                            - 7 * tsString.size() / 2.0,
                            m_timeLineWidget.position().y() + m_timeLineWidget.plotBottom() - 14,
                            1.0,
                            m_textColor );
}

void BALEEN::renderTimeSeriesPlots2(
    const std::vector< float > & data,
    const Vec2< float > & RANGE,
    const int N_STEPS,
    const int CURRENT_STEP,
    const int N_SETS,
    bool plotAll )
{
    float plHT = m_timeLineWidget.size().y() - m_timeLineWidget.MARGIN_BOTTOM - m_timeLineWidget.MARGIN_TOP;
    float pltOffY = m_timeLineWidget.position().y() + m_timeLineWidget.MARGIN_BOTTOM;

    glViewport(
        m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT,
        pltOffY,
        m_timeLineWidget.size().x() - m_timeLineWidget.MARGIN_LEFT - m_timeLineWidget.MARGIN_RIGHT,
        plHT );

    double yWidth = RANGE.b() - RANGE.a();
    QMatrix4x4 M;
    M.scale( 2.0 / ( N_STEPS-1), 2.0 / yWidth );
    M.translate( -( N_STEPS-1 ) / 2.0,  -( RANGE.a() + ( yWidth / 2.0 ) ) );

    std::vector< float > x( N_STEPS );
    std::vector< float > y( N_STEPS );

    for( int t = 0; t < N_STEPS; ++t )
    {
        x[ t ] = t;
    }

    QVector4D outlineColor( 1, 1, 1, 1.0 );
    QVector4D allColor( 0.1, 0.1, 0.1, 1.0 );

    y.assign( data.begin() + ( 0 * N_STEPS ), data.begin() + ( 0 * N_STEPS ) + N_STEPS );

    glLineWidth( 6 );
    glPointSize( 6 );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        outlineColor,
        M,
        GL_POINTS
    );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        outlineColor,
        M,
        GL_LINE_STRIP
    );

    glLineWidth( 4 );
    glPointSize( 4 );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        allColor,
        M,
        GL_POINTS
    );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        allColor,
        M,
        GL_LINE_STRIP
    );

    y.assign( data.begin() + ( 1 * N_STEPS ), data.begin() + ( 1 * N_STEPS ) + N_STEPS );

    glLineWidth( 6 );
    glPointSize( 6 );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        outlineColor,
        M,
        GL_POINTS
    );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        outlineColor,
        M,
        GL_LINE_STRIP
    );

    glLineWidth( 4 );
    glPointSize( 4 );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        m_combinationColor,
        M,
        GL_POINTS
    );
    m_renderer->renderPrimitivesFlatSerial(
        x,
        y,
        flatProgSerial,
        m_combinationColor,
        M,
        GL_LINE_STRIP
    );
}

void BALEEN::computeTemporalPlots(
    LineData & data,
    const std::string & ptype,
    const TimePlotDefinition & timePlotDef,
    const SpacePartitioningDefinition & spacePartitioning )
{
//    std::vector< double > & meanWeights = data.values[ LineData::MEAN ];
//    std::vector< double > & meanSquaredWeights = data.values[ LineData::MEAN_SQUARED ];
//    Vec2< double > & meanRange = data.ranges[ LineData::MEAN ];
//    Vec2< double > & meanSquaredRange = data.ranges[ LineData::MEAN_SQUARED ];

//    TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
//                                : m_dataSetManager->distributionManager().currentTimeSeries();

//    const int FIRST = tsrs.firstIdx;
//    const int LAST  = tsrs.lastIdx;
//    const int STRIDE = tsrs.idxStride;

//    static std::vector< Vec2< double > > perThreadMeanRange;
//    static std::vector< Vec2< double > > perThreadMeanSquaredRange;

//    int nThreads;
//    #pragma omp parallel
//    {
//        nThreads = omp_get_num_threads();
//    }

//    perThreadMeanRange.resize( nThreads );
//    perThreadMeanSquaredRange.resize( nThreads );

//    #pragma omp parallel
//    {
//        const int idx = omp_get_thread_num();
//        perThreadMeanRange[ idx ] = Vec2< double >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() );
//        perThreadMeanSquaredRange[ idx ] = perThreadMeanRange[ idx ];
//    }

//    const std::int32_t cellIdx = m_sampler.getSelectedCellId();
//    const Vec2< float >  spatialScaling = spatialScaling2D();

//    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

//    #pragma omp parallel for
//    for( int t = FIRST; t <= LAST; t += STRIDE )
//    {
//        const int T_ID = omp_get_thread_num();
//        const int ti = ( t - FIRST ) / STRIDE;

//        const std::vector< float > & xSpatial = *( m_dataSetManager->particleDataManager().values( t, ptype, spacePartitioning.attributes[ 0 ] ) );
//        const std::vector< float > & ySpatial = *( m_dataSetManager->particleDataManager().values( t, ptype, spacePartitioning.attributes[ 1 ] ) );
//        const std::vector< float > & w = *( m_dataSetManager->particleDataManager().values( t, ptype, timePlotDef.attr ) );

//        meanWeights[ ti ] = 0.L;
//        meanSquaredWeights[ ti ] = 0.L;

//        int count = 0;
//        for( int i = 0; i < NP; ++i )
//        {
//            if( m_sampler.gridCellIndex( xSpatial[ i ] * spatialScaling.a(), ySpatial[ i ] * spatialScaling.b() ) == cellIdx )
//            {
//                ++count;
//                meanWeights[ ti ] += w[ i ];
//                meanSquaredWeights[ ti ] += std::pow( static_cast< double >( w[ i ] ), 2.L );
//            }
//        }

//        meanWeights[ ti ] /= static_cast< double >( count );
//        perThreadMeanRange[ T_ID ].a( std::min( perThreadMeanRange[ T_ID ].a(), meanWeights[ ti ] ) );
//        perThreadMeanRange[ T_ID ].b( std::max( perThreadMeanRange[ T_ID ].b(), meanWeights[ ti ] ) );

//        meanSquaredWeights[ ti ] /= static_cast< double >( count );
//        perThreadMeanSquaredRange[ T_ID ].a( std::min( perThreadMeanSquaredRange[ T_ID ].a(), meanSquaredWeights[ ti ] ) );
//        perThreadMeanSquaredRange[ T_ID ].b( std::max( perThreadMeanSquaredRange[ T_ID ].b(), meanSquaredWeights[ ti ] ) );
//    }

//    for( int th = 0; th < nThreads; ++th )
//    {
//        meanRange.a( std::min( meanRange.a(), perThreadMeanRange[ th ].a() ) );
//        meanRange.b( std::max( meanRange.b(), perThreadMeanRange[ th ].b() ) );
//        meanSquaredRange.a( std::min( meanSquaredRange.a(), perThreadMeanSquaredRange[ th ].a() ) );
//        meanSquaredRange.b( std::max( meanSquaredRange.b(), perThreadMeanSquaredRange[ th ].b() ) );
//    }
}

void BALEEN::computeTemporalPlotsAll(
    LineData & data,
    const std::string & ptype,
    const TimePlotDefinition & timePlotDef )
{
    std::vector< double > & meanWeights = data.values[ LineData::MEAN ];
    std::vector< double > & meanSquaredWeights = data.values[ LineData::MEAN_SQUARED ];
    Vec2< double > & meanRange = data.ranges[ LineData::MEAN ];
    Vec2< double > & meanSquaredRange = data.ranges[ LineData::MEAN_SQUARED ];

    TimeSeriesDefinition tsrs = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().currentTimeSeries()
                                : m_dataSetManager->distributionManager().currentTimeSeries();

    const int FIRST = tsrs.firstIdx;
    const int LAST  = tsrs.lastIdx;
    const int STRIDE = tsrs.idxStride;

    static std::vector< Vec2< double > > perThreadMeanRange;
    static std::vector< Vec2< double > > perThreadMeanSquaredRange;

    //////////////qDebug()  << "inital phase";

    int nThreads;
    #pragma omp parallel
    {
        nThreads = omp_get_num_threads();
    }

    perThreadMeanRange.resize( nThreads );
    perThreadMeanSquaredRange.resize( nThreads );

    #pragma omp parallel
    {
        const int idx = omp_get_thread_num();
        perThreadMeanRange[ idx ] = Vec2< double >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() );
        perThreadMeanSquaredRange[ idx ] = perThreadMeanRange[ idx ];
    }

    //////////////qDebug()  << "created per thread datastructures";

    //#pragma omp parallel for
    for( int t = FIRST; t <= LAST; t += STRIDE )
    {
        const int T_ID = omp_get_thread_num();
        const int ti = ( t - FIRST ) / STRIDE;

        const std::vector< float > & w = *( m_dataSetManager->particleDataManager().values( t, ptype, timePlotDef.attr ) );

        meanWeights[ ti ] = 0.L;
        meanSquaredWeights[ ti ] = 0.L;

        const int NP = w.size();
        for( int i = 0; i < NP; ++i )
        {
            meanWeights[ ti ] += w[ i ];
            meanSquaredWeights[ ti ] += std::pow( static_cast< double >( w[ i ] ), 2.L );
        }

        meanWeights[ ti ] /= static_cast< double >( w.size() );
        perThreadMeanRange[ T_ID ].a( std::min( perThreadMeanRange[ T_ID ].a(), meanWeights[ ti ] ) );
        perThreadMeanRange[ T_ID ].b( std::max( perThreadMeanRange[ T_ID ].b(), meanWeights[ ti ] ) );

        meanSquaredWeights[ ti ] /= static_cast< double >( w.size() );
        perThreadMeanSquaredRange[ T_ID ].a( std::min( perThreadMeanSquaredRange[ T_ID ].a(), meanSquaredWeights[ ti ] ) );
        perThreadMeanSquaredRange[ T_ID ].b( std::max( perThreadMeanSquaredRange[ T_ID ].b(), meanSquaredWeights[ ti ] ) );
    }

    //////////////qDebug()  << "threads finished";

    for( int th = 0; th < nThreads; ++th )
    {
        meanRange.a( std::min( meanRange.a(), perThreadMeanRange[ th ].a() ) );
        meanRange.b( std::max( meanRange.b(), perThreadMeanRange[ th ].b() ) );
        meanSquaredRange.a( std::min( meanSquaredRange.a(), perThreadMeanSquaredRange[ th ].a() ) );
        meanSquaredRange.b( std::max( meanSquaredRange.b(), perThreadMeanSquaredRange[ th ].b() ) );
    }

    //////////////qDebug()  << "done";
}

// updates all of the combo boxes and other ui elements to reflect the
// definitions and parameters currently available for the current ptype
// if a selected parameter or definition for the previous ptype is
// avaiable for the new ptype (according to it's name), then it will
// be selected for the new ptype
void BALEEN::updatePartType()
{
    const std::string ptype = m_particleCombo.selectedItemText();

    const std::string phasePlot = m_phasePlotCombo.selectedItemText();
    const std::string timePlot = m_timePlotCombo.selectedItemText();
    const std::string filter = m_filterCombo.selectedItemText();
    const std::string subset = m_subsetCombo.selectedItemText();
    const std::string timeSeries = m_timeSeriesCombo.selectedItemText();

    m_phasePlotCombo.clear();
    m_timePlotCombo.clear();
    m_timeSeriesCombo.clear();
    m_filterCombo.clear();
    m_subsetCombo.clear();

    m_subsetCombo.addItem( "All" );
    m_filterCombo.addItem( "None" );

    int selectedIdx = 0;
    int idx = 0;

    ////////qDebug()  << "updating tp sel  " << ptype.c_str();

    selectedIdx = 0;
    idx = 0;
    for( auto & tp : m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( ptype ) )
    {
        if( tp.second.name == timePlot )
        {
            selectedIdx = idx;
        }
        ++idx;
        m_timePlotCombo.addItem( tp.second.name );
    }
    m_timePlotCombo.selectItem( selectedIdx );

    ////////qDebug()  << "updating pf sel";
    for( auto & f : m_currentConfigurations.particleBasedConfiguration.m_particleFilters.at( ptype ) )
    {
        m_filterCombo.addItem( f.second.name );
    }
    m_filterCombo.selectByText( filter );

    ////////qDebug()  << "updating sd sel";
    for( auto & s : m_currentConfigurations.particleBasedConfiguration.m_subsetDefinitions.at( ptype ) )
    {
        m_subsetCombo.addItem( s.second.name );
    }
    m_subsetCombo.selectByText( subset );

    ////////qDebug()  << "updating ts sel";
    selectedIdx = 0;
    idx = 0;
    for( auto & ts : m_currentConfigurations.timeSeriesDefinitions().at( ptype ) )
    {
        if( ts.second.name == timeSeries )
        {
            selectedIdx = idx;
        }
        ++idx;
        m_timeSeriesCombo.addItem( ts.second.name );
    }
    m_timeSeriesCombo.selectItem( selectedIdx );

    ////////qDebug()  << "updating pp sel";
    selectedIdx = 0;
    idx = 0;
    for( auto & fp : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( ptype ) )
    {
        if( fp.second.name == phasePlot )
        {
            selectedIdx = idx;
        }
        ++idx;
        m_phasePlotCombo.addItem( fp.second.name );
    }
    m_phasePlotCombo.selectItem( selectedIdx );
}

std::uint8_t BALEEN::getErrorRemovalOption()
{
    std::uint8_t errorRemovalState = 0;
    if( m_errorRemovalCombo.selectedItemText() == "NaN,Inf" )
    {
        errorRemovalState |= ParticleDataManager<float>::REMOVE_INF | ParticleDataManager<float>::REMOVE_NAN;
    }
    else if ( m_errorRemovalCombo.selectedItemText() == "NaN,Inf,Unexp." )
    {
        errorRemovalState |= ParticleDataManager<float>::REMOVE_INF | ParticleDataManager<float>::REMOVE_NAN |
                             ParticleDataManager<float>::REMOVE_UNEXPECTED_VALUES;
    }

    return errorRemovalState;
}

std::string BALEEN::getWarningSummary( int populationDataTypeEnum, const std::string & ptype )
{
    std::string summary;
    std::uint8_t storedGlobalWarningState = 0;

    if( populationDataTypeEnum == PopulationDataType::PARTICLES )
    {
        storedGlobalWarningState = m_dataSetManager->particleDataManager().getStoredGlobalWarningState( ptype );
    }
    else
    {

    }

    if( storedGlobalWarningState == 0 )
    {
        return "No Errors Detected";
    }

    summary += "Detected: ";

    if( storedGlobalWarningState & ParticleDataManager< float >::HAS_INF )
    {
        summary += "Inf, ";
    }
    if( storedGlobalWarningState & ParticleDataManager< float >::HAS_NAN )
    {
        summary += "NaN, ";
    }
    if( m_dataSetManager->particleDataManager().getHasZeros( ptype ) )
    {
        summary += "Zero Points, ";
    }
    if( storedGlobalWarningState & ParticleDataManager< float >::HAS_UNEXPECTED )
    {
        summary += "Unexpected Values, ";
    }

    summary.pop_back();
    summary.pop_back();

    return summary;
}

std::set< std::string > BALEEN::getActiveVariables( const std::string & ptype )
{
    std::set< std::string > vs;

    // from time series definition
    {
        auto it = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.find( ptype );
        if( it != m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.end() )
        {
            std::string timePlotKey = m_timePlotCombo.selectedItemText();

            auto it2 = it->second.find( timePlotKey );
            if( it2 != it->second.end() )
            {
                vs.insert( it2->second.attr );
            }
            else
            {
                //////////////qDebug()  << "key not found";
            }
        }
    }

    // get active phase plot definitions
    {
        auto it = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.find( ptype );
        if( it != m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.end() )
        {
            for( auto & pp : it->second )
            {
                for( auto & a : pp.second.attrs )
                {
                    vs.insert( a );
                }
            }
        }
    }

    // get active phase space
    vs.insert( m_activePhaseSpace.begin(), m_activePhaseSpace.end() );

    return vs;
}

void BALEEN::renderSelection(
    const QMatrix4x4 & M,
    const std::string & x,
    const std::string & y,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QVector4D & color1,
    const QVector4D & color2,
    bool enhanced,
    bool points,
    bool combo,
    bool thin )
{
    const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() );
    const size_t NPT = m_subsetBitmasks.size();

    const long int xOffset = m_vboIndexes.at( x );
    const long int yOffset = m_vboIndexes.at( y );
    const long int cOffset = m_vboIndexes.size();

    glEnable(GL_MULTISAMPLE);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glEnable( GL_BLEND );
    glEnable( GL_LINE_SMOOTH );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    ////////////////////////////////////////////////////////////////

    for( int i = 0; i < 2; ++i )
    {

        trajectoryProgramNoWrap->bind();

        GLuint x1 = trajectoryProgramNoWrap->attributeLocation( "xAttr" );
        GLuint x2 = trajectoryProgramNoWrap->attributeLocation( "yAttr" );
        GLuint c  = trajectoryProgramNoWrap->attributeLocation( "c" );

        m_phaseVAO->bind();
        m_phaseVBO->bind();

        trajectoryProgramNoWrap->setUniformValue( "MVP", M );
        trajectoryProgramNoWrap->setUniformValue( "combo", combo );

        trajectoryProgramNoWrap->enableAttributeArray( x1 );
        trajectoryProgramNoWrap->enableAttributeArray( x2 );
        trajectoryProgramNoWrap->enableAttributeArray(  c );

        trajectoryProgramNoWrap->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
        trajectoryProgramNoWrap->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
        glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ),  (void*)( cOffset*NPT*sizeof( float ) ) );

        if( points )
        {
            glPointSize( i == 0 ? 6 : 3 );
            trajectoryProgramNoWrap->setUniformValue( "color", i == 0 ? QVector4D( 0.1, 0.1, 0.1, 1 ) : color2  );
            glMultiDrawArrays( GL_POINTS, firsts.data(), counts.data(), NP );
        }
        else if( ! thin )
        {

            glLineWidth( i == 0 ? 8 : 5 );
            glPointSize( i == 0 ? 8 : 5 );
            trajectoryProgramNoWrap->setUniformValue( "color", i == 0 ? color1 : color2  );
            glMultiDrawArrays( GL_POINTS, firsts.data(), counts.data(), NP );
        }
        else
        {
            glLineWidth( i == 0 ? 5 : 2 );
            glPointSize( i == 0 ? 5 : 2 );

            trajectoryProgramNoWrap->setUniformValue( "color", i == 0 ? color1 : color2  );
            glMultiDrawArrays( GL_POINTS, firsts.data(), counts.data(), NP );
        }

        trajectoryProgramNoWrap->disableAttributeArray( x1 );
        trajectoryProgramNoWrap->disableAttributeArray( x2 );
        trajectoryProgramNoWrap->disableAttributeArray(  c );

        m_phaseVBO->release();
        m_phaseVAO->release();

        trajectoryProgramNoWrap->release();

        ////////////////////////////////////////////////////////////////////////////

        trajectoryProgramWraparound->bind();

        x1 = trajectoryProgramWraparound->attributeLocation( "xAttr" );
        x2 = trajectoryProgramWraparound->attributeLocation( "yAttr" );
        c  = trajectoryProgramWraparound->attributeLocation( "c" );

        m_phaseVAO->bind();
        m_phaseVBO->bind();

        trajectoryProgramWraparound->setUniformValue( "wrap", x == "poloidal_angle" );
        trajectoryProgramWraparound->setUniformValue( "wrap1", -1.f );
        trajectoryProgramWraparound->setUniformValue( "wrap2",  1.f );

        trajectoryProgramWraparound->setUniformValue( "MVP", M );
        trajectoryProgramWraparound->setUniformValue( "combo", combo );

        trajectoryProgramWraparound->enableAttributeArray( x1 );
        trajectoryProgramWraparound->enableAttributeArray( x2 );
        trajectoryProgramWraparound->enableAttributeArray(  c );

        trajectoryProgramWraparound->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
        trajectoryProgramWraparound->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
        glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ),  (void*)( cOffset*NPT*sizeof( float ) ) );

        if( points )
        {

        }
        else if( ! thin )
        {
            glLineWidth( i == 0 ? 8 : 5 );
            trajectoryProgramWraparound->setUniformValue( "color", i == 0 ? color1 : color2 );
            glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
        }
        else
        {
            glLineWidth( i == 0 ? 5 : 2 );
            trajectoryProgramWraparound->setUniformValue( "color", i == 0 ? color1 : color2  );
            glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
        }

        trajectoryProgramWraparound->disableAttributeArray( x1 );
        trajectoryProgramWraparound->disableAttributeArray( x2 );
        trajectoryProgramWraparound->disableAttributeArray(  c );

        m_phaseVBO->release();
        m_phaseVAO->release();

        trajectoryProgramWraparound->release();
    }
}

void BALEEN::sampleSelection( size_t maxSampleSize )
{
    const size_t SAMPLE_SIZE = std::min( maxSampleSize, m_particleSelection.size() );
    m_particleSelectionSample.resize( SAMPLE_SIZE );

    if( SAMPLE_SIZE <= 0 )
    {
        return;
    }

    for( size_t id = 0; id < SAMPLE_SIZE; ++id )
    {
        m_particleSelectionSample[ id ] = m_particleSelection[ id ];
    }

    TimeSeriesDefinition * timeSeries = getTimeSeries( m_particleCombo.selectedItemText() );
    const size_t NT = timeSeries->numSteps();
    const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() );

    #pragma omp parallel for
    for( size_t p = 0; p < NP; ++p )
    {
        m_particleSelectionSampleCounts[ p ] = 0;
    }

    const size_t NS = m_particleSelectionSample.size();
    for( size_t id = 0; id < NS; ++id )
    {
        m_particleSelectionSampleCounts[ m_particleSelectionSample[ id ] ] = NT;
    }

    m_selectedParticle = -1;
}

void BALEEN::getSelectionFromPolaritySwap( const std::string & attr )
{
    m_selectedParticle = -1;

    if( ! m_phasePlotPosSz.count( m_selectedPlot ) )
    {
        return;
    }

    const Vec2< float > CENTER = getSelectionCenter( m_selectionPosition );
    const Vec2< float > RADIUS =
        getSelectionRadius(
            getRelativeSelectionRadius(
                m_phasePlotPosSz.at( m_selectedPlot ).second,
                m_selectionRadius ) );

    TimeSeriesDefinition * timeSeries = getTimeSeries( m_particleCombo.selectedItemText() );

    const size_t NT = timeSeries->numSteps();
    const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() );

    auto & data = *( m_trajectoryData.at( attr ) );

    unsigned int nthreads = 1;

    #pragma omp parallel
    {
        nthreads = omp_get_num_threads();
    }

    static std::vector< std::vector< std::size_t > > perThreadIds( nthreads );

    for( unsigned int th = 0; th < nthreads; ++th )
    {
        perThreadIds[ th ].clear();
        perThreadIds[ th ].reserve( size_t( std::ceil( NP / nthreads ) ) );
    }

    float * xData;
    float * yData;

    if( m_selectedPlot == "timeline" )
    {
        auto & plt = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_timePlotCombo.selectedItemText() );
        yData = m_trajectoryData.at( plt.attr )->data();
    }

    else
    {
        auto & plt   =  m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_selectedPlot );
        xData = m_trajectoryData.at( plt.attrs[ 0 ] )->data();
        yData = m_trajectoryData.at( plt.attrs[ 1 ] )->data();
    }

    if( m_selectionRefinementButton.isPressed() )
    {
        if( m_particleSelection.size() == 0 )
        {
            return;
        }

        else
        {
            if( m_selectedPlot == "timeline" )
            {
                const size_t SZ = m_particleSelection.size() ;
                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    auto & v = perThreadIds[ omp_get_thread_num() ];
                    const size_t OFFSET = m_particleSelection[ i ] * NT;
                    for( size_t t = 1; t < NT; ++t )
                    {
                        if( data[ i*NT + t ] * data[ i * NT + t - 1 ] <= 0 )
                        {
                            const float relativeRadius =
                                std::sqrt(
                                    std::pow( ( t - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                    std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                            if( relativeRadius <= 1.f )
                            {
                                v.push_back( m_particleSelection[ i ] );
                            }
                        }
                    }
                }
            }
            else
            {
                const size_t SZ = m_particleSelection.size() ;
                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    auto & v = perThreadIds[ omp_get_thread_num() ];
                    const size_t OFFSET = m_particleSelection[ i ] * NT;
                    for( size_t t = 0; t < NT; ++t )
                    {
                        bool select = ( ( m_subsetBitmasks[ OFFSET + t ] & 1 ) != 0 ) && ( ( m_subsetBitmasks[ OFFSET + t ] & 2 ) != 0 );
                        if( ( data[ OFFSET + t ] * data[ OFFSET + t - 1 ] ) <= 0 && select )
                        {
                            const float relativeRadius =
                                std::sqrt(
                                    std::pow( ( xData[ OFFSET + t ] - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                    std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                            if( relativeRadius <= 1.f )
                            {
                                v.push_back( m_particleSelection[ i ] );
                            }
                        }
                    }
                }
            }
        }
    }

    else
    {
        m_particleSelection.clear();
        if( m_selectedPlot == "timeline" )
        {
            #pragma omp parallel for
            for( size_t p = 0; p < NP; ++p )
            {
                auto & v = perThreadIds[ omp_get_thread_num() ];
                const size_t OFFSET = p * NT;
                for( size_t t = 0; t < NT; ++t )
                {
                    if( ( data[ p*NT + t ] * data[ p*NT + t - 1 ] ) <= 0 &&
                        ( m_subsetBitmasks[ OFFSET + t ] & 1 != 0  ) &&
                        ( m_subsetBitmasks[ OFFSET + t ] & 2 != 0  ) )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( t - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( p );
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            perThreadIds[ 0 ].clear();
            #pragma omp parallel for
            for( size_t p = 0; p < NP; ++p )
            {
                auto & v = perThreadIds[ omp_get_thread_num()  ];
                const size_t OFFSET = p * NT;
                for( size_t t = 0; t < NT; ++t )
                {
                    bool select = ( ( m_subsetBitmasks[ OFFSET + t ] & 1 ) != 0 ) && ( ( m_subsetBitmasks[ OFFSET + t ] & 2 ) != 0 );
                    if( ( data[ OFFSET + t ] * data[ OFFSET + t - 1 ] ) <= 0 && select )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( xData[ OFFSET + t ] - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( p );
                            break;
                        }
                    }
                }
            }
        }
    }

    m_particleSelection.clear();
    for( int th = 0; th < nthreads; ++th )
    {
        m_particleSelection.insert( m_particleSelection.begin(), perThreadIds[ th ].begin(), perThreadIds[ th ].end() );
    }

    TN::Sequential::randomize( m_particleSelection );

    m_particleSelectionFirsts.resize( NP );
    for( size_t p = 0; p < NP; ++p )
    {
        m_particleSelectionCounts[ p ] = 0;
        m_particleSelectionFirsts[ p ] = NT*p;
    }

    const size_t NS = m_particleSelection.size();
    #pragma omp parallel for
    for( size_t id = 0; id < NS; ++id )
    {
        m_particleSelectionCounts[ m_particleSelection[ id ] ] = NT;
    }
}

void BALEEN::getSelectionFromEvent()
{
    m_selectedParticle = -1;

    if( ! m_phasePlotPosSz.count( m_selectedPlot ) )
    {
        return;
    }

    const Vec2< float > CENTER = getSelectionCenter( m_selectionPosition );
    const Vec2< float > RADIUS =
        getSelectionRadius(
            getRelativeSelectionRadius(
                m_phasePlotPosSz.at( m_selectedPlot ).second,
                m_selectionRadius ) );

    TimeSeriesDefinition * timeSeries = getTimeSeries( m_particleCombo.selectedItemText() );

    const size_t NT = timeSeries->numSteps();
    const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() );

    unsigned int nthreads = 1;

    #pragma omp parallel
    {
        nthreads = omp_get_num_threads();
    }

    static std::vector< std::vector< std::size_t > > perThreadIds( nthreads );

    for( unsigned int th = 0; th < nthreads; ++th )
    {
        perThreadIds[ th ].clear();
        perThreadIds[ th ].reserve( size_t( std::ceil( NP / nthreads ) ) );
    }

    float * xData;
    float * yData;

    if( m_selectedPlot == "timeline" )
    {
        auto & plt = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_timePlotCombo.selectedItemText() );
        yData = m_trajectoryData.at( plt.attr )->data();
    }

    else
    {
        auto & plt   =  m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_selectedPlot );
        xData = m_trajectoryData.at( plt.attrs[ 0 ] )->data();
        yData = m_trajectoryData.at( plt.attrs[ 1 ] )->data();
    }

    if( m_selectionRefinementButton.isPressed() )
    {
        if( m_particleSelection.size() == 0 )
        {
            return;
        }

        else
        {
            if( m_selectedPlot == "timeline" )
            {
                const size_t SZ = m_particleSelection.size() ;
                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    auto & v = perThreadIds[ omp_get_thread_num() ];
                    const size_t OFFSET = m_particleSelection[ i ] * NT;
                    for( size_t t = 1; t < NT; ++t )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( t - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( m_particleSelection[ i ] );
                            break;
                        }

                        if( m_subsetBitmasks[ OFFSET + t ] & 1 != 0 )
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                const size_t SZ = m_particleSelection.size() ;
                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    auto & v = perThreadIds[ omp_get_thread_num() ];
                    const size_t OFFSET = m_particleSelection[ i ] * NT;
                    for( size_t t = 0; t < NT; ++t )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( xData[ OFFSET + t ] - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( m_particleSelection[ i ] );
                            break;
                        }
                        if( m_subsetBitmasks[ OFFSET + t ] & 1 != 0 )
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    else
    {
        m_particleSelection.clear();
        if( m_selectedPlot == "timeline" )
        {
            #pragma omp parallel for
            for( size_t p = 0; p < NP; ++p )
            {
                auto & v = perThreadIds[ omp_get_thread_num() ];
                const size_t OFFSET = p * NT;
                for( size_t t = 0; t < NT; ++t )
                {
                    bool select = ( ( m_subsetBitmasks[ NT * p + t ] & 1 ) != 0 ) && ( ( m_subsetBitmasks[ NT * p + t ] & 2 ) != 0 );
                    if( select )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( t - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( p );
                            break;
                        }
                        if( m_subsetBitmasks[ OFFSET + t ] & 1 != 0 )
                        {
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel for
            for( size_t p = 0; p < NP; ++p )
            {
                auto & v = perThreadIds[ omp_get_thread_num() ];
                const size_t OFFSET = p * NT;
                for( size_t t = 0; t < NT; ++t )
                {
                    bool select = ( ( m_subsetBitmasks[ NT * p + t ] & 1 ) != 0 ) && ( ( m_subsetBitmasks[ NT * p + t ] & 2 ) != 0 );
                    if( select )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( xData[ OFFSET + t ] - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( p );
                            break;
                        }
                        if( m_subsetBitmasks[ OFFSET + t ] & 1 != 0 )
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    m_particleSelection.clear();
    for( int th = 0; th < nthreads; ++th )
    {
        m_particleSelection.insert( m_particleSelection.begin(), perThreadIds[ th ].begin(), perThreadIds[ th ].end() );
    }

    TN::Sequential::randomize( m_particleSelection );

    m_particleSelectionFirsts.resize( NP );
    for( size_t p = 0; p < NP; ++p )
    {
        m_particleSelectionCounts[ p ] = 0;
        m_particleSelectionFirsts[ p ] = NT*p;
    }

    const size_t NS = m_particleSelection.size();
    #pragma omp parallel for
    for( size_t id = 0; id < NS; ++id )
    {
        m_particleSelectionCounts[ m_particleSelection[ id ] ] = NT;
    }
}

void BALEEN::getSelection()
{
    m_selectedParticle = -1;

    if( ! m_phasePlotPosSz.count( m_selectedPlot ) )
    {
        return;
    }

    const Vec2< float > CENTER = getSelectionCenter( m_selectionPosition );
    const Vec2< float > RADIUS =
        getSelectionRadius(
            getRelativeSelectionRadius(
                m_phasePlotPosSz.at( m_selectedPlot ).second,
                m_selectionRadius ) );

    TimeSeriesDefinition * timeSeries = getTimeSeries( m_particleCombo.selectedItemText() );

    const size_t NT = timeSeries->numSteps();
    const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() );

    unsigned int nthreads = 1;

    #pragma omp parallel
    {
        nthreads = omp_get_num_threads();
    }

    static std::vector< std::vector< std::size_t > > perThreadIds( nthreads );

    for( unsigned int th = 0; th < nthreads; ++th )
    {
        perThreadIds[ th ].clear();
        perThreadIds[ th ].reserve( size_t( std::ceil( NP / nthreads ) ) );
    }

    float * xData;
    float * yData;

    if( m_selectedPlot == "timeline" )
    {
        auto & plt = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_timePlotCombo.selectedItemText() );
        yData = m_trajectoryData.at( plt.attr )->data();
    }

    else
    {
        auto & plt   =  m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_selectedPlot );
        xData = m_trajectoryData.at( plt.attrs[ 0 ] )->data();
        yData = m_trajectoryData.at( plt.attrs[ 1 ] )->data();
    }

    if( m_selectionRefinementButton.isPressed() )
    {
        if( m_particleSelection.size() == 0 )
        {
            return;
        }

        else
        {
            if( m_selectedPlot == "timeline" )
            {
                const size_t SZ = m_particleSelection.size() ;
                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    auto & v = perThreadIds[ omp_get_thread_num() ];
                    const size_t OFFSET = m_particleSelection[ i ] * NT;
                    for( size_t t = 0; t < NT; ++t )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( t - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( m_particleSelection[ i ] );
                            break;
                        }
                    }
                }
            }
            else
            {
                const size_t SZ = m_particleSelection.size() ;
                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    auto & v = perThreadIds[ omp_get_thread_num() ];
                    const size_t OFFSET = m_particleSelection[ i ] * NT;
                    for( size_t t = 0; t < NT; ++t )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( xData[ OFFSET + t ] - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( m_particleSelection[ i ] );
                            break;
                        }
                    }
                }
            }
        }
    }

    else
    {
        m_particleSelection.clear();
        if( m_selectedPlot == "timeline" )
        {
            #pragma omp parallel for
            for( size_t p = 0; p < NP; ++p )
            {
                auto & v = perThreadIds[ omp_get_thread_num() ];
                const size_t OFFSET = p * NT;
                for( size_t t = 0; t < NT; ++t )
                {
                    bool select = ( ( m_subsetBitmasks[ NT * p + t ] & 1 ) != 0 ) && ( ( m_subsetBitmasks[ NT * p + t ] & 2 ) != 0 );
                    if( select )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( t - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( p );
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel for
            for( size_t p = 0; p < NP; ++p )
            {
                auto & v = perThreadIds[ omp_get_thread_num() ];
                const size_t OFFSET = p * NT;
                for( size_t t = 0; t < NT; ++t )
                {
                    bool select = ( ( m_subsetBitmasks[ NT * p + t ] & 1 ) != 0 ) && ( ( m_subsetBitmasks[ NT * p + t ] & 2 ) != 0 );
                    if( select )
                    {
                        const float relativeRadius =
                            std::sqrt(
                                std::pow( ( xData[ OFFSET + t ] - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                std::pow( ( yData[ OFFSET + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                        if( relativeRadius <= 1.f )
                        {
                            v.push_back( p );
                            break;
                        }
                    }
                }
            }
        }
    }

    m_particleSelection.clear();
    for( int th = 0; th < nthreads; ++th )
    {
        m_particleSelection.insert( m_particleSelection.begin(), perThreadIds[ th ].begin(), perThreadIds[ th ].end() );
    }

    TN::Sequential::randomize( m_particleSelection );

    m_particleSelectionFirsts.resize( NP );
    for( size_t p = 0; p < NP; ++p )
    {
        m_particleSelectionCounts[ p ] = 0;
        m_particleSelectionFirsts[ p ] = NT*p;
    }

    const size_t NS = m_particleSelection.size();
    #pragma omp parallel for
    for( size_t id = 0; id < NS; ++id )
    {
        m_particleSelectionCounts[ m_particleSelection[ id ] ] = NT;
    }
}

Vec2< float > BALEEN::getRelativeSelectionCenter( const Vec2< float > & selectionPosition, const Vec2< float > & plotPosition, const Vec2< float > & plotSize  )
{
    return { ( ( selectionPosition.x() - plotPosition.x() ) / plotSize.x() ) * 2 - 1,
             ( ( selectionPosition.y() - plotPosition.y() ) / plotSize.y() ) * 2 - 1
           };
}

Vec2< float > BALEEN::getSelectionCenter( const Vec2< float > & relativeCenter )
{
    if( m_selectedPlot == "" )
    {
        return Vec2< float >( 1, 1 );
    }
    else
    {
        if( m_selectedPlot == "timeline" )
        {
            auto & plt = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_timePlotCombo.selectedItemText() );
            Vec2< double > rX = { 0.0, (double) getTimeSeries( m_particleCombo.selectedItemText() )->numSteps() };
            Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( plt.attr )->second;
            Vec2< float > to11 = ( relativeCenter + Vec2<float >( 1, 1 ) ) / 2.f;
            return { rX.a() + to11.x() * ( rX.b() - rX.a() ), rY.a() + to11.y() * ( rY.b() - rY.a() ) };
        }
        else
        {
            auto & plt =  m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_selectedPlot );
            Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( plt.attrs[ 0 ] )->second;
            Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( plt.attrs[ 1 ] )->second;
            Vec2< float > to11 = ( relativeCenter + Vec2<float >( 1, 1 ) ) / 2.f;
            return { rX.a() + to11.x() * ( rX.b() - rX.a() ), rY.a() + to11.y() * ( rY.b() - rY.a() ) };
        }
    }
}

Vec2< float > BALEEN::getRelativeSelectionRadius( const Vec2< float > & sz, float r )
{
    Vec2< float > scale( 1.f, sz.x() / sz.y() );
    scale = scale * r * ( scaledWidth() / sz.x() ) * m_plotGridView.size().y() / 800.0;
    return scale;
}

Vec2< float > BALEEN::getSelectionRadius( const Vec2< float > & relativeRadius )
{
    if( m_selectedPlot == "" )
    {
        return Vec2< float >( 1, 1 );
    }
    else
    {
        if( m_selectedPlot == "timeline" )
        {
            auto & plt = m_currentConfigurations.particleBasedConfiguration.m_timePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_timePlotCombo.selectedItemText() );
            Vec2< double > rX = { 0.0, (double) getTimeSeries( m_particleCombo.selectedItemText() )->numSteps() };
            Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( plt.attr )->second;
            Vec2< float > to11 = relativeRadius * 0.5;
            return { ( rX.b() - rX.a() ) * to11.x(), ( rY.b() - rY.a() ) * to11.y() };
        }
        else
        {
            auto & plt =  m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( m_particleCombo.selectedItemText() ).at( m_selectedPlot );
            Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( plt.attrs[ 0 ] )->second;
            Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( plt.attrs[ 1 ] )->second;
            Vec2< float > to11 = relativeRadius * 0.5;
            return { ( rX.b() - rX.a() ) * to11.x(), ( rY.b() - rY.a() ) * to11.y() };
        }
    }
}

void BALEEN::renderSelectionRadius( const Vec2< float > & pos, const Vec2< float > & scale )
{
    const int N_POINTS = 128;
    std::vector< Vec2< float > > circle( N_POINTS );

    for( int i = 0; i < N_POINTS; ++i )
    {
        float angle = ( i / ( float ) ( N_POINTS - 1 ) ) * 2.f * 3.14159265359;
        circle[ i ] = { pos + Vec2< float >( scale.x() * std::cos( angle ), scale.y() * std::sin( angle ) )  };
    }

    std::vector< Vec2< float > > lines =
    {
        { -1, pos.y()                    }, { pos.x() + scale.x(), pos.y() },
        { -1, pos.y() + scale.y() }, { pos.x(), pos.y() +  scale.y() },
        { -1, pos.y() - scale.y() }, { pos.x(), pos.y() -  scale.y() },
        { pos.x(), -1 }, { pos.x(), pos.y() + scale.y() },
        { pos.x() + scale.x(), -1 }, {  pos.x() + scale.x(), pos.y() },
        { pos.x() - scale.x(), -1 }, {  pos.x() - scale.x(), pos.y() }
    };

    glLineWidth( 4 );
    glPointSize( 4 );
    m_renderer->renderPrimitivesFlat( circle, cartesianProg, QVector4D( 115 / 255.f, 81 / 255.f, 150 / 255.f, 1 ), QMatrix4x4(), GL_LINE_STRIP );
    m_renderer->renderPrimitivesFlat( circle, cartesianProg, QVector4D( 115 / 255.f, 81 / 255.f, 150 / 255.f, 1 ), QMatrix4x4(), GL_POINTS );

//    glLineWidth( 1 );
//    glPointSize( 1 );
//    m_renderer->renderPrimitivesFlat( circle, cartesianProg, QVector4D( 1, 0, 0, 1 ), QMatrix4x4(), GL_LINE_STRIP );
//    m_renderer->renderPrimitivesFlat( circle, cartesianProg, QVector4D( 1, 0, 0, 1 ), QMatrix4x4(), GL_POINTS );

//    m_renderer->renderPrimitivesFlat( lines, cartesianProg, QVector4D( 1, 0, 0, 1 ), QMatrix4x4(), GL_LINES );
//    m_renderer->renderPrimitivesFlat( lines, cartesianProg, QVector4D( 1, 0, 0, 1 ), QMatrix4x4(), GL_POINTS );
}

void BALEEN::renderStatsView()
{

}

void BALEEN::renderRecurrenceView()
{

}

void BALEEN::renderTrajectoryView()
{
    if( m_particleSelectionSample.size() < 0 )
    {
        return;
    }

    float space = m_trajectoryView.size().x() - m_trajectoryView.SCROLL_BAR_HEIGHT;

    m_trajectoryViewNCols = std::floor( space / 74.f );
    int colspace = ( space / m_trajectoryViewNCols );

    m_trajectoryViewNRows = std::floor( ( m_trajectoryView.size().y() - 60 + BUTTON_HEIGHT ) / BUTTON_HEIGHT );
    int rowSize = ( m_trajectoryView.size().y() - 60 + BUTTON_HEIGHT ) / ( float ) m_trajectoryViewNRows;
    float yOffset = m_trajectoryView.position().y() + m_trajectoryView.size().y() - 60;

    if( m_selectedPlot ==  "" )
    {
        return;
    }

    for( int i = 0, col = 0, row = 0; i < m_particleSelectionSample.size(); ++i )
    {
        float w = colspace;
        if( col+1 >= m_trajectoryViewNCols )
        {
            w = ( m_trajectoryView.position().x() + m_trajectoryView.size().x() - m_trajectoryView.SCROLL_BAR_HEIGHT ) - ( m_trajectoryView.position().x() + col*colspace );
        }

        ViewPort vprt;
        vprt.bkgColor( Vec4( .99, 0.99, .99, 1 ) );
        vprt.setSize( w, rowSize );
        vprt.offsetX( m_trajectoryView.position().x() + col*colspace );
        vprt.offsetY( yOffset - row*rowSize );

        if( m_selectedParticle == m_particleSelectionSample[ i ] )
        {
            vprt.bkgColor( Vec4( 1, 0.8, 0.7, 1 ) );
        }

        m_renderer->clearViewPort( vprt, cartesianProg );
        glViewport( vprt.offsetX(), vprt.offsetY(), vprt.width(), vprt.height() );

        if( m_selectedParticle == m_particleSelectionSample[ i ] )
        {
            m_renderer->renderViewPortBoxShadow( vprt, simpleColorProg );
        }
        else
        {
            m_renderer->renderPopSquare( simpleColorProg );
        }

        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText(
            false,
            textProg,
            scaledWidth(),
            scaledHeight(),
            std::to_string( m_particleSelectionSample[ i ] ),
            vprt.offsetX() + 4,
            vprt.offsetY() + 7,
            1.0,
            m_textColor );

        ++col;
        if( col >= m_trajectoryViewNCols )
        {
            ++row;
            col = 0;
        }
    }
}

void BALEEN::constructLineIndicators(
    const Vec2< float > & linePos,
    const RangeSelector & rangeSelector,
    std::vector< TN::Vec2< float > > & lines,
    std::vector< TN::Vec2< float > > & triangles )
{
    const int OFFSET = rangeSelector.size().y() + DIVIDER_WIDTH/2.0 - 2;

    lines =
        std::vector< TN::Vec2< float > > (
    {
        {
            linePos.a(),
            ( rangeSelector.position().y() - OFFSET )
        },
        {
            linePos.a(),
            ( rangeSelector.position().y() + rangeSelector.size().y() )
        },

        {
            linePos.b(),
            ( rangeSelector.position().y() - OFFSET )
        },
        {
            linePos.b(),
            ( rangeSelector.position().y() + rangeSelector.size().y() )
        }
//        { linePos.a(),
//          ( rangeSelector.position().y() + rangeSelector.size().y() + OFFSET ) },
//        {   rangeSelector.position().x(),
//          ( rangeSelector.position().y() + rangeSelector.size().y() + OFFSET )},

//        { linePos.b(),
//          ( rangeSelector.position().y() + rangeSelector.size().y() + OFFSET ) },
//        { ( rangeSelector.position().x() + rangeSelector.size().x() ),
//          ( rangeSelector.position().y() + rangeSelector.size().y() + OFFSET )},

//        {  rangeSelector.position().x(),
//           ( rangeSelector.position().y() + rangeSelector.size().y() + OFFSET )},
//        {   rangeSelector.position().x(),
//          ( position().y() ) },

//        { ( rangeSelector.position().x() + rangeSelector.size().x() ),
//          ( rangeSelector.position().y() + rangeSelector.size().y() + OFFSET ) },
//        { ( rangeSelector.position().x() + rangeSelector.size().x() ) ,
//          ( position().y() ) }
    } );

    const float T_SIZE = 8;

//    triangles =
//    std::vector< TN::Vec2< float > > ( {
//        { linePos.a(),
//          ( rangeSelector.position().y() + rangeSelector.size().y() - 2 ) },

//        { linePos.a() - T_SIZE / 2,
//          ( rangeSelector.position().y() + rangeSelector.size().y() + T_SIZE - 2 ) },

//        { linePos.a() + T_SIZE / 2,
//          ( rangeSelector.position().y() + rangeSelector.size().y() + T_SIZE - 2 ) },

//        { linePos.b(),
//          ( rangeSelector.position().y() + rangeSelector.size().y() - 2 ) },

//        { linePos.b() - T_SIZE / 2,
//          ( rangeSelector.position().y() + rangeSelector.size().y() + T_SIZE - 2 ) },

//        { linePos.b() + T_SIZE / 2,
//          ( rangeSelector.position().y() + rangeSelector.size().y() + T_SIZE - 2 ) },
//    } );

    for( auto & p : lines )
    {
        p.x( p.x() * 2.f /  scaledWidth() - 1 );
        p.y( p.y() * 2.f / scaledHeight() - 1 );
    }

    for( auto & p : triangles )
    {
        p.x( p.x() * 2.f /  scaledWidth() - 1 );
        p.y( p.y() * 2.f / scaledHeight() - 1 );
    }
}

void BALEEN::computeRecurrence( const std::vector< std::string > & attrs, const std::vector< float > & scales, const size_t PIDX, std::vector< float > & distanceMatrix )
{
    const size_t NT = getTimeSeries( "ions" )->numSteps();
    distanceMatrix = std::vector< float >( NT * NT, 0 );

    for( size_t ta = 0; ta < NT; ++ta )
    {
        for( size_t tb = 0; tb < NT; ++tb )
        {
            for( int i = 0; i < attrs.size(); ++i )
            {
                const std::vector< float > & va = *( m_dataSetManager->particleDataManager().values( "ions", attrs[ i ] ) [ ta ] );
                const std::vector< float > & vb = *( m_dataSetManager->particleDataManager().values( "ions", attrs[ i ] ) [ tb ] );

                if( attrs[ i ] == "zeta" || attrs[ i ] == "poloidal_angle" )
                {
                    float d =  std::atan2( std::sin(va[ PIDX ] -vb[ PIDX ]), std::cos(va[ PIDX ] -vb[ PIDX ] ) );
                    distanceMatrix[ ta*NT + tb ] += d*d;
                }
                else
                {
                    distanceMatrix[ ta*NT + tb ] += std::pow( ( va[ PIDX ] - vb[ PIDX ] ) * scales[ i ], 2.f );
                }
            }
        }
    }
    for( size_t ta = 0; ta < NT; ++ta )
    {
        for( size_t tb = 0; tb < NT; ++tb )
        {
            distanceMatrix[ ta*NT + tb ] = std::sqrt( distanceMatrix[ ta*NT + tb ] );
        }
    }
}

void BALEEN::renderRangeView()
{
    float spacing = DIVIDER_WIDTH;
    float rwHeight = BUTTON_HEIGHT;
    float rwWidth  = m_rangeView.size().x() - m_rangeView.SCROLL_BAR_HEIGHT - spacing*2;
    float labelHeight =  16;
    float LINEPLOT_HT = 96;
    float fullYSize = labelHeight * 2 + rwHeight * 2 + spacing*2 + LINEPLOT_HT;
    float yOffset = m_rangeView.position().y() + m_rangeView.size().y() - 52 + LINEPLOT_HT - labelHeight * 2 - spacing;
    float xOffset = m_rangeView.position().x() + spacing;
    Vec2< float >  plotSZ( rwWidth, LINEPLOT_HT );

    std::sort(
        m_rangeView.rangeWidgets.begin(),
        m_rangeView.rangeWidgets.end(),
        [=]( const std::pair< RangeSelector, RangeSelector > & a, const std::pair< RangeSelector, RangeSelector > & b ) -> bool
    {
        if( m_recurrenceSpace.count( a.first.id ) && ! m_recurrenceSpace.count( b.first.id ) )
        {
            return true;
        }
        return false;
    } );


    bool updateBins = false;
    static int tStatic = m_selectedTimeStep;
    if( m_selectedTimeStep != tStatic )
    {
        updateBins = true;
    }
    tStatic = m_selectedTimeStep;

    const int N_BINS = 200;

    QVector4D OcclusionColor( 0, 0, 0, .2 );
    int i = 0;

    static std::vector< float > result;
    result.resize( N_BINS * m_rangeView.rangeWidgets.size() * 2 );

    if( updateBins || m_updateBins )
    {
        m_statsComputer.allocateResultArray(
            getTimeSeries( m_particleCombo.selectedItemText() )->numSteps(),
            m_rangeView.rangeWidgets.size(),
            N_BINS );

        m_statsComputer.computeCurrentHistograms(
            m_dataSetManager->particleDataManager().numLoadedParticles( m_particleCombo.selectedItemText() ),
            getTimeSeries( m_particleCombo.selectedItemText() )->numSteps(),
            m_selectedTimeStep,
            m_rangeView.rangeWidgets.size(),
            N_BINS,
            result,
            true );

        m_updateBins = false;
    }

    const int NV = m_rangeView.rangeWidgets.size();

    int c = 0;
    for( auto & rw : m_rangeView.rangeWidgets )
    {
        if( c > 4 )
        {
            break;
        }
        ++c;

        int SALL  = 0;
        int SCOMB = 1;

        int V = m_vboIndexes.at( rw.first.id );

        const int NB = rw.first.N_BINS;

        auto beginAll = result.begin() + SALL*NV*NB + V*NB;
        auto endAll = beginAll + NB;

        std::vector< float > histAll(   beginAll,  endAll );

        auto beginCOMB = result.begin() + SCOMB*NV*NB + V*NB;
        auto endCOMB = beginCOMB + NB;
        std::vector< float > histComb( beginCOMB, endCOMB );

        if( updateBins )
        {
            rw.first.colorMode = 1;
            rw.first.setHistogram(   histAll );
            rw.second.colorMode = 2;
            rw.second.setHistogram( histComb );
        }

        rw.first.setPosition( xOffset, yOffset - fullYSize*( i + 1 ) );
        rw.first.setSize( rwWidth, rwHeight );

        rw.second.setPosition( xOffset, yOffset - fullYSize*( i + 1 ) + rwHeight + 2 );
        rw.second.setSize( rwWidth, rwHeight );

//        if(  m_recurrenceSpace.count( rw.first.id ) )
//        {
//            m_rangeView.varSelectButtons[ i ].setPressed( true );
//        }
//        else
//        {
//            m_rangeView.varSelectButtons[ i ].setPressed( false );
//        }

//        m_rangeView.varSelectButtons[ i ].setPosition( xOffset + rwWidth + spacing, yOffset - fullYSize*( i + 1 ) + rwHeight + 2 );
//        m_rangeView.varSelectButtons[ i ].setSize( rwHeight, rwHeight );
//        m_renderer->renderPressButton( m_rangeView.varSelectButtons[ i ], cartesianProg, simpleColorProg );

        glViewport( 0, 0, scaledWidth(), scaledHeight() );

//        m_renderer->renderText(
//            false,
//            textProg, scaledWidth(),
//            scaledHeight(),
//            " r ",
//            m_rangeView.varSelectButtons[ i ].position().x() + TEXT_PAD - 4,
//            m_rangeView.varSelectButtons[ i ].position().y() + TEXT_PAD, 1.0,
//            m_textColor );

        m_renderer->renderText(
            true,
            textProg,
            scaledWidth(),
            scaledHeight(),
            rw.first.id,
            rw.first.position().x(),
            rw.first.position().y() + ( fullYSize - labelHeight*2 ),
            1.0,
            m_textColor );

        glViewport(
            rw.first.position().x(),
            rw.first.position().y(),
            rw.first.size().x(),
            rw.first.size().y() );
        m_renderer->clear( QVector4D( 1, 1, 1, 1 ), cartesianProg );

        m_renderer->renderPrimitivesColored(
            rw.first.geometry(),
            rw.first.colors(),
            1.0,
            simpleColorProg,
            QMatrix4x4(),
            GL_TRIANGLES );
        m_renderer->renderPopSquare( simpleColorProg );

        glViewport(
            rw.second.position().x(),
            rw.second.position().y(),
            rw.second.size().x(),
            rw.second.size().y() );

        m_renderer->clear( QVector4D( 1, 1, 1, 1 ), cartesianProg );
        m_renderer->renderPrimitivesColored(
            rw.second.geometry(),
            rw.second.colors(),
            1.0,
            simpleColorProg,
            QMatrix4x4(),
            GL_TRIANGLES );

        m_renderer->renderPopSquare( simpleColorProg );
        ++i;

        Vec2< float > linePos = rw.first.getRangeSelectionPositions();
        std::vector< Vec2< float > > lineIndicators;
        std::vector< Vec2< float > > lineIndicatorsArrow;
        constructLineIndicators( linePos, rw.first, lineIndicators, lineIndicatorsArrow );

//        glViewport( 0, 0, scaledWidth(), scaledHeight() );
//        glLineWidth( 2 );
//        m_renderer->renderPrimitivesFlat(
//            lineIndicators,
//            cartesianProg,
//            QVector4D( 1, 1, 1, 1 ),
//            QMatrix4x4(),
//            GL_LINES
//        );

        glViewport(
            rw.first.position().x(),
            rw.first.position().y(),
            linePos.a() - rw.first.position().x(),
            rw.first.size().y() );
        m_renderer->clear( OcclusionColor, cartesianProg );

        glViewport(
            linePos.b(),
            rw.first.position().y(),
            ( rw.first.position().x() + rw.first.size().x() ) - linePos.b(),
            rw.first.size().y() );
        m_renderer->clear( OcclusionColor, cartesianProg );

        glViewport(
            rw.second.position().x(),
            rw.second.position().y(),
            linePos.a() - rw.second.position().x(),
            rw.second.size().y() );
        m_renderer->clear( OcclusionColor, cartesianProg );

        glViewport(
            linePos.b(),
            rw.second.position().y(),
            ( rw.second.position().x() + rw.second.size().x() ) - linePos.b(),
            rw.second.size().y() );
        m_renderer->clear( OcclusionColor, cartesianProg );

        renderPhasePlotGrid(
            plotSZ,
        { xOffset, rw.second.position().y() + rw.second.size().y() + spacing },
        { 0, 0 },
        "",
        "f",
        { 0.0, 1.0 },
        { 0.0, 1.0 },
        false,
        false );

        glScissor( 0, 0, scaledWidth(), scaledHeight() );
        glDisable( GL_SCISSOR_TEST );

        Vec2< double > modifiedRange( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() );
        I2 idx2 = rw.second.getSelectedRangeStartEnd();
        const int N_SETS = m_subsetCombo.numItems() + 3;

        // Just the combination and selection
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       for( int i = 0; i < 2; ++i )
       {
           const int OFFSET = i*NV*NB + V*NB;

           double mx = * std::max_element( result.begin() + OFFSET + idx2.a(), result.begin() + OFFSET + idx2.b() );
           if( mx > 0 )
           {
               double mn = * std::min_element( result.begin() + OFFSET + idx2.a(), result.begin() + OFFSET + idx2.b() );
               modifiedRange.a( std::min( mn, modifiedRange.a() ) );
               modifiedRange.b( std::max( mx, modifiedRange.b() ) );
           }
       }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        glViewport(
            xOffset,
            rw.second.position().y() + rw.second.size().y() + spacing,
            plotSZ.x(),
            plotSZ.y() );

        const int DRAW_SIZE = 4.0;

        glLineWidth( DRAW_SIZE );
        glPointSize( DRAW_SIZE );

        double yWidth = modifiedRange.b() - modifiedRange.a();

        const int NMB = idx2.b() - idx2.a();

        QMatrix4x4 M;
        M.scale( 2.0 / ( NMB - 1 ),  2.0 / yWidth );
        M.translate( -(  NMB - 1 ) / 2.0,  -( modifiedRange.a() + ( yWidth / 2.0 ) ) );

        std::vector< float > x( NMB );
        for( int b = 0; b < NMB; ++b )
        {
            x[ b ] = b;
        }

        for( int i = 0; i < 2; ++i )
        {
            const int OFFSET = i*NV*NB + V*NB;
            std::vector< float > y( result.begin() + OFFSET + idx2.a(), result.begin() + OFFSET + idx2.b() );

            QVector4D color = i == 0 ? QVector4D( 0.2, 0.2, 0.2, 1 ) : m_combinationColor;

            glLineWidth( 2 );
            glPointSize( 2 );

            m_renderer->renderPrimitivesFlatSerial(
                x,
                y,
                flatProgSerial,
                color,
                M,
                GL_LINE_STRIP
            );

            m_renderer->renderPrimitivesFlatSerial(
                x,
                y,
                flatProgSerial,
                color,
                M,
                GL_POINTS
            );
        }


//        for( int i = 0; i < N_SETS; ++i )
//        {
//            if( i == 3  || ( ! m_timeSubsetStatsSelector.isPressed() && i > 2 ) )
//            {
//                continue;
//            }

//            const int OFFSET = i*NV*NB + V*NB;
//            std::vector< float > y( result.begin() + OFFSET + idx2.a(), result.begin() + OFFSET + idx2.b() );

//            QVector4D color = m_combinationColor;

//            int CIDX = i;
//            if( i > 3 )
//            {
//                CIDX -= 3;
//            }
//            if( i == 0 || i > 3 )
//            {
//                color = QVector4D( m_subsetColorCodes[ CIDX ].r(), m_subsetColorCodes[ CIDX ].g(), m_subsetColorCodes[ CIDX ].b(), 1.0 );
//            }

//            color.setW( 1.0 );

//            glLineWidth( 2 );
//            glPointSize( 2 );

//            m_renderer->renderPrimitivesFlatSerial(
//                x,
//                y,
//                flatProgSerial,
//                color,
//                M,
//                GL_LINE_STRIP
//            );
//        }
    }
}

// * range
void BALEEN::computeContextLayersRange(
    PhasePlotCache & cache,
    std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
    const std::string & x,
    const std::string & y,
    const std::string & ws,
    const Vec2< int > & resolution,
    const std::string & ptype,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QMatrix4x4 & M,
    size_t N_TEST,
    bool wrap )
{
    std::pair< TNR::TextureLayer, TNR::TextureLayer > p1;
    std::pair< TNR::TextureLayer, TNR::TextureLayer > p2;

    computeContextLayersMax( cache, p1, x, y, ws, resolution, ptype, counts, firsts, M, N_TEST, wrap );
    computeContextLayersMin( cache, p2, x, y, ws, resolution, ptype, counts, firsts, M, N_TEST, wrap );

    layers.first.resize( resolution.a(), resolution.b() );
    layers.second.resize( resolution.a(), resolution.b() );

    cache.setTextures( layers.first, layers.second );

    cache.bindFrameBuffer();

    glViewport( 0, 0, layers.second.width, layers.second.height );
    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    m_texVAO->bind();
    m_texVBO->bind();

    differenceProgram->bind();

    glActiveTexture( GL_TEXTURE0 );
    p1.first.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE1 );
    p2.first.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE2 );
    p1.second.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE3 );
    p2.second.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture(GL_TEXTURE0);

    differenceProgram->setUniformValue( "tex1", 0 );
    differenceProgram->setUniformValue( "tex2", 1 );
    differenceProgram->setUniformValue( "tex3", 2 );
    differenceProgram->setUniformValue( "tex4", 3 );

    GLuint posAttr = differenceProgram->attributeLocation( "ps" );
    GLuint uvAttr  = differenceProgram->attributeLocation( "tc" );

    differenceProgram->enableAttributeArray( posAttr );
    differenceProgram->enableAttributeArray( uvAttr );

    differenceProgram->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    differenceProgram->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    differenceProgram->disableAttributeArray( posAttr );
    differenceProgram->disableAttributeArray( uvAttr );

    differenceProgram->release();

    m_texVBO->release();
    m_texVAO->release();

    layers.first.computeRange(  );
    layers.second.computeRange( );

    cache.releaseFrameBuffer();

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

// * min
void BALEEN::computeContextLayersMin(
    PhasePlotCache & cache,
    std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
    const std::string & x,
    const std::string & y,
    const std::string & ws,
    const Vec2< int > & resolution,
    const std::string & ptype,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QMatrix4x4 & M,
    size_t N_TEST,
    bool wrap )
{
    auto & program = wrap ? m_minMaxProgramWrapAround : m_minMaxProgram;

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    const long int xOffset = m_vboIndexes.at( x );
    const long int yOffset = m_vboIndexes.at( y );
    const long int wOffset = m_vboIndexes.at( ws );
    const long int cOffset = m_vboIndexes.size();
    const long int NPT = m_subsetBitmasks.size();
    const size_t NT = NPT / NP;

    layers.first.resize( resolution.a(), resolution.b() );
    layers.second.resize( resolution.a(), resolution.b() );

    cache.setTextures(
        layers.first,
        layers.second );

    cache.bindFrameBuffer();

    glClearColor( std::numeric_limits< float >::max() * 0.95, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT );
    glClearColor( 0, 0, 0, 1 );

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);
    glBlendEquation( GL_MIN );

    glLineWidth( 2.0 );
    glPointSize( 2.0 );
    glViewport( 0, 0, layers.second.width, layers.second.height );

    program->bind();

    GLuint x1 = program->attributeLocation( "x1" );
    GLuint x2 = program->attributeLocation( "x2" );
    GLuint w = m_minMaxProgram->attributeLocation(  "weight" );
    GLuint c  = program->attributeLocation(  "c" );

    m_phaseVAO->bind();
    m_phaseVBO->bind();

    if( wrap )
    {
        program->setUniformValue( "wrap1", -1.f );
        program->setUniformValue( "wrap2",  1.f );
    }

    program->setUniformValue( "M", M );

    program->enableAttributeArray( x1 );
    program->enableAttributeArray( x2 );
    program->enableAttributeArray( w );
    program->enableAttributeArray( c );

    program->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( w, GL_FLOAT, wOffset*NPT*sizeof( float ), 1 );
    glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ), (void*)( cOffset*NPT*sizeof( float ) ) );

    //glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
    glDrawArrays( GL_LINE_STRIP, 0, N_TEST*NT );

    program->disableAttributeArray( x1 );
    program->disableAttributeArray( x2 );
    program->disableAttributeArray(  w );
    program->disableAttributeArray(  c );

    m_phaseVBO->release();
    m_phaseVAO->release();
    program->release();

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glBlendEquation( GL_FUNC_ADD );

    layers.first.maxMask(  std::numeric_limits< float >::max() * 0.9 );
    layers.second.maxMask( std::numeric_limits< float >::max() * 0.9 );

    layers.first.computeRange(  );
    layers.second.computeRange( );

    cache.releaseFrameBuffer();

    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

// * max
void BALEEN::computeContextLayersMax(
    PhasePlotCache & cache,
    std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
    const std::string & x,
    const std::string & y,
    const std::string & ws,
    const Vec2< int > & resolution,
    const std::string & ptype,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QMatrix4x4 & M,
    size_t N_TEST,
    bool wrap )
{
    auto & program = wrap ? m_minMaxProgramWrapAround : m_minMaxProgram;

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    const long int xOffset = m_vboIndexes.at( x );
    const long int yOffset = m_vboIndexes.at( y );
    const long int wOffset = m_vboIndexes.at( ws );
    const long int cOffset = m_vboIndexes.size();
    const long int NPT = m_subsetBitmasks.size();
    const size_t NT = NPT / NP;

    layers.first.resize( resolution.a(), resolution.b() );
    layers.second.resize( resolution.a(), resolution.b() );

    cache.setTextures(
        layers.first,
        layers.second );

    cache.bindFrameBuffer();

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);
    glBlendEquation( GL_MAX );

    glLineWidth( 2.0 );
    glPointSize( 2.0 );
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glViewport( 0, 0, layers.second.width, layers.second.height );
    glClear( GL_COLOR_BUFFER_BIT );

    program->bind();

    GLuint x1 = program->attributeLocation( "x1" );
    GLuint x2 = program->attributeLocation( "x2" );
    GLuint w = program->attributeLocation(  "weight" );
    GLuint c  = program->attributeLocation(  "c" );

    m_phaseVAO->bind();
    m_phaseVBO->bind();

    program->setUniformValue( "M", M );

    if( wrap )
    {
        program->setUniformValue( "wrap1", -1.f );
        program->setUniformValue( "wrap2",  1.f );
    }

    program->enableAttributeArray( x1 );
    program->enableAttributeArray( x2 );
    program->enableAttributeArray( w );
    program->enableAttributeArray( c );

    program->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( w, GL_FLOAT, wOffset*NPT*sizeof( float ), 1 );
    glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ), (void*)( cOffset*NPT*sizeof( float ) ) );

//    glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
    glDrawArrays( GL_LINE_STRIP, 0, N_TEST*NT );

    program->disableAttributeArray( x1 );
    program->disableAttributeArray( x2 );
    program->disableAttributeArray(  w );
    program->disableAttributeArray(  c );

    m_phaseVBO->release();
    m_phaseVAO->release();
    program->release();

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glBlendEquation( GL_FUNC_ADD );

    layers.first.computeRange(  );
    layers.second.computeRange( );

    cache.releaseFrameBuffer();

    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

// * weighted density
void BALEEN::computeContextLayerWeightedDensity(
    PhasePlotCache & cache,
    std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
    const std::string & x,
    const std::string & y,
    const std::string & ws,
    const Vec2< int > & resolution,
    const std::string & ptype,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QMatrix4x4 & M,
    size_t N_TEST,
    bool wrap )
{
    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    auto & program = wrap ? m_densityProgramWeightedWrapAround : m_densityProgramWeighted; //m_densityProgramWeighted;

    const long int xOffset = m_vboIndexes.at( x );
    const long int yOffset = m_vboIndexes.at( y );
    const long int wOffset = m_vboIndexes.at( ws );
    const long int cOffset = m_vboIndexes.size();
    const long int NPT = m_subsetBitmasks.size();
    const size_t NT = NPT / NP;

    layers.first.resize( resolution.a(), resolution.b() );
    layers.second.resize( resolution.a(), resolution.b() );

    cache.setTextures(
        layers.first,
        layers.second );

    cache.bindFrameBuffer();

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);

    glLineWidth( 2.0 );
    glPointSize( 2.0 );
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glViewport( 0, 0, layers.second.width, layers.second.height );
    glClear( GL_COLOR_BUFFER_BIT );

    program->bind();

    GLuint x1 = program->attributeLocation( "x1" );
    GLuint x2 = program->attributeLocation( "x2" );
    GLuint w = program->attributeLocation(  "weight" );
    GLuint c  = program->attributeLocation(  "c" );

    m_phaseVAO->bind();
    m_phaseVBO->bind();

    glActiveTexture( GL_TEXTURE0 );
    cache.angleToPixelDensityTexture.bind();
    glActiveTexture( GL_TEXTURE0 );

    program->setUniformValue( "angleToNormalization", 0 );
    program->setUniformValue( "M", M );
    program->setUniformValue( "width", static_cast< float >( layers.second.width ) );
    program->setUniformValue( "height", static_cast< float >( layers.second.height ) );

    if( wrap )
    {
        program->setUniformValue( "wrap1", -1.f );
        program->setUniformValue( "wrap2",  1.f );
    }

    program->enableAttributeArray( x1 );
    program->enableAttributeArray( x2 );
    program->enableAttributeArray( w );
    program->enableAttributeArray( c );

    program->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( w, GL_FLOAT, wOffset*NPT*sizeof( float ), 1 );
    glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ), (void*)( cOffset*NPT*sizeof( float ) ) );

//    glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
    glDrawArrays( GL_LINE_STRIP, 0, N_TEST*NT );

    program->disableAttributeArray( x1 );
    program->disableAttributeArray( x2 );
    program->disableAttributeArray(  w );
    program->disableAttributeArray(  c );

    m_phaseVBO->release();
    m_phaseVAO->release();
    program->release();

    layers.first.computeRange(  );
    layers.second.computeRange( );

    cache.releaseFrameBuffer();

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

// * mean
void BALEEN::computeContextLayersMean(
    PhasePlotCache & cache,
    std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
    const std::string & x,
    const std::string & y,
    const std::string & ws,
    const Vec2< int > & resolution,
    const std::string & ptype,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QMatrix4x4 & M,
    size_t N_TEST,
    bool wrap )
{
    auto & program = wrap ? m_summationProgramWrapAround : m_summationProgram;

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    const long int xOffset = m_vboIndexes.at( x );
    const long int yOffset = m_vboIndexes.at( y );
    const long int wOffset = m_vboIndexes.at( ws );
    const long int cOffset = m_vboIndexes.size();
    const long int NPT = m_subsetBitmasks.size();
    const size_t NT = NPT / NP;

    m_countAll.resize( resolution.a(), resolution.b() );
    layers.first.resize( resolution.a(), resolution.b() );
    m_sumAll.resize( resolution.a(), resolution.b() );
    layers.second.resize( resolution.a(), resolution.b() );
    m_countComb.resize( resolution.a(), resolution.b() );
    m_sumComb.resize( resolution.a(), resolution.b() );

    cache.setTextures(
        m_sumAll,
        m_sumComb,
        m_countAll,
        m_countComb );

    cache.bindFrameBuffer();

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);

    glLineWidth( 2.0 );
    glPointSize( 2.0 );
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glViewport( 0, 0, layers.second.width, layers.second.height );
    glClear( GL_COLOR_BUFFER_BIT );

    program->bind();

    GLuint x1 = program->attributeLocation( "x1" );
    GLuint x2 = program->attributeLocation( "x2" );
    GLuint w = program->attributeLocation(  "weight" );
    GLuint c  = program->attributeLocation(  "c" );

    m_phaseVAO->bind();
    m_phaseVBO->bind();

    glActiveTexture( GL_TEXTURE0 );
    cache.angleToPixelDensityTexture.bind();
    glActiveTexture( GL_TEXTURE0 );

    program->setUniformValue( "angleToNormalization", 0 );
    program->setUniformValue( "M", M );
    program->setUniformValue( "width", static_cast< float >( layers.second.width ) );
    program->setUniformValue( "height", static_cast< float >( layers.second.height ) );

    if( wrap )
    {
        program->setUniformValue( "wrap1", -1.f );
        program->setUniformValue( "wrap2",  1.f );
    }

    program->enableAttributeArray( x1 );
    program->enableAttributeArray( x2 );
    program->enableAttributeArray( w );
    program->enableAttributeArray( c );

    program->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( w, GL_FLOAT, wOffset*NPT*sizeof( float ), 1 );
    glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ), (void*)( cOffset*NPT*sizeof( float ) ) );

    glDrawArrays( GL_LINE_STRIP, 0, N_TEST*NT );

    program->disableAttributeArray( x1 );
    program->disableAttributeArray( x2 );
    program->disableAttributeArray(  w );
    program->disableAttributeArray(  c );

    m_phaseVBO->release();
    m_phaseVAO->release();
    program->release();

    cache.setTextures( layers.first, layers.second );
    cache.bindFrameBuffer();
    glViewport( 0, 0, layers.second.width, layers.second.height );
    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    m_texVAO->bind();
    m_texVBO->bind();

    textureDivideProgram->bind();

    glActiveTexture( GL_TEXTURE0 );
    m_sumAll.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE1 );
    m_countAll.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE2 );
    m_sumComb.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE3 );
    m_countComb.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture(GL_TEXTURE0);
    glGenerateMipmap( GL_TEXTURE_2D );

    textureDivideProgram->setUniformValue(  "sumAll",    0 );
    textureDivideProgram->setUniformValue(  "countAll",  1 );
    textureDivideProgram->setUniformValue( "sumComb",    2 );
    textureDivideProgram->setUniformValue( "countComb",  3 );

    GLuint posAttr = textureDivideProgram->attributeLocation( "ps" );
    GLuint uvAttr  = textureDivideProgram->attributeLocation( "tc" );

    textureDivideProgram->enableAttributeArray( posAttr );
    textureDivideProgram->enableAttributeArray( uvAttr );

    textureDivideProgram->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    textureDivideProgram->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    textureDivideProgram->disableAttributeArray( posAttr );
    textureDivideProgram->disableAttributeArray( uvAttr );

    textureDivideProgram->release();

    layers.first.computeRange(  );
    layers.second.computeRange( );

    m_texVBO->release();
    m_texVAO->release();

    cache.releaseFrameBuffer();

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

// * variance
void BALEEN::computeContextLayersVariance(
    PhasePlotCache & cache,
    std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
    const std::string & x,
    const std::string & y,
    const std::string & ws,
    const Vec2< int > & resolution,
    const std::string & ptype,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QMatrix4x4 & M,
    size_t N_TEST,
    bool wrap )
{
    computeContextLayersMean(  cache, layers, x, y, ws, resolution, ptype, counts, firsts, M, N_TEST, wrap );

    //////////

    auto & program = wrap ? wrapProgramV : m_varianceProgram;

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    const long int xOffset = m_vboIndexes.at( x );
    const long int yOffset = m_vboIndexes.at( y );
    const long int wOffset = m_vboIndexes.at( ws );
    const long int cOffset = m_vboIndexes.size();
    const long int NPT = m_subsetBitmasks.size();
    const size_t NT = NPT / NP;

    m_sumAll.resize( resolution.a(), resolution.b() );
    m_sumComb.resize( resolution.a(), resolution.b() );

    cache.setTextures(
        m_sumAll,
        m_sumComb );

    cache.bindFrameBuffer();

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);

    glLineWidth( 2.0 );
    glPointSize( 2.0 );
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glViewport( 0, 0, layers.second.width, layers.second.height );
    glClear( GL_COLOR_BUFFER_BIT );

    program->bind();

    GLuint x1 = program->attributeLocation( "x1" );
    GLuint x2 = program->attributeLocation( "x2" );
    GLuint w = program->attributeLocation(  "weight" );
    GLuint c  = program->attributeLocation(  "c" );

    m_phaseVAO->bind();
    m_phaseVBO->bind();

    glActiveTexture( GL_TEXTURE0 );
    layers.first.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE1 );
    layers.second.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE2 );
    cache.angleToPixelDensityTexture.bind();

    glActiveTexture( GL_TEXTURE0 );

    program->setUniformValue( "tex1", 0 );
    program->setUniformValue( "tex2", 1 );
    program->setUniformValue( "angleToNormalization", 2 );

    program->setUniformValue( "M", M );
    program->setUniformValue( "width", static_cast< float >( layers.second.width ) );
    program->setUniformValue( "height", static_cast< float >( layers.second.height ) );

    if( wrap )
    {
        program->setUniformValue( "wrap1", -1.f );
        program->setUniformValue( "wrap2",  1.f );
    }

    program->enableAttributeArray( x1 );
    program->enableAttributeArray( x2 );
    program->enableAttributeArray( w );
    program->enableAttributeArray( c );

    program->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( w, GL_FLOAT, wOffset*NPT*sizeof( float ), 1 );
    glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ), (void*)( cOffset*NPT*sizeof( float ) ) );

    glDrawArrays( GL_LINE_STRIP, 0, N_TEST*NT );

    program->disableAttributeArray( x1 );
    program->disableAttributeArray( x2 );
    program->disableAttributeArray(  w );
    program->disableAttributeArray(  c );

    m_phaseVBO->release();
    m_phaseVAO->release();
    program->release();

    //////////

    cache.setTextures( layers.first, layers.second );
    cache.bindFrameBuffer();
    glViewport( 0, 0, layers.second.width, layers.second.height );
    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    m_texVAO->bind();
    m_texVBO->bind();

    textureDivideProgram->bind();

    glActiveTexture( GL_TEXTURE0 );
    m_sumAll.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE1 );
    m_countAll.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE2 );
    m_sumComb.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture( GL_TEXTURE3 );
    m_countComb.tex.bind();
    glGenerateMipmap( GL_TEXTURE_2D );

    glActiveTexture(GL_TEXTURE0 );
    glGenerateMipmap( GL_TEXTURE_2D );

    textureDivideProgram->setUniformValue(  "sumAll",    0 );
    textureDivideProgram->setUniformValue(  "countAll",  1 );
    textureDivideProgram->setUniformValue( "sumComb",    2 );
    textureDivideProgram->setUniformValue( "countComb",  3 );

    GLuint posAttr = textureDivideProgram->attributeLocation( "ps" );
    GLuint uvAttr  = textureDivideProgram->attributeLocation( "tc" );

    textureDivideProgram->enableAttributeArray( posAttr );
    textureDivideProgram->enableAttributeArray( uvAttr );

    textureDivideProgram->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
    textureDivideProgram->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

    glDrawArrays( GL_TRIANGLES, 0, 6 );

    textureDivideProgram->disableAttributeArray( posAttr );
    textureDivideProgram->disableAttributeArray( uvAttr );

    textureDivideProgram->release();

    layers.first.computeRange(  );
    layers.second.computeRange(  );

    m_texVBO->release();
    m_texVAO->release();

    cache.releaseFrameBuffer();

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

void BALEEN::computeContextLayers2(
    PhasePlotCache & cache,
    std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
    const std::string & x,
    const std::string & y,
    const Vec2< int > & resolution,
    const std::string & ptype,
    const std::vector< GLsizei > & counts,
    const std::vector< GLint > & firsts,
    const QMatrix4x4 & M,
    bool distanceWeighted,
    size_t N_TEST,
    bool wrap )
{
    auto & program = wrap ? m_densityProgramWrapAround : m_densityProgram;

    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    const long int xOffset = m_vboIndexes.at( x );
    const long int yOffset = m_vboIndexes.at( y );
    const long int cOffset = m_vboIndexes.size();

    const long int NPT = m_subsetBitmasks.size();
    const int NT = NPT / NP;//m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

    cache.setSize( resolution.a(), resolution.b() );
    layers.first.resize( resolution.a(), resolution.b() );
    layers.second.resize( resolution.a(), resolution.b() );
    cache.setTextures( layers.first, layers.second );
    cache.bindFrameBuffer();

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE);

    glLineWidth( 2.0 );
    glPointSize( 2.0 );
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glViewport( 0, 0, layers.second.width, layers.second.height );
    glClear( GL_COLOR_BUFFER_BIT );

    program->bind();

    cache.angleToPixelDensityTexture.bind();

    program->setUniformValue( "angleToNormalization", 0 );
    program->setUniformValue( "M", M );
    program->setUniformValue( "width", static_cast< float >( layers.second.width ) );
    program->setUniformValue( "height", static_cast< float >( layers.second.height ) );
    program->setUniformValue( "distanceWeighted", distanceWeighted );

    if( wrap )
    {
        program->setUniformValue( "wrap1", -1.f );
        program->setUniformValue( "wrap2",  1.f );
    }

    GLuint x1 = program->attributeLocation( "x1" );
    GLuint x2 = program->attributeLocation( "x2" );
    GLuint c  = program->attributeLocation(  "c" );

    m_phaseVAO->bind();
    m_phaseVBO->bind();

    program->enableAttributeArray( x1 );
    program->enableAttributeArray( x2 );
    program->enableAttributeArray( c );

    program->setAttributeBuffer( x1, GL_FLOAT, xOffset*NPT*sizeof( float ), 1 );
    program->setAttributeBuffer( x2, GL_FLOAT, yOffset*NPT*sizeof( float ), 1 );
    glVertexAttribIPointer( c, 1, GL_INT, sizeof( int ), (void*)( cOffset*NPT*sizeof( float ) ) );

    //glMultiDrawArrays( GL_LINE_STRIP, firsts.data(), counts.data(), NP );
    glDrawArrays( GL_LINE_STRIP, 0, N_TEST*NT );

    program->disableAttributeArray( x1 );
    program->disableAttributeArray( x2 );
    program->disableAttributeArray(  c );

    m_phaseVBO->release();
    m_phaseVAO->release();
    program->release();

    layers.first.computeRange( );
    layers.second.computeRange( );

    cache.releaseFrameBuffer();

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_LINE_SMOOTH );
    glViewport( 0, 0, scaledWidth(), scaledHeight() );
}

void BALEEN::selectionToSubset()
{
    auto timeSeries = getTimeSeries( m_particleCombo.selectedItemText() );
    const int FIRST  = timeSeries->firstIdx;
    const int LAST   = timeSeries->lastIdx;
    const int STRIDE = timeSeries->idxStride;
    const int NT     = timeSeries->numSteps();

    int MASK = ( m_subsetCombo.numItems() + 3 ) - 1;

    ////////qDebug()  << "converting selection to subset " << m_particleSelection.size() << " " << std::pow( 2, MASK );
    for( auto i : m_subsetCombo.items() )
    {
        ////////qDebug()  << i.c_str();
    }

    #pragma omp parallel for
    for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
    {
        int ti = ( t - FIRST ) / STRIDE;

        for ( auto i : m_particleSelection )
        {
            m_subsetBitmasks[ i*NT + ti ] |= int( std::pow( 2, MASK ) );
        }
    }

    m_phaseVAO->bind();
    m_phaseVBO->bind();
    m_phaseVBO->write( m_vboIndexes.size() * sizeof( float )  *m_subsetBitmasks.size(), m_subsetBitmasks.data(), m_subsetBitmasks.size() * sizeof( int ) );
    m_phaseVAO->release();
    m_phaseVBO->release();

    m_statsComputer.allocateResultArray(
        getTimeSeries( m_particleCombo.selectedItemText() )->numSteps(),
        m_rangeView.rangeWidgets.size(),
        200 );

    m_addNewSubset = false;
}

void BALEEN::renderEventList()
{
    m_helpButtonEvent.setPosition( DIVIDER_WIDTH, m_combinationExpression.position().y() + m_combinationExpression.size().y() * ( 5.3 ) + 4 );

    m_emissionEventButton.setSize(
        m_selectionView.size().x() - DIVIDER_WIDTH*2 - m_selectionView.SCROLL_BAR_HEIGHT - BUTTON_HEIGHT,
        m_combinationExpression.size().y() );

    m_emissionEventButton.setPosition(
        DIVIDER_WIDTH,
        m_combinationExpression.position().y() + m_combinationExpression.size().y() * ( 4 ) );

    const float colorCodeSize = m_emissionEventButton.size().y();
    glViewport(
        m_emissionEventButton.position().x() + m_emissionEventButton.size().x(),
        m_emissionEventButton.position().y(),
        colorCodeSize,
        colorCodeSize );
    m_renderer->clear( m_emissionEventColor, cartesianProg );
    m_renderer->renderPopSquare( simpleColorProg );

    /////

    m_absorptionEventButton.setSize(
        m_selectionView.size().x() - DIVIDER_WIDTH*2 - m_selectionView.SCROLL_BAR_HEIGHT - BUTTON_HEIGHT,
        m_combinationExpression.size().y() );

    m_absorptionEventButton.setPosition(
        DIVIDER_WIDTH,
        m_combinationExpression.position().y() + m_combinationExpression.size().y() * ( 3 ) );

    glViewport(
        m_absorptionEventButton.position().x() + m_absorptionEventButton.size().x(),
        m_absorptionEventButton.position().y(),
        colorCodeSize,
        colorCodeSize );
    m_renderer->clear( m_absorptionEventColor, cartesianProg );
    m_renderer->renderPopSquare( simpleColorProg );

    m_renderer->renderPressButton( m_absorptionEventButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton(   m_emissionEventButton, cartesianProg, simpleColorProg );

    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    m_renderer->renderText(
        false,
        textProg,
        scaledWidth(),
        scaledHeight(),
//        "emissions",
        "Direction Change",
        m_emissionEventButton.position().x() + 20 - 8,
        m_emissionEventButton.position().y() + 8,
        1.0, m_textColor, false );

    glViewport( 0, 0, scaledWidth(), scaledHeight() );
    m_renderer->renderText(
        false,
        textProg,
        scaledWidth(),
        scaledHeight(),
//        "absorpsions",
        "W0W1 Sign Change",
        m_absorptionEventButton.position().x() +20 - 8,
        m_absorptionEventButton.position().y() + 8,
        1.0, m_textColor, false );

//    m_renderer->renderText( false,
//                            textProg,
//                            scaledWidth(),
//                            scaledHeight(),
//                            to_string_with_precision( yRange.a(), TEXT_PREC ),
//                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                            m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
//                            1.0,
//                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                            true );

}

void BALEEN::renderCombinationList()
{

    for( int i = 0; i < m_combinationList.size(); ++i )
    {
        m_combinationSelectorList[ i ].setSize(
            m_selectionView.size().x() - DIVIDER_WIDTH*2 - m_selectionView.SCROLL_BAR_HEIGHT,
            m_combinationExpression.size().y() );

        m_combinationSelectorList[ i ].setPosition(
            DIVIDER_WIDTH,
            m_combinationExpression.position().y() - m_combinationExpression.size().y() * ( 1 + i ) );

        renderExpressionSelector( m_combinationList[ i ], m_combinationSelectorList[ i ] );
    }
}

void BALEEN::render()
{
    static std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
    std::chrono::time_point<std::chrono::system_clock> current = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = ( current - start );
    double elapsed = elapsed_seconds.count() * 3;

    for( auto & combo : m_comboRefs )
    {
        combo->flashPhase( elapsed );
    }

    m_initiateSessionButton.flashPhase( elapsed );
    //m_definePhasePlotButton.flashPhase( elapsed );
    m_defineTimeSeriesButton.flashPhase( elapsed );
    m_definePlotButton.flashPhase( elapsed );
    m_defineFilterButton.flashPhase( elapsed );
    m_defineSubsetButton.flashPhase( elapsed );

    const int TEXT_PAD = 8;

    if ( ! m_haveInitializedOpenGL )
    {
        initGL();
        m_haveInitializedOpenGL = true;
        //////////////////////////////qDebug()  << "openGL initialized";
    }

    if( m_datasetSelected == false &&  m_datasetCombo.selectedItemText() != "select dataset" )
    {
        afterSelectedDataset();
    }

    if( m_datasetSelected == true && m_presetSelected == false && m_configurationCombo.selectedItemText() != "select preset" )
    {
        afterSelectedPreset();
    }

    if( ! m_sessionInitiated )
    {
        renderLater();
    }

    if( scaledHeight() < 700 )
    {
        return;
    }

    if( m_uiLayoutEdited )
    {
        recalculateUiLayout();
        m_phasePlotsValid = false;
    }

    static QPointF lastMousePos = m_previousMousePosition;
    lastMousePos = m_previousMousePosition;

    static std::string previousPtype = m_particleCombo.selectedItemText();
    const std::string ptype = m_particleCombo.selectedItemText();

    bool activePTypeChanged = previousPtype != ptype;
    if( activePTypeChanged && m_presetSelected )
    {
        ////////////////////////qDebug()  << "updating part type";
        updatePartType();
        previousPtype = ptype;
        //////////////////////////qDebug()  << "updated part type";
    }

    if( m_presetSelected )
    {
        //////////////////////////qDebug()  << "preset was selected";
        if( ! m_timeSeriesDefined )
        {
            if( m_timeSeriesCombo.numItems() <= 0 )
            {
                m_defineTimeSeriesButton.flash( true );
            }
            else
            {
                m_timeSeriesDefined = true;
                m_defineTimeSeriesButton.flash( false );
            }
        }
        if( m_timePlotCombo.numItems() <= 0 )
        {
            m_definePlotButton.flash( true );
        }
        else
        {
            m_definePlotButton.flash( false );
        }
//        if( m_phasePlotCombo.numItems() <= 0 )
//        {
//            m_definePhasePlotButton.flash( true );
//        }
//        else
//        {
//            m_definePhasePlotButton.flash( false );
//        }
    }

    static std::string dataS = m_datasetCombo.selectedItemText();
    std::string _ds = m_datasetCombo.selectedItemText();

    if( _ds != dataS && m_presetSelected )
    {
        //////////////////////////qDebug()  << "switching dataset";
        datasetSwitched();
    }
    dataS = _ds;

    static int SCurrentTimeStep = m_selectedTimeStep;
    bool timeStepChanged = m_selectedTimeStep != SCurrentTimeStep;
    SCurrentTimeStep = m_selectedTimeStep;

    TimePlotDefinition   * timePlotDef   = getTimePlotDefinition( ptype );

    static TimeSeriesDefinition timeSeriesS;
    TimeSeriesDefinition * timeSeries = getTimeSeries( ptype );

    static std::set< std::string > variablesInUseS = getActiveVariables( ptype );
    std::set< std::string > activeVariables = getActiveVariables( ptype );

    // conditions to test to determine if in core memory needs to be altered
    if( timeSeries != nullptr && m_sessionInitiated )
    {
        const std::string filter = m_filterCombo.selectedItemText();

        bool activeVariablesChanged = activeVariables != variablesInUseS;
        bool activeTimeSeriesChanged = ! ( timeSeriesS == (*timeSeries) );
        bool activeFilterChanged = filter != m_previousFilter;

        static std::uint8_t lastErrorRemovalOption = getErrorRemovalOption();
        std::uint8_t errorRemovalOption = getErrorRemovalOption();

        bool errorRemovalOptionChanged = false;
        if( lastErrorRemovalOption != errorRemovalOption )
        {
            errorRemovalOptionChanged = true;
        }

        bool hasErrors = m_populationDataTypeEnum == PopulationDataType::PARTICLES ? m_dataSetManager->particleDataManager().getStoredGlobalWarningState( ptype ) : false;
        bool needToUpdateErrorRemove = hasErrors && ( errorRemovalOptionChanged != 0 );

        if( activeVariablesChanged || activeTimeSeriesChanged || activeFilterChanged || activePTypeChanged || needToUpdateErrorRemove || m_filterEdited )
        {
            if( m_populationDataTypeEnum == PopulationDataType::PARTICLES )
            {
                m_dataSetManager->particleDataManager().setErrorRemovalOption( errorRemovalOption );

                if( activeFilterChanged || m_filterEdited )
                {
                    if( filter != "None" )
                    {
                        m_dataSetManager->particleDataManager().setFilter( ptype, m_currentConfigurations.particleBasedConfiguration.m_particleFilters.at( ptype ).at( filter ) );
                    }
                    else
                    {
                        // filters are default constructed as null filters
                        m_dataSetManager->particleDataManager().setFilter( ptype, ParticleFilter() );
                    }
                }

                bool recalculateFilter = m_filterEdited || activeFilterChanged;
                m_filterEdited = false;

                // in this case, we will need to reload from disk, because what's in memory has been effected by filtering
                bool invalidateCurrentlyHeldData = recalculateFilter;

                ////////qDebug()  << "loading";

                std::atomic< int > filterProgress( 0 );
                std::atomic< int > derivePrepareProgress( 0 );
                std::atomic< int > deriveCalculateProgress( 0 );
                std::atomic< int > baseProgress( 0 );
                std::atomic< int > NaNCheckProgress( 0 );
                std::atomic< int > InfCheckProgress( 0 );
                std::atomic< int > ZeroCheckProgress( 0 );
                std::atomic< int > ErrorRemovalProgress( 0 );
                std::atomic< int > rangeProgress( 0 );

                std::atomic< bool > importStatus( false );

                std::thread td( static_cast<void (ParticleDataManager<float>::*)(
                                    const std::string &,
                                    const std::set< std::string > &,
                                    const TimeSeriesDefinition & timeSeries,
                                    bool,
                                    bool,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< int > &,
                                    std::atomic< bool > & )>(
                                    &ParticleDataManager<float>::load ),
                                &( m_dataSetManager->particleDataManager() ),
                                std::ref( ptype ),
                                std::ref( activeVariables ),
                                std::ref( *timeSeries ),
                                invalidateCurrentlyHeldData,
                                recalculateFilter,
                                std::ref( filterProgress ),
                                std::ref( derivePrepareProgress ),
                                std::ref( deriveCalculateProgress ),
                                std::ref( baseProgress ),
                                std::ref( NaNCheckProgress ),
                                std::ref( InfCheckProgress ),
                                std::ref( ZeroCheckProgress ),
                                std::ref( ErrorRemovalProgress ),
                                std::ref( rangeProgress ),
                                std::ref( importStatus ) );

                std::atomic< int > currentProgress( 0 );

                while( filterProgress < 100 )
                {
                    if( currentProgress < filterProgress )
                    {
                        currentProgress.exchange( filterProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Filtering: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }
                currentProgress = 0;

                while( derivePrepareProgress < 100 )
                {
                    if( currentProgress < derivePrepareProgress )
                    {
                        currentProgress.exchange( derivePrepareProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Loading to Derive: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }
                currentProgress = 0;

                m_renderer->renderLoadingScreen(
                    textProg,
                    cartesianProg,
                    simpleColorProg,
                    scaledWidth(),
                    scaledHeight(),
                    "Calculating Derivations: ",
                    0 );
                swapBuffers();

                while( deriveCalculateProgress < 100 )
                {
                    if( currentProgress < deriveCalculateProgress )
                    {
                        currentProgress.exchange( deriveCalculateProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Calculating Derivations: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }
                currentProgress = 0;

                m_renderer->renderLoadingScreen(
                    textProg,
                    cartesianProg,
                    simpleColorProg,
                    scaledWidth(),
                    scaledHeight(),
                    "Loading: ",
                    0 );
                swapBuffers();

                while( baseProgress < 100 )
                {
                    if( currentProgress < baseProgress )
                    {
                        currentProgress.exchange( baseProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Loading: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }

                currentProgress = 0;
                if( errorRemovalOption & ParticleDataManager<float>::REMOVE_NAN )
                {
                    while( NaNCheckProgress < 100 )
                    {
                        if( currentProgress < NaNCheckProgress )
                        {
                            ////////////////qDebug()  << "error checking";
                            currentProgress.exchange( NaNCheckProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Checking for NaN: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }

                currentProgress = 0;
                if( errorRemovalOption & ParticleDataManager<float>::REMOVE_INF )
                {
                    while( InfCheckProgress < 100 )
                    {
                        if( currentProgress < InfCheckProgress )
                        {
                            currentProgress.exchange( InfCheckProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Checking for inf: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }

                currentProgress = 0;
                if( errorRemovalOption & ParticleDataManager<float>::REMOVE_ZEROS )
                {
                    while( ZeroCheckProgress < 100 )
                    {
                        if( currentProgress < ZeroCheckProgress )
                        {
                            currentProgress.exchange( ZeroCheckProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Checking for Zeros: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }
                currentProgress = 0;

                if( errorRemovalOption != 0 )
                {
                    while( ErrorRemovalProgress < 100 )
                    {
                        if( currentProgress < ErrorRemovalProgress )
                        {
                            currentProgress.exchange( ErrorRemovalProgress );
                            m_renderer->renderLoadingScreen(
                                textProg,
                                cartesianProg,
                                simpleColorProg,
                                scaledWidth(),
                                scaledHeight(),
                                "Removing Bad Data: ",
                                currentProgress );
                            swapBuffers();
                        }
                        std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                    }
                }
                currentProgress = 0;

                while( rangeProgress < 100 || importStatus == false )
                {
                    if( currentProgress < rangeProgress )
                    {
                        currentProgress.exchange( rangeProgress );
                        m_renderer->renderLoadingScreen(
                            textProg,
                            cartesianProg,
                            simpleColorProg,
                            scaledWidth(),
                            scaledHeight(),
                            "Recalculating Ranges: ",
                            currentProgress );
                        swapBuffers();
                    }
                    std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
                }

                td.join();

                m_rangeView.rangeWidgets.clear();
                m_rangeView.varSelectButtons.clear();

                std::vector< float > t_ranges( activeVariables.size() * 2 );

                int vIDX = 0;
                for( auto & v : activeVariables )
                {
                    RangeSelector rw;
                    rw.id = v;

                    Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( v )->second;
                    rw.setRange( rY );
                    rw.resetSelectedRange();

                    ////////qDebug()  << v.c_str() << " range = " << rY.a() << " ," << rY.b();

                    m_rangeView.rangeWidgets.push_back( { rw, rw } );
                    m_rangeView.varSelectButtons.push_back( PressButton() );

                    t_ranges[ vIDX*2     ] = rY.a();
                    t_ranges[ vIDX*2 + 1 ] = rY.b();

                    ++vIDX;
                }

                m_statsComputer.allocateRanges( activeVariables.size() );
                m_statsComputer.bufferRanges( t_ranges.data(), activeVariables.size() );

                renderLater();
            }

            variablesInUseS = activeVariables;
            timeSeriesS = *timeSeries;
            m_selectedTimeStep = timeSeries->firstIdx;

            std::vector< float > tSteps( timeSeries->numSteps() );
            std::vector< float > tCodomain( timeSeries->numSteps(), 0.5 );
            for( int i = timeSeries->firstIdx, k = 0; i <= timeSeries->lastIdx; i += timeSeries->idxStride )
            {
                tSteps[ k++ ] = i;
            }

            m_timeLineWidget.update( tSteps, tCodomain, Vec2< float >( 0, 1 ) );
            m_recalculateFullTimePlot = true;
            m_previousFilter = m_filterCombo.selectedItemText();
            previousPtype = ptype;
            timeStepChanged = true;

            for( auto & pp : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( ptype ) )
            {
                PhasePlotDefinition & p = pp.second;
                if( m_trajectoryData.count( p.attrs[ 0 ] ) <= 0 )
                {
                    m_trajectoryData.insert(
                        std::pair< std::string, std::unique_ptr< std::vector< float > > >(
                            p.attrs[ 0 ],
                            std::unique_ptr< std::vector< float > >( nullptr ) ) );
                }
                if( m_trajectoryData.count( p.attrs[ 1 ] ) <= 0 )
                {
                    m_trajectoryData.insert(
                        std::pair< std::string, std::unique_ptr< std::vector< float > > >(
                            p.attrs[ 1 ],
                            std::unique_ptr< std::vector< float > >( nullptr ) ) );
                }
            }

            if( ! m_trajectoryData.count( getTimePlotDefinition( ptype )->attr ) )
            {
                m_trajectoryData.insert(
                    std::pair< std::string, std::unique_ptr< std::vector< float > > >(
                        getTimePlotDefinition( ptype )->attr,
                        std::unique_ptr< std::vector< float > >( nullptr ) ) );
            }

            const size_t FIRST  = timeSeries->firstIdx;
            const size_t LAST   = timeSeries->lastIdx;
            const size_t STRIDE = timeSeries->idxStride;
            const size_t NT     = timeSeries->numSteps();
            const size_t NP     = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );
            const size_t NPT = NP*NT;

            m_recomputeSubsets = true;
            m_updateSelection = true;

            m_phaseVAO->bind();
            m_phaseVBO->bind();

            const int PhaseDims = m_trajectoryData.size() + 1;

            if ( static_cast< size_t >( m_phaseVBO->size() )
                    < size_t( PhaseDims )* size_t( NPT*sizeof( float ) ) + NPT * size_t( sizeof( int ) ) )
            {
                m_phaseVBO->allocate( size_t( PhaseDims )*NPT*size_t( sizeof( float ) ) + NPT * size_t( sizeof( int ) ) );
            }

            ////////qDebug()  << "allocated  " << PhaseDims*NPT*sizeof( float ) + NPT * sizeof( int );

            int vboIdx = 0;
            for( auto & v : m_trajectoryData )
            {
                v.second.reset( new std::vector< float >( NPT ) );
                std::vector< float > & buffer = *( v.second );

                const std::vector< std::unique_ptr< std::vector< float > > > & xAttr
                    = m_dataSetManager->particleDataManager().values( ptype, v.first );

                #pragma omp parallel for
                for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
                {
                    int ti = ( t - FIRST ) / STRIDE;

                    const std::vector< float > & xPos = *( xAttr[ t ] );

                    for ( unsigned i = 0; i < NP; ++i )
                    {
                        buffer[ i*NT + ti ] = xPos[ i ];
                    }
                }

                m_phaseVBO->write( vboIdx*sizeof( float )*NPT, buffer.data(), buffer.size() * sizeof( float ) );
                m_vboIndexes.insert( { v.first, vboIdx++ } );
            }

            m_timePoints.resize( NPT );
            #pragma omp parallel for
            for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
            {
                int ti = ( t - FIRST ) / STRIDE;
                for ( unsigned i = 0; i < NP; ++i )
                {
                    m_timePoints[ i*NT + ti ] = tSteps[ ti ];
                }
            }

            m_phaseVBO->write( vboIdx*sizeof( float )*NPT, m_timePoints.data(), m_timePoints.size() * sizeof( float ) );
            m_vboIndexes.insert( { "time", vboIdx } );

            m_phaseVAO->release();
            m_phaseVBO->release();

            m_statsComputer.mapBuffer( m_phaseVBO->bufferId(), NT, NP, m_vboIndexes.size() );

            m_statsComputer.compileProgram( "$" );
            regenerateCombinationShader( "$" );

            m_statsComputer.allocateResultArray( NT, activeVariables.size(), 200 );

            m_phaseTextureLayers.resize(
                m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( ptype ).size() );

            m_phaseBufferFirsts.resize( NP );
            m_phaseBufferCounts.resize( NP );
            m_particleSelectionCounts.resize( NP );
            m_particleSelectionSampleCounts.resize( NP );
            m_particleSelectionSubSampleCounts.resize( NP );

            #pragma omp parallel
            for ( int i = 0; i < NP; ++i )
            {
                m_phaseBufferFirsts[ i ] = NT * i;
                m_phaseBufferCounts[ i ] = NT;
            }

            ////////qDebug()  << "selected plot set to empty";
            m_selectedPlot = "";


            /////////////////////////////////////////////////////////

            {
                const int FIRST  = timeSeries->firstIdx;
                const int LAST   = timeSeries->lastIdx;
                const int STRIDE = timeSeries->idxStride;
                const int NT     = timeSeries->numSteps();
                const int NP     = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

                m_subsetBitmasks.resize( NP * NT );
                const std::vector<  int > & temp = m_dataSetManager->particleDataManager().subsetMasks();

                #pragma omp parallel for
                for ( unsigned int t = FIRST; t <= LAST; t += STRIDE )
                {
                    int ti = ( t - FIRST ) / STRIDE;
                    for ( unsigned i = 0; i < NP; ++i )
                    {
                        m_subsetBitmasks[ i*NT + ti ] = temp[ NP*ti + i ];
                    }
                }

                m_phaseVAO->bind();
                m_phaseVBO->bind();
                m_phaseVBO->write( m_vboIndexes.size() * sizeof( float )*m_subsetBitmasks.size(), m_subsetBitmasks.data(), m_subsetBitmasks.size() * sizeof( int ) );
                m_phaseVAO->release();
                m_phaseVBO->release();

                std::vector< std::string > defaultSubsets = m_dataSetManager->particleDataManager().getDefaultSubsetNames();
                for( int i = 0; i < defaultSubsets.size(); ++i )
                {
                    m_subsetCombo.addItem( defaultSubsets[ i ] );
                }

                m_statsComputer.allocateResultArray(
                    getTimeSeries( m_particleCombo.selectedItemText() )->numSteps(),
                    m_rangeView.rangeWidgets.size(),
                    200 );
            }

            aggregator.setDataSZandMap( NP, NT, m_vboIndexes );

            /////////////////////////////////////////////////////////////////////////////////

        }

        if(  activeVariablesChanged || activeTimeSeriesChanged || activeFilterChanged || activePTypeChanged )
        {
            if( m_dataInitializedFirst == false )
            {
                m_uiLayoutEdited = true;
                m_dataInitializedFirst = true;
                renderLater();
                return;
            }
        }
    }

    //////////////////////////////////////////////////////////////////

    ////checkError( "before anything" );

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    m_renderer->clearViewPort( m_plotGridView.viewPort(), cartesianProg );
    m_renderer->renderScrollBar( m_plotGridView.scrollBar(), cartesianProg, simpleColorProg );

    m_renderer->clearViewPortInverse( scaledHeight(), scaledWidth(), m_plotGridView.viewPort(), m_dividerColor, cartesianProg );

    m_renderer->clearViewPort( m_selectionView.viewPort(), cartesianProg );
    m_renderer->renderScrollBar( m_selectionView.scrollBar(), cartesianProg, simpleColorProg );

    m_renderer->clearViewPort( m_timeLineWidget.viewPort(), cartesianProg );
    //m_renderer->clearViewPort( m_statsView.viewPort(), cartesianProg );

    m_renderer->clearViewPort( m_recurrenceView.viewPort(), cartesianProg );

    m_renderer->clearViewPort( m_rangeView.viewPort(), cartesianProg );
    m_renderer->renderScrollBar( m_rangeView.scrollBar(), cartesianProg, simpleColorProg );

    ViewPort vpt = m_trajectoryView.viewPort();
    vpt.height( vpt.height() - 34 );
    vpt.width( vpt.width() - m_trajectoryView.SCROLL_BAR_HEIGHT + 1 );
    m_renderer->clearViewPort( vpt, cartesianProg );

    m_renderer->renderScrollBar( m_trajectoryView.scrollBar(), cartesianProg, simpleColorProg );

    // Menu Bar
    glViewport( 0, scaledHeight() - MENU_HEIGHT, scaledWidth(), MENU_HEIGHT );
    m_renderer->clear( m_dividerColor, cartesianProg );

    // Data Info Panel
    glViewport(
        m_dataSetInfoWidget.position().x(),
        m_dataSetInfoWidget.position().y(),
        m_dataSetInfoWidget.size().x(),
        m_dataSetInfoWidget.size().y() );
    m_renderer->clear( QVector4D( .99, .99, .99, 1 ), cartesianProg );
    m_renderer->renderViewPortBoxShadow( m_dataSetInfoWidget.viewPort(), simpleColorProg );

    ////checkError( "checkpoint 1" );

    // Stats info panel
//    glViewport(
//        m_statsView.position().x(),
//        m_statsView.position().y() + m_statsView.size().y() - m_plotGridView.PANEL_HEIGHT,
//        m_statsView.size().x(),
//        m_plotGridView.PANEL_HEIGHT );
//    m_renderer->clear( QVector4D( .99, .99, .99, 1 ), cartesianProg );
//    m_renderer->renderPopSquare( simpleColorProg );

    if( timeSeries != nullptr && m_sessionInitiated )
    {
//        m_renderer->renderTimeLinePlot( m_timeLineWidget, cartesianProg, flatProgSerial, QVector4D( .1, .1, .1, 1 ) );
//        renderTimeLineAnnotation();
//        renderTimeSeriesInfo( ptype );

        if( timePlotDef != nullptr )
        {
//            static int S_plotKey = -1;

//            const int TEXT_PREC = 4;
//            const int MIN_MAX_TEX_OFF_A = 30;
//            const int MIN_MAX_TEX_OFF_B = 20;
//            const int PLOT_MM_TEX_X = 48;

//            int plotKey = m_timePlotModeCombo.selectedItemText() == "mean" ? LineData::MEAN : LineData::MEAN_SQUARED;

//            // update textures, in the future will dynamically generate symbols and equations from user specified functions
//            if( S_plotKey != plotKey )
//            {
//                std::string allTexPath = "";
//                std::string regionTexPath = "";

//                if( plotKey == LineData::MEAN )
//                {
//                    allTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanAll.png";
//                    regionTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanRegion.png";
//                }
//                else if ( plotKey == LineData::MEAN_SQUARED )
//                {
//                    allTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanSquaredAll.png";
//                    regionTexPath = RELATIVE_PATH_PREFIX + "/textures/latex/meanSquaredRegion.png";
//                }

//                m_timeAggregateAllEquation.setTexFromPNG( allTexPath, allTexPath );
//                m_timeAggregateRegionEquation.setTexFromPNG( regionTexPath, regionTexPath );
//                m_timeAggregateAllEquation.resizeByHeight( EQUATION_HEIGHT );
//                m_timeAggregateRegionEquation.resizeByHeight( EQUATION_HEIGHT );

//                S_plotKey = plotKey;
//            }

//            m_timeAggregateEquationDefinitionsAll.setPosition(
//                m_timeLineWidget.position().x() + m_timeLineWidget.size().x() - m_timeLineWidget.MARGIN_RIGHT - m_timeAggregateEquationDefinitionsAll.size().x(),
//                m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - EQUATION_HEIGHT - 15 );

//            m_timeAggregateEquationDefinitions.setPosition(
//                m_timeLineWidget.position().x() + m_timeLineWidget.size().x() - m_timeLineWidget.MARGIN_RIGHT - m_timeAggregateEquationDefinitions.size().x(),
//                m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - EQUATION_HEIGHT - 15 );

//                if( /*m_timePlotAllSelector.isPressed() || m_timePlotBothSelector.isPressed()*/ true )
//                {
//                    m_timeAggregateAllEquation.setPosition( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT + 40, m_timeAggregateEquationDefinitions.position().y() );
//                    m_timeAggregateRegionEquation.setPosition( m_timeAggregateAllEquation.position().x() + m_timeAggregateAllEquation.size().x() + 80, m_timeAggregateEquationDefinitions.position().y() );
//                    m_renderer->renderTexturedPressButton( m_timeAggregateAllEquation, ucProg, cartesianProg, simpleColorProg );

//                    glViewport( m_timeAggregateAllEquation.position().x() - 40, m_timeAggregateAllEquation.position().y() + 10, EQUATION_HEIGHT-20, EQUATION_HEIGHT-20 );
//                    m_renderer->clear( allColorTP, cartesianProg );
//                }
//                else if( m_timePlotSelectionSelector.isPressed() )
//                {
//                    m_timeAggregateRegionEquation.setPosition( m_timeLineWidget.position().x() + m_timeLineWidget.MARGIN_LEFT + 40, m_timeAggregateEquationDefinitions.position().y() );
//                }
//                if( /*m_timePlotHistogramSelector.isPressed() || m_timePlotBothSelector.isPressed()*/ true )
//                {
//                    m_renderer->renderTexturedPressButton( m_timeAggregateRegionEquation, ucProg, cartesianProg, simpleColorProg );
//                    glViewport( m_timeAggregateRegionEquation.position().x() - 40, m_timeAggregateRegionEquation.position().y() + 10, EQUATION_HEIGHT-20, EQUATION_HEIGHT-20 );
//                    m_renderer->clear( regionsColorTP, cartesianProg );
//                }
//                if( m_timeAggregateRegionEquation.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) )
//                        && ( m_timePlotSelectionSelector.isPressed() || m_timePlotBothSelector.isPressed() ) )
//                {
//                    m_renderer->renderTexturedPressButton( m_timeAggregateEquationDefinitions, ucProg, cartesianProg, simpleColorProg );
//                }
//                if( m_timeAggregateAllEquation.pointInViewPort( Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) )
//                        && ( m_timePlotAllSelector.isPressed() || m_timePlotBothSelector.isPressed() ) )
//                {
//                    m_renderer->renderTexturedPressButton( m_timeAggregateEquationDefinitionsAll, ucProg, cartesianProg, simpleColorProg );
//                }


            //////////////////////////////////////////////////////////////////


//                if( ! ( m_timePlotCache.timeSeries == *timeSeries ) )
//                {
//                    m_recalculateFullTimePlot = true;
//                    m_timePlotCache.setParameters( *timeSeries );
//                }
//                if( m_recalculateFullTimePlot )
//                {
//                    m_timePlotCache.resetFullAggregateRanges();
//                    computeTemporalPlotsAll( m_timePlotCache.fullAggregate, ptype, *timePlotDef );
//                    m_recalculateFullTimePlot = false;
//                }

//                float pltOffY = m_timeLineWidget.position().y() + m_timeLineWidget.MARGIN_BOTTOM;
//                float plHT = m_timeLineWidget.size().y() - m_timeLineWidget.MARGIN_BOTTOM - m_timeLineWidget.MARGIN_TOP;

//                if( m_timePlotAllSelector.isPressed() )
//                {
//                    const std::vector< double > & x = m_timePlotCache.timePoints;
//                    const std::vector< double > & y = m_timePlotCache.fullAggregate.values[ plotKey ];
//                    const Vec2< double > & yRange = m_timePlotCache.fullAggregate.ranges[ plotKey ];
//                    renderTemporalPlot( x, y, yRange, *timeSeries, m_selectedTimeStep, allColorTP );

//                    m_renderer->renderText( false,
//                                            textProg,
//                                            scaledWidth(),
//                                            scaledHeight(),
//                                            to_string_with_precision( yRange.a(), TEXT_PREC ),
//                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                            m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
//                                            1.0,
//                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                            true );

//                    m_renderer->renderText( false,
//                                            textProg,
//                                            scaledWidth(),
//                                            scaledHeight(),
//                                            to_string_with_precision( yRange.b(), TEXT_PREC ),
//                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                            m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
//                                            1.0,
//                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                            true );
//                }

            ////////////////////////////////////////////////////////

//                else if( m_timePlotSelectionSelector.isPressed() )
//                {
//                    if( cellSelection >= 0 && ( ! userIsPanning ) )
//                    {
//                        bool tscValid;
//                        LineData & lineData = m_timePlotCache.get( cellSelection, tscValid );
//                        if( ! tscValid )
//                        {
//                            computeTemporalPlots( lineData, ptype, *timePlotDef, spacePartitioning );
//                        }

//                        const std::vector< double > & x = m_timePlotCache.timePoints;
//                        const std::vector< double > & y = lineData.values[ plotKey ];
//                        const Vec2< double > & yRange = lineData.ranges[ plotKey ];
//                        renderTemporalPlot( x, y, yRange, *timeSeries, m_selectedTimeStep, regionsColorTP );

//                        m_renderer->renderText( false,
//                                                textProg,
//                                                scaledWidth(),
//                                                scaledHeight(),
//                                                to_string_with_precision( yRange.a(), TEXT_PREC ),
//                                                m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                                m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
//                                                1.0,
//                                                Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                                true );

//                        m_renderer->renderText( false,
//                                                textProg,
//                                                scaledWidth(),
//                                                scaledHeight(),
//                                                to_string_with_precision( yRange.b(), TEXT_PREC ),
//                                                m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                                m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
//                                                1.0,
//                                                Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                                true );

//                    }
//                }
//                else if( m_timePlotBothSelector.isPressed() )
//                {
//                    const std::vector< double > & xAll = m_timePlotCache.timePoints;
//                    const std::vector< double > & yAll = m_timePlotCache.fullAggregate.values[ plotKey ];
//                    const Vec2< double > & yRangeAll = m_timePlotCache.fullAggregate.ranges[ plotKey ];

//                    Vec2< double > fullRange = yRangeAll;

//                    if( cellSelection >= 0 && ( ! userIsPanning ) )
//                    {
//                        bool tscValid;
//                        LineData & lineData = m_timePlotCache.get( cellSelection, tscValid );
//                        if( ! tscValid )
//                        {
//                            computeTemporalPlots( lineData, ptype, *timePlotDef, spacePartitioning );
//                        }

//                        const std::vector< double > & y = lineData.values[ plotKey ];
//                        const Vec2< double > & yRange = lineData.ranges[ plotKey ];

//                        fullRange.a( std::min( fullRange.a(), yRange.a() ) );
//                        fullRange.b( std::max( fullRange.b(), yRange.b() ) );

//                        renderTemporalPlot( xAll, yAll, fullRange, *timeSeries, m_selectedTimeStep, allColorTP );
//                        renderTemporalPlot( xAll, y, fullRange, *timeSeries, m_selectedTimeStep, regionsColorTP, true );
//                    }
//                    else
//                    {
//                        renderTemporalPlot( xAll, yAll, fullRange, *timeSeries, m_selectedTimeStep, allColorTP );
//                    }

//                    m_renderer->renderText( false,
//                                            textProg,
//                                            scaledWidth(),
//                                            scaledHeight(),
//                                            to_string_with_precision( fullRange.a(), TEXT_PREC ),
//                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                            m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
//                                            1.0,
//                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                            true );

//                    m_renderer->renderText( false,
//                                            textProg,
//                                            scaledWidth(),
//                                            scaledHeight(),
//                                            to_string_with_precision( fullRange.b(), TEXT_PREC ),
//                                            m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
//                                            m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
//                                            1.0,
//                                            Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
//                                            true );
//                }
        }

        //////////////////////////////////////////////////////////////////

        // get rows and cols needed for each plots, and min rows needed overall

        std::map< std::string, Vec2< int > > rowsAndCols;
        int minRows = 1;
        for( auto & p : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( ptype ) )
        {
            PhasePlotDefinition & plot = p.second;
            if( plot.phasePlotSizeMode == PhasePlotSizeMode::PRESERVE_ASPECT )
            {
                Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plot.attrs[ 0 ] )->second;
                Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( plot.attrs[ 1 ]  )->second;

                float xwidth = rX.b() - rX.a();
                float ywidth = rY.b() - rY.a();

                plot.rowsReq = std::ceil( ywidth / xwidth );
                plot.colsReq = std::ceil( xwidth / ywidth );
            }
            else if( plot.phasePlotSizeMode != PhasePlotSizeMode::CUSTOM )
            {
                plot.rowsReq = 1;
                plot.colsReq = 1;
            }

            rowsAndCols.insert( { plot.name, Vec2< int >( plot.rowsReq, plot.colsReq ) } );
            minRows = std::max( minRows, plot.rowsReq );
        }

        const int ROWS = std::max( m_plotGridView.rows, minRows );

        const float MARGIN_LEFT = 50;
        const float MARGIN_BOTTOM = 50;
        const float MARGIN_TOP = 20;
        const float MARGIN_RIGHT = 20;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // now sort by number or rows needed, then number of cols needed

        std::vector< PhasePlotDefinition > orderedPlots;
        std::vector< Vec2< int > > plotPlacement;

        for( auto & defP : m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( ptype ) )
        {
            orderedPlots.push_back( defP.second );
            plotPlacement.push_back( Vec2< int >() );
        }

        std::sort(
            orderedPlots.begin(),
            orderedPlots.end(),
            [=]( const PhasePlotDefinition & a, const PhasePlotDefinition & b ) -> bool
        {
            if( rowsAndCols.at(      a.name ).a() != rowsAndCols.at( b.name ).a() )
            {
                return rowsAndCols.at(      a.name ).a() > rowsAndCols.at( b.name ).a();
            }
            return rowsAndCols.at( a.name ).b() > rowsAndCols.at( b.name ).b();
        } );

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        int MAX_COLS = 500;
        std::uint8_t freeCell[ ROWS ][ MAX_COLS ];

        for( int i = 0; i < ROWS; ++i )
        {
            for( int j = 0; j < MAX_COLS; ++j )
            {
                freeCell[ i ][ j ] = 1;
            }
        }

        // one pass over layout to find number of cols need
        int pIdx = 0;
        for( auto & def : orderedPlots )
        {
            Vec2< int > dims = rowsAndCols.at( def.name );

            bool placed = false;
            for( int j = 0; j < MAX_COLS && ! placed; ++j )
            {
                for( int i = 0; i < ROWS && ! placed; ++i )
                {
                    bool valid = true;

                    if( ( i + 1 ) - dims.a() < 0 )
                    {
                        valid = false;
                    }
                    else
                    {
                        for( int k = i; k >= 0 && k > i - dims.a() && valid; --k )
                        {
                            for( int l = j; l < dims.b() + j && valid; ++l )
                            {
                                if( ! freeCell[ k ][ l ] )
                                {
                                    valid = false;
                                }
                            }
                        }
                    }
                    if( valid )
                    {
                        plotPlacement[ pIdx ] = Vec2< int >( i, j );
                        for( int k = i; k > i - dims.a(); --k )
                        {
                            for( int l = j; l < dims.b() + j; ++l )
                            {
                                freeCell[ k ][ l ] = 0;
                            }
                        }
                        placed = true;
                    }
                }
            }
            ++pIdx;
        }

        int colsRequired = 0;
        for( int j = 0; j < MAX_COLS; ++j )
        {
            for( int i = 0; i < ROWS; ++i )
            {
                if( ! freeCell[ i ][ j ] )
                {
                    colsRequired = std::max( colsRequired, j );
                }
            }
        }
        ++colsRequired;

        ////////////qDebug()  << "cols require " << colsRequired;

        // now we know the width, and can set the scroll bar and scroll offset sizes

        const float pltUnitSize = ( m_plotGridView.size().y() - m_plotGridView.SCROLL_BAR_HEIGHT ) / ROWS - ( MARGIN_TOP + MARGIN_BOTTOM );
        const float figUnitSizeX = pltUnitSize + MARGIN_LEFT + MARGIN_RIGHT;
        const float figUnitSizeY = pltUnitSize + MARGIN_TOP + MARGIN_BOTTOM;

        float areaLength = figUnitSizeX * colsRequired;

        m_plotGridView.setAreaLength( areaLength   );
        m_plotGridView.setJumpLength( figUnitSizeX );

        float offSetY = m_plotGridView.position().y() + m_plotGridView.SCROLL_BAR_HEIGHT + MARGIN_BOTTOM;
        float offSetX = m_plotGridView.position().x() + MARGIN_LEFT;

        float scrollOffset = - m_plotGridView.scrollBar().getAreaOffset();

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        if( m_addNewSubset )
        {
            selectionToSubset();
            m_addNewSubset = false;
        }

        static std::string exprS = "";
        std::string expr;
        if( m_selectedCombination >= 0 )
        {
            expr = m_combinationList[ m_selectedCombination ];
            ////////qDebug()  << "expession " << expr.c_str();
        }
        else
        {
            expr = "";
        }

        static bool updateDirectionChangeIDs = false;

        if( expr != exprS )
        {
            ////////qDebug()  << "updating combo shaders " << expr.c_str();
            regenerateCombinationShader( expr );
            m_statsComputer.compileProgram( expr );

//            auto start = std::chrono::system_clock::now();

            m_statsComputer.setCombination( timeSeries->numSteps(), m_dataSetManager->particleDataManager().numLoadedParticles( ptype ), m_subsetBitmasks );
            m_updateBins = true;
            m_recalculateFullTimePlot = true;

            //m_statsComputer.setCombination( timeSeries->numSteps(), m_dataSetManager->particleDataManager().numLoadedParticles( ptype ) );

//            auto end = std::chrono::system_clock::now();

//            std::chrono::duration<double> elapsed_seconds = end-start;
//            std::cout << "NP " << NP << " NT " << NT << " "
//                      << "elapsed time: "  << elapsed_seconds.count() << "s\n";

//            if( m_phasePlotBlendingCombo.selectedItemText() == "Run Test" )
//            {

//                for( auto & ex : { "(A-((B&C&D)^E))|F" }  )
//                {
//                    regenerateCombinationShader( ex );
//                    m_statsComputer.compileProgram( ex );

//                    const double N_TRIALS = 10;
//                    float result = 0;
//                    for( int trial = 0; trial < N_TRIALS; ++trial )
//                    {
//                        auto start = std::chrono::system_clock::now();
//                        m_statsComputer.setCombination( timeSeries->numSteps(), m_dataSetManager->particleDataManager().numLoadedParticles( ptype ) );
//                        auto end = std::chrono::system_clock::now();
//                        std::chrono::duration<double> elapsed_seconds = end-start;
//                        result += elapsed_seconds.count();
//                    }
//                    std::cout << ex << "\n";
//                    std::cout << "(" << NT*NP << "," <<  result / N_TRIALS << ")\n";
//                }
//                exit( 0 );
//            }

            m_phasePlotsValid = false;
            m_recalculateFullTimePlot = true;

            updateDirectionChangeIDs = true;
        }
        exprS = expr;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////

        static std::string prevRenderMode = m_phasePlotBlendingCombo.selectedItemText();
        std::string currRenderMode = m_phasePlotBlendingCombo.selectedItemText();

        if( currRenderMode != prevRenderMode )
        {
            m_phasePlotsValid = false;
        }

        prevRenderMode = currRenderMode;
        bool recomputed = false;

        if( m_updateSelection )
        {
            ////////////qDebug()  << "updating selection";

            if( ! m_resizeWidgetA.isPressed() && ! m_resizeWidgetB.isPressed() && ! m_resizeWidgetD.isPressed() )
            {
                if( m_selectFromEventsButton.isPressed() )
                {
                    if( m_emissionEventButton.isPressed() )
                    {
                        getSelectionFromPolaritySwap( "vpara" );
                    }
                    else if( m_absorptionEventButton.isPressed() )
                    {
                        getSelectionFromPolaritySwap( "w0w1" );
                    }
                    //getSelectionFromEvent();
                }
                else
                {
                    getSelection();
                }

                m_updateSelection = false;
                m_selectedParticle = -1;
                sampleSelection( 45 );
            }

            m_recalculateFullTimePlot = true;
        }

        ////////////qDebug()  << "rendering/computing phase plots";

//        QVector4D color1( 0.5, 0.5, 0.5, 1 );
        QVector4D color2( 0.9, 0.9, 0.9, 1.0);

        QVector4D color1( 0.2, 0.2, 0.2, 1 );//166.0 / 700, 206.0 / 700, 227.0 / 700, 1 );
//        QVector4D color2( 166.0 / 255, 206.0 / 255, 227.0 / 255, 1 );

        QVector4D color3( 0.2, 0.2, 0.1, 1.0 );
        QVector4D color4(1.0, 1.0, 0.89803921568627454, 1.0);//( 0.050382999999999997, 0.029803, 0.52797499999999997, 1.0 );

        static std::vector< std::vector< size_t > > directionChangeIds;

        if( ptype == "ions" )
        {
            if( m_emissionEventButton.isPressed() )
            {
                if( updateDirectionChangeIDs )
                {
                    const size_t NT = timeSeries->numSteps();
                    const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( "ions" );
                    directionChangeIds.resize( NT );
                    directionChangeIds[ 0 ].clear();

                    auto & vpara = *(m_trajectoryData.at( "vpara" ) );

                    #pragma omp parallel for
                    for( size_t t = 1; t < NT; ++t )
                    {
                        directionChangeIds[ t ].clear();

                        for( size_t p = 0; p < NP; ++p )
                        {
                            if( vpara[ p*NT + t ] * vpara[ p*NT + ( t - 1 ) ] < 0 )
                            {
                                if( ( m_subsetBitmasks[ p * NT + t ] & 1 ) && ( m_subsetBitmasks[ p * NT + t ] & 2 ) )
                                {
                                    directionChangeIds[ t ].push_back( p );
                                }
                            }
                        }
                    }

                    updateDirectionChangeIDs = false;
                }
            }
        }

        pIdx = 0;
        for( auto & def : orderedPlots )
        {
            Vec2< int > dims = rowsAndCols.at( def.name );

            int i = plotPlacement[ pIdx ].a();
            int j = plotPlacement[ pIdx ].b();

            const float figDiffX = figUnitSizeX - pltUnitSize;
            const float figDiffY = figUnitSizeY - pltUnitSize;

            float xSize = figUnitSizeX*dims.b() - figDiffX;
            float ySize = figUnitSizeY*dims.a() - figDiffY;

            ////////////

            QMatrix4x4 M;
            M.setToIdentity();

            Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( def.attrs[ 0 ] )->second;
            Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( def.attrs[ 1 ] )->second;

            const float xW = ( rX.b() - rX.a() );
            const float yW = ( rY.b() - rY.a() );
            M.scale( 2.0 / xW, 2.0 / yW );
            M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );

            //////////

            if( m_phasePlotsValid == false )
            {
                if( ! m_resizeWidgetA.isPressed() && ! m_resizeWidgetB.isPressed() && ! m_resizeWidgetD.isPressed() )
                {
                    const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

//                    if( m_phasePlotBlendingCombo.selectedItemText() == "Run Test" && false )
//                    {
//                        std::vector< std::vector< float > > timeing( 8, std::vector< float >( 1, 0 ) );

//                        const int NT = timeSeries->numSteps();
//                        int N_TRIAL = 5;
//                        const int N = NP;
//                        const int k = 0;

////                        const size_t N_CHUNK = NP / 21;

//                        for( int trial = 0; trial < N_TRIAL; ++trial )
//                        {
////                            for( size_t N = N_CHUNK, k = 0; N <= NP && k < 20; N += N_CHUNK, ++k )
////                            {
//                                for( int mode = 0; mode < 8; ++mode )
//                                {
//                                    if( mode == 0 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        computeContextLayers2(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            false,
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
//                                    else if( mode == 1 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        computeContextLayers2(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            true, // path density
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
//                                    else if( mode == 2 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        ////qDebug() << timePlotDef->attr.c_str();

//                                        computeContextLayersMax(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            timePlotDef->attr,
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
//                                    else if( mode == 3 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        computeContextLayersMin(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            timePlotDef->attr,
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
//                                    else if( mode == 4 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        computeContextLayersMean(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            timePlotDef->attr,
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
//                                    else if( mode == 5 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        computeContextLayerWeightedDensity(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            timePlotDef->attr,
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
//                                    else if( mode == 6 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        computeContextLayersRange(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            timePlotDef->attr,
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
//                                    else if( mode == 7 )
//                                    {
//                                        auto start = std::chrono::system_clock::now();

//                                        computeContextLayersVariance(
//                                            m_phasePlotCache,
//                                            m_phaseTextureLayers[ pIdx ],
//                                            def.attrs[ 0 ],
//                                            def.attrs[ 1 ],
//                                            timePlotDef->attr,
//                                            Vec2< int >( xSize, ySize ),
//                                            ptype,
//                                            m_phaseBufferCounts,
//                                            m_phaseBufferFirsts,
//                                            M,
//                                            N
//                                        );

//                                        auto end = std::chrono::system_clock::now();
//                                        std::chrono::duration<double> elapsed_seconds = end-start;

//                                        timeing[ mode ][ k ] += elapsed_seconds.count();

//                                        std::cout << "compute context ... NP " << N << " NT " << NT << " "
//                                                  << "elapsed time: "  << elapsed_seconds.count() << "s\n";
//                                    }
////                                }
//                            }
//                        }
//                        for( int i = 0; i < 8; ++i )
//                        {
//                            std::cout << "mode: " << i << "\n";

////                            for( size_t j = 0; j < 20; ++j )
////                            {
//                                std::cout << "( " << /*size_t( N_CHUNK )*(j+1)*/NP*size_t(NT) << "," << ( timeing[  i ][ 0 ] / 5.0 ) << ")";
////                                if( j < 19 )
////                                {
////                                    std::cout << ",";
////                                }
//                                std::cout << "\n";
////                            }

//                            std::cout << "\n";
//                        }

//                        exit( 0 );
//                    }
                    /*else */

                    const float FACTOR = std::max( m_resolutionSlider.sliderPosition(), 1 / 20.f );
                    Vec2< int > resolution = { xSize * FACTOR, ySize * FACTOR };

                    if( m_phasePlotBlendingCombo.selectedItemText() == "Weighted Density" )
                    {
                        aggregator.computeWeightedDensity(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            "w0w1",
                            resolution.a(),
                            resolution.b(),
                            M,
                            false );

//                        computeContextLayerWeightedDensity(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            "w0w1",
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle"
//                        );
                    }
                    else if( m_phasePlotBlendingCombo.selectedItemText() == "Number Density" )
                    {
                        aggregator.computeDensity(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            resolution.a(),
                            resolution.b(),
                            M,
                            true,
                            false ); //def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta" );

//                        computeContextLayers2(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            true,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta"
//                        );
                    }
                    else if ( m_phasePlotBlendingCombo.selectedItemText() == "Path Density" )
                    {

                        aggregator.computeDensity(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            resolution.a(),
                            resolution.b(),
                            M,
                            false,
                            false );

                        //computeContextLayers2(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            false,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta"
//                        );
                    }
                    else if ( m_phasePlotBlendingCombo.selectedItemText() == "Variance" )
                    {
                        aggregator.computeVariance(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            "w0w1",
                            resolution.a(),
                            resolution.b(),
                            M,
                            false );

//                        computeContextLayersVariance(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            "w0w1",
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta"
//                        );
                    }
                    else if ( m_phasePlotBlendingCombo.selectedItemText() == "Min" )
                    {

                        aggregator.computeMinOrMax(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            "w0w1",
                            resolution.a(),
                            resolution.b(),
                            M,
                            true,
                            false );

//                        computeContextLayersMin(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            "w0w1",
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta"
//                        );
                    }
                    else if ( m_phasePlotBlendingCombo.selectedItemText() == "Max" )
                    {
                        aggregator.computeMinOrMax(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            "w0w1",
                            resolution.a(),
                            resolution.b(),
                            M,
                            false,
                            false );

//                        computeContextLayersMax(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            "w0w1",
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta"
//                        );
                    }
                    else if ( m_phasePlotBlendingCombo.selectedItemText() == "Mean" )
                    {
                        aggregator.computeMean(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            "w0w1",
                            resolution.a(),
                            resolution.b(),
                            M,
                            false );

//                        computeContextLayersMean(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            "w0w1",
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta"
//                        );
                    }
                    else if ( m_phasePlotBlendingCombo.selectedItemText() == "Range" )
                    {
                        aggregator.computeRange(
                            m_phaseTextureLayers[ pIdx ],
                            def.attrs[ 0 ],
                            def.attrs[ 1 ],
                            "w0w1",
                            resolution.a(),
                            resolution.b(),
                            M,
                            false );

//                        computeContextLayersRange(
//                            m_phasePlotCache,
//                            m_phaseTextureLayers[ pIdx ],
//                            def.attrs[ 0 ],
//                            def.attrs[ 1 ],
//                            "w0w1",
//                            resolution,
//                            ptype,
//                            m_phaseBufferCounts,
//                            m_phaseBufferFirsts,
//                            M,
//                            NP,
//                            def.attrs[ 0 ] == "poloidal_angle" || def.attrs[ 0 ] == "zeta" || def.attrs[ 1 ] == "poloidal_angle" || def.attrs[ 1 ] == "zeta"
//                        );
                    }
                    else if ( m_phasePlotBlendingCombo.selectedItemText() == "Recurrence Density" )
                    {
                        std::pair< int, int > heatmapSpace = { m_vboIndexes.at( def.attrs[ 0 ] ), m_vboIndexes.at( def.attrs[ 1 ] ) };
                        std::pair< int, int > heatmapDims  = { resolution.b(), resolution.a() };
                        std::vector< int   > phaseIndexes = { m_vboIndexes.at( "poloidal_angle" ), m_vboIndexes.at( "zeta" ),  m_vboIndexes.at( "psin" ), m_vboIndexes.at( "vpara" ), m_vboIndexes.at( "vperp" ) };
                        std::vector< float > phaseScaling = { 1.f / 6.5, 1.f / 6.5,  1, 1 / 3.3e+6, 1 / 3.3e+6 };
                        std::vector< int > distanceType = { 1, 1, 0, 0, 0  };
                        std::vector< float > resultsA( resolution.a() * resolution.b(), 0.f );
                        std::vector< float > resultsC( resolution.a() * resolution.b(), 0.f );
                        std::pair< std::pair< float, float >, std::pair< float, float > > ranges = { std::pair< float, float >( { rX.a(), rX.b() } ), std::pair< float, float >{ rY.a(), rY.b() } };
                        int weightIndex = m_vboIndexes.at( "w0w1" );

                        m_statsComputer.computeRecurrenceHotSpots(
                            NP,
                            timeSeries->numSteps(),
                            weightIndex,
                            phaseIndexes,
                            phaseScaling,
                            distanceType,
                            heatmapSpace,
                            heatmapDims,
                            ranges,
                            resultsA,
                            resultsC );

                        m_phaseTextureLayers[ pIdx ].first.load(  resultsA, resolution.x(), resolution.y() );
                        m_phaseTextureLayers[ pIdx ].second.load( resultsC, resolution.x(), resolution.y() );
                    }
                    recomputed = true;
                }
            }

            Vec2< float > scissor( 0, 0 );

            // clip plot section to the right of the plot grid view widget
            if( ( j*figUnitSizeX ) + scrollOffset > m_plotGridView.size().x() )
            {
                ++pIdx;
                continue;
            }
            else if( ( ( j + dims.b() ) * figUnitSizeX + scrollOffset - MARGIN_RIGHT ) > m_plotGridView.size().x() )
            {
                scissor.b( ( ( j + dims.b() ) * figUnitSizeX + scrollOffset - MARGIN_RIGHT ) - m_plotGridView.size().x() );
            }

            // clip plot section to the left of the plot grid view widget
            if( ( ( j + dims.b() ) * ( figUnitSizeX ) + scrollOffset - MARGIN_RIGHT ) <= 0 )
            {
                ++pIdx;
                continue;
            }
            else if( ( j * figUnitSizeX + scrollOffset ) <= 0 )
            {
                scissor.a( -( j*( figUnitSizeX ) + scrollOffset ) - MARGIN_LEFT );
            }

            glScissor( m_plotGridView.position().x(), 0, m_plotGridView.size().x() - 4, scaledHeight() );
            glEnable( GL_SCISSOR_TEST );

            if( scissor.b() >= xSize )
            {
                ++pIdx;
                continue;
            }

            Vec2< float > sz( xSize, ySize );
            Vec2< float > pos( offSetX + scrollOffset + j * figUnitSizeX, offSetY + ( ROWS - 1 - i) * figUnitSizeY );

            renderPhasePlotGrid(
                sz,
                pos,
                scissor,
                def.attrs[ 0 ],
                def.attrs[ 1 ],
                rX,
                rY,
                true,
                ! m_gridOverlayButton.isPressed() );

//            glScissor( 0, 0, scaledWidth(), scaledHeight() );
//            glDisable( GL_SCISSOR_TEST );

            ////////////qDebug()  << "rendering " << def.name.c_str();

            // render the selection first...

            glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
            if(  QApplication::keyboardModifiers() & Qt::ControlModifier  )
            {
                //renderSelection( M, def.attrs[ 0 ], def.attrs[ 1 ], m_phaseBufferCounts, m_phaseBufferFirsts, color1, color2, true, false, false );
            }
            bool hasRadius = false;

            ViewPort vp;
            vp.setSize( std::min( m_plotGridView.size().x(), sz.x() ), sz.y() );
            vp.setPosition( std::max( m_plotGridView.position().x(), pos.x() ), pos.y() );
            Vec2< float > posM( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() );

            if( vp.pointInViewPort( posM ) && posM.x() < ( m_plotGridView.position().x() + m_plotGridView.size().x() ) )
            {
                hasRadius = true;
            }

            if( ! m_selectFocusButton.isPressed() )
            {
//                renderFocusContext(
//                    m_phaseTextureLayers[ pIdx ].first,
//                    m_phaseTextureLayers[ pIdx ].second,
//                    m_phasePlotTFAll,
//                    m_phasePlotTFRegion,
//                    pos,
//                    sz,
//                    scissor,
//                    2 );
            }
            else
            {
                Vec2< float > localRange;
                Vec2< float > localCenter;
                Vec2< float > localRadius;

                if( hasRadius )
                {
                    localCenter = getRelativeSelectionCenter( posM, pos, sz );
                    localRadius = getRelativeSelectionRadius( sz, m_hoverRadius );
                    localRange  = m_phaseTextureLayers[ pIdx ].second.getRangeInRadius( localCenter, localRadius );

                    ////qDebug() << "local max is " << localRange.y();
                }
                if( QApplication::keyboardModifiers() & Qt::ControlModifier )
                {
                    //renderSelection( M, def.attrs[ 0 ], def.attrs[ 1 ], m_phaseBufferCounts, m_phaseBufferFirsts, color3, color4, true, false, true, false );
                }
                renderFocusContext(
                    m_phaseTextureLayers[ pIdx ].first,
                    m_phaseTextureLayers[ pIdx ].second,
                    m_phasePlotTFAll,
                    m_phasePlotTFRegion,
                    pos,
                    sz,
                    scissor,
                    1,
                    hasRadius,
                    localCenter,
                    localRadius,
                    localRange );

                // get contour for each layer then render them

                std::vector< float > layerPixels1 = m_phaseTextureLayers[ pIdx ].first.get();
                std::vector< float > layerPixels2 = m_phaseTextureLayers[ pIdx ].second.get();

                for( auto & v : layerPixels1 )
                {
                    v = std::abs( v );
                }

                for( auto & v : layerPixels2 )
                {
                    v = std::abs( v );
                }

                std::vector< Vec2< float >  > contour1;
                std::vector< Vec2< float >  > contour2;

                std::vector< float > rowCoords;
                std::vector< float > columnCoords;

                float dS = 2.0 / m_phaseTextureLayers[ pIdx ].first.width;
                float ofs = -1 + 1.0 / m_phaseTextureLayers[ pIdx ].first.width;

                for( int row = 0; row < m_phaseTextureLayers[ pIdx ].first.width; ++row )
                {
                    rowCoords.push_back( ofs + dS*row );
                }

                dS = 2.0 / m_phaseTextureLayers[ pIdx ].first.height;
                ofs = -1 + 1.0 / m_phaseTextureLayers[ pIdx ].first.height;
                for( int col = 0; col < m_phaseTextureLayers[ pIdx ].first.height; ++col )
                {
                    columnCoords.push_back( ofs + dS*col );
                }

                #pragma omp parallel
                {
                    if( omp_get_thread_num() == 0 )
                    {
                        conrec2(
                            layerPixels1.data(),
                            m_phaseTextureLayers[ pIdx ].first.height,
                            m_phaseTextureLayers[ pIdx ].first.width,
                            columnCoords,
                            rowCoords,
                            { 0.0001 },
                            contour1 );
                    }
                    if( omp_get_thread_num() == 1 )
                    {
                        conrec2(
                            layerPixels2.data(),
                            m_phaseTextureLayers[ pIdx ].second.height,
                            m_phaseTextureLayers[ pIdx ].second.width,
                            columnCoords,
                            rowCoords,
                            { 0.0001 },
                            contour2 );
                    }
                }

                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );

                QMatrix4x4 rotation;
                rotation.setToIdentity();

                rotation.scale( QVector3D( -1, 1, 1 ) );
                rotation.rotate( 90.0, QVector3D( 0, 0, 1 ) );

                QVector4D ccl( 0.4, 0.4, 0.4, 1.0 );

                glLineWidth( 1.0 );
                glPointSize( 1.0 );
                m_renderer->renderPrimitivesFlat(contour1, cartesianProg, QVector4D( 0.2, 0.2, 0.2, 1.0 ), rotation,  GL_LINES );
                m_renderer->renderPrimitivesFlat(contour1, cartesianProg, QVector4D( 0.2, 0.2, 0.2, 1.0 ), rotation, GL_POINTS );
                m_renderer->renderPrimitivesFlat(contour2, cartesianProg, ccl, rotation,  GL_LINES );
                m_renderer->renderPrimitivesFlat(contour2, cartesianProg, ccl, rotation, GL_POINTS );
            }

            // render start points
//            if( m_emissionEventButton.isPressed() )
//            {
//                const int NT     = timeSeries->numSteps();
//                std::vector< GLsizei > startCounts( m_phaseBufferCounts.size(), 0 );
//                for( size_t c = 0; c < m_phaseBufferCounts.size(); ++c )
//                {
//                    if( m_phaseBufferCounts[ c ] >= 0 )
//                    {
//                        float offset = 0;
//                        for( size_t t = 0; t < NT; ++t )
//                        {
//                            if( m_subsetBitmasks[ c * NT + t ] & 1 )
//                            {
//                                offset = t;
//                                break;
//                            }
//                        }
//                        startCounts[ c ] = offset + 1;
//                    }
//                }
//                renderSelection( M, def.attrs[ 0 ], def.attrs[ 1 ], startCounts, m_phaseBufferFirsts,
//                                 QVector4D( 0, 0, 0, 1 ),
//                                 m_emissionEventColor, false, true, true );
//            }

            // render end points ?
//            if( m_absorptionEventButton.isPressed() )
//            {
//                const int NT = timeSeries->numSteps();
//                std::vector< GLsizei > endCounts( m_phaseBufferCounts.size(), 0 );
//                std::vector< GLsizei > starts( m_phaseBufferCounts.size(), 0 );

//                for( size_t c = 0; c < m_phaseBufferCounts.size(); ++c )
//                {
//                    if( m_phaseBufferCounts[ c ] >= 0 )
//                    {
//                        float offset = 0;
//                        for( size_t t = 1; t < NT; ++t )
//                        {
//                            if( ( ! m_subsetBitmasks[ c * NT + t ] & 1 ) && ( m_subsetBitmasks[ c * NT + t - 1 ] & 1 ) )
//                            {
//                                offset = c * NT + t - 1;
//                                break;
//                            }
//                        }
//                        if( offset > 0 )
//                        {
//                            starts[ c ] = offset;
//                            endCounts[ c ] = 1;
//                        }
//                    }
//                }
//                renderSelection( M, def.attrs[ 0 ], def.attrs[ 1 ], endCounts, starts,
//                                 QVector4D(   0, 0,   0, 1 ),
//                                 m_absorptionEventColor, false, true, true );
//            }

            ////////////////////////////////////////////////////
            // HACK CODE to render events (for Tokamak data)

            if( m_selectedPlot != "" /*&& m_updateSelection == false*/ )
            {
                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
                if( m_plotAllSampleButton.isPressed() )
                {
                    renderSelection( M, def.attrs[ 0 ], def.attrs[ 1 ],
                                     m_particleSelectionCounts, m_particleSelectionFirsts, QVector4D( 0.2, 0.2, 0.2, 1 ), QVector4D( 1.0, 1.0, 1.0, 1 ),
                                     false, false, false, false );
                }

//                qDebug() << "rendering selection";

                if( m_selectedParticle >= 0 )
                {
                    renderSelection( M, def.attrs[ 0 ], def.attrs[ 1 ], m_particleSelectionSubSampleCounts, m_particleSelectionFirsts,
                                      QVector4D(  m_selectedParticleColor.r * .6, m_selectedParticleColor.g * .6, m_selectedParticleColor.b*.6, m_selectedParticleColor.a ), QVector4D( m_selectedParticleColor.r, m_selectedParticleColor.g, m_selectedParticleColor.b, m_selectedParticleColor.a ), false, false, false, true );
                }
            }

            // BACK_HERE

            // direction change
            if( m_emissionEventButton.isPressed() )
            {
                const size_t NT = timeSeries->numSteps();

                auto & xData = *(m_trajectoryData.at( def.attrs[ 0 ] ) );
                auto & yData = *(m_trajectoryData.at( def.attrs[ 1 ] ) );

                static std::vector< float > xVerts;
                static std::vector< float > yVerts;

                xVerts.clear();
                yVerts.clear();

                const Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( def.attrs[ 0 ] )->second;
                const Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( m_particleCombo.selectedItemText() )->second.find( def.attrs[ 1 ] )->second;

                Vec2< float > RELATIVE_CENTER;
                Vec2< float > to11;
                Vec2< float > CENTER;
                Vec2< float > RELATIVE_RADIUS;
                Vec2< float > RADIUS;

                if( hasRadius )
                {
                    RELATIVE_CENTER = getRelativeSelectionCenter( posM, pos, sz );
                    to11 = ( RELATIVE_CENTER + Vec2<float >( 1, 1 ) ) / 2.f;
                    CENTER  = { rX.a() + to11.x() * ( rX.b() - rX.a() ), rY.a() + to11.y() * ( rY.b() - rY.a() ) };
                    RELATIVE_RADIUS = getRelativeSelectionRadius( sz, m_hoverRadius );
                    to11 = RELATIVE_RADIUS * 0.5;
                    RADIUS = { ( rX.b() - rX.a() ) * to11.x(), ( rY.b() - rY.a() ) * to11.y() };
                }

                for( size_t t = 1; t < NT; ++t )
                {
                    for( size_t p = 0, end = directionChangeIds[ t ].size(); p < end; ++p )
                    {
                        const size_t pID = directionChangeIds[ t ][ p ];
                        if( hasRadius && m_toolCombo.selectedItemText() == "declutter" && m_mouseIsPressed )
                        {
                            const float relativeRadius =
                                std::sqrt(
                                    std::pow( ( xData[ pID * NT + t ] - CENTER.x() ) / RADIUS.x(), 2.f ) +
                                    std::pow( ( yData[ pID * NT + t ] - CENTER.y() ) / RADIUS.y(), 2.f ) );

                            if( relativeRadius <= 1.f )
                            {
                                continue;
                            }
                        }
                        xVerts.push_back( xData[ pID*NT + t ] );
                        yVerts.push_back( yData[ pID*NT + t ] );
                    }
                }

                QVector4D cl = m_emissionEventColor;
                cl.setW( 0.3 );

                glPointSize( 1 );
                m_renderer->renderPrimitivesFlatSerial(
                    xVerts,
                    yVerts,
                    flatProgSerial,
                    cl,
                    M,
                    GL_POINTS );
            }

            // weight sign change
            if( m_absorptionEventButton.isPressed() )
            {
                const size_t NT = timeSeries->numSteps();
                const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( "ions" );

                auto & w0w1 = *(m_trajectoryData.at( "w0w1" ) );
                auto & xData = *(m_trajectoryData.at( def.attrs[ 0 ] ) );
                auto & yData = *(m_trajectoryData.at( def.attrs[ 1 ] ) );

                static std::vector< float > xVerts;
                static std::vector< float > yVerts;

                xVerts.clear();
                yVerts.clear();

                for( size_t p = 0; p < NP; ++p )
                {
                    for( size_t t = 1; t < NT; ++t )
                    {
                        if( w0w1[ p*NT + t ] * w0w1[ p*NT + ( t - 1 ) ] < 0 )
                        {
                            if( ( m_subsetBitmasks[ p * NT + t ] & 1 ) && ( m_subsetBitmasks[ p * NT + t ] & 2 ) )
                            {
                                xVerts.push_back( xData[ p*NT + t ] );
                                yVerts.push_back( yData[ p*NT + t ] );
                            }
                        }
                    }
                }

                QVector4D cl = m_absorptionEventColor;
                cl.setW( 0.2 );
                glPointSize( 1 );

                m_renderer->renderPrimitivesFlatSerial(
                    xVerts,
                    yVerts,
                    flatProgSerial,
                    cl,
                    M,
                    GL_POINTS );
            }

            ///////////////////////////////////////////////////////////////////

            glScissor( 0, 0, scaledWidth(), scaledHeight() );
            glDisable( GL_SCISSOR_TEST );

            if( m_gridOverlayButton.isPressed() )
            {
                renderPhasePlotGrid(
                    sz,
                    pos,
                    scissor,
                    def.attrs[ 0 ],
                    def.attrs[ 1 ],
                    rX,
                    rY,
                    false,
                    false );

                glScissor( 0, 0, scaledWidth(), scaledHeight() );
                glDisable( GL_SCISSOR_TEST );
            }

            if( hasRadius )
            {
                renderSelectionRadius(
                    getRelativeSelectionCenter( posM, pos, sz ),
                    getRelativeSelectionRadius( sz, m_hoverRadius ) );

                if( ! m_phasePlotPosSz.count( def.name ) )
                {
                    m_phasePlotPosSz.insert( { def.name, { pos, sz } } );
                }
                else
                {
                    m_phasePlotPosSz.at( def.name ) = { pos, sz };
                }
            }

            if( m_selectedPlot == def.name )
            {
                renderSelectionRadius( m_selectionPosition, getRelativeSelectionRadius( sz, m_selectionRadius ) );
            }

            ++pIdx;
        }

        if( timePlotDef != nullptr )
        {
            QMatrix4x4 M;
            M.setToIdentity();

            Vec2< double > rXT = { 0.0, double( timeSeries->numSteps() - 1 ) };
            Vec2< double > rYT = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( timePlotDef->attr )->second;

            const float xWT = ( rXT.b() - rXT.a() );
            const float yWT = ( rYT.b() - rYT.a() );
            M.scale( 2.0 / xWT, 2.0 / yWT );
            M.translate( -rXT.a() - xWT / 2.0, -rYT.a() - yWT / 2.0 );

            if( m_phasePlotsValid == false )
            {
                const float FACTOR = std::max( m_resolutionSlider.sliderPosition(), 1 / 20.f );
                const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );
//                computeContextLayers2(
//                    m_phasePlotCache,
//                    m_timeSeriesLayers,
//                    "time",
//                    timePlotDef->attr,
//                    Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
//                    ptype,
//                    m_phaseBufferCounts,
//                    m_phaseBufferFirsts,
//                    M,
//                    true,
//                    NP
//                );

                if( m_phasePlotBlendingCombo.selectedItemText() == "Weighted Density" )
                {
                    computeContextLayerWeightedDensity(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        "w0w1",
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        NP
                    );
                }
                else if( m_phasePlotBlendingCombo.selectedItemText() == "Number Density" )
                {
                    computeContextLayers2(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        true,
                        NP
                    );
                }
                else if ( m_phasePlotBlendingCombo.selectedItemText() == "Path Density" )
                {
                    computeContextLayers2(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        false,
                        NP
                    );
                }
                else if ( m_phasePlotBlendingCombo.selectedItemText() == "Variance" )
                {
                    computeContextLayersVariance(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        "w0w1",
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        NP
                    );
                }
                else if ( m_phasePlotBlendingCombo.selectedItemText() == "Min" )
                {
                    ////qDebug() << "computing min";
                    computeContextLayersMin(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        "w0w1",
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        NP
                    );
                }
                else if ( m_phasePlotBlendingCombo.selectedItemText() == "Max" )
                {
                    ////qDebug() << "computing max";
                    computeContextLayersMax(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        "w0w1",
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        NP
                    );
                }
                else if ( m_phasePlotBlendingCombo.selectedItemText() == "Mean" )
                {
                    computeContextLayersMean(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        "w0w1",
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        NP
                    );
                }
                else if ( m_phasePlotBlendingCombo.selectedItemText() == "Range" )
                {
                    computeContextLayersRange(
                        m_phasePlotCache,
                        m_timeSeriesLayers,
                        "time",
                        timePlotDef->attr,
                        "w0w1",
                        Vec2< int >( m_timeLineWidget.size().x() * FACTOR, m_timeLineWidget.size().y() * FACTOR ),
                        ptype,
                        m_phaseBufferCounts,
                        m_phaseBufferFirsts,
                        M,
                        NP
                    );
                }
            }

            Vec2< float > scissor( 0, 0 );
            Vec2< float > pos = m_timeLineWidget.plotPosition();
            Vec2< float > sz = m_timeLineWidget.plotSize();

            const int N_SETS = m_subsetCombo.numItems() + 3;
            const int NT = timeSeries->numSteps();
            const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );
            const int N_VARS = m_rangeView.rangeWidgets.size();
            static float tp_max = -std::numeric_limits< float >::max();
            static float tp_min = std::numeric_limits< float >::max();
            static std::vector< float > ts_result;

            if( ! m_gridOverlayButton.isPressed() )
            {
                renderPhasePlotGrid(
                    sz,
                    pos,
                { 0, 0 },
                "t",
                timePlotDef->attr,
                { 0, NT-1 },
                { tp_min, tp_max },
                false,
                true );
                glScissor( 0, 0, scaledWidth(), scaledHeight() );
                glDisable( GL_SCISSOR_TEST );
            }

            bool hasRadius = false;
            Vec2< float > posM( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() );

            if( m_timeLineWidget.pointInViewPort( posM + Vec2< float >( -m_timeLineWidget.MARGIN_LEFT,  0 ) )
                    && m_timeLineWidget.pointInViewPort( posM + Vec2< float >(  m_timeLineWidget.MARGIN_RIGHT, 0 ) ) )
            {
                hasRadius = true;
            }

            if( m_timeTextureSelector.isPressed() )
            {
//                renderFocusContext(
//                    m_timeSeriesLayers.first,
//                    m_timeSeriesLayers.second,
//                    m_phasePlotTFAll,
//                    m_phasePlotTFRegion,
//                    pos,
//                    sz,
//                    scissor, );

                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
                //renderSelection( M, "time", timePlotDef->attr, m_phaseBufferCounts, m_phaseBufferFirsts, color1, color2, true, false, false, false );

                if( ! m_selectFocusButton.isPressed() )
                {
//                    renderFocusContext(
//                        m_timeSeriesLayers.first,
//                        m_timeSeriesLayers.second,
//                        m_phasePlotTFAll,
//                        m_phasePlotTFRegion,
//                        pos,
//                        sz,
//                        scissor,
//                        2, );
                }
                else
                {
                    //renderSelection( M, "time", timePlotDef->attr, m_phaseBufferCounts, m_phaseBufferFirsts, color3, color4, true, false, true, false );

                    Vec2< float > localRange;
                    Vec2< float > localCenter;
                    Vec2< float > localRadius;

                    if( hasRadius )
                    {
                        localCenter = getRelativeSelectionCenter( posM, pos, sz );
                        localRadius = getRelativeSelectionRadius( sz, m_hoverRadius );
                        localRange  = m_timeSeriesLayers.second.getRangeInRadius( localCenter, localRadius );

                        ////qDebug() << "local max is " << localRange.y();
                    }

//                    renderFocusContext(
//                        m_timeSeriesLayers.first,
//                        m_timeSeriesLayers.second,
//                        m_phasePlotTFAll,
//                        m_phasePlotTFRegion,
//                        pos,
//                        sz,
//                        scissor,
//                        1, hasRadius, localCenter, localRadius, localRange );

                    renderFocusContext(
                        m_timeSeriesLayers.first,
                        m_timeSeriesLayers.second,
                        m_phasePlotTFAll,
                        m_phasePlotTFRegion,
                        pos,
                        sz,
                        scissor,
                        1,
                        hasRadius,
                        localCenter,
                        localRadius,
                        localRange );

                    std::vector< float > layerPixels1 = m_timeSeriesLayers.first.get();
                    std::vector< float > layerPixels2 = m_timeSeriesLayers.second.get();

                    for( auto & v : layerPixels1 )
                    {
                        v = std::abs( v );
                    }

                    for( auto & v : layerPixels2 )
                    {
                        v = std::abs( v );
                    }

                    std::vector< Vec2< float >  > contour1;
                    std::vector< Vec2< float >  > contour2;

                    std::vector< float > rowCoords;
                    std::vector< float > columnCoords;

                    float dS = 2.0 /  m_timeSeriesLayers.first.width;
                    float ofs = -1 + 1.0 /  m_timeSeriesLayers.first.width;

                    for( int row = 0; row <  m_timeSeriesLayers.first.width; ++row )
                    {
                        rowCoords.push_back( ofs + dS*row );
                    }

                    dS = 2.0 / m_timeSeriesLayers.first.height;
                    ofs = -1 + 1.0 / m_timeSeriesLayers.first.height;
                    for( int col = 0; col < m_timeSeriesLayers.first.height; ++col )
                    {
                        columnCoords.push_back( ofs + dS*col );
                    }

                    conrec2(
                        layerPixels1.data(),
                        m_timeSeriesLayers.first.height,
                        m_timeSeriesLayers.first.width,
                        columnCoords,
                        rowCoords,
                        { 0.0001 },
                        contour1 );

                    conrec2(
                        layerPixels2.data(),
                        m_timeSeriesLayers.second.height,
                        m_timeSeriesLayers.second.width,
                        columnCoords,
                        rowCoords,
                        { 0.0001 },
                        contour2 );

                    glViewport( pos.x(), pos.y(), sz.x(), sz.y() );

                    QMatrix4x4 rotation;
                    rotation.setToIdentity();

                    rotation.scale( QVector3D( -1, 1, 1 ) );
                    rotation.rotate( 90.0, QVector3D( 0, 0, 1 ) );

                    QVector4D ccl( 0.4, 0.4, 0.4, 1.0 );

                    glLineWidth( 1.0 );
                    glPointSize( 1.0 );
                    m_renderer->renderPrimitivesFlat(contour1, cartesianProg, QVector4D( 0.2, 0.2, 0.2, 1.0 ), rotation,  GL_LINES );
                    m_renderer->renderPrimitivesFlat(contour1, cartesianProg, QVector4D( 0.2, 0.2, 0.2, 1.0 ), rotation, GL_POINTS );
                    m_renderer->renderPrimitivesFlat(contour2, cartesianProg, ccl, rotation,  GL_LINES );
                    m_renderer->renderPrimitivesFlat(contour2, cartesianProg, ccl, rotation, GL_POINTS );
                }
            }

            glScissor( 0, 0, scaledWidth(), scaledHeight() );
            glDisable( GL_SCISSOR_TEST );

            static std::string timePlotS = timePlotDef->attr;
            std::string timePlotC = timePlotDef->attr;
            if( timePlotC != timePlotS )
            {
                m_recalculateFullTimePlot = true;
            }
            timePlotS = timePlotC;

            if( m_recalculateFullTimePlot )
            {
                ts_result.resize( 2 * NT );
                const int NT = timeSeries->numSteps();
                const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );

                m_statsComputer.computeTimeSeriesSumsAndCounts( NP, NT, m_vboIndexes.at( timePlotDef->attr ), false );
                m_statsComputer.getSeriesStats( NT, ts_result );

                m_recalculateFullTimePlot = false;
            }

            tp_max = -std::numeric_limits< float >::max();
            tp_min =  std::numeric_limits< float >::max();

            if( m_timeTextureSelector.isPressed() )
            {
                Vec2< double > r = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( timePlotDef->attr )->second;
                tp_max = r.b();
                tp_min = r.a();
            }
            else
            {
                for( auto f : ts_result )
                {
                    if( ! std::isnan( f ) )
                    {
                        tp_min = std::min( tp_min, f );
                        tp_max = std::max( tp_max, f );
                    }
                }

                if( tp_max > 0 )
                {
                    tp_max *= 1.1;
                }
                else
                {
                    tp_max *= 0.9;
                }

                if( tp_min > 0 )
                {
                    tp_min *= 0.9;
                }
                else
                {
                    tp_min *= 1.1;
                }
            }

            float pltOffY = m_timeLineWidget.position().y() + m_timeLineWidget.MARGIN_BOTTOM;
            float plHT = m_timeLineWidget.size().y() - m_timeLineWidget.MARGIN_BOTTOM - m_timeLineWidget.MARGIN_TOP;

            const int TEXT_PREC = 4;
            const int MIN_MAX_TEX_OFF_A = 30;
            const int MIN_MAX_TEX_OFF_B = 20;
            const int PLOT_MM_TEX_X = 48;
            Vec2< double > yRange( tp_min, tp_max );

            glViewport( 0, 0, scaledWidth(), scaledHeight() );

            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    to_string_with_precision( yRange.a(), TEXT_PREC ),
                                    m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                    m_timeLineWidget.position().y() + pltOffY - MIN_MAX_TEX_OFF_A,
                                    1.0,
                                    Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                    true );

            m_renderer->renderText( false,
                                    textProg,
                                    scaledWidth(),
                                    scaledHeight(),
                                    to_string_with_precision( yRange.b(), TEXT_PREC ),
                                    m_timeLineWidget.position().x() + m_timeLineWidget.plotLeft() - PLOT_MM_TEX_X,
                                    m_timeLineWidget.position().y() + pltOffY + plHT - MIN_MAX_TEX_OFF_B,
                                    1.0,
                                    Vec3< float >( m_textColor.x(), m_textColor.y(), m_textColor.z() )*.9,
                                    true );

            renderTimeLineAnnotation();
            renderTimeSeriesInfo( ptype );

            if( m_selectedPlot != "" && m_updateSelection == false )
            {
                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
                if( m_plotAllSampleButton.isPressed() )
                {
                    renderSelection( M, "time", timePlotDef->attr,
                                     m_particleSelectionCounts, m_particleSelectionFirsts, QVector4D( 0.2, 0.2, 0.2, 1 ),
                                     QVector4D( 1, 1, 1, 1 ), false, false, false, false );
                }

                if( m_selectedParticle >= 0 )
                {
                    renderSelection( M, "time", timePlotDef->attr, m_particleSelectionSubSampleCounts, m_particleSelectionFirsts,
                                     QVector4D(  m_selectedParticleColor.r * .6, m_selectedParticleColor.g * .6, m_selectedParticleColor.b*.6, m_selectedParticleColor.a ), QVector4D( m_selectedParticleColor.r, m_selectedParticleColor.g, m_selectedParticleColor.b, m_selectedParticleColor.a ), false, false, false, true );
                }
            }

            //renderTemporalPlot( x, y, yRange, *timeSeries, m_selectedTimeStep, QVector4D( .8, .8, .8, 1 ) );

            if( m_timeSubsetCombStatsSelector.isPressed() || m_timeSubsetStatsSelector.isPressed() )
            {
                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
                renderTimeSeriesPlots2(
                    ts_result,
                { tp_min, tp_max },
                NT,
                m_selectedTimeStep,
                N_SETS,
                true );
            }

            if( m_gridOverlayButton.isPressed() )
            {
                renderPhasePlotGrid(
                    sz,
                    pos,
                { 0, 0 },
                "t",
                timePlotDef->attr,
                { 0, NT-1 },
                { tp_min, tp_max },
                false,
                false );
                glScissor( 0, 0, scaledWidth(), scaledHeight() );
                glDisable( GL_SCISSOR_TEST );
            }

            m_renderer->renderTimeLinePlot( m_timeLineWidget, cartesianProg, flatProgSerial, QVector4D( .1, .1, .1, 1 ) );

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////

//            if( m_emissionEventButton.isPressed() )
//            {
//                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
//                const int NT     = timeSeries->numSteps();
//                std::vector< GLsizei > startCounts( m_phaseBufferCounts.size(), 0 );
//                for( size_t c = 0; c < m_phaseBufferCounts.size(); ++c )
//                {
//                    if( m_phaseBufferCounts[ c ] >= 0 )
//                    {
//                        float offset = 0;
//                        for( size_t t = 0; t < NT; ++t )
//                        {
//                            if( m_subsetBitmasks[ c * NT + t ] & 1 )
//                            {
//                                offset = t;
//                                break;
//                            }
//                        }
//                        startCounts[ c ] = offset + 1;
//                    }
//                }
//                renderSelection( M, "time", timePlotDef->attr, startCounts, m_phaseBufferFirsts,
//                                 QVector4D(   0, 0, 0, 1 ),
//                                 m_emissionEventColor, false, true, true );
//            }
//            if( m_absorptionEventButton.isPressed() )
//            {
//                const int NT = timeSeries->numSteps();
//                std::vector< GLsizei > endCounts( m_phaseBufferCounts.size(), 0 );
//                std::vector< GLsizei > starts( m_phaseBufferCounts.size(), 0 );

//                for( size_t c = 0; c < m_phaseBufferCounts.size(); ++c )
//                {
//                    if( m_phaseBufferCounts[ c ] >= 0 )
//                    {
//                        float offset = 0;
//                        for( size_t t = 1; t < NT; ++t )
//                        {
//                            if( ( ! m_subsetBitmasks[ c * NT + t ] & 1 ) && ( m_subsetBitmasks[ c * NT + t - 1 ] & 1 ) )
//                            {
//                                offset = c * NT + t - 1;
//                                break;
//                            }
//                        }
//                        if( offset > 0 )
//                        {
//                            starts[ c ] = offset-10;
//                            endCounts[ c ] = 20;
//                        }
//                    }
//                }
//                renderSelection( M, "time", timePlotDef->attr, endCounts, starts,
//                                 QVector4D(   0, 0,   0, 1 ),
//                                 m_absorptionEventColor, false, true, true );
//            }

            // direction change
            if( m_emissionEventButton.isPressed() )
            {
                const size_t NT = timeSeries->numSteps();
                auto & yData = *(m_trajectoryData.at( timePlotDef->attr ) );

                static std::vector< float > xVerts;
                static std::vector< float > yVerts;

                xVerts.clear();
                yVerts.clear();

                for( size_t t = 1; t < NT; ++t )
                {
                    for( size_t p = 0, end = directionChangeIds[ t ].size(); p < end; ++p )
                    {
                        const size_t pID = directionChangeIds[ t ][ p ];
                        xVerts.push_back( t );
                        yVerts.push_back( yData[ pID*NT + t ] );
                    }
                }

                QVector4D cl = m_emissionEventColor;
                cl.setW( 0.3 );
                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
                glPointSize( 1 );
                m_renderer->renderPrimitivesFlatSerial(
                    xVerts,
                    yVerts,
                    flatProgSerial,
                    cl,
                    M,
                    GL_POINTS );
            }

            // weight sign change
            if( m_absorptionEventButton.isPressed() )
            {
                const size_t NT = timeSeries->numSteps();
                const size_t NP = m_dataSetManager->particleDataManager().numLoadedParticles( "ions" );

                auto & w0w1 = *(m_trajectoryData.at( "w0w1" ) );
                //auto & xData = *(m_trajectoryData.at( "time" ) );
                auto & yData = *(m_trajectoryData.at( timePlotDef->attr ) );

                static std::vector< float > xVerts;
                static std::vector< float > yVerts;

                xVerts.clear();
                yVerts.clear();

                for( size_t p = 0; p < NP; ++p )
                {
                    for( size_t t = 1; t < NT; ++t )
                    {
                        if( w0w1[ p*NT + t ] * w0w1[ p*NT + ( t - 1 ) ] < 0 )
                        {
                            if( ( m_subsetBitmasks[ p * NT + t ] & 1 ) && ( m_subsetBitmasks[ p * NT + t ] & 2 ) )
                            {
                                xVerts.push_back( t );
                                yVerts.push_back( yData[ p*NT + t ] );
                            }
                        }
                    }
                }

                QVector4D cl = m_absorptionEventColor;
                cl.setW( 0.5 );
                glPointSize( 3 );
                glViewport( pos.x(), pos.y(), sz.x(), sz.y() );
                m_renderer->renderPrimitivesFlatSerial(
                    xVerts,
                    yVerts,
                    flatProgSerial,
                    cl,
                    M,
                    GL_POINTS );
            }


            if( hasRadius )
            {
                renderSelectionRadius(
                    getRelativeSelectionCenter( posM, pos, sz ),
                    getRelativeSelectionRadius( sz, m_hoverRadius ) );

                if( ! m_phasePlotPosSz.count( "timeline" ) )
                {
                    ////////qDebug()  << "inserted timeline " << pos.x() << " " << pos.y() << " " << sz.x() << " " << sz.y();
                    m_phasePlotPosSz.insert( { "timeline", { pos, sz } } );
                }
                else
                {
                    m_phasePlotPosSz.at( "timeline" ) = { pos, sz };
                }
            }

            if( m_selectedPlot == "timeline" )
            {
                renderSelectionRadius( m_selectionPosition, getRelativeSelectionRadius( sz, m_selectionRadius ) );
            }
        }

        if( m_phasePlotsValid == false )
        {
            if( recomputed )
            {
                m_phasePlotsValid = true;
            }
        }

        renderRangeView();
        renderTrajectoryView();
        renderStatsView();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if( m_selectedParticle >= 0 && m_showRecurrenceButton.isPressed() )
        {
            const int NT = timeSeries->numSteps();

//            std::vector< std::string > attrs = { "vpara",    "vperp" };
//            std::vector< float > scales = {   8 / 3.3e+6, 8 / 3.3e+6 };

//            std::vector< std::string > attrs = { "psin", "poloidal_angle", "zeta" };
//            std::vector< float > scales = { 1, 1/6.83, 1/6.83 };

            std::vector< std::string > attrs = { "poloidal_angle" };
            std::vector< float > scales = { 1 };

            std::vector< float > distances;

            computeRecurrence( attrs, scales, m_selectedParticle, distances );

            float mx = TN::Parallel::max( distances );
            float mn = TN::Parallel::min( distances );

//            if( 1 )
//            {
//                const int NP = m_dataSetManager->particleDataManager().numLoadedParticles( ptype );
//                std::vector< Vec2< float > > coords;
//                for( int p = 0; p < NP; ++p )
//                {
//                    computeRecurrence( attrs, scales, p, distances );

//                    for( int t = 0; t < NT; ++t )
//                    {
//                        if( m_subsetBitmasks[ p*NT + t ] & 1 == 0 )
//                        {
//                            continue;
//                        }

//                        for( int t2 = 0; t2 < t - 100; ++t2 )
//                        {
//                             if( distances[ t*NT + t2 ] < 0.05 )
//                             {
//                                 coords.push_back( {
//                                                       ( *( m_dataSetManager->particleDataManager().values( "ions", "r" )[ t ] ) )[ p ],
//                                                       ( *( m_dataSetManager->particleDataManager().values( "ions", "z" )[ t ] ) )[ p ]
//                                                   } );

//                                 coords.push_back( {
//                                                       ( *( m_dataSetManager->particleDataManager().values( "ions", "r" )[ t2 ] ) )[ p ],
//                                                       ( *( m_dataSetManager->particleDataManager().values( "ions", "z" )[ t2 ] ) )[ p ]
//                                                   } );
//                             }
//                        }
//                    }

//                    qDebug() << "done with " << p << " have " << coords.size() << " points ";
//                }

//                auto psz1 = m_phasePlotPosSz.at( "plt 1" );
//                Vec2< double > rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( "r" )->second;
//                Vec2< double > rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( "z" )->second;

//                QMatrix4x4 M;
//                M.setToIdentity();
//                float xW = ( rX.b() - rX.a() );
//                float yW = ( rY.b() - rY.a() );
//                M.scale( 2.0 / xW, 2.0 / yW );
//                M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );
//                glViewport( psz1.first.x(), psz1.first.y(), psz1.second.x(), psz1.second.y() );

//                glPointSize( 20 );
//                m_renderer->renderPrimitivesFlat( coords, cartesianProg, QVector4D( 0, 0, 0, 1 ), M, GL_POINTS );

//                glPointSize( 10 );
//                m_renderer->renderPrimitivesFlat( coords, cartesianProg, QVector4D( 1, 1, 1, 0.005 ), M, GL_POINTS );

//                return;
//            }


            float threshold = ( mx - mn ) * m_recurrenceSlider.sliderPosition()* m_recurrenceSlider.sliderPosition();
            if( m_thresholdRecurrenceButton.isPressed() )
            {
                for( auto & d : distances )
                {
                    if( d < threshold )
                    {
                        d = 0;
                    }
                    else
                    {
                        d = 1;
                    }
                }

                mn = 0;
                mx = 1;
            }

            TNR::TextureLayer layer;
            layer.load( distances, NT, NT );

            const float pltUnitSize = ( m_plotGridView.size().y() - m_plotGridView.SCROLL_BAR_HEIGHT ) / 2 - ( MARGIN_TOP + MARGIN_BOTTOM );
            glViewport( m_phasePlotPosSz.at( "plt 4" ).first.x() + m_phasePlotPosSz.at( "plt 4" ).second.x() + 70, m_phasePlotPosSz.at( "plt 4" ).first.y(), pltUnitSize, pltUnitSize );

            m_texVAO->bind();
            m_texVBO->bind();

            heatMapRenderProg->bind();

            glActiveTexture( GL_TEXTURE0 );
            layer.tex.bind();
            glGenerateMipmap( GL_TEXTURE_2D );

            glActiveTexture( GL_TEXTURE1 );
            m_phasePlotTFWeightBin.bind();

            glActiveTexture( GL_TEXTURE0 );

            heatMapRenderProg->setUniformValue(  "tex",  0 );
            heatMapRenderProg->setUniformValue(  "tf",   1 );
            heatMapRenderProg->setUniformValue(  "mx",  mx );
            heatMapRenderProg->setUniformValue(  "mn",  mn );

            GLuint posAttr = heatMapRenderProg->attributeLocation( "ps" );
            GLuint uvAttr  = heatMapRenderProg->attributeLocation( "tc" );

            heatMapRenderProg->enableAttributeArray( posAttr );
            heatMapRenderProg->enableAttributeArray( uvAttr );

            heatMapRenderProg->setAttributeBuffer( posAttr, GL_FLOAT, 0, 2 );
            heatMapRenderProg->setAttributeBuffer( uvAttr,  GL_FLOAT, 12 * sizeof( float ), 2 );

            glDrawArrays( GL_TRIANGLES, 0, 6 );

            heatMapRenderProg->disableAttributeArray( posAttr );
            heatMapRenderProg->disableAttributeArray( uvAttr );

            heatMapRenderProg->release();

            m_texVBO->release();
            m_texVAO->release();

            m_renderer->renderPopSquare( simpleColorProg );

            if( m_emissionEventButton.isPressed() )
            {
                float * data = m_trajectoryData.at( "vpara" )->data();
                for( size_t t = 0; t < NT; ++t )
                {
                    bool select = ( ( m_subsetBitmasks[ m_selectedParticle*NT + t ] & 1 ) != 0 ) && ( ( m_subsetBitmasks[ m_selectedParticle*NT + t ] & 2 ) != 0 );
                    if( ( data[ m_selectedParticle*NT + t ] * data[ m_selectedParticle*NT + t - 1 ] < 0 )  && select )
                    {
                        Vec2< float > A = { t, 0 };
                        Vec2< float > B = { t, t };
                        Vec2< float > C = { 0, t };
                        Vec2< float > D = { t, t };

                        Vec2< double > rX = { 0, NT-1 };
                        Vec2< double > rY = { 0, NT-1 };

                        QMatrix4x4 M;
                        M.setToIdentity();
                        float xW = ( rX.b() - rX.a() );
                        float yW = ( rY.b() - rY.a() );
                        M.scale( 2.0 / xW, 2.0 / yW );
                        M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );

                        std::vector< Vec2< float > > coords1 = { A, B };
                        std::vector< Vec2< float > > coords2 = { B, C };
                        std::vector< Vec2< float > > coords3 = { B };

                        glPointSize( 10 );
                        m_renderer->renderPrimitivesFlat( coords3, cartesianProg, QVector4D( .2, .2, .2, 1 ), M, GL_POINTS );

                        glPointSize( 7 );
                        m_renderer->renderPrimitivesFlat( coords3, cartesianProg, m_emissionEventColor, M, GL_POINTS );

                        glPointSize( 1 );
                        m_renderer->renderPrimitivesFlat( coords1, cartesianProg, m_emissionEventColor, M, GL_LINES );

                        glPointSize( 1 );
                        m_renderer->renderPrimitivesFlat( coords2, cartesianProg, m_emissionEventColor, M, GL_LINES );
                    }
                }
            }

            glViewport( 0, 0, scaledWidth(), scaledHeight() );

            Vec2< float > posM( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() );
            ViewPort vp;
            vp.setSize( pltUnitSize, pltUnitSize );
            vp.setPosition( m_phasePlotPosSz.at( "plt 4" ).first.x() + m_phasePlotPosSz.at( "plt 4" ).second.x() + 70, m_phasePlotPosSz.at( "plt 4" ).first.y() );

            if( vp.pointInViewPort( posM ) )
            {
                int tb = std::min( (int)std::floor( ( ( posM.x() - vp.offsetX() ) /  vp.width() ) * NT ), NT-1 );
                int ta = std::min( (int)std::floor( ( ( posM.y() - vp.offsetY() ) / vp.height() ) * NT ), NT-1 );
                glViewport( 0, 0, scaledWidth(), scaledHeight() );
                m_renderer->renderText(
                    true,
                    textProg,
                    scaledWidth(),
                    scaledHeight(),
                    "time steps: (" + std::to_string( ta ) + ", " + std::to_string( tb ) + ")",
                    vp.offsetX(),
                    vp.offsetY() - 20,
                    1.0f,
                    Vec3< float >( 0, 0, 0 ) );

                for( auto & def : m_phasePlotPosSz )
                {
                    std::string xAttr;
                    std::string yAttr;

                    if( def.first == "timeline" )
                    {
                        xAttr =  "time";
                        yAttr = "vperp";
                    }
                    else
                    {
                        xAttr = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( "ions" ).at(  def.first ).attrs[ 0 ];
                        yAttr = m_currentConfigurations.particleBasedConfiguration.m_phasePlotDefinitions.at( "ions" ).at(  def.first ).attrs[ 1 ];
                    }

                    Vec2< float > pA;
                    Vec2< float > pB;

                    Vec2< double > rX;
                    Vec2< double > rY;

                    if( def.first == "timeline" )
                    {
                        pA = {
                            ta,
                            ( *( m_dataSetManager->particleDataManager().values( "ions", yAttr )[ ta ] ) )[ m_selectedParticle ]
                        };

                        pB = {
                            tb,
                            ( *( m_dataSetManager->particleDataManager().values( "ions", yAttr )[ tb ] ) )[ m_selectedParticle ]
                        };

                        rX = { 0, NT-1 };
                        rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( yAttr )->second;

                        qDebug() << "got range";
                    }
                    else
                    {
                        pA = {
                            ( *( m_dataSetManager->particleDataManager().values( "ions", xAttr )[ ta ] ) )[ m_selectedParticle ],
                            ( *( m_dataSetManager->particleDataManager().values( "ions", yAttr )[ ta ] ) )[ m_selectedParticle ]
                        };

                        pB = {
                            ( *( m_dataSetManager->particleDataManager().values( "ions", xAttr )[ tb ] ) )[ m_selectedParticle ],
                            ( *( m_dataSetManager->particleDataManager().values( "ions", yAttr )[ tb ] ) )[ m_selectedParticle ]
                        };

                        rX = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( xAttr )->second;
                        rY = m_dataSetManager->particleDataManager().attributeRanges().find( ptype )->second.find( yAttr )->second;
                    }

                    QMatrix4x4 M;
                    M.setToIdentity();
                    float xW = ( rX.b() - rX.a() );
                    float yW = ( rY.b() - rY.a() );
                    M.scale( 2.0 / xW, 2.0 / yW );
                    M.translate( -rX.a() - xW / 2.0, -rY.a() - yW / 2.0 );
                    std::vector< Vec2< float > > coords1 = { pA };
                    std::vector< Vec2< float > > coords2 = { pB };

                    Vec3< float > c1 = { 0, 1, 0.3 };
                    Vec3< float > c2 = { 1, 0,  1 };

                    if( def.first == "timeline" )
                    {
                        glViewport(
                            m_timeLineWidget.plotPosition().x(),
                            m_timeLineWidget.plotPosition().y(),
                            m_timeLineWidget.plotSize().x(),
                            m_timeLineWidget.plotSize().y() );

                        qDebug() << "set viewport";
                    }
                    else
                    {
                        glViewport(
                                m_phasePlotPosSz.at( def.first ).first.x(),
                                m_phasePlotPosSz.at( def.first ).first.y(),
                                m_phasePlotPosSz.at( def.first ).second.x(),
                                m_phasePlotPosSz.at( def.first ).second.y() );
                    }

                    glPointSize( 14 );
                    m_renderer->renderPrimitivesFlat( coords1, cartesianProg, QVector4D( 0, 0, 0, 1 ), M, GL_POINTS );
                    glPointSize( 10 );
                    m_renderer->renderPrimitivesFlat( coords1, cartesianProg, QVector4D( c1.x(), c1.y(), c1.z(), 1 ), M, GL_POINTS );

                    glPointSize( 14 );
                    m_renderer->renderPrimitivesFlat( coords2, cartesianProg, QVector4D( 0, 0, 0, 1 ), M, GL_POINTS );
                    glPointSize( 10 );
                    m_renderer->renderPrimitivesFlat( coords2, cartesianProg, QVector4D( c2.x(), c2.y(), c2.z(), 1 ), M, GL_POINTS );
                }
            }
        }
        //renderRecurrenceView();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    ////checkError( "checkpoint 2" );

    m_renderer->renderViewPortBoxShadow( vpt, simpleColorProg );

    ////checkError( "checkpoint 2.01" );

    m_renderer->renderViewPortBoxShadow( m_rangeView.viewPort(), simpleColorProg );

    ////checkError( "checkpoint 2.02" );

    ViewPort vprt2;
    vprt2.bkgColor( Vec4( m_dividerColor.x(), m_dividerColor.y(), m_dividerColor.z(), 1 ) );
    vprt2.setSize( m_recurrenceView.size().x(), 80 );
    vprt2.offsetX( m_recurrenceView.position().x() );
    vprt2.offsetY( m_recurrenceView.position().y() + m_recurrenceView.size().y() - 80 );
    m_renderer->clearViewPort( vprt2, cartesianProg );
    glViewport( vprt2.offsetX(),vprt2.offsetY(), vprt2.width(), vprt2.height() );

    ////checkError( "checkpoint 2.03" );

//    vprt2 = m_recurrenceView.viewPort();
//    vprt2.setSize( vprt2.width(), vprt2.height() - 80 );

//    m_renderer->renderViewPortBoxShadow( vprt2, simpleColorProg );

///////

    ////checkError( "checkpoint 2.1" );

    std::vector< Vec2< float > > line( 2, Vec2< float >( 0, 0 ) );

    line[0].x(   0 );
    line[1].x(   0 );
    line[0].y(   1 );
    line[1].y(  -1 );

    glViewport(
        m_plotGridView.position().x() + m_plotGridView.size().x() - 1,
        m_plotGridView.position().y() + m_plotGridView.size().y() + 8,
        10,
        m_plotGridView.PANEL_HEIGHT - 16
    );
    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 0, 0, 0, 0.2 ), QMatrix4x4(), GL_LINES );

    glViewport(
        m_plotGridView.position().x() + m_plotGridView.size().x(),
        m_plotGridView.position().y() + m_plotGridView.size().y() + 8,
        10,
        m_plotGridView.PANEL_HEIGHT - 16
    );
    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 1, 1, 1, 0.5 ), QMatrix4x4(), GL_LINES );


    ////checkError( "checkpoint 2.2" );


    //

    glViewport(
        m_rangeView.position().x() + m_rangeView.size().x() - 1,
        m_rangeView.position().y() + m_rangeView.size().y() + 8,
        10,
        m_plotGridView.PANEL_HEIGHT - 16
    );
    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 0, 0, 0, 0.2 ), QMatrix4x4(), GL_LINES );

    glViewport(
        m_rangeView.position().x() + m_rangeView.size().x(),
        m_rangeView.position().y() + m_rangeView.size().y() + 8,
        10,
        m_plotGridView.PANEL_HEIGHT - 16
    );
    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 1, 1, 1, 0.5 ), QMatrix4x4(), GL_LINES );

    ////checkError( "checkpoint 2.3" );

    ////

    // Subset list

    float subsetWidgetOffset = m_subsettingWidget.position().y();
    if( m_subsetCombo.numItems() >= 1 )
    {
        std::vector< std::string > subsetListText = m_subsetCombo.items();

        subsetWidgetOffset = renderList(
                                 subsetListText,
                                 m_subsetColorCodes,
                                 m_subsetSymbols,
                                 Vec2< float >(
                                     m_subsetCombo.position().x(),
                                     m_subsetCombo.position().y() - m_subsetCombo.size().y() - DIVIDER_WIDTH
                                 ),
                                 m_subsetCombo.size().x(),
                                 BUTTON_HEIGHT );
    }

    ////checkError( "checkpoint 2.4" );

    m_subsettingWidget.setPosition(
        m_subsettingWidget.position().x(),
        subsetWidgetOffset );

    m_subsettingWidget.setSize(
        m_subsettingWidget.size().x(),
        m_selectionView.position().y() + m_selectionView.size().y() - subsetWidgetOffset );

    ////checkError( "checkpoint 2.5" );

    m_helpButtonSelection.setPosition(
        m_subsettingWidget.position().x(),
        m_subsettingWidget.position().y() - DIVIDER_WIDTH - m_helpButtonSelection.size().y()- BUTTON_HEIGHT *( 3 ) );

    m_combinationExpression.setSize( m_selectionView.size().x() - DIVIDER_WIDTH*2 - m_selectionView.SCROLL_BAR_HEIGHT - m_subsetCombo.size().y(), m_subsetCombo.size().y() );
    m_combinationExpression.setPosition( DIVIDER_WIDTH, m_subsettingWidget.position().y() - 70 - BUTTON_HEIGHT *( 3 ) );

    m_saveCombinationButton.setSize( m_combinationExpression.size().y(), m_combinationExpression.size().y() );
    m_saveCombinationButton.setPosition( DIVIDER_WIDTH + m_combinationExpression.size().x(), m_combinationExpression.position().y() );

    ////checkError( "checkpoint 2.6" );

    renderExpressionEdit( m_combinationExpression );

    ////checkError( "checkpoint 2.7" );

    renderCombinationList();

    ////checkError( "checkpoint 2.8" );

    renderEventList();

    ////checkError( "checkpoint 3" );

    /////////////////////////////////////////////////////////////////

    // Widget Labels

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Tool", m_toolCombo.position().x() - 40, m_toolCombo.position().y() + TEXT_PAD, 1.0, m_textColor );


    QVector4D labelColor( 0.94, 0.96, 1.0, .8 );
    Vec3< float > labelTextColor( 0, 0, 0 );
    float labelPad = TEXT_PAD - 3;
    float txtOff = 10;

    for( HelpLabelWidget * hlRef : m_helpLabelWidgetRefs )
    {
        glViewport(
            hlRef->position().x(),
            hlRef->position().y(),
            hlRef->size().x(),
            hlRef->size().y() );
        m_renderer->clear( labelColor, cartesianProg );
        m_renderer->renderPopSquare( simpleColorProg );
        glViewport( 0, 0, scaledWidth(), scaledHeight() );
        m_renderer->renderText(
            true,
            textProg,
            scaledWidth(),
            scaledHeight(),
            hlRef->text(),
            hlRef->position().x() + txtOff,
            hlRef->position().y() + labelPad, 1.0f, labelTextColor );

        // question mark background

        glViewport(
            hlRef->position().x() + hlRef->size().x() - BUTTON_HEIGHT - 3,
            hlRef->position().y(),
            BUTTON_HEIGHT,
            BUTTON_HEIGHT );

//        QVector4D hColor = hlRef->pointInViewPort(
//                               Vec2< float >( m_previousMousePosition.x(), scaledHeight() - m_previousMousePosition.y() ) ) ? QVector4D( 0, .7, .3, .7 )
//                           : QVector4D( 0, .5, .2, .2 );

//        Vec3< float > lColor = Vec3< float >( 1, 1, 1 );

//        if( hlRef->isPressed() )
//        {
//            glPointSize( BUTTON_HEIGHT - 6 );
//            m_renderer->renderPrimitivesFlat(
//                std::vector< Vec2< float > >( { Vec2< float >( 0, 0 ) } ),
//                cartesianProg,
//                QVector4D( .1, .1, .1, 0.7 ),
//                QMatrix4x4(),
//                GL_POINTS );

//            glPointSize( BUTTON_HEIGHT - 9 );
//            m_renderer->renderPrimitivesFlat(
//                std::vector< Vec2< float > >( { Vec2< float >( 0.08, -0.1 ) } ),
//                cartesianProg,
//                hColor,
//                QMatrix4x4(),
//                GL_POINTS );

//        }
//        else
//        {
//            glPointSize( BUTTON_HEIGHT - 6 );
//            m_renderer->renderPrimitivesFlat(
//                std::vector< Vec2< float > >( { Vec2< float >( 0, 0 ) } ),
//                cartesianProg,
//                hColor,
//                QMatrix4x4(),
//                GL_POINTS );
//        }

//        glViewport( 0, 0, scaledWidth(), scaledHeight() );
//        m_renderer->renderText(
//            true,
//            textProg,
//            scaledWidth(),
//            scaledHeight(),
//            "?",
//            hlRef->position().x() + hlRef->size().x() - BUTTON_HEIGHT + 4.5,
//            hlRef->position().y() + labelPad, 1.0f,
//            lColor );
    }

    /////////////////////////////////////////////////////////////

//    std::vector< Vec2< float > > line( 2, Vec2< float >( 0, 0 ) );

//    line[0].x(   0 );
//    line[1].x(   0 );
//    line[0].y(   1 );
//    line[1].y(  -1 );

//    glViewport(
//        m_subsettingWidget.position().x() + m_subsettingWidget.size().x() / 2.f - 1,
//        m_subsettingWidget.position().y() + m_subsettingWidget.size().y() - m_plotGridView.PANEL_HEIGHT + 2,
//        8,
//        m_plotGridView.PANEL_HEIGHT - 4
//    );
//    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 0, 0, 0, 0.2 ), QMatrix4x4(), GL_LINES );

//    glViewport(
//        m_subsettingWidget.position().x() + m_subsettingWidget.size().x() / 2.f,
//        m_subsettingWidget.position().y() + m_subsettingWidget.size().y() - m_plotGridView.PANEL_HEIGHT + 2,
//        8,
//        m_plotGridView.PANEL_HEIGHT - 4
//    );

//    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 1, 1, 1, 0.7 ), QMatrix4x4(), GL_LINES );

//    //

//    line[0].x(   -1 );
//    line[1].x(    1 );
//    line[0].y(    0 );
//    line[1].y(    0 );

//    glViewport(
//        m_subsettingWidget.position().x(),
//        scaledHeight() - MENU_HEIGHT * 2,
//        m_subsettingWidget.size().x(),
//        MENU_HEIGHT * 2
//    );
//    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 0, 0, 0, 0.2 ), QMatrix4x4(), GL_LINES );

//    glViewport(
//        m_subsettingWidget.position().x(),
//        scaledHeight() - MENU_HEIGHT * 2 - 1,
//        m_subsettingWidget.size().x(),
//        MENU_HEIGHT * 2
//    );

//    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 1, 1, 1, 0.7 ), QMatrix4x4(), GL_LINES );

//    //

//    line[0].x(   -1 );
//    line[1].x(    1 );
//    line[0].y(    0 );
//    line[1].y(    0 );

//    glViewport(
//        m_subsettingWidget.position().x(),
//        m_plotGridView.position().y() + m_plotGridView.size().y() + 1,
//        m_subsettingWidget.size().x(),
//        m_plotGridView.PANEL_HEIGHT * 2
//    );
//    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 0, 0, 0, 0.2 ), QMatrix4x4(), GL_LINES );

//    glViewport(
//        m_subsettingWidget.position().x(),
//        m_plotGridView.position().y() + m_plotGridView.size().y(),
//        m_subsettingWidget.size().x(),
//        m_plotGridView.PANEL_HEIGHT * 2
//    );

//    m_renderer->renderPrimitivesFlat( line, cartesianProg, QVector4D( 1, 1, 1, 0.7 ), QMatrix4x4(), GL_LINES );

    //
    m_renderer->renderPressButton( m_timeTextureSelector, cartesianProg, simpleColorProg );
//    m_renderer->renderPressButton( m_timeSubsetStatsSelector, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_timeSubsetCombStatsSelector, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_selectFromEventsButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_selectFromTrajectoriesButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_selectionRefinementButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_selectFocusButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_selectTintButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_selectionToSubsetButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton(   m_expandButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_reselectButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_plotAllSampleButton, cartesianProg, simpleColorProg );


    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Show All", m_plotAllSampleButton.position().x() + m_plotAllSampleButton.size().x() / 2.0 - 30, m_plotAllSampleButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    ///

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Expand", m_expandButton.position().x() + m_expandButton.size().x() / 2.0 - 20, m_expandButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Reselect", m_reselectButton.position().x() + m_reselectButton.size().x() / 2.0 - 20, m_reselectButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Select", m_selectFromEventsButton.position().x() - 65, m_selectFromEventsButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Events", m_selectFromEventsButton.position().x() +  m_selectFromEventsButton.size().x() / 2.0 - 18, m_selectFromEventsButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Trajectories", m_selectFromTrajectoriesButton.position().x() + m_selectFromTrajectoriesButton.size().x() / 2.0 - 40, m_selectFromTrajectoriesButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Refine", m_selectionRefinementButton.position().x() + m_selectionRefinementButton.size().x() / 2.0 - 20, m_selectionRefinementButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Focus", m_selectFocusButton.position().x() +  m_selectFocusButton.size().x() / 2.0 - 18, m_selectFocusButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Tint", m_selectTintButton.position().x() + m_selectTintButton.size().x() / 2.0 - 18, m_selectTintButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "To Subset", m_selectionToSubsetButton.position().x() + m_selectionToSubsetButton.size().x() / 2.0 - 38, m_selectionToSubsetButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Texture", m_timeTextureSelector.position().x() +  m_timeTextureSelector.size().x() / 2.0 - 18, m_timeTextureSelector.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "  Sum. Stat.", m_timeSubsetCombStatsSelector.position().x() + TEXT_PAD-4, m_timeSubsetCombStatsSelector.position().y() + TEXT_PAD, 1.0, m_textColor );

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Threshold",
//                            m_recurrenceView.epsilonSlider.position().x() - 84,
//                            m_recurrenceView.epsilonSlider.position().y() + TEXT_PAD,
//                            1.0,
//                            m_textColor );
//
//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Mode",
//                            m_recurrenceView.modeCombo.position().x() - 64,
//                            m_recurrenceView.modeCombo.position().y() + TEXT_PAD,
//                            1.0,
//                            m_textColor );

    //m_renderer->renderSlider( m_recurrenceView.epsilonSlider, cartesianProg, simpleColorProg );

    m_renderer->renderSlider( m_resolutionSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_thresholdSlider, cartesianProg, simpleColorProg );
    m_renderer->renderSlider( m_recurrenceSlider, cartesianProg, simpleColorProg );

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    // warning widget

    m_renderer->clearViewPort( m_warningWidget.viewPort(), cartesianProg );
    m_renderer->renderPopSquare( simpleColorProg );

    // Box Shadows

    m_renderer->renderViewPortBoxShadow( m_plotGridView.viewPort(), simpleColorProg );
    m_renderer->renderViewPortBoxShadow( m_selectionView.viewPort(), simpleColorProg );
//    m_renderer->renderViewPortBoxShadow( m_statsView.viewPort(), simpleColorProg );

    ViewPort vprt;
    vprt.setSize( m_timeLineWidget.size().x(), m_timeLineWidget.size().y() );
    vprt.offsetX( m_resizeWidgetA.position().x() + DIVIDER_WIDTH );
    vprt.offsetY( m_timeLineWidget.position().y() );
    m_renderer->renderViewPortBoxShadow( vprt, simpleColorProg );

    //////////

    m_renderer->renderResizeWidget( m_resizeWidgetA, cartesianProg );
    m_renderer->renderResizeWidget( m_resizeWidgetB, cartesianProg );
    m_renderer->renderResizeWidget( m_resizeWidgetD, cartesianProg );

    ////checkError( "checkpoint 4" );

    /////////

    // Buttons

    m_renderer->renderPressButton( m_showRecurrenceButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_recurrenceSettingsButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_thresholdRecurrenceButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_defineTimeSeriesButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_saveButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_definePlotButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_editTimePlotButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_editTimeSeriesButton, cartesianProg, simpleColorProg );

    if( m_presetSelected && m_datasetSelected && ! m_sessionInitiated && m_timeSeriesDefined )
    {
        m_renderer->renderPressButton( m_initiateSessionButton, cartesianProg, simpleColorProg );
    }

    m_renderer->renderPressButton( m_saveProjectButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_loadProjectFileButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_defineSubsetButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_defineFilterButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_editFilterButton, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_deletePhasePlotButton, cartesianProg, simpleColorProg );
    //m_renderer->renderPressButton( m_definePhasePlotButton, cartesianProg, simpleColorProg );
    m_renderer->renderPressButton( m_editPhasePlotButton, cartesianProg, simpleColorProg );

    m_renderer->renderTexturedPressButton( m_zoomInPhaseViewButton, ucProg, cartesianProg, simpleColorProg );
    m_renderer->renderTexturedPressButton( m_zoomOutPhaseViewButton, ucProg, cartesianProg, simpleColorProg );
    m_renderer->renderTexturedPressButton( m_gridOverlayButton, ucProg, cartesianProg, simpleColorProg );
    m_renderer->renderTexturedPressButton( m_definePhasePlotButton, ucProg, cartesianProg, simpleColorProg );

    m_renderer->renderPressButton( m_saveCombinationButton, cartesianProg, simpleColorProg );


    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    ///////////////

    // dividers

    //////////////// Text

    glViewport( 0, 0, scaledWidth(), scaledHeight() );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Recurrence", m_showRecurrenceButton.position().x() + m_showRecurrenceButton.size().x() / 2.0 - 40, m_showRecurrenceButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " Set.", m_recurrenceSettingsButton.position().x() + m_recurrenceSettingsButton.size().x() / 2.0 - 20, m_recurrenceSettingsButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Thresh.", m_thresholdRecurrenceButton.position().x() + m_thresholdRecurrenceButton.size().x() / 2.0 - 24, m_thresholdRecurrenceButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "resolution", m_resolutionSlider.position().x() - 80, m_resolutionSlider.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "threshold", m_thresholdSlider.position().x() - 80, m_thresholdSlider.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " + ", m_saveCombinationButton.position().x() + TEXT_PAD-4, m_saveCombinationButton.position().y() + TEXT_PAD, 1.0, m_textColor );

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            " + ", m_definePhasePlotButton.position().x() + TEXT_PAD-4, m_definePhasePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " - ", m_deletePhasePlotButton.position().x() + TEXT_PAD-4, m_deletePhasePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Plots", m_phasePlotCombo.position().x() - 92, m_phasePlotCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Interp.", m_phasePlotInterpolationCombo.position().x() - 66, m_phasePlotInterpolationCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Texture", m_phasePlotBlendingCombo.position().x() - 66, m_phasePlotBlendingCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Mode", m_phasePlotModeCombo.position().x() - 50, m_phasePlotModeCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Edit", m_editPhasePlotButton.position().x() + TEXT_PAD - 1, m_editPhasePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " + ", m_definePlotButton.position().x() + TEXT_PAD-4, m_definePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Edit", m_editTimePlotButton.position().x() + TEXT_PAD - 1, m_editTimePlotButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " + ", m_defineTimeSeriesButton.position().x() + TEXT_PAD-4, m_defineTimeSeriesButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Save", m_saveButton.position().x() + TEXT_PAD - 1, m_saveButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Edit", m_editTimeSeriesButton.position().x() + TEXT_PAD - 1, m_editTimeSeriesButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    if( m_presetSelected && m_datasetSelected && ! m_sessionInitiated && m_timeSeriesDefined  )
    {
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                "Begin", m_initiateSessionButton.position().x() + TEXT_PAD - 3, m_initiateSessionButton.position().y() + TEXT_PAD, 1.0, m_textColor );
    }

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Preset", m_configurationCombo.position().x() - 52, m_configurationCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Data Object ", m_particleCombo.position().x() - 90, m_particleCombo.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Time Series ", m_defineTimeSeriesButton.position().x() - 90, m_defineTimeSeriesButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Filtering ", m_defineFilterButton.position().x() - 64, m_defineFilterButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " + ", m_defineFilterButton.position().x() + TEXT_PAD-4, m_defineFilterButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Edit", m_editFilterButton.position().x() + TEXT_PAD - 1, m_editFilterButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            " Create Subset ", m_defineSubsetButton.position().x() + TEXT_PAD-4, m_defineSubsetButton.position().y() + TEXT_PAD, 1.0, m_textColor );

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Warnings ", m_warningWidget.position().x() - 74, m_warningWidget.position().y() + TEXT_PAD, 1.0, m_textColor );

    if( timeSeries != nullptr )
    {
        std::string wMssg = getWarningSummary( m_populationDataTypeEnum, ptype );
        m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                                wMssg, m_warningWidget.position().x() + TEXT_PAD - 1, m_warningWidget.position().y() + TEXT_PAD, 1.0,
                                ( wMssg[ 0 ] == 'D' ? Vec3< float >( .9, .2, 0 ) : Vec3< float >( .5, .5, .5 ) ) );
    }

    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
                            "Remove ", m_errorRemovalCombo.position().x() + TEXT_PAD - 1 - 68,
                            m_errorRemovalCombo.position().y() + TEXT_PAD, 1.0,
                            m_textColor );

//    m_renderer->renderText( false, textProg, scaledWidth(), scaledHeight(),
//                            "Advanced", m_inspectWarningsButton.position().x() + TEXT_PAD - 1,
//                            m_inspectWarningsButton.position().y() + TEXT_PAD, 1.0,
//                            m_textColor );

    // Combos
    for( auto & combo : m_comboRefs )
    {
        m_renderer->renderCombo( *combo, cartesianProg, simpleColorProg );
        renderComboItems( *combo );
    }

    ////checkError( "checkpoint 5" );
}

void BALEEN::keyPressEvent(QKeyEvent *e)
{
    if ( e->key() == Qt::Key_Q )
    {
        compileShaders();
    }

    static const std::set< char > VALID_SUBSET_EXPRESSION_TEXT =
    {
        '$','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '(', ')', '|', '&', '-','^', '!', ' '
    };

    if( m_combinationExpression.hasFocus() )
    {
        if( e->key() == Qt::Key_Left )
        {
            m_combinationExpression.decrementCursorPos();
        }
        else if( e->key() == Qt::Key_Right )
        {
            m_combinationExpression.incrementCursorPos();
        }
        else if( e->key() == Qt::Key_Backspace )
        {
            m_combinationExpression.deleteFromCursor();
        }
        else if( e->text().size() > 0 )
        {
            std::string str = e->text().toStdString();
            if( VALID_SUBSET_EXPRESSION_TEXT.count( str[ 0 ] ) )
            {
                m_combinationExpression.addFromCursor( str[ 0 ] );
            }
        }
    }

    renderLater();
}

void BALEEN::resizeEvent(QResizeEvent *e)
{
    float w = scaledWidth();
    float h = scaledHeight();

    QVector3D c1;
    QVector3D c2;
    QVector3D c3;
    QVector3D c4;

    c1.setX( -1.0 );
    c1.setY(  1.0 );
    c1.setZ(  0.0 );

    c2.setX( 1.0 );
    c2.setY( 1.0 );
    c2.setZ( 0.0 );

    c3.setX(  1.0 );
    c3.setY( -1.0 );
    c3.setZ(  0.0 );

    c4.setX( -1.0 );
    c4.setY( -1.0 );
    c4.setZ(  0.0 );

    camera.setSize( w, h );

    c1 = ( camera.proj()* camera.view() ).inverted() * c1;
    c2 = ( camera.proj()* camera.view() ).inverted() * c2;
    c3 = ( camera.proj()* camera.view() ).inverted() * c3;
    c4 = ( camera.proj()* camera.view() ).inverted() * c4;

    m_upperLeftWindowCornerWorldSpace  = Vec2< float >( c1.x(), c1.y() );
    m_upperRightWindowCornerWorldSpace = Vec2< float >( c2.x(), c2.y() );
    m_lowerRightWindowCornerWorldSpace = Vec2< float >( c3.x(), c3.y() );
    m_lowerLeftWindowCornerWorldSpace  = Vec2< float >( c4.x(), c4.y() );

    m_uiLayoutInitialized = false;
    recalculateUiLayout();

    renderLater();
}

void BALEEN::recalculateUiLayout()
{
    const int STATS_VIEW_HEIGHT = 300;

    /*
    ------------------------
    |    |          |      |
    |    |A         |B     |
    |    |          |      |
    |    |_______D_ |______|
    |    | ________________|
    ------------------------
    */

    // set default layout parameters at initialization
    if( ! m_uiLayoutInitialized )
    {
        float offsetA = DETAILVIEW_MIN_WIDTH;
        float offsetB = std::max( scaledWidth() - 2*offsetA - 2*DIVIDER_WIDTH, offsetA + SAMPLER_MIN_WIDTH + 1 );

        float offsetD = scaledHeight() / 6.0 + DIVIDER_WIDTH;

        m_resizeWidgetA.setPosition( DIVIDER_WIDTH + offsetA, 0 );
        m_resizeWidgetA.setSize( DIVIDER_WIDTH, scaledHeight() - MENU_HEIGHT );

        m_resizeWidgetB.setPosition( offsetB, offsetD + DIVIDER_WIDTH );
        m_resizeWidgetB.setSize( DIVIDER_WIDTH, scaledHeight() - MENU_HEIGHT - offsetD - DIVIDER_WIDTH );

        m_resizeWidgetD.setPosition( offsetA + DIVIDER_WIDTH*2, offsetD );
        m_resizeWidgetD.setSize( scaledWidth() - offsetA - DIVIDER_WIDTH * 2, DIVIDER_WIDTH );
    }

    m_uiLayoutEdited = false;

    const int RANGEVIEW_WIDTH = 310;
    const int TIME_PH = m_plotGridView.PANEL_HEIGHT;

    m_timeLineWidget.setPosition( m_resizeWidgetA.position().x() + DIVIDER_WIDTH, 0 );
    m_timeLineWidget.setSize( scaledWidth() - m_timeLineWidget.position().x() - DIVIDER_WIDTH*2 - RANGEVIEW_WIDTH, m_resizeWidgetD.position().y() );

    m_dataSetInfoWidget.setSize( m_resizeWidgetA.position().x(), TIME_PH - DIVIDER_WIDTH );
    m_dataSetInfoWidget.setPosition( 0, scaledHeight() - MENU_HEIGHT - m_dataSetInfoWidget.size().y() );

    m_selectionView.setSize(
        m_resizeWidgetA.position().x(),
        scaledHeight() - ( m_resizeWidgetD.position().y() + DIVIDER_WIDTH*2 + MENU_HEIGHT + m_dataSetInfoWidget.size().y() ) );

    m_selectionView.setPosition(
        0,
        m_resizeWidgetD.position().y() + DIVIDER_WIDTH );

    m_plotGridView.setSize(
        ( scaledWidth() - 310 - DIVIDER_WIDTH ) - m_resizeWidgetA.position().x() - DIVIDER_WIDTH*2,
        scaledHeight() - m_resizeWidgetD.position().y() - DIVIDER_WIDTH - MENU_HEIGHT /*- GROUP_WIDGET_HT*/ - m_plotGridView.PANEL_HEIGHT );

    m_plotGridView.setPosition(
        m_resizeWidgetA.position().x() + DIVIDER_WIDTH,
        m_resizeWidgetD.position().y() + DIVIDER_WIDTH );

    m_subsettingWidget.setSize( m_selectionView.size().x() - m_selectionView.SCROLL_BAR_HEIGHT, m_selectionView.size().y() / 2.0 );
    m_subsettingWidget.setPosition(
        m_selectionView.position().x(),
        m_selectionView.position().y() + m_selectionView.size().y() - m_subsettingWidget.size().y() );

    const int EDIT_WDTH = 40;

    int BTN_WDTH = ( m_selectionView.size().x() - MENU_SPACING * 3.0 ) / 2.0;
    const int LABEL_OFF1 = 130;
    int offS = LABEL_OFF1;

    //

    m_definePlotButton.setPosition( 0, m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - BUTTON_HEIGHT * 3  );
    m_definePlotButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_timePlotCombo.setPosition( m_definePlotButton.position().x() + BUTTON_HEIGHT, m_definePlotButton.position().y() );
    m_timePlotCombo.setSize( m_selectionView.size().x() - EDIT_WDTH - BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_editTimePlotButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );
    m_editTimePlotButton.setPosition( m_timePlotCombo.position().x() + m_timePlotCombo.size().x(), m_timePlotCombo.position().y() );

    m_timePlotModeCombo.setPosition( m_definePlotButton.position().x(), m_definePlotButton.position().y() - BUTTON_HEIGHT - MENU_SPACING );
    m_timePlotModeCombo.setSize(  m_selectionView.size().x(), BUTTON_HEIGHT );

    //

    float SW_WDT = std::floor( ( ( m_selectionView.viewPort().width() - m_definePlotButton.position().x() )
                                 - ( m_selectionView.viewPort().width() -  m_editTimePlotButton.position().x() - m_editTimePlotButton.size().x() ) ) / 3.0 );

    m_timeSubsetStatsSelector.setSize( SW_WDT, BUTTON_HEIGHT );
    m_timeSubsetStatsSelector.setPosition( std::floor( m_definePlotButton.position().x() ), m_timePlotCombo.position().y() - BUTTON_HEIGHT * 2 - MENU_SPACING*2 );

    m_timeSubsetCombStatsSelector.setSize( SW_WDT, BUTTON_HEIGHT );
    m_timeSubsetCombStatsSelector.setPosition( std::floor( m_definePlotButton.position().x() ) + m_timeSubsetStatsSelector.size().x(), m_timePlotCombo.position().y() - BUTTON_HEIGHT * 2 - MENU_SPACING*2 );

    m_timeTextureSelector.setSize( SW_WDT, BUTTON_HEIGHT );
    m_timeTextureSelector.setPosition( m_timeSubsetCombStatsSelector.position().x() + m_timeSubsetCombStatsSelector.size().x(), m_timePlotCombo.position().y() - BUTTON_HEIGHT * 2 - MENU_SPACING*2 );

    ////////////////////////////////////

    BTN_WDTH = scaledWidth() < 1901 ? 112 : 230;
    offS = 90;

    m_datasetCombo.setPosition( offS, scaledHeight() - BUTTON_HEIGHT - MENU_SPACING );
    m_datasetCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    offS += BTN_WDTH + MENU_SPACING + 40;
    offS += MENU_SPACING*2;

    m_configurationCombo.setPosition( offS, m_datasetCombo.position().y() );
    m_configurationCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    offS += BTN_WDTH + BUTTON_HEIGHT*2 + MENU_SPACING * 2 + 90;

    m_saveButton.setPosition( m_configurationCombo.position().x()+ m_configurationCombo.size().x(), m_datasetCombo.position().y() );
    m_saveButton.setSize( BUTTON_HEIGHT*2, BUTTON_HEIGHT );

    m_particleCombo.setPosition( offS, m_datasetCombo.position().y() );
    m_particleCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    offS += BTN_WDTH + MENU_SPACING * 2 + 90;

    m_defineTimeSeriesButton.setPosition( offS, m_datasetCombo.position().y());
    m_defineTimeSeriesButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_timeSeriesCombo.setPosition( offS + BUTTON_HEIGHT, m_defineTimeSeriesButton.position().y() );
    m_timeSeriesCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    m_editTimeSeriesButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );
    m_editTimeSeriesButton.setPosition( m_timeSeriesCombo.position().x() + m_timeSeriesCombo.size().x(), m_defineTimeSeriesButton.position().y() );

    //

    m_defineSubsetButton.setPosition(
        m_subsettingWidget.position().x() + DIVIDER_WIDTH,
        m_subsettingWidget.position().y() + m_subsettingWidget.size().y() - 60 );
    m_defineSubsetButton.setSize( 118, BUTTON_HEIGHT );

    m_subsetCombo.setPosition( m_defineSubsetButton.position().x() + BUTTON_HEIGHT, m_defineSubsetButton.position().y() );
    m_subsetCombo.setSize(
        m_subsettingWidget.size().x() - BUTTON_HEIGHT - EDIT_WDTH - 2*DIVIDER_WIDTH,
        BUTTON_HEIGHT );

    //

    m_defineFilterButton.setPosition( m_editTimeSeriesButton.position().x() + m_editTimeSeriesButton.size().x() + 80, m_datasetCombo.position().y());
    m_defineFilterButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    m_filterCombo.setPosition( m_defineFilterButton.position().x() + m_defineFilterButton.size().x(), m_defineFilterButton.position().y() );
    m_filterCombo.setSize( BTN_WDTH, BUTTON_HEIGHT );

    m_editFilterButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );
    m_editFilterButton.setPosition( m_filterCombo.position().x() + m_filterCombo.size().x(), m_defineFilterButton.position().y() );

    m_initiateSessionButton.setPosition( m_editFilterButton.size().x() + m_editFilterButton.position().x() + MENU_SPACING*2, scaledHeight() - BUTTON_HEIGHT - MENU_SPACING );
    m_initiateSessionButton.setSize( 50, BUTTON_HEIGHT );

    offS = m_plotGridView.viewPort().offsetX() + MENU_SPACING * 3 + 75 + 180;
    offS += 94 + MENU_SPACING * 3 + 90;

    m_closeButton.setPosition( scaledWidth() - BUTTON_HEIGHT - MENU_SPACING, m_datasetCombo.position().y() );
    m_closeButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

    //////////////////

    // Widget Labels / help buttons

    float labelMargin = 1;
    float hbH = m_definePlotButton.size().y();

    m_helpButtonTimeline.setPosition(
        labelMargin,
        m_timeLineWidget.position().y() + m_timeLineWidget.size().y() - m_definePlotButton.size().y() - labelMargin );
    m_helpButtonTimeline.setSize(
        296 + BUTTON_HEIGHT,
        hbH );

    m_helpButtonPlotGrid.setPosition(
        m_plotGridView.viewPort().offsetX() + labelMargin,
        m_plotGridView.viewPort().offsetY() + m_plotGridView.viewPort().height() - BUTTON_HEIGHT - labelMargin + m_plotGridView.PANEL_HEIGHT );
    m_helpButtonPlotGrid.setSize(
        110 + BUTTON_HEIGHT,
        hbH );

    m_helpButtonData.setPosition(
        labelMargin,
        m_datasetCombo.position().y() );

    m_helpButtonData.setSize(
        54 + BUTTON_HEIGHT,
        hbH );

    m_helpButtonSubsetting.setPosition(
        m_subsettingWidget.position().x(),
        m_subsettingWidget.position().y() + m_subsettingWidget.size().y() - hbH );

    m_helpButtonSubsetting.setSize(
        110 + BUTTON_HEIGHT,
        hbH );

    m_helpButtonSelection.setPosition(
        m_subsettingWidget.position().x(),
        m_subsettingWidget.position().y() - DIVIDER_WIDTH - hbH - BUTTON_HEIGHT * 2.5 );

    m_helpButtonSelection.setSize(
        130 + BUTTON_HEIGHT,
        hbH );

    m_helpButtonEvent.setPosition(
        m_subsettingWidget.position().x(),
        m_emissionEventButton.position().y() + BUTTON_HEIGHT * 2.5 );

    m_helpButtonEvent.setSize(
        130 + BUTTON_HEIGHT,
        hbH );

//

    // Warning Widget

    m_errorRemovalCombo.setSize( 120, m_datasetCombo.size().y() );
    m_inspectWarningsButton.setSize( 80, m_datasetCombo.size().y() );

    m_warningWidget.setSize( 354,
                             m_datasetCombo.size().y() );

    m_warningWidget.setPosition(
        scaledWidth() - m_warningWidget.size().x() /*- m_inspectWarningsButton.size().x()*/ - m_errorRemovalCombo.size().x() - 74,
        m_datasetCombo.position().y() );

    m_inspectWarningsButton.setPosition(
        m_warningWidget.position().x() + m_warningWidget.size().x(),
        m_datasetCombo.position().y() );

    m_errorRemovalCombo.setPosition( m_inspectWarningsButton.position().x() /*+ m_inspectWarningsButton.size().x() */ + 74, m_datasetCombo.position().y() );

    //

//    m_statsView.setPosition( m_resizeWidgetB.position().x() + DIVIDER_WIDTH, m_resizeWidgetD.position().y() + DIVIDER_WIDTH + leftOver );
//    m_statsView.setSize( scaledWidth() - ( m_resizeWidgetB.position().x() + DIVIDER_WIDTH ),
    /*m_plotGridView.viewPort().height() + m_plotGridView.PANEL_HEIGHT*/ //STATS_VIEW_HEIGHT );
    float RViewXSZ = scaledWidth() - ( m_resizeWidgetB.position().x() + DIVIDER_WIDTH );

    ////////////////////////////////////////////////////

    // trajectory view

    m_trajectoryView.setSize( RANGEVIEW_WIDTH, 400 );
    m_trajectoryView.setPosition( scaledWidth() - RANGEVIEW_WIDTH - DIVIDER_WIDTH, 0  );
    //range view

    m_rangeView.setSize( RANGEVIEW_WIDTH, m_plotGridView.size().y() + m_plotGridView.position().y() - m_trajectoryView.size().y() - DIVIDER_WIDTH );
    m_rangeView.setPosition( scaledWidth() - RANGEVIEW_WIDTH - DIVIDER_WIDTH,
                             m_trajectoryView.position().y() + m_trajectoryView.size().y() + DIVIDER_WIDTH );

    // recurrence view

//    m_recurrenceView.setSize( RViewXSZ - RANGEVIEW_WIDTH - DIVIDER_WIDTH, m_plotGridView.size().y() + TIME_PH );
//    m_recurrenceView.setPosition( m_resizeWidgetB.position().x() + DIVIDER_WIDTH*2 + m_rangeView.size().x(),
//                                  m_plotGridView.position().y()   );

//    float WWD3 = ( m_recurrenceView.size().x() - 170 ) / 2.0 - MENU_SPACING*3;
//    m_recurrenceView.epsilonSlider.setSize( WWD3, BUTTON_HEIGHT );
//    m_recurrenceView.epsilonSlider.setPosition(
//        m_recurrenceView.position().x() + 100,
//        m_recurrenceView.position().y() + m_recurrenceView.size().y() - 60
//    );

//    m_recurrenceView.modeCombo.setSize( WWD3, BUTTON_HEIGHT );
//    m_recurrenceView.modeCombo.setPosition(
//        m_recurrenceView.position().x() + m_recurrenceView.size().x() - m_recurrenceView.epsilonSlider.size().x() - MENU_SPACING,
//        m_recurrenceView.position().y() + m_recurrenceView.size().y() - 60
//    );

//    m_helpButtonReccu.setPosition(
//        m_recurrenceView.position().x()+1,
//        m_recurrenceView.position().y() + m_recurrenceView.size().y() - hbH );

//    m_helpButtonReccu.setSize(
//        140 + BUTTON_HEIGHT,
//        hbH );

    m_helpButtonTraje.setPosition(
        m_trajectoryView.position().x()+1,
        m_trajectoryView.position().y() + m_trajectoryView.size().y() - hbH - 1 );

    m_helpButtonTraje.setSize(
        154 + BUTTON_HEIGHT,
        hbH );

    m_plotAllSampleButton.setPosition( m_helpButtonTraje.position().x() + m_helpButtonTraje.size().x() + 10, m_helpButtonTraje.position().y() );
    m_plotAllSampleButton.setSize( 70, BUTTON_HEIGHT );

    m_helpButtonRange.setPosition(
        m_rangeView.position().x()+1,
        m_rangeView.position().y() + m_rangeView.size().y() - hbH - 1 + TIME_PH );

    m_helpButtonRange.setSize(
        180 + BUTTON_HEIGHT,
        hbH );

    ////////////////////////////////////////////////////

//    m_helpButtonStatsView.setPosition(
//        m_statsView.position().x(),
//        m_statsView.position().y() + m_statsView.size().y() - hbH );

//    m_helpButtonStatsView.setSize(
//        90 + BUTTON_HEIGHT,
//        hbH );

    float yPosition = m_plotGridView.position().y() + m_plotGridView.size().y() + m_plotGridView.PANEL_HEIGHT - BUTTON_HEIGHT*2 - MENU_SPACING*2;

//    m_definePhasePlotButton.setPosition( m_plotGridView.position().x() + 48,
//                                         m_plotGridView.position().y() + m_plotGridView.size().y() + m_plotGridView.PANEL_HEIGHT - BUTTON_HEIGHT*2 - MENU_SPACING*2 );

//    m_deletePhasePlotButton.setPosition( m_definePhasePlotButton.position().x() + BUTTON_HEIGHT,
//                                         m_definePhasePlotButton.position().y() );

    /*    m_deletePhasePlotButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );
        m_definePhasePlotButton.setSize( BUTTON_HEIGHT, BUTTON_HEIGHT );

        m_phasePlotCombo.setPosition(
            m_deletePhasePlotButton.position().x() + BUTTON_HEIGHT,
            m_deletePhasePlotButton.position().y() )*/;

//    m_editPhasePlotButton.setPosition( m_phasePlotCombo.position().x() + m_phasePlotCombo.size().x(),
//                                       yPosition );

//    m_editPhasePlotButton.setSize( EDIT_WDTH, BUTTON_HEIGHT );

    m_phasePlotModeCombo.setPosition( m_plotGridView.position().x() + 80 + EDIT_WDTH + 100,  m_plotGridView.position().y() + m_plotGridView.size().y() + m_plotGridView.PANEL_HEIGHT - BUTTON_HEIGHT - MENU_SPACING  );
    m_phasePlotModeCombo.setSize( 180, BUTTON_HEIGHT );

    m_phasePlotInterpolationCombo.setPosition(  m_phasePlotModeCombo.position().x() + m_phasePlotModeCombo.size().x()*2 + LABEL_OFF1*2 -EDIT_WDTH, m_phasePlotModeCombo.position().y() );
    m_phasePlotInterpolationCombo.setSize( 180, BUTTON_HEIGHT );

    m_phasePlotBlendingCombo.setPosition( m_phasePlotModeCombo.position().x(), yPosition );
    m_phasePlotBlendingCombo.setSize( 180, BUTTON_HEIGHT );

    m_selectTintButton.setPosition( m_phasePlotInterpolationCombo.position().x(), yPosition );
    m_selectTintButton.setSize( 90, BUTTON_HEIGHT );

    m_selectFocusButton.setPosition( m_phasePlotInterpolationCombo.position().x() + 90, yPosition );
    m_selectFocusButton.setSize( 90, BUTTON_HEIGHT );

    //

    m_selectFromEventsButton.setPosition( m_phasePlotInterpolationCombo.position().x() + m_phasePlotInterpolationCombo.size().x() + 100, m_phasePlotModeCombo.position().y() );
    m_selectFromEventsButton.setSize( 90, BUTTON_HEIGHT );

    m_selectFromTrajectoriesButton.setPosition( m_selectFromEventsButton.position().x() + m_selectFromEventsButton.size().x(), m_selectFromEventsButton.position().y() );
    m_selectFromTrajectoriesButton.setSize( 90, BUTTON_HEIGHT );

    m_selectionRefinementButton.setPosition( m_selectFromEventsButton.position().x(), m_phasePlotBlendingCombo.position().y() );
    m_selectionRefinementButton.setSize( 90, BUTTON_HEIGHT );

    m_expandButton.setPosition( m_selectionRefinementButton.position().x() + m_selectionRefinementButton.size().x(), m_selectionRefinementButton.position().y() );
    m_expandButton.setSize( 90, BUTTON_HEIGHT );

    m_reselectButton.setPosition( m_expandButton.position().x() + m_expandButton.size().x(), m_selectionRefinementButton.position().y() );
    m_reselectButton.setSize( 90, BUTTON_HEIGHT );

    m_selectionToSubsetButton.setPosition( m_selectFromTrajectoriesButton.position().x() + m_selectFromTrajectoriesButton.size().x() + 20,
                                           m_selectFromTrajectoriesButton.position().y() );
    m_selectionToSubsetButton.setSize( 90, BUTTON_HEIGHT );

    m_toolCombo.setPosition(  m_plotGridView.position().x() + 40,
                             m_reselectButton.position().y() );
    m_toolCombo.setSize( 100, BUTTON_HEIGHT );

    //

//    m_phasePlotCombo.setSize( m_phasePlotModeCombo.position().x() + m_phasePlotModeCombo.size().x() - m_phasePlotCombo.position().x() - EDIT_WDTH, BUTTON_HEIGHT );

    const float ZOOM_SIZE = 48;

    m_zoomInPhaseViewButton.setSize(  ZOOM_SIZE, ZOOM_SIZE );
    m_zoomOutPhaseViewButton.setSize( ZOOM_SIZE, ZOOM_SIZE );
    m_gridOverlayButton.setSize( ZOOM_SIZE, ZOOM_SIZE );
    m_definePhasePlotButton.setSize( ZOOM_SIZE, ZOOM_SIZE );

    m_zoomOutPhaseViewButton.setPosition(
        m_plotGridView.position().x() + m_plotGridView.size().x() - ZOOM_SIZE * 2 - DIVIDER_WIDTH,
        yPosition );

    m_zoomInPhaseViewButton.setPosition(
        m_zoomOutPhaseViewButton.position().x() + ZOOM_SIZE + DIVIDER_WIDTH,
        yPosition );

    m_gridOverlayButton.setPosition(
        m_plotGridView.position().x() + m_plotGridView.size().x() - ZOOM_SIZE*3 - DIVIDER_WIDTH*2,
        yPosition );

    m_definePhasePlotButton.setPosition(
        m_plotGridView.position().x() + m_plotGridView.size().x() - ZOOM_SIZE*4 - DIVIDER_WIDTH*3,
        yPosition );

    ////////////////////////////

    m_resolutionSlider.setPosition( m_phasePlotModeCombo.position().x() + m_phasePlotModeCombo.size().x() + 100, m_phasePlotInterpolationCombo.position().y() );
    m_resolutionSlider.setSize( 200, BUTTON_HEIGHT );

    m_thresholdSlider.setPosition( m_resolutionSlider.position().x(),  m_toolCombo.position().y() );
    m_thresholdSlider.setSize( 200, BUTTON_HEIGHT );


    m_showRecurrenceButton.setPosition( m_selectionToSubsetButton.position().x() + m_selectionToSubsetButton.size().x() + DIVIDER_WIDTH,  m_resolutionSlider.position().y() );
    m_showRecurrenceButton.setSize( 140, BUTTON_HEIGHT );

    m_recurrenceSettingsButton.setPosition( m_showRecurrenceButton.position().x() + m_showRecurrenceButton.size().x(),  m_resolutionSlider.position().y() );
    m_recurrenceSettingsButton.setSize( 60, BUTTON_HEIGHT );

    m_thresholdRecurrenceButton.setPosition( m_showRecurrenceButton.position().x(),  m_thresholdSlider.position().y() );
    m_thresholdRecurrenceButton.setSize( 60, BUTTON_HEIGHT );

    m_recurrenceSlider.setPosition( m_thresholdRecurrenceButton.position().x() + m_thresholdRecurrenceButton.size().x() + DIVIDER_WIDTH,  m_thresholdSlider.position().y() );
    m_recurrenceSlider.setSize( 140-DIVIDER_WIDTH, BUTTON_HEIGHT );
}

}
