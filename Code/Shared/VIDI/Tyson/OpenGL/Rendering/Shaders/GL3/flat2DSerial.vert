#version 130



in float xAttr;
in float yAttr;

uniform mat4 MVP;

void main(void)
{
    gl_Position = MVP * vec4(
        xAttr,
        yAttr,
        0.0,
        1.0
    );
}
