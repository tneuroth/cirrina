
#ifndef TN_COMPUTE_STATS_HPP
#define TN_COMPUTE_STATS_HPP

#include "../Types/Vec.hpp"

#include <vector>
#include <string>

#include <GL/glew.h>
#include <nvrtc.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cudaGL.h>

class StatsComputer
{
    bool m_useGLInterop;

    CUdeviceptr m_timeAveragedHistograms;
    CUdeviceptr m_currentHistograms;
    CUdeviceptr m_sums;
    CUdeviceptr m_counts;

    CUdeviceptr m_buffer;
    CUdeviceptr m_ranges;
    CUdeviceptr m_bitmasks;

    CUdevice   m_cuDevice;
    CUcontext  m_context;
    CUmodule   m_module;
    CUmodule   m_module2;

    CUfunction m_timeAverageHistogramKernel;
    CUfunction m_currentHistogramKernel;
    CUfunction m_sumAndCountKernel;
    CUfunction m_absoluteSumAndCountKernel;
    CUfunction m_radialSelectionKernel;
    CUfunction m_combinationKernel; 
    CUfunction m_divideKernel;
    CUfunction m_RecurrenceHotSpotKernel;

    std::string template_kernel;
    std::string template_kernel2;

    CUgraphicsResource * m_glResources;
    int m_nResources;

    void clean();

public :

    StatsComputer();

    void cleanMappedBuffers();

    void mapBuffer(
        GLuint buffer,
        const size_t N_STEPS,
        const size_t N_PARTICLES,
        const size_t N_VARS );

    void allocateRanges(
        const size_t N_VARS );

    void bufferRanges(
        float * ranges,
        const size_t N_VARS );

    void bufferData(
        float * data,
        const size_t N );

    void bufferBitmasks(
        int * bitmasks,
        const size_t N_PARTICLES,
        const size_t N_STEPS );

    void allocateResultArray(
        const size_t N_STEPS,
        const size_t N_VARS,
        const size_t N_BINS );

    void cleanResultArray();
    void cleanBuffers();

    void setCombination(
        int N_STEPS,
        int N_PARTICLES,
        std::vector< int > & result );

    void setCombination( 
        int N_STEPS, 
        int N_PARTICLES );

    void computeRadialSelection(
        float radiusX,
        float radiusY,
        float centerX,
        float centerY,
        unsigned long N_PARTICLES,
        unsigned long N_STEPS,
        unsigned  int V1_IDX,
        unsigned  int V2_IDX );

    void computeRecurrenceHotSpots(
        unsigned long N_PARTICLES,
        unsigned long N_STEPS,
        int weightIndex,
        std::vector< int > & phaseIndexes,
        std::vector< float > & phaseScaling,
        std::vector< int > & distType,
        std::pair< int, int > & heatmapSpace,
        std::pair< int, int > & heatmapDims,
        std::pair< std::pair< float, float >, std::pair< float, float > > & heatmapRanges,
        std::vector< float > & resultsA,
        std::vector< float > & resultsC );

    void getSeriesStats( 
        unsigned long N_STEPS, 
        std::vector<float> & results );

    void computeTimeSeriesSumsAndCounts(
        unsigned long N_PARTICLES,
        unsigned long N_STEPS,     
        unsigned int V_IDX,
        bool absolute );

    void computeCurrentHistograms(
        unsigned long N_PARTICLES,
        unsigned long N_STEPS,
        unsigned long STEP,
        unsigned int N_VARS,
        unsigned int N_BINS,
        std::vector< float > & result,
        bool probability = false );

    void resizeBuffer(
        const size_t NUM_PARTICLES,
        const size_t N_STEPS,
        const size_t N_VARS );

    void compileProgram( const std::string & expression );
    void initRuntime();

    //////////////////////////////////////////////////////
    // CPU versions for benchmarking



    ///////////////////////////////////////////////////////

    ~StatsComputer();
};

#endif
