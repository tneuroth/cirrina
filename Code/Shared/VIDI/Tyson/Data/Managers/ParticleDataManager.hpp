
#ifndef TN_PARTICLE_DATA_MANAGER_HPP
#define TN_PARTICLE_DATA_MANAGER_HPP

#include "Types/Vec.hpp"
#include "Data/Importers/ParticleDataImporter.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Data/Definitions/ParticleFilter.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"
#include "Data/Interpolators/FieldInterpolator.h"

#include "Data/Definitions/SubsetDefinition.hpp"

#include <QDebug>

#include <omp.h>

#include <chrono>
#include <vector>
#include <map>
#include <cstdint>
#include <memory>
#include <unordered_set>
#include <atomic>

namespace TN {

template< typename TargetFloatType >
class ParticleDataManager{


    std::vector< std::string > m_defaultSubsetNames;
    std::vector<  int > m_subsetFlagsTimeVarying;

    std::map< std::string, std::map< std::string, LookupTexture< TargetFloatType > > > m_lookUpTextures;
    TimeSeriesDefinition m_fullTimeSeries;
    TimeSeriesDefinition m_currentTimeSeries;
    std::map< std::string, std::size_t > m_numParticles;

    std::map< std::string, std::map< std::string, BaseVariable > > m_baseVariables;
    std::map< std::string, std::map< std::string, DerivedVariable > > m_derivedVariables;

    std::map< std::string, CustomConstant > m_customConstants;

    std::map< std::string, std::vector< uint8_t > > m_filtered;
    std::map< std::string, std::vector< uint64_t > > m_filterIndices;

    std::map< std::string, ParticleFilter > m_filters;

    std::map< std::string, std::set< std::string > > m_storedVariables;

    std::map< std::string, std::map< std::string, std::uint8_t > > m_storedVariableErrorState;
    bool m_hasZeros;
    std::uint8_t m_errorRemovalOption;
    std::vector< size_t > m_errorFilterIndices;
    bool m_errorIndicesAreRemoved;

    ExpressionWrapper m_expressionCalculator;

    std::set< std::string > m_particleTypes;

    public :

    enum {
        REMOVE_NAN = 1,
        REMOVE_INF = 2,
        REMOVE_ZEROS = 4,
        REMOVE_UNEXPECTED_VALUES = 8
    };

    enum {
        HAS_NAN = 1,
        HAS_INF = 2,
        HAS_UNEXPECTED = 4
    };

    std::size_t numLoadedParticles( const std::string & ptype ) const;
    std::size_t numParticles( const std::string & ptype ) const;

    void updateDerivations( const std::string & ptype, const std::map< std::string, DerivedVariable > & derived );
    void updateCustomConstants( const std::map< std::string, CustomConstant > & cc );

    // the only thing that can have changed is the user specified descriptions
    void updateBaseConstants( const std::map< std::string, BaseConstant > & bc );

    // the only thing that can have changed is the user specified descriptions
    void updateBaseVariables( const std::string & ptype, const std::map< std::string, BaseVariable > & bv );

private:

    ///////////////////////////////////////////////////////

     /** The main data structure through which the Particle data is stored/accessed.
        The first dimension corresponds to the different types of particles available.
        The second dimension corresoponds to the different time variant attributes.
        The third dimensions corresponds to the available time steps.
        The fourth corresponds to the per-particle instance values of the attribute
        The unique_ptr<  > will be equal to nullptr unless loaded in memory */
     std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > m_particleAndTimeVariantAttributes;

     std::map< std::string, std::vector< TargetFloatType > > m_particleVariantAttributes;

     std::map< std::string, std::map< std::string, Vec2< double > > > m_variantAttributeRanges;

     std::map< std::string, BaseConstant > m_baseConstants;

     std::vector< std::int32_t > m_simulationTimeSteps;
     std::vector< double > m_realTimeValues;
     std::int32_t m_numTimeSteps;
     ParticleDataImporter< TargetFloatType > *m_dataImporter;

     std::map< std::string, VisualContextModel< TargetFloatType > > m_visualContextModels;

     void release( const std::string & ptype );

     void release(
         const std::vector< std::int32_t > & iSteps, const std::map< std::string, std::set< std::string > > & toRelease );

     void release( const std::vector< std::int32_t > & iSteps );

     void release();

 public:

     ParticleDataManager();
     ~ParticleDataManager();

     void reset( const BasicDataInfo & basicDataInfo, ParticleDataImporter< TargetFloatType > * dataImporter );

     /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     // compute filter based on given expression
     void computeFilter( const std::string & ptype, const std::set< std::string > & toRetain, std::atomic< int > & filterProgress );
     void applyFilter( int step, const std::string & ptype, const std::string & var );
     void applyErrorFilter( int step, const std::string & ptype, const std::string & var );
     void computeSubsets( const std::string & ptype, std::vector< SubsetDefinition > & subsetDefinitions );

     void computeCombination( const std::string & ptype, const std::string & expression );

     void initializeSubsets( const std::vector< std::vector< std::int32_t > > & masks );

     void load(
             const std::string & ptype,
             const std::set< std::string > & variables,
             const TimeSeriesDefinition & timeSeries,
             bool invalidateMemory,
             bool updateFilter,
             std::atomic< int > & filterProgress,
             std::atomic< int > & derivationPrepareProgress,
             std::atomic< int > & derivationCalculateProgress,
             std::atomic< int > & baseProgress,
             std::atomic< int > & NaNCheckProgress,
             std::atomic< int > & InfCheckProgress,
             std::atomic< int > & ZeroCheckProgress,
             std::atomic< int > & ErrorRemoveProgress,
             std::atomic< int > & rangeProgress,
             std::atomic< bool > & status );

     std::uint8_t getStoredGlobalWarningState( const std::string & ptype ) const;

     ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     void loadAll(
         bool releaseLast = true );
     const std::unique_ptr< std::vector< TargetFloatType > > & values( std::int32_t timeStep, const std::string & particleType, const std::string & attribute ) const;
     const std::vector< std::unique_ptr< std::vector< TargetFloatType > > > & values( const std::string & particleType, const std::string & attribute ) const;
     const std::map< std::string, std::map< std::string, Vec2< double > > > & attributeRanges() const;
     Vec2< double > variableRange( const std::string & ptype, const std::string & key ) const;
     const std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes() const;
     std::set< std::string > attributeKeys( const std::string & ptype ) const;
     const std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels() const;
     const std::map< std::string, std::map< std::string, BaseVariable > > & baseVariables() const;
     const std::map< std::string, CustomConstant > & customConstants() const;
     const std::map< std::string, BaseConstant > & baseConstants() const;
     const std::map< std::string, std::map< std::string, DerivedVariable > > & derivedVariables() const;
     std::size_t numTimeSteps() const;
     std::size_t numLoadedTimeSteps() const;
     TimeSeriesDefinition currentTimeSeries() const;
     TimeSeriesDefinition fullTimeSeries() const;
     const std::vector< double > & realTime() const;
     const std::vector< std::int32_t > & simTime() const;

     void setFilter( const std::string & ptype, const ParticleFilter & filter );
     void setErrorRemovalOption( std::uint8_t errorRemovalOption );
     bool getHasZeros( const std::string & ptype ) const;

     std::vector< std::string > getDefaultSubsetNames() const
     {
         return m_defaultSubsetNames;
     }

     std::set< std::string > particleTypes() const;

     const std::vector<  int > & subsetMasks() const;

     void clear();
};

}

#endif // PARTICLEDATAMANAGER_HPP
