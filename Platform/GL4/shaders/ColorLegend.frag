#version 130



in  vec2 uv;
uniform sampler1D tex;

void main(){
    gl_FragColor = texture( tex, uv.s );
}
