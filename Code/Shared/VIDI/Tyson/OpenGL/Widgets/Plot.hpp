


#ifndef PLOT_HPP
#define PLOT_HPP

#include "OpenGL/Widgets/Widget.hpp"

#include <string>
#include <QGLWidget>

namespace TN
{

// plots will not hold data nor point to data, only the variable identifiers
// or keys in data map...
// rendering is to be done externally

class Plot : public Widget
{

public:

    enum PlotScale
    {
        LOG_10_SCALE,
        LOG_E_SCALE,
        LINEAR_SCALE,
    };

    enum PlotType
    {
        PLOT2D,
        PLOT3D
    };

protected:

    PlotType m_plotType;
    std::string m_title;

public:

    const std::string & title()
    {
        return m_title;
    }
    void title( const std::string & t )
    {
        m_title = t;
    }
    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }
    PlotType plotType() const
    {
        return m_plotType;
    }
    void plotType( PlotType t )
    {
        m_plotType = t;
    }
};

}

#endif // PLOT_HPP

