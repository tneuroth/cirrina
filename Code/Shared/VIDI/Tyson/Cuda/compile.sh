nvcc -std=c++11 -O3  -m 64 \
-gencode=arch=compute_20,code=sm_20 \
-gencode=arch=compute_30,code=sm_30 \
-gencode=arch=compute_35,code=sm_35 \
-gencode=arch=compute_35,code=compute_35 \
-gencode=arch=compute_50,code=sm_50 \
--ptxas-options=-v --compiler-options '-fPIC' -o libhistiso.so --shared HistogramStackIso.cu

g++ -std=c++11 -O3 \
-fpic -o libpstats.so -shared ComputeStatistics.cpp \
-I /media/dev/deploy/baleen/Code/Shared/VIDI/Tyson/Expressions \
-I /usr/local/cuda-7.0/include \
-L /usr/local/cuda-7.0/lib64 -lnvrtc -lcuda \
-Wl,-rpath,/usr/local/cuda-7.0/lib64 

