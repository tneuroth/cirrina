#define COM_MASK 2

extern "C" __global__
void setCombination( int * b, size_t N )
{
    const size_t IDX = blockIdx.x * blockDim.x + threadIdx.x;

    if( IDX >= N )
    {
        return;
    }

    const int x = b[ IDX ] & ~COM_MASK;
    const int c = b[ IDX ];
    b[ IDX ] = x | int( @ ) * COM_MASK;
}
