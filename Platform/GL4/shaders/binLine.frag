#version 400



layout(location = 0) out float freq;

const float unit = 1.0 / 1048576.0;

in float d;

void main(void)
{
    float normalization = 1;
    if( d >= 1 ) {
        normalization = d;
    }
    freq = unit / normalization;
}
