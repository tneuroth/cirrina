
#ifndef TN_SLAC_IMPORTER_HPP
#define TN_SLAC_IMPORTER_HPP

#include "Types/Vec.hpp"
#include "Data/Interpolators/FieldInterpolator.h"
#include "../ParticleDataImporter.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Algorithms/Standard/MyAlgorithms.hpp"

#include <QDebug>
#include <fstream>
#include <map>
#include <string>
#include <vector>
#include <memory>
#include <limits>

namespace TN
{

template< typename TargetFloatType >

class SLACImporter : public ParticleDataImporter< TargetFloatType >
{
    typedef float OrigionalFloatType;

    std::vector< OrigionalFloatType > m_swap;
    std::vector< std::int32_t > m_simulationTimeSteps;
    size_t m_numParticles;
    std::vector< std::string > m_defaultSubsets;
    std::vector< int32_t > m_staticSubsetMasks;

public :

    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        this->m_basicDataInfo = basicDataInfo;
        this->m_initialized = true;
        return true;
    }
    void getMagnitudes(
        const std::vector< float > & x,
        const std::vector< float > & y,
        const std::vector< float > & z,
        std::vector< float > & result )
    {
        const size_t N = x.size();
        #pragma omp parallel for
        for( size_t i = 0; i < N; ++i )
        {
            result[ i ] = std::sqrt( x[ i ] * x [ i ] + y[ i ] * y[ i ] + z[ i ] * z[ i ] );
        }
    }

    void getMagnitudes( const std::vector< float > & x, const std::vector< float > & y, std::vector< float > & result )
    {
        const size_t N = x.size();
        #pragma omp parallel for
        for( size_t i = 0; i < N; ++i )
        {
            result[ i ] = std::sqrt( x[ i ] * x [ i ] + y[ i ] * y[ i ] );
        }
    }

    void loadTimeSteps(
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime )
    {
        for( int i = 0; i < 4993; ++i )
        {
            simulationTimeSteps.push_back( i );
            realTime.push_back( i );
        }
    }

    void computeStaticSubsetFlags()
    {
        m_staticSubsetMasks = std::vector< int >( m_numParticles, 0 );
        const int NT = m_simulationTimeSteps.size();

        std::vector< std::uint8_t > mapped( m_numParticles, 0 );
        qDebug() << "computing " << m_numParticles;

        for( int t = 0; t < NT; ++t )
        {
            std::vector< float > zt(   m_numParticles );
            std::vector< int > exists( m_numParticles );

            readStep( zt, "z", m_numParticles, m_simulationTimeSteps[ t ] );
            readStep( exists, "exists", m_numParticles, m_simulationTimeSteps[ t ] );

            for( int p = 0; p < m_numParticles; ++p )
            {
                if( exists[ p ] && ! mapped[ p ] )
                {
                    float z = zt[ p ] - 4.15;

                    if( z >= -.02 && z < 0.0591 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 4 ) );
                    }
                    else if ( z >= 0.0591 && z < .17461 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 5 ) );
                    }
                    else if ( z >= .17461 && z < .29246 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 6 ) );
                    }
                    else if ( z >= 0.29246 && z < .40568 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 7 ) );
                    }
                    else if ( z >= .40568 && z < .52092 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 8 ) );
                    }
                    else if ( z >= 0.52092 && z < .63850 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 9 ) );
                    }
                    else if ( z >= .29246 && z < .75128 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 10 ) );
                    }
                    else if ( z >= 0.75128 && z < .87071 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 11 ) );
                    }
                    else if ( z >= .87071 && z < .9802 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 12 ) );
                    }
                    else if ( z >= 0.9802 && z < 1.06133 ) {
                        m_staticSubsetMasks[ p ] = int( pow( 2, 13 ) );
                    }
                    mapped[ p ] = 1;
                }
            }
        }
    }

    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes,
        std::map< std::string, std::map< std::string, Vec2< double > > > & variantAttributeRanges,
        std::map< std::string, std::vector< TargetFloatType > >  & particleVariantAttributes,
        std::map< std::string, BaseConstant > & constants,
        std::map< std::string, std::map< std::string, DerivedVariable > > & derivedVariables,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::map< std::string, std::map< std::string, LookupTexture< TargetFloatType > > > & lookupTextures,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        std::int32_t & numTimeSteps,
        std::map< std::string, std::size_t > & numParticles )
    {
        loadTimeSteps( simulationTimeSteps, realTime );
        m_simulationTimeSteps = simulationTimeSteps;
        numTimeSteps = 4993;

        std::vector< std::string > attributes = { "x", "y", "z", "r", "mx", "my", "mz", "mr", "m" };
        std::string ptype = "electrons";

        particleAndTimeVariantAttributes.insert(
            std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                ptype,
                std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > () ) );

        variantAttributeRanges.insert(
            std::pair< std::string, std::map< std::string, Vec2< double > > > (
                ptype,
                std::map< std::string, Vec2< double > >( ) ) );

        derivedVariables.insert(
            std::pair< std::string, std::map< std::string, DerivedVariable > > (
                ptype,
                std::map< std::string, DerivedVariable >( ) ) );

        m_numParticles = 15139;
        numParticles.insert( { "electrons", m_numParticles } );

        for( auto & attr : attributes )
        {
            particleAndTimeVariantAttributes.at( ptype ).insert(
                std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                    attr,
                    std::vector< std::unique_ptr< std::vector< TargetFloatType > > > () ) );

            variantAttributeRanges.at( ptype ).insert( { attr, Vec2< double >() } );

            for( int t = 0; t < numTimeSteps; ++t )
            {
                particleAndTimeVariantAttributes.at( ptype ).at( attr ).push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
            }
        }
        computeStaticSubsetFlags();
    }

    template< typename T >
    void readStep( std::vector< T > & result, const std::string & var, size_t NP, size_t t )
    {
        std::string tstr = std::to_string( t );
        std::ifstream infile(
            this->m_basicDataInfo.location + "/ParticleData/Sample300/" + std::string( 4 - tstr.size(), '0' ) + tstr  + "." + var + ".bin",
            std::ios::in | std::ios::binary );

        if( !infile.is_open() )
        {
            qDebug() << "couldn't open "
                     <<  ( this->m_basicDataInfo.location + "/ParticleData/Sample300/" + std::string( 4 - tstr.size(), '0' ) + tstr  + "." + var + ".bin" ).c_str();
        }

        result.resize( NP );
        infile.read( ( char * ) result.data(), NP * sizeof( T ) );
        infile.close();
    }

    virtual void loadDefaultSubsetNames( std::vector< std::string > & names )
    {
        m_defaultSubsets = { "iris1", "iris2", "iris3", "iris4", "iris5", "iris6", "iris7", "iris8", "iris9", "iris10" };
        names = m_defaultSubsets;
    }

    virtual void loadTimeStepsMasks( std::vector< std::int32_t > & masks, size_t t )
    {
        std::vector< std::int32_t > exists( m_numParticles );
        readStep( exists, "exists", m_numParticles, t );
        masks = std::vector< std::int32_t >( m_numParticles );

        for( int i = 0; i < m_numParticles; ++i )
        {
            if( exists[ i ] )
            {
                masks[ i ] = m_staticSubsetMasks[ i ] | 1;
            }
            else
            {
                masks[ i ] = 0;
            }
        }
    }

    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes )
    {
        int t = m_simulationTimeSteps[ timeStep ];
        for( const auto & partType : needToLoad )
        {
            for( const auto & attr : partType.second  )
            {
                std::unique_ptr< std::vector< TargetFloatType > > & dat =
                    particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ];

                if( dat == nullptr )
                {
                    dat.reset( new std::vector< TargetFloatType >( m_numParticles ) );
                }
                if( attr == "r" || attr == "mr" )
                {
                    std::vector< OrigionalFloatType > x(   m_numParticles    );
                    std::vector< OrigionalFloatType > y(   m_numParticles    );
                    readStep( x, attr == "r" ? "x" : "mx", m_numParticles, t );
                    readStep( y, attr == "r" ? "y" : "my", m_numParticles, t );
                    getMagnitudes( x, y, *dat );
                }
                else if(  attr == "m" )
                {
                    std::vector< OrigionalFloatType > mx( m_numParticles );
                    std::vector< OrigionalFloatType > my( m_numParticles );
                    std::vector< OrigionalFloatType > mz( m_numParticles );
                    readStep( mx, "mx", m_numParticles, t );
                    readStep( my, "my", m_numParticles, t );
                    readStep( mz, "mz", m_numParticles, t );
                    getMagnitudes( mx, my, mz, *dat );
                }
                else
                {
                    readStep( *dat, attr, m_numParticles, t );
                }
            }
        }
    }

    virtual ~SLACImporter() {}
};

}

#endif // SLACImporter_HPP
