
#ifndef PARALLELALGORITHM_CPP
#define PARALLELALGORITHM_CPP

//#include <QDebug>
#include <random>
#include <vector>
#include <algorithm>

namespace TN
{

namespace Parallel
{

template< typename T >
T max( const std::vector< T > & v );

template< typename T >
T min( const std::vector< T > & v );

template< typename T >
T maxMag( const std::vector< T > & v );

template < typename T1, typename T2 >
void sortTogether( std::vector< T1 > & v1,
                   std::vector< T2 > & v2 )
{
    const int SZ = v1.size();

    std::vector< std::size_t > indices( SZ );
    std::vector< T1 > copy1 = v1;
    std::vector< T2 > copy2 = v2;

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b ) {
            return ( v1[ a ] < v1[ b ] );
        }
    );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        v1[ i ] = copy1[ indices[ i ] ];
        v2[ i ] = copy2[ indices[ i ] ];
    }
}

}

namespace Sequential
{

template< typename T >
inline void randomize( std::vector< T > & v )
{
    std::random_device random_device;
    std::mt19937 rand_engine( random_device( ) );;
    auto counter = v.size() - 1;

    for ( auto & e : v )
    {
        std::uniform_int_distribution<> distribution( 0, counter-- );
        const int randomIndex = distribution( rand_engine );
        std::swap( v[ randomIndex ], e );
    }
}

template< typename T >
T max( const std::vector< T > & v );

template< typename T >
T min( const std::vector< T > & v );

template< typename T >
T maxMag( const std::vector< T > & v );

}

}

#endif // PARALLELALGORITHM_CPP
