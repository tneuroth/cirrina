
#ifndef TN_HISTOGRAM_DEFINITION
#define TN_HISTOGRAM_DEFINITION

#include "Types/Vec.hpp"
#include "Data/Definitions/ParticleFilter.hpp"

#include <QJsonObject>
#include <QJsonArray>

#include <cstdint>
#include <vector>

namespace TN {

struct PopulationDataType
{
    enum
    {
        PARTICLES,
        GRID_POINTS
    };
};

struct SpacePartitioningDefinition
{
    std::vector< std::string > attributes;
    bool preserveAspectRatio;
    std::vector< double > scaling;

    SpacePartitioningDefinition() : preserveAspectRatio( true ) {}

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "preserveAspectRatio", preserveAspectRatio );
        QJsonArray partitionSpaceArray;
        QJsonArray partitionSpaceScalingArray;

        for( unsigned i = 0; i < attributes.size(); ++i )
        {
            partitionSpaceArray.append( attributes[ i ].c_str() );
            partitionSpaceScalingArray.append( scaling[ i ] );
        }

        obj.insert( "attributes", partitionSpaceArray );
        obj.insert( "scaling", partitionSpaceScalingArray );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        preserveAspectRatio = obj[ "preserveAspectRatio" ].toBool();

        attributes.clear();
        foreach ( QJsonValue v, obj[ "attributes" ].toArray() )
        {
            attributes.push_back( v.toString().toStdString() );
        }

        scaling.clear();
        foreach ( QJsonValue v, obj[ "scaling" ].toArray() )
        {
            scaling.push_back( v.toDouble() );
        }
    }
};

struct BaseHistDefinition
{
    std::string name;
    std::string description;
    std::vector< std::int32_t > resolution;
    std::vector< Vec2< float > > valueRange;
    std::vector< std::string > binningSpace;
    //std::vector< std::string > partitionSpace;
    //bool preserveAspectRatio;
    //std::vector< double > partitionSpaceScaling;

    BaseHistDefinition() {}  //: preserveAspectRatio( true ) {}

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "name", name.c_str() );
        obj.insert( "description", description.c_str() );
//        obj.insert( "preserveAspectRatio", preserveAspectRatio );

        QJsonArray resolutionArray;
        QJsonArray valueRangeArray;
        QJsonArray binningSpaceArray;
//        QJsonArray partitionSpaceArray;
//        QJsonArray partitionSpaceScalingArray;

        for( unsigned i = 0; i < resolution.size(); ++i )
        {
            resolutionArray.append( resolution[ i ] );
            valueRangeArray.append( QString( std::to_string( valueRange[ i ].a() ).c_str() + QString( "," ) + std::to_string( valueRange[ i ].b() ).c_str() ) );
            binningSpaceArray.append( binningSpace[ i ].c_str() );
//            partitionSpaceArray.append( partitionSpace[ i ].c_str() );
//            partitionSpaceScalingArray.append( partitionSpaceScaling[ i ] );
        }

        obj.insert( "resolution", resolutionArray );
        obj.insert( "valueRange", valueRangeArray );
        obj.insert( "binningSpace", binningSpaceArray );
//        obj.insert( "partitionSpace", partitionSpaceArray );
//        obj.insert( "partitionSpaceScaling", partitionSpaceScalingArray );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        name = obj[ "name" ].toString().toStdString();
        description = obj[ "description" ].toString().toStdString();
//        preserveAspectRatio = obj[ "preserveAspectRatio" ].toBool();

        resolution.clear();
        foreach ( QJsonValue v, obj[ "resolution" ].toArray() )
        {
            resolution.push_back( v.toInt() );
        }

        valueRange.clear();
        foreach ( QJsonValue v, obj[ "valueRange" ].toArray() )
        {
            QString r = v.toString();
            valueRange.push_back(
                Vec2< float >(
                    std::stof( r.split( "," )[0].toStdString() ),
                    std::stof( r.split( "," )[1].toStdString() ) ) );
        }

        binningSpace.clear();
        foreach ( QJsonValue v, obj[ "binningSpace" ].toArray() )
        {
            binningSpace.push_back( v.toString().toStdString() );
        }

//        partitionSpace.clear();
//        foreach ( QJsonValue v, obj[ "partitionSpace" ].toArray() )
//        {
//            partitionSpace.push_back( v.toString().toStdString() );
//        }

//        partitionSpaceScaling.clear();
//        foreach ( QJsonValue v, obj[ "partitionSpaceScaling" ].toArray() )
//        {
//            partitionSpaceScaling.push_back( v.toDouble() );
//        }
    }

    virtual ~BaseHistDefinition() {}
};

struct PredefinedHistogram : public BaseHistDefinition
{
    double normalizationFactor;

    QJsonObject toJSON() const
    {
        QJsonObject obj = BaseHistDefinition::toJSON();
        obj.insert( "normFactor", normalizationFactor  );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        BaseHistDefinition::fromJSON( obj );
        normalizationFactor = obj[ "normFactor" ].toDouble();
    }
};

struct HistogramWeightType
{
    enum
    {
        VARIABLE,
        CONSTANT,
        NONE
    };
};

struct HistogramDefinition : public BaseHistDefinition
{
    int weightType;

    // only used if weightType is not VARIABLE, or NONE
    double constWeightValue;
    std::string weight;

    QJsonObject toJSON() const
    {
        QJsonObject obj = BaseHistDefinition::toJSON();
        obj.insert( "constWeightValue", constWeightValue  );
        obj.insert( "weightAttr", weight.c_str()  );
        obj.insert( "weightType", weightType );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        BaseHistDefinition::fromJSON( obj );
        constWeightValue = obj[ "constWeightValue" ].toDouble();
        weight = obj[ "weightAttr" ].toString().toStdString();
        weightType = obj[ "weightType" ].toInt();
    }

    HistogramDefinition() : BaseHistDefinition(), constWeightValue( 1.0 ) {}
};

}

#endif
