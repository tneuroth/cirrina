#version 130



in vec2 posAttr;
in vec4 colAttr;

uniform mat4 MVP;

out vec4 col;

void main(void)
{
    gl_Position = MVP * vec4(
        posAttr.x,
        posAttr.y,
        0.0,
        1.0
    );

    col = colAttr;
}
