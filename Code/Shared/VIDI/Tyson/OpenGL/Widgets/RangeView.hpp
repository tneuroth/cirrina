


#ifndef TN_RANGE_VIEW_HPP
#define TN_RANGE_VIEW_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/Phase2/ScrollBar.hpp"
#include "Types/Vec.hpp"
#include "OpenGL/Widgets/WeightHistogramWidget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"

namespace TN
{

class RangeView : public Widget
{
    ScrollBar m_scrollBar;

public:

    const int SCROLL_BAR_HEIGHT = 24;
    std::vector< std::pair< RangeSelector, RangeSelector > > rangeWidgets;
    std::vector< PressButton > varSelectButtons;

    RangeView() : Widget(), m_scrollBar( ScrollBar::VERTICAL )
    {}

    ScrollBar & scrollBar()
    {
        return m_scrollBar;
    }

    //! in screen space ( pixels )
    virtual void setSize( float width, float height ) override
    {
        Widget::setSize( width, height );
        m_scrollBar.setSize( SCROLL_BAR_HEIGHT, height );
        m_scrollBar.setJumpLength( ( width - SCROLL_BAR_HEIGHT ) );
        m_scrollBar.setAreaLength( height );
    }

    //! in screen space ( pixels )
    virtual void setPosition( double x, double y ) override
    {
        Widget::setPosition( x, y );
        m_scrollBar.setPosition( x + size().x() - SCROLL_BAR_HEIGHT, y );
    }
};

}

#endif


