


#ifndef COMBOWIDGET_HPP
#define COMBOWIDGET_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"

namespace TN
{

class ComboWidget : public Widget
{
    int m_id;
    Vec3< float > m_color;
    Vec3< float > m_flashColor;
    std::vector< std::string > m_items;

    int m_selectedItemId;
    int m_hoverItemId;
    bool m_isPressed;
    bool m_flash;
    double m_flashPhase;

public:

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    ComboWidget() : m_selectedItemId( -1 ), m_hoverItemId( -1 ), m_isPressed( false ), m_flash( false ), m_flashPhase(0), m_flashColor( 1, .7, .6 ) {}

    ComboWidget( const Vec3< float > & color, int width, int height, int id ) :
        m_id( id ), m_color( color ), m_selectedItemId( -1 ), m_hoverItemId( -1 ), m_isPressed( false ), m_flash( false ), m_flashPhase( 0 ), m_flashColor( 1, .7, .6 )
    {
        resize( width, height );
    }

    int addItem( const std::string & item )
    {
        if( m_selectedItemId == -1 )
        {
            m_selectedItemId = 0;
        }
        int itemIdx = m_items.size();
        m_items.push_back( item );
        return itemIdx;
    }

    void setKeys( const std::vector< std::string > & keys )
    {
        clear();
        for( const auto & key : keys )
        {
            addItem( key );
        }
    }

    void selectItem( int id )
    {
        m_selectedItemId = id;
    }

    std::string selectedItemText()
    {
        if( m_items.size() && m_selectedItemId >= 0 )
        {
            return m_items[ m_selectedItemId ];
        }
        return "";
    }

    void clear()
    {
        m_selectedItemId = -1;
        m_items.clear();
    }

    const Vec3< float > & color() const
    {
        return m_color;
    }

    bool isPressed() const
    {
        return m_isPressed;
    }

    int numItems() const
    {
        return m_items.size();
    }

    const std::vector< std::string > & items() const
    {
        return m_items;
    }

    int selectedItem() const
    {
        return m_selectedItemId;
    }

    int hoveredItem() const
    {
        return m_hoverItemId;
    }

    int id() const
    {
        return m_id;
    }

    void setPressed( bool pressed )
    {
        m_isPressed = pressed;
    }

    int mousedOver( Vec2< float > & p )
    {
        int id = -1;

        if( p.x() >= position().x() && p.x() <= position().x() + size().x() && p.y() <= position().y() + size().y() )
        {
            for( unsigned i = 1, end = m_items.size()+1; i < end; ++i )
            {
                if( p.y() >= position().y() - i*size().y() )
                {
                    id = i-1;
                    break;
                }
            }
        }
        m_hoverItemId = id;
        return id;
    }

    int pressed( Vec2< float > & p )
    {
        int id = -1;

        if( p.x() >= position().x() && p.x() <= position().x() + size().x() && p.y() <= position().y() + size().y() )
        {
            for( unsigned i = 1, end = m_items.size()+1; i < end; ++i )
            {
                if( p.y() >= position().y() - i*size().y() )
                {
                    id = i-1;
                    break;
                }
            }
        }

        m_selectedItemId = id;
        return id;
    }

    virtual bool pointInViewPort( Vec2< float > & p )
    {
        if( m_isPressed )
        {
            bool is = false;
            if( p.x() >= position().x() && p.x() <= position().x() + size().x() && p.y() <= position().y() + size().y() )
            {
                for( unsigned i = 1, end = m_items.size()+1; i < end; ++i )
                {
                    if( p.y() >= position().y() - i*size().y() )
                    {
                        is = true;
                        break;
                    }
                }
            }
            return is;
        }
        else
        {
            return Widget::pointInViewPort( p );
        }
    }

    void selectByText( const std::string & txt )
    {
        if( numItems() < 2 )
        {
            return;
        }

        bool found = false;
        int idx = 0;
        for( int i = 0; i < numItems(); ++i )
        {
            if( items()[ i ] == txt )
            {
                idx = i;
                found = true;
                break;
            }
        }
        if( found )
        {
            selectItem( idx );
        }
    }

    void flashColor( const Vec3< float > & cl ) { m_flashColor = cl; }
    const Vec3< float > &  flashColor() const { return m_flashColor; }

    bool flash() const { return m_flash; }
    void flash( bool b ) { m_flash = b; }

    double flashPhase() const { return m_flashPhase; }
    void flashPhase( double phase ) { m_flashPhase = phase; }
};

}


#endif // COMBOWIDGET_HPP

