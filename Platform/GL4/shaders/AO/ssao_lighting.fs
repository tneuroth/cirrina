#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedo;
uniform sampler2D ssao;

struct Light {
    vec3 direction;
};

uniform Light light;

struct Material {
    float Ks;
    float Kd;
    float Ka;
    float alpha;
};

uniform Material material;

void main()
{             
    // retrieve data from gbuffer
    vec3 FragPos = texture(gPosition, TexCoords).rgb;
    vec3 Normal = texture(gNormal, TexCoords).rgb;
    vec3 Diffuse = texture(gAlbedo, TexCoords).rgb;
    float AmbientOcclusion = texture(ssao, TexCoords).r;
    
    // then calculate lighting as usual
    vec3 ambient = vec3( Diffuse * AmbientOcclusion );

    // diffuse
    vec3 lightDir = normalize( light.direction );
    vec3 diffuse = max( dot( Normal, lightDir ), 0.0) * Diffuse *.3;

    // specular
    vec3 viewDir  = normalize(-FragPos); // viewpos is (0.0.0)
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(Normal, halfwayDir), 0.0), material.alpha ) * .3;
    vec3 specular = vec3( spec );

    vec3 lighting = ambient * material.Ka + diffuse * material.Kd + specular * material.Ks;
    FragColor = vec4( min( lighting, vec3( 1.0 ) ), 1.0 );
}
