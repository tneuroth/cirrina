#ifndef FRVOLUMECACHE_HPP
#define FRVOLUMECACHE_HPP

#include <vector>
#include <unordered_map>
#include <limits>
#include <QDebug>

struct FrVolume
{
    std::vector< float > values;
    int rows;
    int cols;
    int steps;
    std::vector< double > normalizationFactors;
    bool normalized;

    float * atStep( int step )
    {
        return & values[ step * rows * cols ];
    }

    void normalize()
    {
        if( normalized )
        {
            return;
        }

        const int N_BINS = rows * cols;
        const int STEPS = steps;

        #pragma omp parallel for
        for( int t = 0; t < STEPS; ++t )
        {
            normalizationFactors[ t ] = 1.0;

            float maxF = 0;

            const int OFFSET = t * N_BINS;

            #pragma omp simd
            for( int b = 0; b < N_BINS; ++b )
            {
                maxF = std::max( maxF, std::abs( values[ OFFSET + b ] ) );
            }

            if( maxF > 0 )
            {
                #pragma omp simd
                for( int b = 0; b < N_BINS; ++b )
                {
                    values[ OFFSET + b ] /= maxF;
                }

                normalizationFactors[ t ] = maxF;
            }
        }

        normalized = true;
    }

    void denormalize()
    {
        if( ! normalized )
        {
            return;
        }

        const int N_BINS = rows * cols;
        const int STEPS = steps;

        #pragma omp parallel for
        for( int t = 0; t < STEPS; ++t )
        {
            const int OFFSET = t * N_BINS;
            #pragma omp simd
            for( int b = 0; b < N_BINS; ++b )
            {
                values[ OFFSET + b ] *= normalizationFactors[ t ];
            }
            normalizationFactors[ t ] = 1.0;
        }
        normalized = false;
    }
};

struct FrVolumeCache
{
    const size_t N_CACHE = 200;
    std::vector< FrVolume > volumes;
    std::vector< unsigned char > free;

    bool invalidated;

    int rows;
    int cols;
    int steps;

    bool normalize;

    FrVolumeCache() : volumes( N_CACHE ), free( N_CACHE, true ), invalidated( true )
    {
        for( auto & v : volumes )
        {
            v.values.reserve( 32*32*500 );
        }
        normalize = false;
    }

    void setNormalization( bool n )
    {
        normalize = n;

        for( size_t i = 0, end = volumes.size(); i < end; ++i )
        {
            if( ! free[ i ] )
            {
                if( n && ! volumes[ i ].normalized )
                {
                    volumes[ i ].normalize();
                }
                else if( ! n && volumes[ i ].normalized )
                {
                    volumes[ i ].denormalize();
                }
            }
        }
    }

    // if a volume is already cached for a given spatial index, then
    // a map element will exist, mapping the spatial index to the index
    // where it exists
    std::unordered_map< int, int > spatialIndexToCacheIndex;
    std::unordered_map< int, int > cacheIndexToSpatialIndex;

    FrVolume & get( int spatialIndex, bool & valid )
    {
        invalidated = false;

        // if we already have this volume cached
        if( spatialIndexToCacheIndex.count( spatialIndex ) )
        {
            valid = true;
            return volumes[ spatialIndexToCacheIndex.at( spatialIndex ) ];
        }
        else
        {
            int index = 0;

            // if we have any free slots ...
            if( spatialIndexToCacheIndex.size() < volumes.size() )
            {
                for( size_t i = 0, end = free.size(); i < end; ++i )
                {
                    if( free[ i ] )
                    {
                        index = i;
                        break;
                    }
                }

                cacheIndexToSpatialIndex.insert( { index, spatialIndex } );
                spatialIndexToCacheIndex.insert( { spatialIndex, index } );
                free[ index ] = false;
            }
            else
            {
                // replace the old index mappings with the new one
                int previousSpatialIndex = cacheIndexToSpatialIndex.at( index );
                spatialIndexToCacheIndex.erase( previousSpatialIndex );
                spatialIndexToCacheIndex.insert( { spatialIndex, index }  );
                cacheIndexToSpatialIndex.at( index ) = spatialIndex;
                volumes[ index ].normalized = false;
            }

            valid = false;
            return volumes[ index ];
        }
    }

    void normalizeVolume( int spatialIndex )
    {
        if( spatialIndexToCacheIndex.count( spatialIndex ) )
        {
            size_t index = spatialIndexToCacheIndex.at( spatialIndex );

            if( ! free[ index ] && ! volumes[ index ].normalized )
            {
                volumes[ index ].normalize();
            }
        }
    }

    void invalidate()
    {
        if( ! invalidated )
        {
            spatialIndexToCacheIndex.clear();
            cacheIndexToSpatialIndex.clear();

            for( auto & f : free )
            {
                f = true;
            }
            for( auto & v : volumes )
            {
                v.normalized = false;
            }

            invalidated = true;
        }
    }

    void setParameters( int _rows, int _cols, int _steps )
    {
        bool sizeChanged = ( _rows * _cols * _steps ) != ( rows * cols * steps );

        rows = _rows;
        cols = _cols;
        steps = _steps;

        for( auto & v : volumes )
        {
            v.rows = rows;
            v.cols = cols;
            v.steps = steps;
            v.normalized = false;

            if( v.normalizationFactors.size() != steps )
            {
                v.normalizationFactors.resize( steps );
            }

            if( sizeChanged )
            {
                v.values.resize( rows * cols * steps );
            }
        }

        invalidate();
    }
};

#endif // FRVOLUMECACHE_HPP
