#version 400



#define PIo2 1.57079632679

layout(lines) in;
layout(line_strip, max_vertices=4) out;

in vData
{
    float weight;
    flat int select;
    flat int isReal;
} vertices[];


out G2F{
    float value;
    flat int select;
} g2f;

uniform float wrap1;
uniform float wrap2;

void main( void )
{
    int s = vertices[ 0 ].select * vertices[ 1 ].select;
    int r = vertices[ 0 ].isReal * vertices[ 1 ].isReal;

    if( bool( r ) )
    {
        float x1 = gl_in[0].gl_Position.x;
        float x2 = gl_in[1].gl_Position.x;

        vec4 p1 = gl_in[0].gl_Position;
        vec4 p2 = gl_in[1].gl_Position;

        float w1 = vertices[ 0 ].weight;
        float w2 = vertices[ 1 ].weight;

        if( x2 < x1 )
        {
            float temp = x2;
            x2 = x1;
            x1 = temp;

            vec4 temp2 = p2;
            p2 = p1;
            p1 = temp2;

            float wTemp = w2;
            w2 = w1;
            w1 = wTemp;
        }

        float wrapDist = ( x1 - wrap1 ) + ( wrap2 - x2 );
        float strtDist =   x2 - x1;

        if( wrapDist < strtDist )
        {
            float r = ( wrap2 - x2 ) / wrapDist;
            float yWrap = p2.y + r * ( p1.y - p2.y );
            float wWrap = w2   + r * ( w2 - w1 );

            // 0 -------|___0

            g2f.value  = w2;
            g2f.select = s;
            gl_Position = p2;
            EmitVertex();

            vec4 tmp = p2;
            tmp.x = wrap2;
            tmp.y = yWrap;
            g2f.value  = wWrap;
            g2f.select = s;
            gl_Position = tmp;
            EmitVertex();

            EndPrimitive();

            // 0 __|--------0

            tmp.y = yWrap;
            tmp.x = wrap1;
            g2f.value  = wWrap;
            g2f.select = s;
            gl_Position = tmp;
            EmitVertex();

            tmp = p1;
            g2f.value  = w1;
            g2f.select = s;
            gl_Position = p1;
            EmitVertex();

            EndPrimitive();
        }
        else
        {
            g2f.value  = vertices[ 0 ].weight;
            g2f.select = s;

            gl_Position = gl_in[0].gl_Position;
            EmitVertex();

            g2f.value  = vertices[ 1 ].weight;
            g2f.select = s;

            gl_Position = gl_in[1].gl_Position;
            EmitVertex();

            //EndPrimitive();
        }
    }
}
