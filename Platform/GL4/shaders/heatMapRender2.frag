#version 130



in  vec2 uv;
out vec4 color;

uniform sampler2D tex1;
uniform sampler2D tex2;

uniform sampler1D tf1;
uniform sampler1D tf2;

uniform float mx1;
uniform float mx2;

uniform float mn1;
uniform float mn2;

uniform float inverseTexWidth;
uniform float inverseTexHeight;

uniform bool outline;
uniform int mode;
uniform int which;

uniform bool localize;
uniform vec2 localCenter;
uniform vec2 localRadius;
uniform vec2 localRange;

uniform bool forground;

void main(){

    float f2 = texture( tex2, uv.st ).r;
    float f1 = texture( tex1, uv.st ).r;

    if( forground )
    {
        float px = uv.s * 2 - 1;
        float py = uv.t * 2 - 1;

        float relativeRadius =
            sqrt(
                pow( ( px - localCenter.x ) / localRadius.x, 2.0 ) +
                pow( ( py - localCenter.y ) / localRadius.y, 2.0 ) );

        if( relativeRadius <= 1 )
        {
            if( abs( f1 ) > 0 )
            {
                float absMax = max( abs( mn1 ), abs( mx1 ) );
                float r = 0.5 + ( f1 / absMax ) / 2.0;
                vec3 c2 = texture( tf2, 1.0 - r ).rgb;
                color = vec4( c2.r, c2.g, c2.b, 1 );
            }
            else
            {
                color = vec4( 0, 0, 0, 0 );
            }
        }
        else
        {
            if( abs(f2 ) > 0 )
            {
                float absMax = max( abs( mn2 ), abs( mx2 ) );
                float r = 0.5 + ( f2 / absMax ) / 2.0;
                vec3 c2 = texture( tf1, 1.0 - r ).rgb;
                color = vec4( c2.r, c2.g, c2.b, 1 );
            }
            else if( abs( f1 ) > 0 )
            {
                color = vec4( 0, 0, 0, 0.2 );
            }
            else
            {
                color = vec4( 0, 0, 0, 0 );
            }
        }
    }
    else if( localize )
    {
        if( abs( f2 ) > 0 )
        {
            float px = uv.s * 2 - 1;
            float py = uv.t * 2 - 1;

            float relativeRadius =
                sqrt(
                    pow( ( px - localCenter.x ) / localRadius.x, 2.0 ) +
                    pow( ( py - localCenter.y ) / localRadius.y, 2.0 ) );

            float absMax;
            if( relativeRadius <= 1 )
            {
                float absMax = max( abs( localRange.x ), abs( localRange.y ) );
                float r = 0.5 + ( f2 / absMax ) / 2.0;
                vec3 c2 = texture( tf2, 1.0 - r ).rgb;
                color = vec4( c2.r, c2.g, c2.b, 1 );
            }
            else
            {
                float absMax = max( abs( mn2 ), abs( mx2 ) );
                float r = 0.5 + ( f2 / absMax ) / 2.0;
                vec3 c2 = texture( tf1, 1.0 - r ).rgb;
                color = vec4( c2.r, c2.g, c2.b, 1 );
            }
        }
        else if( abs( f1 ) > 0 )
        {
            color = vec4( 0, 0, 0, 0.2 );
        }
        else
        {
            color = vec4( 0, 0, 0, 0 );
        }
    }
    else
    {
        if( abs( f2 ) > 0 )
        {
            float absMax = max( abs( mn2 ), abs( mx2 ) );
            float r = 0.5 + ( f2 / absMax ) / 2.0;
            vec3 c2 = texture( tf1, 1.0 - r ).rgb;
            color = vec4( c2.r, c2.g, c2.b, 1 );
        }
        else if( abs( f1 ) > 0 )
        {
            color = vec4( 0, 0, 0, 0.2 );
        }
        else
        {
            color = vec4( 0, 0, 0, 0 );
        }
    }
}
