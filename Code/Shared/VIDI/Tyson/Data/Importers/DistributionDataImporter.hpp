
#ifndef TN_DISTRIBUTION_DATA_IMPORTER
#define TN_DISTRIBUTION_DATA_IMPORTER

#include "Data/Importers/DataImporter.hpp"
#include "Data/Definitions/HistogramDefinition.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"

#include <set>
#include <map>
#include <string>
#include <vector>
#include <memory>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The DistributionDataImporter class
 */
class DistributionDataImporter :  public DataImporter< TargetFloatType >
{

public :

    /**
     * @brief init
     * @return
     */
    virtual bool init( const BasicDataInfo & ) = 0;

    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & m_distributions,
        std::map< std::string, std::vector< TargetFloatType > > & perGridPointAttributes,
        std::map< std::string, Vec2< double > > & perGridPointAttributeRanges,
        std::vector< std::string > & gridPointSpatialAttributes,
        std::map< std::string, std::map< std::string, PredefinedHistogram > > & perParticleTypePerDistributionTypeParameters,
        std::map< std::string, std::map< std::string, double > > & perParticleTypeConstants,
        std::set< std::string > & particleTypes,
        std::map< std::string, std::set< std::string > > & perParticleTypeDistributionTypes,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::int32_t & numTimeSteps ) = 0;

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param m_distributions
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & m_distributions
    ) = 0;

    /**
     * @brief ~DistributionDataImporter
     */
    virtual ~DistributionDataImporter() {}
};

}

#endif // TN_DISTRIBUTION_DATA_IMPORTER

