# Cirrina

A visualization system for multivariate phase space trajectory data.

## Dependencies

This project depends on QT5, under the [LGPLv3 license](https://www.gnu.org/licenses/lgpl-3.0.en.html).

## Authors and acknowledgment

Baleen was written by Tyson Neuroth. Its design was informed through dicussions with Franz Sauer and Kwan-Liu Ma at UC Davis, and Robert Hager, Stephane Ethier, and C.S. Chang at Princeton Plasma Physics Laboratory.

## License

Cirrina is licensed under a modified MIT license which requires explicit perfmission from the copyright holder for commercial use. See LICENSE.txt for details.  

## Citations

When citing Cirrina, please use the following paper: 

Neuroth, T.A., Sauer, F. and Ma, K.L. (2019), [An Interactive Visualization System for Large Sets of Phase Space Trajectories](https://onlinelibrary.wiley.com/doi/abs/10.1111/cgf.13690). Computer Graphics Forum, 38: 297-309. https://doi.org/10.1111/cgf.13690
