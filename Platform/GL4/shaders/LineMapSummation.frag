#version 400



out float fragColor[4];

in G2F{
    float valAll;
    float valComb;
    float countAll;
    float countComb;
}g2f;

void main(void)
{
    fragColor[ 0 ] = g2f.valAll;
    fragColor[ 1 ] = g2f.valComb;
    fragColor[ 2 ] = g2f.countAll;
    fragColor[ 3 ] = g2f.countComb;
}
