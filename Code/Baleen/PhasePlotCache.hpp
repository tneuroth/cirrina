#ifndef PHASEPLOTCACHE_HPP
#define PHASEPLOTCACHE_HPP

#include "Repo/OpenGL/Utils/TNTextureLayer.hpp"

#include <QGLFramebufferObject>
#include <QGLFramebufferObjectFormat>
#include <vector>
#include <unordered_map>
#include <limits>

namespace TN
{

//struct TextureLayer
//{
//    TNR::Texture1D3 tex;
//    int width;
//    int height;

//    float minValue;
//    float maxValue;
//    std::vector< float > m_storage;

//    std::vector< float > & get( )
//    {
//        m_storage.resize( width*height );

//        tex.bind();
//        glGetTexImage(
//            GL_TEXTURE_2D,
//            0,
//            GL_RED,
//            GL_FLOAT,
//            ( void * ) m_storage.data() );

//        return m_storage;
//    }

//    Vec2< float > getMaxInRadius( const Vec2< float > & center, const Vec2< float > & radius )
//    {
//        float mx = 0;
//        float mn = std::numeric_limits< float >::max();

//        if( m_storage.size() < width*height )
//        {
//            return Vec2< float> ( 0, 0 );
//        }

//        tex.bind();
//        glGetTexImage(
//            GL_TEXTURE_2D,
//            0,
//            GL_RED,
//            GL_FLOAT,
//            ( void * ) m_storage.data() );

//        const int END = width * height;

//        for( int i = 0; i < END; ++i )
//        {
//            int r = i / width;
//            int c = i % width;

//            float rn = 2.f * ( (float ) r / height ) - 1;
//            float cn = 2.f * ( (float ) c / width  ) - 1;

//            const float relativeRadius =
//                sqrt(
//                    pow( ( cn - center.x() ) / radius.x(), 2.0 ) +
//                    pow( ( rn - center.y() ) / radius.y(), 2.0 ) );

//            if( relativeRadius <= 1.0 )
//            {
//                if( m_storage[ i ] > mx )
//                {
//                    mx = m_storage[ i ];
//                }
//                if( m_storage[ i ] < mn && m_storage[ i ] > 0 )
//                {
//                    mn = m_storage[ i ];
//                }
//            }
//        }

//        return { mn, mx };
//    }

//    void maxMask( float m )
//    {
//        if( m_storage.size() < width * height )
//        {
//            m_storage.resize( width * height );
//        }

//        tex.bind();
//        glGetTexImage(
//            GL_TEXTURE_2D,
//            0,
//            GL_RED,
//            GL_FLOAT,
//            ( void * ) m_storage.data() );

//        const int END = width * height;

//        #pragma omp parallel for
//        for( int i = 0; i < END; ++i )
//        {
//            if( m_storage[ i ] >= m )
//            {
//                m_storage[ i ] = 0;
//            }
//        }

//        glTexImage2D(
//            GL_TEXTURE_2D,
//            0,
//            GL_R32F,
//            width,
//            height,
//            0,
//            GL_RED,
//            GL_FLOAT,
//            (void*) m_storage.data() );
//    }

//    void computeMax()
//    {
//        if( m_storage.size() < width * height )
//        {
//            m_storage.resize( width * height );
//        }

//        tex.bind();
//        glGetTexImage(
//            GL_TEXTURE_2D,
//            0,
//            GL_RED,
//            GL_FLOAT,
//            ( void * ) m_storage.data() );

//        const int END = width * height;

//        float mx = m_storage[ 0 ];
//        float mn = std::numeric_limits< float >::max();

////        #pragma omp parallel for reduction(max:mx)
//        for( int i = 0; i < END; ++i )
//        {
//            if( m_storage[ i ] > mx )
//            {
//                mx = m_storage[ i ];
//            }
//            if( m_storage[ i ] < mn/* &&  m_storage[ i ] > 0*/ )
//            {
//                mn = m_storage[ i ];
//            }
//        }

//        maxValue = mx;
//        minValue = mn;

////        qDebug() << mx << " " << mn;
//    }

//    void loadMaxes( int w, int h )
//    {
//        std::vector< float > maxes( width*height, std::numeric_limits< float >::max() );

//        width = w;
//        height = h;

//        if( tex.id() == 0 )
//        {
//            tex.create();
//            tex.loadZeros( width, height );
//        }

//        tex.load( maxes, width, height );
//    }

//    void load( std::vector< float > & data, int w, int h )
//    {
//        if( tex.id() == 0 )
//        {
//            tex.create();
//        }

//        width = w;
//        height = h;

//        tex.load( data, width, height );

//        float mx = data[ 0 ];
//        float mn = std::numeric_limits< float >::max();

//        for( size_t i = 0, END = data.size(); i < END; ++i )
//        {
//            if( data[ i ] > mx )
//            {
//                mx = data[ i ];
//            }
//            if( data[ i ] < mn )
//            {
//                mn = data[ i ];
//            }
//        }

//        maxValue = mx;
//        minValue = mn;
//    }

//    void resize( int w, int h )
//    {
//        if( tex.id() == 0 )
//        {
//            tex.create();
//        }

//        width = w;
//        height = h;

//        tex.loadZeros( width, height );
//    }
//};

struct PhasePlotCache
{
    const int N_CACHE = 144;
    std::vector< TNR::TextureLayer > data;
    std::vector< unsigned char > free;
    bool invalidated;
    int width;
    int height;

    GLuint frameBufferId;

    std::unordered_map< int, int > spatialIndexToCacheIndex;
    std::unordered_map< int, int > cacheIndexToSpatialIndex;

    TNR::TextureLayer fullPlotTexture;
    TNR::TextureLayer binPlotTexture;

    Texture1D1 angleToPixelDensityTexture;

    PhasePlotCache() : data( N_CACHE ), free( N_CACHE, true ), invalidated( true ), frameBufferId( 0 )
    {}

    ~PhasePlotCache()
    {
        if( frameBufferId != 0 )
        {
            glDeleteFramebuffers( 1, &frameBufferId );
        }
    }

    void setAngleToPixelDensityTexture( const std::vector< float > & data )
    {
        angleToPixelDensityTexture.load( data );
    }

    // must be called after openGL has already been initialized
    void initGL()
    {
        glGenFramebuffers( 1, & frameBufferId );
        data = std::vector< TNR::TextureLayer >( N_CACHE );

        fullPlotTexture.tex.create();
        binPlotTexture.tex.create();
        angleToPixelDensityTexture.create();
    }

    TNR::TextureLayer & get( int spatialIndex, bool & valid )
    {
        invalidated = false;

        // if we already have this texture cached
        if( spatialIndexToCacheIndex.count( spatialIndex ) )
        {
            valid = true;
            return data[ spatialIndexToCacheIndex.at( spatialIndex ) ];
        }
        else
        {
            int index = 0;

            // if we have any free slots ...
            if( spatialIndexToCacheIndex.size() < data.size() )
            {
                for( size_t i = 0, end = free.size(); i < end; ++i )
                {
                    if( free[ i ] )
                    {
                        index = i;
                        break;
                    }
                }
                cacheIndexToSpatialIndex.insert( { index, spatialIndex } );
                spatialIndexToCacheIndex.insert( { spatialIndex, index } );
                free[ index ] = false;
            }
            else
            {
                // replace the old index mappings with the new one
                int previousSpatialIndex = cacheIndexToSpatialIndex.at( index );
                spatialIndexToCacheIndex.erase( previousSpatialIndex );
                spatialIndexToCacheIndex.insert( { spatialIndex, index }  );
                cacheIndexToSpatialIndex.at( index ) = spatialIndex;
            }

            valid = false;
            return data[ index ];
        }
    }

    void invalidate()
    {
        if( ! invalidated )
        {
            spatialIndexToCacheIndex.clear();
            cacheIndexToSpatialIndex.clear();

            for( auto & f : free )
            {
                f = true;
            }

            invalidated = true;
        }
    }

    void releaseFrameBuffer()
    {
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    }


    void bindFrameBuffer()
    {
        glBindFramebuffer( GL_FRAMEBUFFER, frameBufferId );
    }

    void setTexture( TNR::TextureLayer & texLayer )
    {
        glDeleteFramebuffers( 1, &frameBufferId );
        glGenFramebuffers( 1, & frameBufferId );

        bindFrameBuffer();
        texLayer.tex.bind();

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texLayer.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            qDebug() << "frame buffer incomplete " << texLayer.height << " " << texLayer.width;
        }
    }

    void setTextures( TNR::TextureLayer & texLayer1, TNR::TextureLayer & texLayer2, TNR::TextureLayer & texLayer3, TNR::TextureLayer & texLayer4 )
    {
        glDeleteFramebuffers( 1, &frameBufferId );
        glGenFramebuffers( 1, & frameBufferId );

        bindFrameBuffer();

        GLint maxDrawBuf = 0;
        glGetIntegerv(GL_MAX_DRAW_BUFFERS, &maxDrawBuf);

        GLint maxAttach = 0;
        glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxAttach);

        //std::cerr << "max draw buffers " <<  maxDrawBuf << ", max color attatchments " << maxAttach << "\n";

        texLayer1.tex.bind();
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texLayer1.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            qDebug() << "frame buffer incomplete " << texLayer1.height << " " << texLayer1.width;
        }

        texLayer2.tex.bind();
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texLayer2.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            qDebug() << "frame buffer incomplete " << texLayer2.height << " " << texLayer2.width;
        }

        texLayer3.tex.bind();
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, texLayer3.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            qDebug() << "frame buffer incomplete " << texLayer3.height << " " << texLayer3.width;
        }

        texLayer4.tex.bind();
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, texLayer4.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            qDebug() << "frame buffer incomplete " << texLayer4.height << " " << texLayer4.width;
        }

        GLenum drawbuf[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
        glDrawBuffers( 4, drawbuf );

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "Frame buffer not created. \n" << glCheckFramebufferStatus(GL_FRAMEBUFFER);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void setTextures( TNR::TextureLayer & texLayer1, TNR::TextureLayer & texLayer2 )
    {
        glDeleteFramebuffers( 1, &frameBufferId );
        glGenFramebuffers( 1, & frameBufferId );

        bindFrameBuffer();
        texLayer1.tex.bind();
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texLayer1.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            qDebug() << "frame buffer incomplete " << texLayer1.height << " " << texLayer1.width;
        }

        texLayer2.tex.bind();
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texLayer2.tex.id(), 0);
        if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
        {
            qDebug() << "frame buffer incomplete " << texLayer2.height << " " << texLayer2.width;
        }

        GLenum drawbuf[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
        glDrawBuffers( 2, drawbuf );

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "Frame buffer not created. \n" << glCheckFramebufferStatus(GL_FRAMEBUFFER);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void setSize( int _width, int _height )
    {
        width = _width;
        height = _height;
    }
};

}

#endif // PHASEPLOTCACHE_HPP
