
#include "Heatmap.hpp"

#include <iostream>
#include <string>

#include <chrono>
#include <ctime>
#include <algorithm>

#include <cuda.h>
#include <cuda_runtime.h>

// ** Kernels ** //

__global__
void computeHeatMap(
    const float * _vx,
    const float * _vy,
    const float * _w,
    const unsigned char * _flags,
    float * _result,
    const float xScale,
    const float yScale,
    const float xMin,
    const float xMax,
    const float yMin,
    const float yMax,
    const int dimX,
    const int dimY,
    const int numParticles )
{
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if( idx >= numParticles )
    {
        return;
    }

    if( _flags[ idx ] == 0 )
    {
        return;
    }

    const float xw = xMax - xMin;
    const float yw = yMax - yMin;

    const float ox = ( xw - xScale * xw ) / ( 2 * xw );
    const float oy = ( yw - yScale * yw ) / ( 2 * yw );

    const float xr = ( ( _vx[ idx ] - xMin  ) / xw ) * xScale + ox;
    const float yr = ( ( _vy[ idx ] - yMin  ) / yw ) * yScale + oy;

    int row = min( int( yr * dimY ), dimY-1 );
    int col = min( int( xr * dimX ), dimX-1 );

    atomicAdd( &_result[ row * dimX + col ], _w[ idx ] );
}


void HeatMapper2D::checkError( const std::string & mssg )
{
    cudaError_t error = cudaGetLastError();
    if( error != cudaSuccess )
    {
        // print the CUDA error message and exit
        printf("%s CUDA error: %s\n", mssg.c_str(), cudaGetErrorString( error ) );
        exit( -1 );
    }
}


HeatMapper2D::HeatMapper2D()
{
      vx_d = 0;
      vy_d = 0;
       w_d = 0;
   flags_d = 0;
  result_d = 0;
}


HeatMapper2D::~HeatMapper2D()
{
    cleanBuffers(); 
    cudaFree( result_d );
}


void HeatMapper2D::cleanBuffers()
{
    cudaFree(    vx_d );
    cudaFree(    vy_d );
    cudaFree(     w_d );
    cudaFree( flags_d );

    checkError( "cleaning buffers" );

       vx_d = 0;
       vy_d = 0;
        w_d = 0;
    flags_d = 0;
}

void HeatMapper2D::allocateResultDevice( int resolutionX, int resolutionY )
{
    cudaFree( result_d );
    m_resolutionX = resolutionX;
    m_resolutionY = resolutionY;
    cudaMalloc( (void **) &result_d, resolutionX * resolutionY * sizeof ( float ) );

    checkError( "allocating result vector" );
}


void HeatMapper2D::resizeBuffer( int numParticles )
{
    cleanBuffers();
    cudaMalloc( (void **) &    vx_d, numParticles * sizeof ( float ) );
    cudaMalloc( (void **) &    vy_d, numParticles * sizeof ( float ) );
    cudaMalloc( (void **) &     w_d, numParticles * sizeof ( float ) );
    cudaMalloc( (void **) & flags_d, numParticles * sizeof ( unsigned int ) );

    m_numParticles = numParticles;

    checkError( "resizing buffer" );
}


void HeatMapper2D::buffer(
        const std::vector< float > & vx,
        const std::vector< float > & vy,
        const std::vector< float > & w,
        const std::vector< unsigned char > & flags )
{
    const int N = vx.size();
    cudaMemcpy(    vx_d,    vx.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy(    vy_d,    vy.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy(     w_d,     w.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( flags_d, flags.data(), N * sizeof( float ), cudaMemcpyHostToDevice );
    checkError( "buffering data" );
}

void HeatMapper2D::compute(
    float vxMin,
    float vxMax,
    float vyMin,
    float vyMax,
    std::vector< float > & result,
    std::pair< float, float > & range,
    bool maintainAspect )
{
    const int N = m_resolutionX * m_resolutionY;
    cudaMemset( result_d, 0, N * sizeof( float ) );
    result.resize( N );

    dim3 block_size( 512, 1, 1 );
    dim3 grid_size(  512 * std::ceil( ( float ) m_numParticles / float( 512 ) ), 1 );

    float xWidth = vxMax - vxMin;
    float yWidth = vyMax - vyMin;
    float yScale = 1, xScale = 1;
 
    if( maintainAspect )
    {
        if( ( float( m_resolutionX ) / m_resolutionY ) > ( xWidth / yWidth ) )
        {
            xScale = ( xWidth / yWidth ) / ( float( m_resolutionX ) / m_resolutionY );
        }
        else
        {
            yScale = ( yWidth / xWidth ) / ( float( m_resolutionY ) / m_resolutionX );   
        }
    }

    computeHeatMap<<< grid_size, block_size >>>(
        vx_d, vy_d, w_d, flags_d, result_d, 
        xScale, yScale,
        vxMin, vxMax, vyMin, vyMax,
        m_resolutionX,
        m_resolutionY,
        m_numParticles );

    cudaThreadSynchronize();
    cudaMemcpy( result.data(), result_d, N * sizeof( float ), cudaMemcpyDeviceToHost );

    auto r = std::minmax_element( result.begin(), result.end() );
    range = std::pair< float, float >( *( r.first ), *( r.second ) );
    
    float mn = range.first;
    float wd = range.second - range.first;

    #pragma omp parallel for 
    for (int i = 0; i < N; ++i)
    {
        if( result[ i ] != 0 )
        {
            result[ i ] = ( result[ i ] - mn ) / wd;
        }
    }
}
