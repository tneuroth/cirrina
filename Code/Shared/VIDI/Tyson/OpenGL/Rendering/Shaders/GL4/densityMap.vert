#version 130



in vec2 ps;
in vec2 tc;

out vec2 uv;

void main(void)
{
    uv = tc;
    gl_Position = vec4( ps.x, ps.y, 0, 1 );
}

