
#include "OpenGL/Qt/Windows/MainWindow.hpp"
#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setStyleSheet( "background-color: rgb( 221, 221, 221); color : rgb( 80, 80, 80 ); font : 12px; font-family: Lucida Grande;" );

    qDebug() << QDir::currentPath();

    w.setWindowTitle( "" );
    //w.setWindowFlags( Qt::FramelessWindowHint);
    w.setMinimumSize( 1900 / w.devicePixelRatio(), 1060 / w.devicePixelRatio() );
    w.showMaximized();
    //w.showFullScreen();

    return a.exec();
}
