#ifndef MyOpenGLBuffer_H
#define MyOpenGLBuffer_H

#include <GL/glew.h>

class MyOpenGLBuffer
{
    GLuint m_id;
    std::uint64_t m_size;
    GLuint m_usagePattern;

public :

    MyOpenGLBuffer() : m_id( 0 ), m_size( 0 ), m_usagePattern( GL_STATIC_DRAW )
    {}

    void setUsagePattern( int pattern )
    {
        m_usagePattern = pattern;
    }

    void create()
    {
        glGenBuffers( 1, & m_id );
    }

    void bind()
    {
        glBindBuffer( GL_ARRAY_BUFFER, m_id );
    }

    void release()
    {
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
    }

    unsigned long long int size()
    {
        return m_size;
    }

    GLuint bufferId()
    {
        return m_id;
    }

    void allocate( long long int N_BYTES )
    {
        m_size = N_BYTES;
        glBufferData( GL_ARRAY_BUFFER, N_BYTES, NULL, m_usagePattern );
    }

    void write( unsigned long long int offset, const void *data, unsigned long long int count )
    {
        glBufferSubData( GL_ARRAY_BUFFER, offset, count, data );
    }

    void free()
    {
        glDeleteBuffers( 1, &m_id );
    }

    ~MyOpenGLBuffer()
    {
        free();
    }
};

#endif
