#-------------------------------------------------
#
# Project created by QtCreator 2015-03-06T14:29:34
#
#-------------------------------------------------

QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = BALEEN
TEMPLATE = app

#INCLUDEPATH += /usr/include/opencv2 
INCLUDEPATH += /usr/include/freetype2 

DEFINES += "VIDI_GL_MAJOR_VERSION=4"
DEFINES += "VIDI_CUDA_SUPPORT=1"

QMAKE_CC = gcc-4.9
QMAKE_CXX = g++-4.9
QMAKE_LINK = g++-4.9

LIBS += -L/usr/local/lib
LIBS += -fopenmp -lGLEW
LIBS += -L/home/user/hdf5/build/hdf5/lib/ -lhdf5

#LIBS += -lopencv_core
#LIBS += -lopencv_imgproc
#LIBS += -lopencv_highgui
#LIBS += -lopencv_ml
#LIBS += -lopencv_video
#LIBS += -lopencv_features2d
#LIBS += -lopencv_calib3d
#LIBS += -lopencv_objdetect
#LIBS += -lopencv_contrib
#LIBS += -lopencv_legacy
#LIBS += -lopencv_flann

LIBS += -lfreetype
LIBS += -lCGAL
LIBS += -lmpfr -lgmp
LIBS += -lboost_system -lboost_filesystem

LIBS += -L../Code/Shared/VIDI/Tyson/Cuda -lpstats
LIBS += -L /usr/local/cuda/lib64 -lnvrtc -lcuda

INCLUDEPATH += -I /usr/local/cuda/include
INCLUDEPATH += ../Code/Baleen
INCLUDEPATH += ../Code/Shared/ThirdParty
INCLUDEPATH += ../Code/Shared/VIDI/Tyson

INCLUDEPATH += /usr/local/cuda/include
INCLUDEPATH += /usr/local/include
INCLUDEPATH += /usr/local/include/freetype2
INCLUDEPATH += /home/user/hdf5/build/hdf5/include

QMAKE_CXXFLAGS += -std=c++11 -fopenmp -frounding-math #-ftime-report

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O0
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O3

# add the desired -O3 if not present
#QMAKE_CXXFLAGS_RELEASE += -O1
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_CXXFLAGS_RELEASE += -std=c++11
#QMAKE_CXXFLAGS_RELEASE += -ftree-vectorizer-verbose=5

SOURCES += \
    ../Code/Baleen/BALEEN.cpp \
    ../Code/Baleen/Main.cpp \
    ../Code/Baleen/Dialogues/HistogramDefinitionDialogue.cpp \
    ../Code/Baleen/Dialogues/ParticleFilterDialogue.cpp \
    ../Code/Baleen/Dialogues/PhasePlotDefinitionDialogue.cpp \
    ../Code/Baleen/Dialogues/TimePlotDefinitionDialogue.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/MainWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.cpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.cpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Dialogs/BaseDataDialogue.cpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressonwrapper.cpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/particledatamanager.cpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/MyAlgorithms.cpp \
    ../Code/Baleen/Dialogues/SubsetDialog.cpp \
    ../Code/Shared/VIDI/Tyson/Cuda/ComputeStatistics.cpp \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/Utils/TNTexture.cpp \
    ../Code/Shared/VIDI/Tyson/Repo/Cuda/FCCompute.cpp

HEADERS += \
    ../Code/Baleen/BALEEN.hpp \
    ../Code/Baleen/Dialogues/AdvancedParticleFilterDialogue.hpp \
    ../Code/Baleen/Dialogues/EditGridProjectionDialogue.hpp \
    ../Code/Baleen/Dialogues/HistogramDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/ParticleFilterDialogue.hpp \
    ../Code/Baleen/Dialogues/PhasePlotDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/TimePlotDefinitionDialogue.hpp \
    ../Code/Baleen/Dialogues/TimeSeriesDefinitionDialogue.hpp \
    ../Code/Shared/VIDI/Tyson/Types/Vec.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ComboWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/HelpLabelWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Plot2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/PressButtonWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ResizeWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Sampler.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/SliderWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/TimeLineWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/WeightHistogramWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Widget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/BoxShadowFrame.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Viewport/ViewPort.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Renderer.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/RenderParams.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Texture.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/Camera2D.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/Cameras/TrackBallCamera.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/MainWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/OpenGLWindow.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Qt/Windows/RenderWindow.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Dialogs/BaseDataDialogue.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/Expression.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/expressionsymbols.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/Calculator/ExpressionWrapper.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/Attributes.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/HistogramDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/LinkedViewDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/ParticleFilter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/TimeSeriesDefinition.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/DataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/DistributionDataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/MetaData.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/ParticleDataImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/CombImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/GTSImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/GTSSingleImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/SLACImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Importers/Plugins/XGCImporter.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/FieldInterpolator.h \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/ParticleInterpolator.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/DataSetManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/DistributionGridManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Managers/ParticleDataManager.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/VisualContextModel.hpp \
    ../Code/Shared/VIDI/Tyson/Data/MetadataParsers/ProjectParser.hpp \
    ../Code/Shared/VIDI/Tyson/Cuda/HistogramStackIso.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Histogram/GenerateGeometry.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Histogram/GradientUncertaintyCalculator.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/IsoContour/Contour.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/MyAlgorithms.hpp \
    ../Code/Shared/VIDI/Tyson/Algorithms/Standard/Util.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/WarningWidget.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ParticleBasedConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/DistributionBasedConfiguration.hpp \
    ../Code/Baleen/Dialogues/SavePresetDialog.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/BaseConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ProjectConfiguration.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/BasicDataInfo.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Configuration/ParameterKeys.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/RenderObject.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Rendering/AORenderScene.hpp \
    ../Code/Baleen/FrVolumeCache.hpp \
    ../Code/Baleen/TimePlotCache.hpp \
    ../Code/Baleen/TimeSeriesAggregator.hpp \
    ../Code/Baleen/PhasePlotCache.hpp \
    ../Code/Baleen/Dialogues/ColorMapDialog.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/FieldLineInterpolator.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Interpolators/TokamakInterpolate.hpp \
    ../Code/Baleen/Dialogues/DialogHelpers.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Phase2/PlotGridView.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Phase2/StatisticsView.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/DynamicGridLayout.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Phase2/ScrollBar.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Phase2/ParticleGroupingWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Phase2/SelectionView.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/SubsetDefinition.hpp \
    ../Code/Baleen/Dialogues/SubsetDialog.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/Phase2/ListViewWidget.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/ExpressionEdit.hpp \
    ../Code/Shared/VIDI/Tyson/Data/Definitions/Event.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/TrajectoryView.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/RecurrenceView.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/RangeView.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/Widgets/RecurrencePlot.hpp \
    GL4/shaders/LineMap2W.geom \
    GL4/shaders/LineMap2A.geom \
    ../Code/Shared/VIDI/Tyson/Cuda/ComputeStatistics.hpp \
    ../Code/Shared/VIDI/Tyson/Expressions/GenerateBooleanExpressionProgram.hpp \
    ../Code/Shared/VIDI/Tyson/OpenGL/MyGLBuffer.hpp \
    GL4/shaders/LineMap2W2.geom \
    GL4/shaders/MinMaxLineMap.geom \
    GL4/shaders/LineMapVariance.geom \
    GL4/shaders/WrapLine3.geom \
    GL4/shaders/WrapLine1.geom \
    GL4/shaders/WrapLine2.geom \
    GL4/shaders/WrapLine4.geom \
    GL4/shaders/WrapLineV.geom \
    GL4/shaders/TrajectoryWrap.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/TrajectoryAggregator.hpp \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/Utils/TNFrameBuffer.hpp \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/Utils/TNGLBuffer.hpp \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/Utils/TNTexture.hpp \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/Utils/TNTextureLayer.hpp \
    ../Code/Shared/VIDI/Tyson/Repo/Cuda/generateBooleanCombinationProgram.hpp \
    ../Code/Shared/VIDI/Tyson/Repo/Cuda/FCCompute.hpp

OTHER_FILES += \
    ../../Cuda/* \
    ../Platform/GL3/shaders/* \
    ../Platform/GL4/shaders/* \
    ../Platform/GL4/shaders/AO/*

FORMS += ../Code/Baleen/MainWindow.ui

DISTFILES += \
    ../Code/Shared/VIDI/Tyson/Cuda/libhistiso.so \
    ../Code/Shared/VIDI/Tyson/Cuda/compile.sh \
    ../Code/Shared/VIDI/Tyson/Cuda/HistogramStackIso.cu \
    GL4/shaders/LineMap.geom \
    GL4/shaders/LineMap.vert \
    GL4/shaders/LineMap.frag \
    GL4/shaders/ColorLegend.frag \
    GL4/shaders/Combination.frag \
    GL4/shaders/LineMap2.frag \
    GL4/shaders/heatMapRender2.frag \
    GL4/shaders/LineMapSummation.frag \
    GL4/shaders/LineMapVariance.frag \
    GL4/shaders/textureDivide.frag \
    ../Code/Shared/VIDI/Tyson/Cuda/ComputeStatsKernel.cu \
    GL4/shaders/flat2DSerialC.vert \
    GL4/shaders/flat2DSerialC.frag \
    ../Code/Shared/VIDI/Tyson/Cuda/ComputeStatsKernel_copy.cu \
    ../Code/Shared/VIDI/Tyson/Cuda/ComputeStatsKernel_copy1.cu \
    ../Code/Shared/VIDI/Tyson/Cuda/ComboCheck.cu \
    GL4/shaders/LineMapSum2.frag \
    GL4/shaders/difference.frag \
    GL4/shaders/MinMaxLineMap.frag \
    GL4/shaders/MinMaxLineMap.vert \
    GL4/shaders/TrajNoWrap.frag \
    GL4/shaders/TrajNoWrap.vert \
    GL4/shaders/CombVS2.vert \
    GL4/shaders/CombVS.vert \
    ../Code/Shared/VIDI/Tyson/Repo/Cuda/FCCompute.cu \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/WrapLineV.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/WrapLine4.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/WrapLine3.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/WrapLine2.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/WrapLine1.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/MinMaxLineMap.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMapVariance.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMap2W2.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMap2W.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMap2A.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMap.geom \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/textureDivide.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/MinMaxLineMap.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMapVariance.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMapSummation.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMapSum2.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/LineMap2.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/heatMapRender2.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/difference.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/MinMaxLineMap.vert \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/CombVS2.vert \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/CombVS.vert \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/quadTex.frag \
    ../Code/Shared/VIDI/Tyson/Repo/OpenGL/GLSL/quadTex.vert

