
#ifndef TIMESERIESDEFINITIONDIALOGUE
#define TIMESERIESDEFINITIONDIALOGUE

#include "Types/Vec.hpp"
#include "Data/Definitions/TimeSeriesDefinition.hpp"
#include "Dialogues/DialogHelpers.hpp"

#include <QDebug>
#include <QTableWidget>
#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>
#include <QHeaderView>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN {

class TimeSeriesDefinitionDialogue : public QDialog
{
    Q_OBJECT

    std::map< std::string, TimeSeriesDefinition > definedTimeSeriess;

    QWindow * m_parentWindow;

    std::string seriesKey;
    std::string m_status;

    QPushButton * acceptButton;
    QPushButton * cancelButton;

    QLabel *errorMessageLabel;

    // series Nameing

    QLabel    * seriesNameingLabel;
    QLineEdit * seriesNameingLineEdit;

    ///////////////////////////////////////////////////////////////////////

    // series info and description

    QVBoxLayout * seriesLayout;

    QLabel * seriesLabel;
    QTableWidget * seriesTable;

    QCheckBox * integrateCheckBox;
    QLabel * integrateLabel;

    QLabel * memoryUseLabel;

    QVBoxLayout * seriesDescriptionLayout;
    QLabel    * seriesDescriptionLabel;
    QTextEdit * seriesDescriptionBox;

    I2 activeCell;

    bool seriesValid;

    /////////////////////////////////////////////////////////////////////

    // All Avalailable Info

    QVBoxLayout * fullSeriesLayout;
    QLabel * fullSeriesLabel;
    QTableWidget * fullSeriesTable;

    QVBoxLayout * fullSeriesDescriptionLayout;
    QLabel    * fullSeriesDescriptionLabel;
    QTextEdit * fullSeriesDescriptionBox;

    //////////////////////////////////////////////////////////////////////

    std::vector< std::int32_t > simStepValues;
    std::vector< double > realTimeValues;

    /////////////////////////////////////////////////////////////////////

public:

    ~TimeSeriesDefinitionDialogue() { }

signals:

    void cancelDefinition();

    void finalizeDefinition(
        const std::string &,
        const std::map< std::string, TimeSeriesDefinition > & );

private slots:

    bool validateNumber( QString number )
    {
        bool isNumber;
        double value = number.toDouble( &isNumber );
        if( ! isNumber )
        {
            return false;
        }
        if( std::isinf( value ) || std::isnan( value ) )
        {
            return false;
        }
        return true;
    }

    bool validateInteger( QString number )
    {
        std::string nm = number.toStdString();

        if( ! validateNumber( number ) )
        {
            return false;
        }
        if( nm.find_first_of( "123456789" ) != 0 && nm != "0" )
        {
            return false;
        }
        if( nm.find_first_not_of( "1234567890Ee+" ) != nm.npos  )
        {
            return false;
        }
        return true;
    }

    void setActiveCell( int i, int j )
    {
        activeCell.i1 = i;
        activeCell.i2 = j;
    }

    void seriesEdited( int i, int j )
    {
        if( activeCell.i1 != i || activeCell.i2 != j )
        {
            return;
        }

        errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );

        QString text = seriesTable->item( i, j )->text();
        if( ! validateInteger( text ) )
        {
            errorMessageLabel->setText( "Time step must be an integer" );
            seriesValid = false;
            return;
        }
        else if ( text.toInt() < 0 )
        {
            errorMessageLabel->setText( "Time step must be non-negative" );
            seriesValid = false;
            return;
        }
        else
        {
            int val = text.toInt();
            int simStepStrideFull = fullSeriesTable->item( 1, 2 )->text().toInt();
            int lastSimStep = fullSeriesTable->item( 1, 1 )->text().toInt();
            int firstSimStep = fullSeriesTable->item( 1, 0 )->text().toInt();
            bool realTimeAvailable = fullSeriesTable->item( 0, 1 )->text() != "NA";

            if( i == 1 )
            {
                if( val > lastSimStep )
                {
                    errorMessageLabel->setText( "Time step is out of range" );
                    seriesValid = false;
                    return;
                }

                else if( val % simStepStrideFull != 0 || val < simStepValues[ 0 ] )
                {
                    errorMessageLabel->setText( "Time step not available" );
                    seriesValid = false;
                    return;
                }

                else if( j == 0 || j == 1 )
                {
                    int output = ( val - firstSimStep ) / simStepStrideFull;
                    seriesTable->item( 2, j )->setText( QString::number( output ) );
                    if ( realTimeAvailable )
                    {
                        double real = realTimeValues[ output ];
                        seriesTable->item( 0, j )->setText( QString::number( real ) );
                    }
                }
                else
                {
                    seriesTable->item( 2, j )->setText( QString::number( val / simStepStrideFull ) );
                    if ( realTimeAvailable )
                    {
                        double real = realTimeValues[ 1 ] - realTimeValues[ 0 ];
                        seriesTable->item( 0, j )->setText( QString::number( real ) );
                    }
                }
            }
            else if ( i == 2 )
            {
                if( val >=  ( int ) simStepValues.size() )
                {
                    errorMessageLabel->setText( "Time step is out of range" );
                    seriesValid = false;
                    return;
                }
                else if( j == 0 || j == 1 )
                {
                    seriesTable->item( 1, j )->setText( QString::number( simStepValues[ val ] ) );
                    if ( realTimeAvailable )
                    {
                        seriesTable->item( 0, j )->setText( QString::number( realTimeValues[ val ] ) );
                    }
                }
                else
                {
                    seriesTable->item( 1, j )->setText( QString::number( val * simStepStrideFull ) );
                    if ( realTimeAvailable )
                    {
                        double real = realTimeValues[ 1 ] - realTimeValues[ 0 ];
                        seriesTable->item( 0, j )->setText( QString::number( real ) );
                    }
                }
            }
        }

        QString firstVal = seriesTable->item(   1, 0 )->text();
        QString lastVal = seriesTable->item(    1, 1 )->text();
        QString strideVal = seriesTable->item(  1, 2 )->text();

        if( !( validateInteger( firstVal ) && validateInteger( lastVal ) &&  validateInteger( strideVal ) ) )
        {
            errorMessageLabel->setText( "Incomplete Definition" );
            seriesValid = false;
            return;
        }
        else if( firstVal.toInt() > lastVal.toInt() )
        {
            seriesValid = false;
            errorMessageLabel->setText( "Last cannot be greater than first" );
            return;
        }
        else if( ( lastVal.toInt() - firstVal.toInt() ) % strideVal.toInt() != 0 )
        {
            seriesValid = true;
            errorMessageLabel->setStyleSheet( "QLabel { color : orange; }" );
            errorMessageLabel->setText( "Warning: interval is not divisible by stride, series will be truncated" );
            return;
        }
        else
        {
            seriesValid = true;
            errorMessageLabel->setText( "Series is valid" );
            errorMessageLabel->setStyleSheet( "QLabel { color : green; }" );
        }
    }

    void finalize()
    {
        if( seriesValid == false )
        {
            return;
        }

        std::string name = seriesNameingLineEdit->text().toStdString();
        if( name.size() == 0 )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }
        if( ( m_status != "defined" ) && ( definedTimeSeriess.find( name ) != definedTimeSeriess.end() ) )
        {
            errorMessageLabel->setText( "Name/key already in use" );
            return;
        }
        if( name == "temp" )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }

        TimeSeriesDefinition definition;

        //////////////////////

        seriesKey = name;
        definition.name = seriesKey;

        definition.firstIdx = seriesTable->item( 2, 0 )->text().toInt();
        definition.lastIdx = seriesTable->item( 2, 1 )->text().toInt();
        definition.idxStride = seriesTable->item( 2, 2 )->text().toInt();
        definition.integrate = integrateCheckBox->isChecked();

        if( definition.integrate && definition.idxStride < 2 )
        {
            errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );
            errorMessageLabel->setText( "Integration requires stride > 1" );
            return;
        }

        if( m_status == "undefined" )
        {
            definedTimeSeriess.insert( { seriesKey, definition } );
        }
        else
        {
            definedTimeSeriess.find( seriesKey )->second = definition;
        }

        setStatus( "defined" );

        emit finalizeDefinition(
            seriesKey,
            definedTimeSeriess
        );

        close();
    }

    void cancel()
    {
        emit cancelDefinition();
        close();
    }

public:

    void update(
        const std::map< std::string, TimeSeriesDefinition > & seriesMap,
            const TimeSeriesDefinition & fullSeriesDefinition,
            const std::vector< double > & realTime,
            const std::vector< std::int32_t > & steps )
    {
        simStepValues = steps;
        realTimeValues = realTime;

        definedTimeSeriess = seriesMap;

        fullSeriesDescriptionBox->setText( fullSeriesDefinition.description.c_str() );

        if( (int) realTime.size() >= fullSeriesDefinition.lastIdx )
        {
            fullSeriesTable->item( 0, 0 )->setText( QString::number( realTime[ fullSeriesDefinition.firstIdx  ] ) );
            fullSeriesTable->item( 0, 1 )->setText( QString::number( realTime[ fullSeriesDefinition.lastIdx   ] ) );
            fullSeriesTable->item( 0, 2 )->setText( QString::number( realTime[ fullSeriesDefinition.firstIdx + 1 ] - realTime[ fullSeriesDefinition.firstIdx ] ) );
        }
        else
        {
            seriesTable->item( 0, 0 )->setText( "NA" );
            seriesTable->item( 0, 1 )->setText( "NA" );
            seriesTable->item( 0, 2 )->setText( "NA" );

            fullSeriesTable->item( 0, 0 )->setText( "NA" );
            fullSeriesTable->item( 0, 1 )->setText( "NA" );
            fullSeriesTable->item( 0, 2 )->setText( "NA" );
        }

        fullSeriesTable->item( 1, 0 )->setText( QString::number( steps[ fullSeriesDefinition.firstIdx  ] ) );
        fullSeriesTable->item( 1, 1 )->setText( QString::number( steps[ fullSeriesDefinition.lastIdx   ] ) );
        fullSeriesTable->item( 1, 2 )->setText( QString::number( steps[ fullSeriesDefinition.firstIdx + 1 ] - steps[ fullSeriesDefinition.firstIdx ]  ) );

        fullSeriesTable->item( 2, 0 )->setText( QString::number( fullSeriesDefinition.firstIdx ) );
        fullSeriesTable->item( 2, 1 )->setText( QString::number( fullSeriesDefinition.lastIdx ) );
        fullSeriesTable->item( 2, 2 )->setText( QString::number( 1 ) );\

        for (int i = 0; i < fullSeriesTable->rowCount(); ++i ) {
            for (int j = 0; j < fullSeriesTable->columnCount(); ++j ) {
                fullSeriesTable->item( i, j )->setFlags( fullSeriesTable->item( i, j )->flags() & ~Qt::ItemIsEditable );
            }
        }
    }

    void setStatus( const std::string & status )
    {
        m_status = status;
        if( status == "defined" )
        {
            acceptButton->setText( "update" );
            seriesNameingLineEdit->setEnabled( false );
        }
    }

    void fromDefinition( const TimeSeriesDefinition & def )
    {
        seriesDescriptionBox->setText( def.description.c_str() );
        setTableFromSeries( def, seriesTable, realTimeValues, simStepValues );
        seriesNameingLineEdit->setText( def.name.c_str() );
        integrateCheckBox->setChecked( def.integrate );
        seriesValid = true;
        errorMessageLabel->setText( "" );

        setStatus( "defined" );
    }

    TimeSeriesDefinitionDialogue( std::string _key, QWindow *parentWindow ) : QDialog( 0 ), m_parentWindow( parentWindow ), seriesKey( _key ), m_status( "undefined" )
    {
        seriesValid = false;

        setStyleSheet("  QWidget { background-color: rgb( 240, 240, 240 ); } QLineEdit { background-color: white; } QComboBox { background-color: white; } QListWidget { background-color: white; }");

        acceptButton = new QPushButton( "create" );
        cancelButton = new QPushButton( "cancel" );
        seriesNameingLabel = new QLabel( "series Name/Key" );
        seriesNameingLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70); font-weight: bold; font-size : 12pt;  }" );
        seriesNameingLineEdit = new QLineEdit;

        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

        ///////////////////////////////////////////////////////////////////////////////

        // Full Series Description

        fullSeriesDescriptionLayout = new QVBoxLayout;
        fullSeriesDescriptionLabel = new QLabel( "Full Series Description" );
        fullSeriesDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        fullSeriesDescriptionBox = new QTextEdit;
        fullSeriesDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );
        fullSeriesDescriptionBox->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );

        fullSeriesLabel = new QLabel( "Full Series" );
        fullSeriesLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        fullSeriesTable = new QTableWidget( 3, 3 );

        fullSeriesTable->setMinimumWidth( 400 );
        fullSeriesTable->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Minimum );
        fullSeriesTable->setHorizontalHeaderLabels(QString("First;Last;Stride").split(";"));
        fullSeriesTable->setVerticalHeaderLabels( QString("Simulation Time;Simulation T.S.;Output T.S.").split(";"));
        fullSeriesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

        for (int i = 0; i < fullSeriesTable->rowCount(); ++i ) {
            for (int j = 0; j < fullSeriesTable->columnCount(); ++j ) {
                fullSeriesTable->setItem( i, j, new QTableWidgetItem );
                QFlags< Qt::ItemFlag > flags = fullSeriesTable->item( i, j )->flags();
                fullSeriesTable->item( i, j )->setFlags( flags &= ~Qt::ItemIsEditable );
            }
        }

        QHBoxLayout * kjncewionoicejnoinw1e = new QHBoxLayout;
        kjncewionoicejnoinw1e->addWidget( fullSeriesDescriptionLabel );
        kjncewionoicejnoinw1e->addStretch();

        fullSeriesDescriptionLayout->addLayout( kjncewionoicejnoinw1e );
        fullSeriesDescriptionLayout->addWidget( fullSeriesDescriptionBox );

        fullSeriesLayout = new QVBoxLayout;

        fullSeriesLayout->addWidget( fullSeriesLabel );
        fullSeriesLayout->addWidget( fullSeriesTable );
        fullSeriesLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );
        fullSeriesLayout->addLayout( fullSeriesDescriptionLayout );

        ///////////////////////////////////////////////////////////////////////////////

        // Series Description

        seriesDescriptionLayout = new QVBoxLayout;
        seriesDescriptionLabel = new QLabel( "Selected Series Description" );
        seriesDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        seriesDescriptionBox = new QTextEdit;
        seriesDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );
        seriesDescriptionBox->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        seriesLabel = new QLabel( "Selected Series" );
        seriesLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        seriesLayout = new QVBoxLayout;

        QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
        kjncewionoicejnoinwe->addWidget( seriesDescriptionLabel );
        kjncewionoicejnoinwe->addStretch();

        errorMessageLabel = new QLabel;
        errorMessageLabel->setStyleSheet( "QLabel { color : red; }" );

        seriesDescriptionLayout->addLayout( kjncewionoicejnoinwe );
        seriesDescriptionLayout->addWidget( seriesDescriptionBox );

        seriesTable = new QTableWidget( 3, 3 );
        seriesTable->setMinimumWidth( 420 );
        seriesTable->setHorizontalHeaderLabels(QString("First;Last;Stride").split(";"));
        seriesTable->setVerticalHeaderLabels(QString("Simulation Time;Simulation T.S.;Output T.S.").split(";"));
        seriesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        seriesTable->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Minimum );

        for (int i = 0; i < fullSeriesTable->rowCount(); ++i ) {
            for (int j = 0; j < fullSeriesTable->columnCount(); ++j ) {
                seriesTable->setItem( i, j, new QTableWidgetItem );
                if( i == 1 || i == 2 )
                {
                    seriesTable->item( i, j )->setBackgroundColor( Qt::white );
                }
            }
        }

        connect( seriesTable, SIGNAL(cellClicked(int,int)), this, SLOT( setActiveCell( int, int ) ) );
        connect( seriesTable, SIGNAL(cellChanged(int,int)), this, SLOT( seriesEdited( int, int ) ) );

        QHBoxLayout * checkLayout = new QHBoxLayout;
        integrateCheckBox = new QCheckBox;
        integrateLabel = new QLabel( "Integrate Between Steps" );
        integrateLabel->setBuddy( integrateCheckBox );
        checkLayout->addWidget( integrateLabel );
        checkLayout->addWidget( integrateCheckBox );
        checkLayout->addStretch();

        memoryUseLabel = new QLabel( "Memory Use (up to) : 0 GB" );

        // series layout

        seriesLayout->addWidget( seriesLabel );
        seriesLayout->addWidget( seriesTable );
        seriesLayout->addLayout( checkLayout );
        //seriesLayout->addWidget( memoryUseLabel );
        seriesLayout->addSpacerItem( new QSpacerItem( 8, 8 ) );
        seriesLayout->addLayout( seriesDescriptionLayout );
        seriesLayout->addWidget( errorMessageLabel );

        QHBoxLayout *dll = new QHBoxLayout;
        dll->addWidget( seriesNameingLabel );
        dll->addWidget( seriesNameingLineEdit );
        dll->addWidget( cancelButton );
        dll->addWidget( acceptButton );
        seriesLayout->addLayout( dll );

        ////////////////////////////////////////////////

        // main layout

        QHBoxLayout * mainLayout = new QHBoxLayout;

        mainLayout->addLayout( fullSeriesLayout );
        mainLayout->addLayout( seriesLayout );

        setLayout( mainLayout );
    }
};

}

#endif // TIMESERIESDEFINITIONDIALOGUE


