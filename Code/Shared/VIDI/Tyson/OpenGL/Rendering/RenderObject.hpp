#ifndef RENDEROBJECT_HPP
#define RENDEROBJECT_HPP

#include "Types/Vec.hpp"

#include <memory>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include "OpenGL/Rendering/RenderParams.hpp"
#include <QMatrix4x4>

namespace TN
{

struct RenderObject
{
    enum
    {
        MULTI_COLOR_TRIANGLE_MESH_INDEXED,
        MULTI_COLOR_TRIANGLE_MESH,
        SINGLE_COLOR_TRIANGLE_MESH,
        LINES,
        POINTS,
        SERIAL
    };

    std::unique_ptr<QOpenGLBuffer> m_vbo;
    std::unique_ptr<QOpenGLVertexArrayObject> m_vao;
    std::unique_ptr<QOpenGLBuffer> m_ibo;

    int num_elements;
    int renderMode;
    unsigned int primitiveType;
    bool interleaved;
    bool castShadow;
    Vec3< float > color;
    float opacity;
    float primitiveWidth;
    QMatrix4x4 modelMatrix;
    Material m_material;

    void init()
    {
        m_vao = std::unique_ptr<QOpenGLVertexArrayObject>(new QOpenGLVertexArrayObject());
        m_vbo = std::unique_ptr<QOpenGLBuffer>(new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer ) );
        m_ibo = std::unique_ptr<QOpenGLBuffer>(new QOpenGLBuffer( QOpenGLBuffer::IndexBuffer ) );
        m_vbo->setUsagePattern( QOpenGLBuffer::DynamicCopy);
        m_vao->create();
        m_vbo->create();
        m_ibo->create();

        m_material.Ka = QVector3D( 1, 1, 1 );
        m_material.Kd = QVector3D( 1, 1, 1 );
        m_material.Ks = QVector3D( 1, 1, 1 );
        m_material.Ka_scale = 0.1;
        m_material.Kd_scale = 1.0;
        m_material.Ks_scale = 1.0;
    }

    void updateBuffers(
        const std::vector< Vec3< float > > & verts, unsigned int pType )
    {
        primitiveType = pType;
        renderMode = ( primitiveType == GL_LINE_STRIP || primitiveType == GL_LINES ) ? RenderObject::LINES :

        num_elements = verts.size();

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast<size_t> ( m_vbo->size() ) < verts.size() * sizeof( Vec3< float > ) )
        {
            m_vbo->allocate( verts.size() * sizeof( Vec3< float > ) );
        }

        m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
        m_vao->release();
    }

    void updateBuffers(
        const std::vector< Vec3< float > > & verts,
        const std::vector< Vec3< float > > & normals,
        const Vec3< float > & color )
    {
        renderMode = RenderObject::SINGLE_COLOR_TRIANGLE_MESH;
        num_elements = verts.size();

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast<size_t> ( m_vbo->size() ) < verts.size() * sizeof( Vec3< float > ) * 2  )
        {
            m_vbo->allocate( verts.size() * sizeof( Vec3< float > ) * 2 );
        }

        m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
        m_vbo->write( verts.size() * sizeof( Vec3< float > ),
                      normals.data(), normals.size() * sizeof( Vec3< float > ) );

        m_vao->release();
    }

    void updateBuffers(
        const std::vector< Vec3< float > > & verts,
        const std::vector< Vec3< float > > & normals,
        const std::vector< Vec3< float > > & colors )
    {
        num_elements = verts.size();

        renderMode = RenderObject::MULTI_COLOR_TRIANGLE_MESH;

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast<size_t> ( m_vbo->size() ) < verts.size() * sizeof( Vec3< float > ) * 3  )
        {
            m_vbo->allocate( verts.size() * sizeof( Vec3< float > ) * 3 );
        }

        m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
        m_vbo->write( verts.size() * sizeof( Vec3< float > ),
                      normals.data(), normals.size() * sizeof( Vec3< float > ) );
        m_vbo->write( verts.size() * sizeof( Vec3< float > ) * 2,
                      colors.data(), colors.size() * sizeof( Vec3< float > ) );

        m_vao->release();
    }

    void updateBuffers(
        const std::vector< Vec3< float > > & verts,
        const std::vector< Vec3< float > > & normals,
        const std::vector< I3 > & indices,
        const std::vector< Vec3< float > > & colors )
    {
        num_elements = verts.size();

        renderMode = RenderObject::MULTI_COLOR_TRIANGLE_MESH_INDEXED;

        m_vao->bind();
        m_vbo->bind();

        if ( static_cast<size_t> ( m_vbo->size() ) < verts.size() * sizeof( Vec3< float > ) * 3  )
        {
            m_vbo->allocate( verts.size() * sizeof( Vec3< float > ) * 3 );
        }

        m_vbo->write( 0, verts.data(), verts.size() * sizeof( Vec3< float > ) );
        m_vbo->write( verts.size() * sizeof( Vec3< float > ),
                      normals.data(), normals.size() * sizeof( Vec3< float > ) );
        m_vbo->write( verts.size() * sizeof( Vec3< float > ) * 2,
                      colors.data(), colors.size() * sizeof( Vec3< float > ) );

        m_ibo->bind();
        if( static_cast< size_t >( m_ibo->size() ) < indices.size() * sizeof( indices[0] ) )
        {
            m_ibo->allocate( indices.size() * sizeof( indices[0] ) );
        }

        m_ibo->write( 0, indices.data(), indices.size() * sizeof( indices[0] ) );
        m_vao->release();
    }

    RenderObject()
    {}

    virtual ~RenderObject()
    {}


};

}

#endif // RENDEROBJECT_HPP
