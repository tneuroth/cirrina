
#include "Types/Vec.hpp"
#include "Algorithms/Standard/Util.hpp"
#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/LinkedViewDefinition.hpp"
#include "Dialogues/PhasePlotDefinitionDialogue.hpp"
#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Expressions/Calculator/expressionsymbols.hpp"
#include "Expressions/Calculator/Expression.hpp"
#include "Dialogues/DialogHelpers.hpp"

#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QDebug>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN {

    PhasePlotDefinitionDialogue::~PhasePlotDefinitionDialogue() {}

    void PhasePlotDefinitionDialogue::plotDescriptionChanged()
    {

    }

    void PhasePlotDefinitionDialogue::changeSpaceDims( QString dimsStr )
    {
        int dims = std::stoi( dimsStr.toStdString() );
        if( dims > 1 )
        {
            spacec2->addWidget( spaceAttrCombo2 );
            spaceAttrCombo2->setVisible( true );
        }
        else
        {
            spacec2->removeWidget( spaceAttrCombo2 );
            spaceAttrCombo2->setVisible( false );
        }

        if( dims > 2 )
        {
            spacec2->addWidget( spaceAttrCombo3 );
            spaceAttrCombo3->setVisible( true );
        }
        else
        {
            spacec2->removeWidget( spaceAttrCombo3 );
            spaceAttrCombo3->setVisible( false );
        }
    }

    void PhasePlotDefinitionDialogue::finalize()
    {
        std::string name = plotNameingLineEdit->text().toStdString();
        if( name.size() == 0 )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }
        if( ( m_status != "defined" ) && ( definedPhasePlots.find( name ) != definedPhasePlots.end() ) )
        {
            errorMessageLabel->setText( "Name/key already in use" );
            return;
        }
        if( name == "temp" )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }

        PhasePlotDefinition definition;
        int spatialDims = std::stoi( spaceDimsCombo->currentText().toStdString() );

        if( spatialDims >= 1 )
        {
            std::string sKey = spaceAttrCombo1->currentText().toStdString();
            sKey = sKey.substr(0, sKey.find("="));
            if( sKey.back() == ' ' ){
                sKey.pop_back();
            }
            definition.attrs.push_back( sKey );
        }
        if( spatialDims >= 2 )
        {
            std::string sKey = spaceAttrCombo2->currentText().toStdString();
            sKey = sKey.substr(0, sKey.find("="));
            if( sKey.back() == ' ' ){
                sKey.pop_back();
            }
            definition.attrs.push_back( sKey );
        }
        if( spatialDims >= 3 )
        {
            std::string sKey = spaceAttrCombo3->currentText().toStdString();
            sKey = sKey.substr(0, sKey.find("="));
            if( sKey.back() == ' ' ){
                sKey.pop_back();
            }
            definition.description = plotDescriptionBox->toPlainText().toStdString();

            definition.attrs.push_back( sKey );
        }

        //////////////////////

        m_key = name;
        definition.name = m_key;
        definition.phasePlotSizeMode = scaleMode->currentIndex();

        if( m_status == "undefined" )
        {
            definedPhasePlots.insert( { m_key, definition } );
        }
        else
        {
            definedPhasePlots.find( m_key )->second = definition;
        }

        setStatus( "defined" );

        emit finalizeDefinition(
            m_key,
            definedPhasePlots,
            baseVariables,
            derivedVariables,
            baseConstants,
            customConstants
        );

        close();
    }

    void PhasePlotDefinitionDialogue::cancel()
    {
        emit cancelDefinition();
        close();
    }

    void PhasePlotDefinitionDialogue::update(
        const std::map< std::string, PhasePlotDefinition > & _histMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap )
    {
        baseVariables = _baseVarMap;
        derivedVariables = _derivedVarMap;
        customConstants = _customConstMap;
        baseConstants = _baseConstMap;
        definedPhasePlots = _histMap;

        attributeList->setStyleSheet( "QListWidget { color : green }" );
        constantList->setStyleSheet( "QListWidget { color : green }" );

        constantList->clear();
        userConstantList->clear();
        derivedAttributeList->clear();
        attributeList->clear();

        spaceDimsCombo->clear();

        spaceAttrCombo1->clear();
        spaceAttrCombo2->clear();
        spaceAttrCombo3->clear();

        for( auto & baseConst : _baseConstMap )
        {
            std::string _nm = baseConst.first + " = " + to_string_with_precision( baseConst.second.value(), 10 );
            constantList->addItem( _nm.c_str() );
        }

        for( auto & custConst : _customConstMap )
        {
            std::string _nm = custConst.second.name()
                    + " = " + custConst.second.expression();

            if( ! m_expressionCalculator->parser().validateNumber( custConst.second.expression() ) )
            {
                _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( custConst.second.expression(), _baseConstMap, _customConstMap ), 10 );
            }

            userConstantList->addItem( _nm.c_str() );
        }

        for( auto & dv : _derivedVarMap )
        {
            std::string _nm = dv.second.name() + " = " + dv.second.expression();
            derivedAttributeList->addItem( _nm.c_str() );

            spaceAttrCombo1->addItem( _nm.c_str() );
            spaceAttrCombo2->addItem( _nm.c_str() );
            spaceAttrCombo3->addItem( _nm.c_str() );
        }

        for( auto & baseVarE : baseVariables )
        {
            std::string _nm = baseVarE.first.c_str();

            attributeList->addItem( _nm.c_str() );

            spaceAttrCombo1->addItem( _nm.c_str() );
            spaceAttrCombo2->addItem( _nm.c_str() );
            spaceAttrCombo3->addItem( _nm.c_str() );
        }

        spaceDimsCombo->addItem( "2" );
        //spaceDimsCombo->addItem( "3" );

        spaceDimsCombo->setCurrentIndex( 0 );

        connect( spaceDimsCombo, SIGNAL(currentTextChanged(QString) ), this, SLOT( changeSpaceDims( QString ) ) );

        spaceDimsCombo->setEnabled( true );
    }

    void PhasePlotDefinitionDialogue::fromDefinition( const PhasePlotDefinition & def )
    {
        plotDescriptionBox->setText( def.description.c_str() );
        if( def.attrs.size() > 0 )
        {
            int index = getAttrComboIndex( def.attrs[ 0 ], spaceAttrCombo1 );
            if( index >= 0 )
            {
                spaceAttrCombo1->setCurrentIndex( index );
            }
        }

        plotDescriptionBox->setText( def.description.c_str() );

        if( def.attrs.size() > 1 )
        {
            int index = getAttrComboIndex( def.attrs[ 1 ], spaceAttrCombo2 );
            if( index >= 0 )
            {
                spaceAttrCombo2->setCurrentIndex( index );
            }
        }

        plotDescriptionBox->setText( def.description.c_str() );
        if( def.attrs.size() > 2 )
        {
            int index = getAttrComboIndex( def.attrs[ 2 ], spaceAttrCombo3 );
            if( index >= 0 )
            {
                spaceAttrCombo3->setCurrentIndex( index );
            }
        }

        // 1 is not an option, only 2->0 or 3->1
        if( spaceDimsCombo->count() > def.attrs.size() - 2 )
        {
            spaceDimsCombo->setCurrentIndex( def.attrs.size() - 2 );
        }
        if( def.phasePlotSizeMode == PhasePlotSizeMode::PRESERVE_ASPECT )
        {
            scaleMode->setCurrentIndex( 1 );
        }
        else
        {
            scaleMode->setCurrentIndex( 0 );
        }

        plotNameingLineEdit->setText( def.name.c_str() );
        errorMessageLabel->setText( "" );

        setStatus( "defined" );
    }

    void PhasePlotDefinitionDialogue::setStatus( const std::string & status )
    {
        m_status = status;
        if( status == "defined" )
        {
            acceptButton->setText( "update" );
            plotNameingLineEdit->setEnabled( false );
        }
    }

    void PhasePlotDefinitionDialogue::attemptDeriveVariable( )
    {
        BaseDataDialogue::attemptDeriveVariable();

        std::string nm = deriveNameEdit->text().toStdString();
        std::string expr = deriveEdit->text().toStdString();

        spaceAttrCombo1->addItem( ( nm + " = " + expr ).c_str() );
        spaceAttrCombo2->addItem( ( nm + " = " + expr ).c_str() );
        spaceAttrCombo3->addItem( ( nm + " = " + expr ).c_str() );

        emit updateAttributes( baseVariables, derivedVariables, baseConstants, customConstants  );
    }


    PhasePlotDefinitionDialogue::PhasePlotDefinitionDialogue( std::string _key, QWindow *parentWindow ) : BaseDataDialogue( _key, parentWindow )
    {

        PlotAttributeSection = new QVBoxLayout;
        ConstraintSection    = new QHBoxLayout;

        acceptButton = new QPushButton( "create" );
        cancelButton = new QPushButton( "cancel" );
        plotNameingLabel = new QLabel( "Plot Name/Key" );
        plotNameingLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        plotNameingLineEdit = new QLineEdit;

        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

        ///////////////////////////////////////////////////////////////////////////

        // space

        spaceAttrLabel = new QLabel( "Attribute" );
        spaceDimsLabel = new QLabel( "Dimensionality" );

        QLabel * spatialRegionLabel = new QLabel(  "Plot Definition" );
        spatialRegionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );

        QHBoxLayout * frabskjdkjdskl = new QHBoxLayout;
        frabskjdkjdskl->addWidget(  spatialRegionLabel );
        frabskjdkjdskl->addStretch();

        spaceDimsCombo = new QComboBox;
        spaceDimsCombo->setMinimumWidth( 100 );

        QHBoxLayout * frabsdsdsfl2 = new QHBoxLayout;
        frabsdsdsfl2->addWidget( spaceDimsLabel );
        frabsdsdsfl2->addWidget( spaceDimsCombo );
        frabsdsdsfl2->addStretch();

        spaceAttrCombo1 = new QComboBox;
        spaceAttrCombo2 = new QComboBox;
        spaceAttrCombo3 = new QComboBox;

        spaceAttrCombo1->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
        spaceAttrCombo2->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );
        spaceAttrCombo3->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );

        spacec2 = new QVBoxLayout;
        spacec3 = new QVBoxLayout;
        spaceLayout = new QHBoxLayout;

        spacec2->addWidget( spaceAttrLabel  );
        spacec2->addWidget( spaceAttrCombo1 );
        spacec2->addWidget( spaceAttrCombo2 );

        spaceLayout->addLayout( spacec2 );
        spaceLayout->addLayout( spacec3 );

        spaceDimsLabel->setBuddy( spaceDimsCombo );

        ///////////////////////////////////////////////////////////////////////////

        // Scale Mode

        scaleMode = new QComboBox;
        scaleMode->addItem( "square" );
        scaleMode->addItem( "preserve aspect ratio" );
        scaleModeLabel = new QLabel( "Plot Size" );

        QHBoxLayout * scaleModeLayout = new QHBoxLayout;
        scaleModeLayout->addWidget( scaleModeLabel );
        scaleModeLabel->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
        scaleModeLayout->addWidget( scaleMode );

        //////////////////////////////////////////////////////////////////////////////

        PlotAttributeSection->addLayout( frabskjdkjdskl );
        PlotAttributeSection->addLayout( frabsdsdsfl2 );
        PlotAttributeSection->addLayout( spaceLayout );
        PlotAttributeSection->addLayout( scaleModeLayout );

        // Description

        plotDescriptionLayout = new QVBoxLayout;
        plotDescriptionLabel = new QLabel( "Plot Description" );
        plotDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        plotDescriptionBox = new QTextEdit;
        plotDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );

        QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
        kjncewionoicejnoinwe->addWidget( plotDescriptionLabel );
        kjncewionoicejnoinwe->addStretch();

        plotDescriptionLayout->addLayout( kjncewionoicejnoinwe );
        plotDescriptionLayout->addWidget( plotDescriptionBox );
        plotDescriptionLayout->addWidget( errorMessageLabel );

        //////////////////////////////////////////////////////////////////////////////

        m_mainLayout->addLayout( PlotAttributeSection );
        m_mainLayout->addLayout( plotDescriptionLayout );

        QHBoxLayout *dll = new QHBoxLayout;
        dll->addWidget( plotNameingLabel );
        dll->addWidget( plotNameingLineEdit );
        dll->addWidget( cancelButton );
        dll->addWidget( acceptButton );
        m_mainLayout->addLayout( dll );
    }
}


