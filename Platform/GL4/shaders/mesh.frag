#version 130



const int SUN   = 0;
const int POINT = 1;
const int SPOT  = 2;

const int MAX_LIGHTS = 8;

struct Material {

    vec3 Ka;
    vec3 Kd;
    vec3 Ks;

    float Ka_scale;
    float Kd_scale;
    float Ks_scale;

    bool overrideAmbient;
    float alpha;
    float opacity;
};

struct LightSource {
    int type;
    vec3 direction;
    vec3 Ia;
    vec3 Id;
    vec3 Is;
    vec3 position;
    float at1;
    float at2;
    float cutOff;
    float exponent;
};

uniform bool useColorAttr;
uniform Material M;
uniform LightSource L[ MAX_LIGHTS ];
uniform bool enabled[ MAX_LIGHTS ];

in vec3 normal;
in vec3 eye;
in vec3 pos;
in vec3 cla;

out vec4 fragColor;

void main() {

    vec3 cl = vec3(0,0,0);
    vec3 tc = vec3(0,0,0);

    vec3 Nm = normalize( normal );
    vec3 Vm = normalize( eye );

    for ( int i = 0; i < MAX_LIGHTS; ++i ) {

        if ( enabled[i] ) {

            vec3 lightDirection;
            float attenuation;

            //lightDirection = normalize( vec3( .1, 0.5, 0.3 ) );
            lightDirection = normalize( L[i].direction );

            attenuation    = 1.0;

            if( useColorAttr ) {
                cl += cla*M.Ka_scale;
            }
            else if ( M.overrideAmbient ) {
                cl += M.Ka;
            }
            else {
                cl += M.Ka*L[i].Ia;
            }

            float angle = dot( Nm, lightDirection );

            if ( angle > 0.0) {
                if( useColorAttr ) {
                    vec3 Rm = normalize( reflect( lightDirection, Nm ) );
                    cl += attenuation * ( cla * angle * M.Kd_scale + ( M.Ks_scale ) * pow( max( dot( Rm, Vm ), 0.0 ), M.alpha ) * L[i].Is );
                }
                else {
                    vec3 Rm = normalize( reflect( lightDirection, Nm ) );
                    cl += attenuation * (  M.Kd * angle * L[i].Id +  0.1 * pow( max( dot( Rm, Vm ), 0 ), M.alpha ) * L[i].Is );
                }
            }
        }
    }

    fragColor = vec4( min( cl, vec3( 1.0 ) ), M.opacity );
}
