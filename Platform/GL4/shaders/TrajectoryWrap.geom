#version 400



#define PIo2 1.57079632679

layout(lines) in;
layout(line_strip, max_vertices=8) out;

uniform sampler1D angleToNormalization;
uniform bool distanceWeighted;
uniform float width;
uniform float height;

in vData
{
    flat int select;
} vertices[];

uniform bool wrap;
uniform float wrap1;
uniform float wrap2;

void wrapLineY( float w1, float w2, vec4 p1, vec4 p2 )
{
    float y1 = p1.y;
    float y2 = p2.y;

    if( y2 < y1 )
    {
        float temp = y2;
        y2 = y1;
        y1 = temp;

        vec4 temp2 = p2;
        p2 = p1;
        p1 = temp2;
    }

    float wrapDistY = ( y1 - w1 ) + ( w2 - y2 );
    float strtDistY =   y2 - y1;

    if( wrapDistY < strtDistY )
    {
        vec4 p1out;
        vec4 p2out;

        float r = ( w2 - y2 ) / wrapDistY;
        float u = p2.x + r * ( p1.x - p2.x );

        vec4 tmp = p2;
        tmp.y = w2;
        tmp.x = u;

        gl_Position = p2;
        EmitVertex();

        gl_Position = tmp;
        EmitVertex();

        EndPrimitive();

        // 0 __|--------0

        tmp.y = w1;
        gl_Position = tmp;
        EmitVertex();

        gl_Position = p1;
        EmitVertex();
        EndPrimitive();
    }
    else
    {
        gl_Position = p1;
        EmitVertex();

        gl_Position = p2;
        EmitVertex();

        EndPrimitive();
    }
}

void wrapLineX( float w1, float w2, vec4 p1, vec4 p2 )
{
    float x1 = p1.x;
    float x2 = p2.x;

    if( x2 < x1 )
    {
        float temp = x2;
        x2 = x1;
        x1 = temp;

        vec4 temp2 = p2;
        p2 = p1;
        p1 = temp2;
    }

    float wrapDistX = ( x1 - w1 ) + ( w2 - x2 );
    float strtDistX =   x2 - x1;

    if( wrapDistX < strtDistX )
    {
        vec4 p1out;
        vec4 p2out;

        float r = ( w2 - x2 ) / wrapDistX;
        float u = p2.y + r * ( p1.y - p2.y );

        vec4 tmp = p2;
        tmp.x = w2;
        tmp.y = u;

        wrapLineY( w1, w2, p2, tmp );

//        gl_Position = p2;
//        EmitVertex();

//        gl_Position = tmp;
//        EmitVertex();

//        EndPrimitive();

        // 0 __|--------0

        tmp.x = w1;

        wrapLineY( w1, w2, tmp, p1 );

//        gl_Position = tmp;
//        EmitVertex();

//        gl_Position = p1;
//        EmitVertex();

//        EndPrimitive();
    }
    else
    {
        wrapLineY( w1, w2, p1, p2 );
//        gl_Position = gl_in[0].gl_Position;
//        EmitVertex();

//        gl_Position = gl_in[1].gl_Position;
//        EmitVertex();

//        EndPrimitive();
    }
}

void main( void )
{
    int s = vertices[ 0 ].select * vertices[ 1 ].select;

    float x1 = gl_in[0].gl_Position.x;
    float x2 = gl_in[1].gl_Position.x;

    if( bool( s ) )
    {
        if( wrap )
        {
            wrapLineX( wrap1, wrap2, gl_in[0].gl_Position, gl_in[1].gl_Position );
        }
        else
        {
            gl_Position = gl_in[0].gl_Position;
            EmitVertex();

            gl_Position = gl_in[1].gl_Position;
            EmitVertex();
        }
    }
}
