
#ifndef EXPRESSIONWRAPPER_HPP
#define EXPRESSIONWRAPPER_HPP

#include <memory>

namespace TN {

template< class T >
struct ExpressionParser;

class ExpressionWrapper
{
    std::unique_ptr< ExpressionParser< double > > m_parser;

public:

    ExpressionWrapper();
    const ExpressionParser< double > & parser() const;
};

}

#endif // EXPRESSIONWRAPPER_HPP
