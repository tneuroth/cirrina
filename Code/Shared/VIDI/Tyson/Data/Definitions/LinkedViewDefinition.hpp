
#ifndef TN_LINKED_VIEW_DEFINITION
#define TN_LINKED_VIEW_DEFINITION

#include "Types/Vec.hpp"
#include "Data/Definitions/ParticleFilter.hpp"

#include <QJsonObject>
#include <QJsonArray>

#include <cstdint>
#include <string>
#include <vector>

namespace TN {

struct LinkedViewType
{
    enum
    {
        SELECTION_CONTEXT,
        PHASE_PLOT,
        FIELD_PLOT
    };
};

struct PlotViewMode
{
    enum
    {
        SCATTER_PLOT,
        TRAJECTORIES
    };
};

struct LinkedViewDefinition
{
    std::string name;
    std::string description;

    QJsonObject toJSON() const
    {
        QJsonObject obj;
        obj.insert( "name", name.c_str() );
        obj.insert( "description", description.c_str() );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        name = obj[ "name" ].toString().toStdString();
        description = obj[ "description" ].toString().toStdString();
    }

    virtual ~LinkedViewDefinition() {}
};

struct PlotDefinition : LinkedViewDefinition
{
    virtual ~PlotDefinition() {}
};

struct PhasePlotSizeMode
{
    enum
    {
        FILL_SPACE,
        PRESERVE_ASPECT,
        ADJUSTABLE,
        CUSTOM
    };
};

struct TimePlotMode
{
    enum
    {
        MEAN,
        MEAN_SQUARED,
        DENISITY
    };
};

struct TimePlotDefinition : public PlotDefinition
{
    std::string attr;

    bool operator==( const TimePlotDefinition & other ) const
    {
        return other.attr == attr;
    }

    QJsonObject toJSON() const
    {
        QJsonObject obj = LinkedViewDefinition::toJSON();
        obj.insert( "attr", attr.c_str() );
        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        LinkedViewDefinition::fromJSON( obj );
        attr = obj[ "attr" ].toString().toStdString();
    }
};

struct PhasePlotDefinition : public PlotDefinition
{
    int phasePlotSizeMode;
    int phasePlotTypeId;

    int rowsReq;
    int colsReq;

    std::vector< std::string > attrs;
    //std::vector< double > scaling;

    QJsonObject toJSON() const
    {
        QJsonObject obj = LinkedViewDefinition::toJSON();
        obj.insert( "sizeMode", phasePlotSizeMode );
        obj.insert( "typeMode", phasePlotTypeId );

        obj.insert( "cols", colsReq );
        obj.insert( "rows", rowsReq );

        QJsonArray attrArray;
        QJsonArray attrScalingArray;

        //qDebug() << attrs.size() << " vs " << scaling.size();

        for( unsigned i = 0; i < attrs.size(); ++i )
        {
            attrArray.append( attrs[ i ].c_str() );
            //attrScalingArray.append( scaling[ i ] );
        }

        obj.insert( "attrs", attrArray );
        //obj.insert( "scaling", attrScalingArray );

        return obj;
    }

    void fromJSON( const QJsonObject & obj )
    {
        LinkedViewDefinition::fromJSON( obj );

        phasePlotSizeMode = obj[ "sizeMode" ].toInt();
        phasePlotTypeId = obj[ "typeMode" ].toInt();
        rowsReq = obj[ "rows" ].toInt();
        colsReq = obj[ "cols" ].toInt();

        attrs.clear();
        foreach ( QJsonValue v, obj[ "attrs" ].toArray() )
        {
            attrs.push_back( v.toString().toStdString() );
        }
    }
};

}


#endif // TN_LINKED_VIEW_DEFINITION

