#ifndef GTSCONFIGURATION_CPP
#define GTSCONFIGURATION_CPP

#include "configuration.hpp"

namespace TN {

class GTSConfiguration : public Configuration
{
    std::map< std::string, std::map< std::string, PredefinedHistogram > >
    m_predefinedHistograms;

// TODO
//    std::map< std::string, std::map< std::string, GridPointFilter > >
//    m_gridPointFilters;

//    std::map< std::string, std::map< std::string, GridPlot > >
//    m_gridPlots;

//    std::map< std::string, std::map< std::string, m_gridTimePlot > >
//    m_gridTimePlot;

// derivations from grid point scalars?
// from distributions themselves ??
// just general calculations from what has been defined ??


public:

    virtual std::string toJSON() override
    {

    }

    ~GTSConfiguration() {}
};

}

#endif // GTSCONFIGURATION_CPP
