
#ifndef BALEEN_H
#define BALEEN_H

#include "OpenGL/Qt/Windows/RenderWindow.hpp"
#include "OpenGL/Rendering/AORenderScene.hpp"
#include "OpenGL/Rendering/Texture.hpp"
#include "Data/Definitions/LinkedViewDefinition.hpp"
#include "OpenGL/Widgets/ComboWidget.hpp"
#include "OpenGL/Widgets/PressButtonWidget.hpp"
#include "OpenGL/Widgets/Sampler.hpp"
#include "OpenGL/Widgets/TimeLineWidget.hpp"
#include "OpenGL/Widgets/ResizeWidget.hpp"
#include "OpenGL/Widgets/WeightHistogramWidget.hpp"
#include "OpenGL/Widgets/HelpLabelWidget.hpp"
#include "OpenGL/Widgets/Viewport/BoxShadowFrame.hpp"
#include "Types/Vec.hpp"
#include "OpenGL/Widgets/WarningWidget.hpp"
#include "OpenGL/Widgets/Viewport/ViewPort.hpp"
#include "OpenGL/Rendering/Cameras/Camera2D.hpp"
#include "OpenGL/Rendering/Cameras/TrackBallCamera.hpp"
#include "OpenGL/Rendering/RenderParams.hpp"
#include "Expressions/Dialogs/BaseDataDialogue.hpp"
#include "Data/Configuration/BaseConfiguration.hpp"
#include "Data/Configuration/ProjectConfiguration.hpp"
#include "Dialogues/SavePresetDialog.hpp"
#include "Data/Configuration/BasicDataInfo.hpp"
#include "FrVolumeCache.hpp"
#include "TimePlotCache.hpp"
#include "PhasePlotCache.hpp"
#include "Dialogues/ColorMapDialog.hpp"

#include "TimePlotCache.hpp"

#include "OpenGL/Widgets/Phase2/StatisticsView.hpp"
#include "OpenGL/Widgets/Phase2/PlotGridView.hpp"
#include "OpenGL/Widgets/Phase2/ParticleGroupingWidget.hpp"
#include "OpenGL/Widgets/Phase2/SelectionView.hpp"
#include "OpenGL/Widgets/ExpressionEdit.hpp"

#include "OpenGL/Widgets/RecurrenceView.hpp"
#include "OpenGL/Widgets/TrajectoryView.hpp"
#include "OpenGL/Widgets/RangeView.hpp"

#include "OpenGL/MyGLBuffer.hpp"
#include "Cuda/ComputeStatistics.hpp"

#include "Repo/OpenGL/Utils/TNGLBuffer.hpp"
#include "Repo/OpenGL/TrajectoryAggregator.hpp"

#include <memory>
#include <string>
#include <map>
#include <algorithm>
#include <array>
#include <atomic>

class QKeyEvent;
class QMouseEvent;
class QGLFramebufferObject;
class QOpenGLShaderProgram;

namespace TN
{

template< class T >
class DataSetManager;

//class CubeMarcher;
class LinkedViewDefinition;
class EditGridProjectionDialogue;
class HistogramDefinitionDialogue;
class PhasePlotDefinitionDialogue;
class TimePlotDefinitionDialogue;
class TimeSeriesDefinitionDialogue;
class ParticleFilterDialogue;
class SubsetDialog;
class ColorMapDialog;

class Renderer;

class BALEEN : public RenderWindow
{
    Q_OBJECT

    //////////////////////////////////////////////////////////////

    TNR::TrajectoryAggregator aggregator;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const std::string RELATIVE_PATH_PREFIX = "../";

    // Data

    std::unique_ptr< DataSetManager<float> > m_dataSetManager;
    int m_populationDataTypeEnum;

    int m_selectedTimeStep;
    int m_numTimeSteps;

    // What's needed to find the data on disk, to know which importer to use, and to know where it came from
    // The importer is reposonsible for knowing details about what's in the data and how to load it
    BasicDataInfo m_basicDataInfo;

    std::pair< Vec2< float >, Vec2< float > > m_contextBoundingBox;

    void updatePartType();

    // configurations for loading and saving, associated with a dataset

    /// Dataset name->( preset name, preset )
    std::map< std::string, std::map< std::string, ProjectConfiguration > > m_savedConfigurations;

    void queryConfigurations();
    void afterSelectedDataset();
    void afterSelectedPreset();

    void datasetSwitched();
    void presetSwitched();

    // CUDA

    StatsComputer m_statsComputer;

    // State

    bool m_datasetSelected;
    bool m_presetSelected;
    bool m_timeSeriesDefined;
    bool m_sessionInitiated;

    QPointF m_previousMousePosition;
    bool m_mouseIsPressed;

    bool m_haveInitializedOpenGL;
    bool m_haveInitializedData;
    bool m_recalculateFullTimePlot;

    Vec2< float > m_upperLeftWindowCornerWorldSpace;
    Vec2< float > m_upperRightWindowCornerWorldSpace;
    Vec2< float > m_lowerLeftWindowCornerWorldSpace;
    Vec2< float > m_lowerRightWindowCornerWorldSpace;

    bool m_uiLayoutEdited;
    bool m_uiLayoutInitialized;
    bool m_filterEdited;
    bool m_dataInitializedFirst;

    std::string m_previousFilter;

    bool m_updateBins;

    PressButton m_plotAllSampleButton;

    bool m_timePlotDialogOpen;
    bool m_phasePlotDialogOpen;
    bool m_timeSeriesDialogOpen;
    bool m_filterDialogOpen;
    bool m_subsetDialogOpen;

    QVector4D m_combinationColor;

    PressButton m_expandButton;
    PressButton m_reselectButton;
    ComboWidget m_toolCombo;

    void clearProject();

    ////////////////////////////////////////

    void renderSelection(
        const QMatrix4x4 & M,
        const std::string & x,
        const std::string & y,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QVector4D & color1,
        const QVector4D & color2,
        bool enhanced = false,
        bool points = false,
        bool combo = false,
        bool thin = false );

    void computeRecurrence( const std::vector< std::string > & attrs, const std::vector< float > & ranges, const size_t PIDX, std::vector< float > & distances );

    bool m_updateSelection;
    Vec2< float > getSelectionRadius( const Vec2< float > & relativeRadius );
    Vec2< float > getRelativeSelectionRadius( const Vec2< float > & sz, float r );

    Vec2< float > getSelectionCenter( const Vec2< float > & relativeCenter );
    Vec2< float > getRelativeSelectionCenter( const Vec2< float > & SelectionPosition, const Vec2< float > & plotPosition, const Vec2< float > & plotSize  );

    void renderSelectionRadius( const Vec2< float > & pos, const Vec2< float > & scale );

    float m_hoverRadius;
    float m_selectionRadius;
    Vec2< float > m_selectionPosition;
    std::string m_selectedPlot;
    std::vector< std::uint64_t > m_particleSelectionSample;
    std::vector< std::uint64_t > m_particleSelection;

    std::vector< GLsizei > m_particleSelectionCounts;
    std::vector< GLint > m_particleSelectionFirsts;

    bool m_addNewSubset;

//    ////////////////////////////////////////////////////////
//    std::vector< GLsizei > m_particleSelectionStartCounts;
//    std::vector<   GLint > m_particleSelectionStartFirsts;
//    std::vector< GLsizei > m_particleSelectionEndCounts;
//    std::vector<   GLint > m_particleSelectionEndFirsts;

//    std::vector< GLsizei > m_particleSelectionEndCounts;
//    std::vector<   GLint > m_particleSelectionEndFirsts;
//    ////////////////////////////////////////////////////////

    std::vector< GLsizei > m_particleSelectionSampleCounts;
    std::vector< GLsizei > m_particleSelectionSubSampleCounts;

    // Subset Color Codes

    std::vector< std::string > m_subsetSymbols;
    std::vector< Vec3< float > > m_subsetColorCodes;
    bool m_recomputeSubsets;

    std::string m_selectedExpression;

    // Phase Plot Textures

    // 0 is timeline, then order of the plots in layout are ids
    PhasePlotCache m_phasePlotCache;
    std::map< std::string, int > m_vboIndexes;
    std::vector< float > m_transferStorage;

    std::vector< float > m_pixelDensityMap;
    float m_phasePlotBufferMax;
    std::vector< float > m_phasePlotPixelBuffer;

    /////////////////////////////////////////////////////

    TNR::TextureLayer m_countAll;
    TNR::TextureLayer m_countComb;

    TNR::TextureLayer m_sumAll;
    TNR::TextureLayer m_sumComb;

    /////////////////////////////////////////////////////

    TNR::Texture1D3  m_phasePlotTFAll;
    TNR::Texture1D3  m_phasePlotTFRegion;
    TNR::Texture1D3  m_phasePlotTFRegionBin;
    TNR::Texture1D3  m_phasePlotTFWeightBin;

    bool m_phasePlotsValid;

    QGLFramebufferObject *m_fbo;
    const int TF_SIZE = 256;

    void loadTF( const std::string & path, TNR::Texture1D3 & tex );
    void computeTF( const std::vector< Vec3< float > > & colors, TNR::Texture1D3 & tf );

    ////////////////////////////////////////

    // trajectory data in proper order

    std::map< std::string, std::unique_ptr< std::vector< float > > > m_trajectoryData;
    std::vector< GLint > m_phaseBufferFirsts;
    std::vector<GLsizei> m_phaseBufferCounts;
    std::vector< int > m_subsetBitmasks;
    std::vector< float > m_timePoints;

    ////////////////////////////////

    void renderFocusContext(
        TNR::TextureLayer & layer1,
        TNR::TextureLayer & layer2,
        TNR::Texture1D3 & tf1,
        TNR::Texture1D3 & tf2,
        Vec2< float > & position,
        Vec2< float > & size,
        const Vec2< float > & scissor,
        int which,
        bool localize,
        const Vec2< float > & localCenter,
        const Vec2< float > & localRadius,
        const Vec2< float > & localRange );

    /////////////////////////////////

    // number / path density
    void computeContextLayers2(
        PhasePlotCache & cache,
        std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        bool withTangentialMotion,
        size_t N,
        bool wrap = false );

    // weighted density
    void computeContextLayerWeightedDensity(
        PhasePlotCache & cache,
        std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        size_t N_TEST,
        bool wrap = false );

    // max
    void computeContextLayersMax(
        PhasePlotCache & cache,
        std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        size_t N_TEST,
        bool wrap = false );

    // min
    void computeContextLayersMin(
        PhasePlotCache & cache,
        std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        size_t N_TEST,
        bool wrap = false );

    // range
    void computeContextLayersRange(
        PhasePlotCache & cache,
        std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        size_t N_TEST,
        bool wrap = false );

    // * mean
    void computeContextLayersMean(
        PhasePlotCache & cache,
        std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        size_t N_TEST,
        bool wrap = false );

    // * variance
    void computeContextLayersVariance(
        PhasePlotCache & cache,
        std::pair< TNR::TextureLayer, TNR::TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        size_t N_TEST,
        bool wrap = false );

    ///////////////////////////////////////////////////////

    void getSelectionFromPolaritySwap( const std::string & attr );


    void getSelectionFromEvent();
    void getSelection();
    void sampleSelection( size_t maxSampleSize = 100 );

    std::vector< std::pair< TNR::TextureLayer, TNR::TextureLayer > > m_phaseTextureLayers;
    std::pair< TNR::TextureLayer, TNR::TextureLayer > m_timeSeriesLayers;

    ////////////////////////////////////////

    // Dialogues and Definitions

    void cleanDialogues();

    // Dialogs

    ColorMapDialog m_histColorMapDialog;
    ColorMapDialog m_plotColorMapDialog;

    SavePresetDialog m_savePresetDialog;

    std::map< std::string, std::map< std::string, PhasePlotDefinitionDialogue * > >
    m_phasePlotDefinitionDialogues;

    std::map< std::string, std::map< std::string, TimePlotDefinitionDialogue * > >
    m_timePlotDefinitionDialogues;

    std::map< std::string, std::map< std::string, TimeSeriesDefinitionDialogue * > >
    m_timeSeriesDefinitionDialogues;

    std::map< std::string, std::map< std::string, ParticleFilterDialogue * > >
    m_particleFilterDialogues;

    std::map< std::string, std::map< std::string, SubsetDialog * > >
    m_subsetDialogs;

    std::map< std::string, std::pair< Vec2< float >, Vec2< float > > > m_phasePlotPosSz;

    std::set< std::string > m_recurrenceSpace;
    ProjectConfiguration m_currentConfigurations;

    ///////////////////////////////////////

    //std::unique_ptr< TNR::TNGLBuffer > m_phaseVBO;
    //std::unique_ptr< QOpenGLVertexArrayObject > m_phaseVAO;

    TNR::TNGLBuffer * m_phaseVBO;
    QOpenGLVertexArrayObject * m_phaseVAO;

    std::unique_ptr< TNR::TNGLBuffer > m_texVBO;
    std::unique_ptr< QOpenGLVertexArrayObject > m_texVAO;

    ///////////////////////////////////////////////////////////////

    // UI dynamics, Widgets & Viewports

    const double EQUATION_HEIGHT = 45;

    const double MENU_HEIGHT   = 40;
    const double MENU_SPACING  = 8;
    const double BUTTON_HEIGHT = ( MENU_HEIGHT - MENU_SPACING * 2 );

    const double TEXT_ROW_HEIGHT  = 25.0;
    const double LABEL_ROW_HEIGHT = 32.0;
    const double DIVIDER_WIDTH    = 10.0;
    const double BUTTON_SPACING   =  4.0;

    const double SAMPLER_MIN_WIDTH    = 1000.0;
    const double SAMPLER_MIN_HEIGHT   =  500.0;
    const double PHASEVIEW_MIN_HEIGHT =  440.0;
    const double PHASEVIEW_MIN_WIDTH  =  540.0;
    const double DETAILVIEW_MIN_WIDTH =  310.0;
    const double DETAIVIEW_MAX_WIDTH  =  650.0;
    const double TIMELINE_MIN_HEIGHT  =  210.0;
    const double ISOVIEW_MIN_HEIGHT   =  210.0;

    const int PANEL_HT = 70;
    const int HIST_PAD   = 54;
    const int HIST_MARGIN = 8;

    ResizeWidget m_resizeWidgetA;
    ResizeWidget m_resizeWidgetB;
    ResizeWidget m_resizeWidgetD;

    /*
    ------------------------
    |    |          |      |
    |    |A         |B     |
    |    |          |      |
    |    |_______D_ |______|
    |    | ________________|
    ------------------------
    */

    RecurrenceView m_recurrenceView;
    TrajectoryView m_trajectoryView;
    int m_trajectoryViewNCols;
    int m_trajectoryViewNRows;
    long int m_selectedParticle;
    Vec4 m_selectedParticleColor;

    RangeView m_rangeView;
    //StatsView m_statsView;

    PlotGridView m_plotGridView;
    SelectionView m_selectionView;
    ParticleGroupingWidget m_subsettingWidget;
    ParticleEventWidget m_eventWidget;

    Widget m_dataSetInfoWidget;

    //ViewPort m_histogramViewPort;


    ViewPort m_timeLinePlotViewPort;
    TimeLineWidget m_timeLineWidget;
    TimePlotCache m_timePlotCache;

    TexturedPressButton m_timeAggregateRegionEquation;
    TexturedPressButton m_timeAggregateAllEquation;
    TexturedPressButton m_timeAggregateEquationDefinitionsAll;
    TexturedPressButton m_timeAggregateEquationDefinitions;

    PressButton m_selectionToSubsetButton;

    PressButton m_saveCombinationButton;
    std::vector< PressButton > m_combinationSelectorList;
    std::vector< std::string > m_combinationList;
    int m_selectedCombination;
    void renderCombinationList();

    PressButton m_emissionEventButton;
    PressButton m_absorptionEventButton;

    PressButton m_saveProjectButton;
    PressButton m_loadProjectFileButton;

    PressButton m_saveButton;
    PressButton m_closeButton;
    PressButton m_initiateSessionButton;

    std::vector< PressButton > m_removeSubsetButtons;
    std::vector< PressButton > m_editSubsetButtons;

    ComboWidget m_datasetCombo;
    ComboWidget m_configurationCombo;
    ComboWidget m_particleCombo;
    ComboWidget m_subsetCombo;

    TexturedPressButton m_zoomInPhaseViewButton;
    TexturedPressButton m_zoomOutPhaseViewButton;
    TexturedPressButton m_gridOverlayButton;

    ComboWidget m_phasePlotBlendingCombo;
    ComboWidget m_phasePlotInterpolationCombo;
    ComboWidget m_phasePlotModeCombo;
    ComboWidget m_phasePlotCombo;

    ComboWidget m_phasePlotSelectionModeCombo;
    ComboWidget m_phasePlotRenderModeCombo;

    TexturedPressButton m_definePhasePlotButton;
    PressButton m_deletePhasePlotButton;

    PressButton m_defineTimeSeriesButton;
    PressButton m_definePlotButton;

    PressButton m_selectFromTrajectoriesButton;
    PressButton m_selectFromEventsButton;
    PressButton m_selectionRefinementButton;

    PressButton m_selectTintButton;
    PressButton m_selectFocusButton;

    ComboWidget m_timeSeriesCombo;

    ComboWidget m_timePlotCombo;
    ComboWidget m_timePlotModeCombo;

    PressButton m_editTimePlotButton;
    PressButton m_editPhasePlotButton;
    PressButton m_editTimeSeriesButton;

    PressButton m_timeTextureSelector;
    PressButton m_timeSubsetStatsSelector;
    PressButton m_timeSubsetCombStatsSelector;

    std::vector< ComboWidget * > m_comboRefs;

    HelpLabelWidget m_helpButtonPlotGrid;
    HelpLabelWidget m_helpButtonData;
    //HelpLabelWidget m_helpButtonStatsView;
    HelpLabelWidget m_helpButtonTimeline;
    //HelpLabelWidget m_helpButtonResultsView;
    HelpLabelWidget m_helpButtonSubsetting;
    HelpLabelWidget m_helpButtonSelection;

    HelpLabelWidget m_helpButtonReccu;
    HelpLabelWidget m_helpButtonTraje;
    HelpLabelWidget m_helpButtonRange;
    HelpLabelWidget m_helpButtonEvent;

    QVector4D m_emissionEventColor;
    QVector4D m_absorptionEventColor;

    std::vector< HelpLabelWidget * > m_helpLabelWidgetRefs;

    PressButton m_defineSubsetButton;
    PressButton m_defineFilterButton;
    PressButton m_editFilterButton;
    ComboWidget m_filterCombo;

    WarningWidget m_warningWidget;
    PressButton m_inspectWarningsButton;
    ComboWidget m_errorRemovalCombo;

    ExpressionEdit m_combinationExpression;

    ///////////////////////////////////////////////////////

    SliderWidget m_resolutionSlider;
    SliderWidget m_thresholdSlider;

    SliderWidget m_recurrenceSlider;
    PressButton  m_showRecurrenceButton;
    PressButton  m_recurrenceSettingsButton;
    PressButton  m_thresholdRecurrenceButton;

    ///////////////////////////////////////////////////////
    // Rendering

    /////////////////////////////////////

    std::unique_ptr< Renderer > m_renderer;

    Camera2D camera;

    float m_contextOpacity;
    Vec3< float > m_contextColor;
    Vec3< float > m_textColor;
    QVector4D m_dividerColor;

    std::unique_ptr< QOpenGLShaderProgram > cartesianProg;
    std::unique_ptr< QOpenGLShaderProgram > cartesianProg3;
    std::unique_ptr< QOpenGLShaderProgram > colorMapProg3;
    std::unique_ptr< QOpenGLShaderProgram > points3DProg;
    std::unique_ptr< QOpenGLShaderProgram > tubePathProgram;
    std::unique_ptr< QOpenGLShaderProgram > simpleProg;
    std::unique_ptr< QOpenGLShaderProgram > simpleColorProg;
    std::unique_ptr< QOpenGLShaderProgram > prog3D;
    std::unique_ptr< QOpenGLShaderProgram > flatProg;
    std::unique_ptr< QOpenGLShaderProgram > flatProgSerial;
    std::unique_ptr< QOpenGLShaderProgram > trajectoryProgramWraparound;
    std::unique_ptr< QOpenGLShaderProgram > trajectoryProgramNoWrap;
    std::unique_ptr< QOpenGLShaderProgram > textProg;
    std::unique_ptr< QOpenGLShaderProgram > ucProg;
    std::unique_ptr< QOpenGLShaderProgram > colorLegendProg;
    std::unique_ptr< QOpenGLShaderProgram > flatMeshProg;
    std::unique_ptr< QOpenGLShaderProgram > timeStackProg;
    std::unique_ptr< QOpenGLShaderProgram > heatMapGenProg;
    std::unique_ptr< QOpenGLShaderProgram > focusContextProg;
    std::unique_ptr< QOpenGLShaderProgram > pointHeatMapGenProg;
    std::unique_ptr< QOpenGLShaderProgram > heatMapRenderProg;
    std::unique_ptr< QOpenGLShaderProgram > binDiagnosticProgram;
    std::unique_ptr< QOpenGLShaderProgram > lineMapProgram;

    std::unique_ptr< QOpenGLShaderProgram > m_densityProgramWrapAround;
    std::unique_ptr< QOpenGLShaderProgram > m_densityProgramWeightedWrapAround;
    std::unique_ptr< QOpenGLShaderProgram > m_summationProgramWrapAround;
    std::unique_ptr< QOpenGLShaderProgram > m_minMaxProgramWrapAround;
    std::unique_ptr< QOpenGLShaderProgram > wrapProgramV;

    std::unique_ptr< QOpenGLShaderProgram > m_densityProgram;
    std::unique_ptr< QOpenGLShaderProgram > m_summationProgram;
    std::unique_ptr< QOpenGLShaderProgram > m_densityProgramWeighted;
    std::unique_ptr< QOpenGLShaderProgram > m_varianceProgram;
    std::unique_ptr< QOpenGLShaderProgram > m_minMaxProgram;
    std::unique_ptr< QOpenGLShaderProgram > textureDivideProgram;

    std::unique_ptr< QOpenGLShaderProgram > trajectoryProgram;
    std::unique_ptr< QOpenGLShaderProgram > differenceProgram;

    std::map<
    std::string,
        std::pair< std::uint8_t, std::unique_ptr< QOpenGLShaderProgram > > >
        m_shaderMap;

    struct ShaderType
    {
        enum
        {
            VERTEX_SHADER = 1,
            FRAGMENT_SHADER = 2,
            GEOMETRY_SHADER = 4,
            COMPUTE_SHADER = 8
        };
    };

private:

    double scaledHeight()
    {
        return height() * devicePixelRatio();
    }

    double scaledWidth()
    {
        return width() * devicePixelRatio();
    }

public:

    BALEEN();

    ~BALEEN();

    void writeVelocity();

    bool setPreset( const std::string & preset );
    bool configure( const std::string & dataset, const std::string & preset );

    //! OpenGL initialization
    void compileShaders();
    void initGL();

    /////////////////////////////////////////////////////////////////////////

    void resetSamplerCam();

    /////////////////////////////////////////////////////////////////////////

    void clearSystem();
    int getStepOffset();

    void regenerateCombinationShader( const std::string & expression );

    void constructLineIndicators(
        const Vec2< float > & linePos,
        const RangeSelector & rangeSelector,
        std::vector< TN::Vec2< float > > & lines,
        std::vector< TN::Vec2< float > > & triangles );

    void preparePhaseTrajectories(
        const std::string & ptype,
        const TimeSeriesDefinition & tsrs,
        const PhasePlotDefinition & plotDef );

    void computePhaseTrajectories(
        PhasePlotCache & cache,
        TNR::TextureLayer & result,
        int dataType,
        const std::string & ptype,
        const TimeSeriesDefinition & tsrs,
        const PhasePlotDefinition & plotDef,
        int weightType,
        const std::string & weightKey );

    QMatrix4x4 computePhaseTexture(
        PhasePlotCache & cache,
        TNR::TextureLayer & result,
        int viewMode,
        const std::string & ptype,
        const PhasePlotDefinition & plotDef,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const std::string & interpolation,
        const std::string & blending,
        const std::string & what,
        int cellSelection
    );

    QMatrix4x4 computePhaseTexture(
        PhasePlotCache & cache,
        TNR::TextureLayer & result,
        ParticleDataManager< float > & dataManager,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        int mode = 0
    );

    void renderPhasePrimitives(
        int viewMode,
        const std::string & interpolation,
        const std::string & ptype,
        const PhasePlotDefinition & plotDef,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        float width,
        float height,
        float x,
        float y,
        const QVector4D & color );

//    void renderPhaseTexture(
//        TNR::TextureLayer & layer,
//        TNR::Texture1D3 & tf,
//        bool fade,
//        Vec2< float > & position,
//        Vec2< float > & size,
//        bool outline,
//        bool onlyOutline,
//        const Vec2< float > & scissor  );

    void renderExpressionEdit( ExpressionEdit & edit );

    void renderEventList();

    void renderExpressionSelector(
        const std::string & expression,
        PressButton & button );

    void computeTemporalPlotsAll(
        LineData & data,
        const std::string & ptype,
        const TimePlotDefinition & timePlotDef );

    void computeTemporalPlots(
        LineData & data,
        const std::string & ptype,
        const TimePlotDefinition & timePlotDef,
        const SpacePartitioningDefinition & spaceParititioing );

    void renderTimeSeriesPlots2(
        const std::vector< float > & data,
        const Vec2< float > & RANGE,
        const int N_STEPS,
        const int CURRENT_STEP,
        const int N_SETS,
        bool plotAll );

    TimeSeriesDefinition * getTimeSeries(
        const std::string & ptype );

    TimePlotDefinition * getTimePlotDefinition( const std::string & partType );

    void renderStatsView();
    void renderRecurrenceView();
    void renderTrajectoryView();
    void renderRangeView();

    float renderList(
        const std::vector< std::string > & itemText,
        const std::vector< Vec3< float > > & itemColorCodes,
        const std::vector< std::string > & itemSymbols,
        const Vec2< float > & pos,
        float width,
        float itemHeight );

    void renderComboItems( const ComboWidget & combo );

    void renderTimeSeriesInfo( const std::string & ptype );
    void renderTimeLineAnnotation();

    void selectionToSubset();

    void renderPhasePlotGrid(
        const Vec2< float > & plotSize,
        const Vec2< float > & plotPosition,
        const Vec2< float > & scissor,
        const std::string & xLabel,
        const std::string & yLabel,
        const Vec2< double > & xRange,
        const Vec2< double > & yRange,
        bool withText,
        bool withClear );

    std::string getWarningSummary( int populationDataTypeEnum, const std::string & ptype );
    std::uint8_t getErrorRemovalOption();

    void recalculateUiLayout();

    bool parseBasicDataInfo( const std::string & dataset, BasicDataInfo & result );

    virtual void render();

    virtual void wheelEvent( QWheelEvent *e );
    virtual void mouseDoubleClickEvent( QMouseEvent *e );
    virtual void mouseMoveEvent( QMouseEvent *e );
    virtual void resizeEvent( QResizeEvent *e );
    virtual void keyPressEvent( QKeyEvent *e );
    virtual void mousePressEvent( QMouseEvent *e );
    virtual void mouseReleaseEvent( QMouseEvent *e );

    void updateSampler( BaseHistDefinition * def );

    void updateDerivations( const std::string & ptype, const std::map< std::string, DerivedVariable > & derived );
    void updateCustomConstants( const std::map< std::string, CustomConstant > & cc );
    void updateBaseVariables( const std::string & ptype, const std::map< std::string, BaseVariable > & variables );
    void updateBaseConstants( const std::map< std::string, BaseConstant > & cc );

    std::set< std::string > getActiveVariables( const std::string & ptype );
    std::set< std::string > m_activePhaseSpace;
    std::vector< SubsetDefinition > m_subsetDefinitions;

public slots:

    void saveProject( bool asDefault );
    void cancelTimeSeriesDefinition();
    void cancelPhasePlotDefinition();
    void cancelTimePlotDefinition();
    void cancelParticleFilterDefinition();
    void cancelParticleSubsetDefinition();

    void updateUserDataDescription( const std::string & );

    void updateAttributes(
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &
    );

    void finalizePhasePlotDefinition(
        const std::string &,
        const std::map<std::string, PhasePlotDefinition > &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeTimePlotDefinition(
        const std::string &,
        const std::map<std::string, TimePlotDefinition > &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeTimeSeriesDefinition(
        const std::string &,
        const std::map<std::string, TimeSeriesDefinition> & );

    void finalizeParticleFilterDefinition(
        const std::string &,
        const std::map<std::string, ParticleFilter> &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeParticleSubsetDefinition(
        const std::string &,
        const std::map<std::string, SubsetDefinition> &,
        const std::map<std::string, BaseVariable> &,
        const std::map<std::string, DerivedVariable> &,
        const std::map<std::string, BaseConstant> &,
        const std::map<std::string, CustomConstant> &);

    void finalizeProjection(
        std::vector< std::string >,
        bool );
};

class BALEENWidget : public QWidget
{
public:
    BALEENWidget(QWidget *parent) : QWidget(parent)
    {
        renWin = new BALEEN();

        QWidget *widget = QWidget::createWindowContainer(renWin);
        QHBoxLayout *layout = new QHBoxLayout(this);

        layout->setMargin(0);
        layout->addWidget(widget);
    }

    ~BALEENWidget() {}

    virtual BALEEN *GetRenderWindow()
    {
        return renWin;
    }



private:
    BALEEN *renWin;
};
}

#endif  // BALEEN_H
