


#ifndef TN_LIST_VIEW_HPP
#define TN_LIST_VIEW_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "OpenGL/Widgets/Phase2/ScrollBar.hpp"
#include "Types/Vec.hpp"


namespace TN
{

class ListView : public Widget
{
    ScrollBar m_scrollBar;
    std::vector< std::string > m_items;

public:

    const int ITEM_HEIGHT = 24;

    ListView() : Widget(), m_scrollBar( ScrollBar::HORIZONTEL ), rows( 2 )
    {}

    ScrollBar & scrollBar()
    {
        return m_scrollBar;
    }

    //! in screen space ( pixels )
    virtual void setSize( float width, float height ) override
    {
        Widget::setSize( width, height );
        m_scrollBar.setSize( width, SCROLL_BAR_HEIGHT );
        m_scrollBar.setJumpLength( ( height - SCROLL_BAR_HEIGHT ) / (double) rows );
        m_scrollBar.setAreaLength( width );
    }

    void zoomIn()
    {
        --rows;
        rows = std::max( rows, 1 );
    }

    void zoomOut()
    {
        ++rows;
    }

    void setAreaLength( float l )
    {
        m_scrollBar.setAreaLength( l );
    }
    void setJumpLength( float l )
    {
        m_scrollBar.setJumpLength( l );
    }

    //! in screen space ( pixels )
    virtual void setPosition( double x, double y ) override
    {
        Widget::setPosition( x, y );
        m_scrollBar.setPosition( x, y );
    }
};

}

#endif

