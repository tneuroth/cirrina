#version 130
uniform vec4 color;
in bool remove;
out vec4 fragColor;

void main(void)
{
    if( remove )
    {
        discard;
    }
    else
    {
        fragColor = color;
    }
}
