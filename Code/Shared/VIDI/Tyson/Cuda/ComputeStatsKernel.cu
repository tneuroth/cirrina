#define ALL_MASK 1
#define COM_MASK 2
#define ALL_SEL_MASK 4
#define COM_SEL_MASK 8

__device__ __constant__
int lookup[ 31 ] = {
    1,2,4,8,16,32,64,128,256,512,1024,2048,4096,
    8192,16384,32768,65536,131072,262144,524288,
    1048576,2097152,4194304,8388608,16777216,
    33554432,67108864,134217728,268435456,536870912,
    1073741824
};

__device__
bool testSet( int s, int b )
{
    return ( ( b & lookup[ s ] ) && ( b & 1 ) );
}

extern "C" __global__
void computeRadialSelection(
    float * data,
    int   * bitmasks,
    float radiusX,
    float radiusY,
    float centerX,
    float centerY,
    const unsigned long N_PARTICLES,
    const unsigned long N_STEPS,
    const unsigned  int V1_IDX,
    const unsigned  int V2_IDX )
{
//    const long int GLOBAL_IDX = blockIdx.x * blockDim.x + threadIdx.x;

//    const int pIDX = GLOBAL_IDX / N_STEPS;
//    const int tIDX = GLOBAL_IDX % N_STEPS;

//    if( pIDX >= N_PARTICLES )
//    {
//        return;
//    }

//    const bool IN_COMB = ( bitmasks[ GLOBAL_IDX ] & COM_MASK ) != 0;

//    bitmasks[ GLOBAL_IDX ] &= ~COM_SEL_MASK;
//    bitmasks[ GLOBAL_IDX ] &= ~ALL_SEL_MASK;

//    float x = data[ V1_IDX * ( N_STEPS * N_PARTICLES ) + tIDX * N_PARTICLES + pIDX ];
//    float y = data[ V2_IDX * ( N_STEPS * N_PARTICLES ) + tIDX * N_PARTICLES + pIDX ];

//    const float relativeRadius =
//        sqrtf(
//            powf( ( x - centerX ) / radiusX, 2.0 ) +
//            powf( ( y - centerY ) / radiusY, 2.0 ) );

//    if( relativeRadius <= 1.f )
//    {
//        bitmasks[ GLOBAL_IDX ] |= ALL_SEL_MASK;

//        if( IN_COMB )
//        {
//            bitmasks[ GLOBAL_IDX ] |= COM_SEL_MASK;
//        }
//    }
}

extern "C" __global__
void divide(
    float * A,
    float * B,
    int N )
{
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= N )
    {
        return;
    }

    if( B[ idx ] != 0 )
    {
        A[ idx ] /= B[ idx ];
    }
}

extern "C" __global__
void computeTimeSeriesSumsAndCounts(
    float * data,
    int   * bitmasks,
    float * sums,
    float * counts,
    const unsigned long NPT,
    const unsigned long N_STEPS,
    const unsigned int V_OFF )
{
    const long int GLOBAL_IDX = blockIdx.x * blockDim.x + threadIdx.x;

    if( GLOBAL_IDX >= NPT )
    {
        return;
    }

    const int pIDX = GLOBAL_IDX / N_STEPS;
    const int tIDX = GLOBAL_IDX % N_STEPS;

    int b = bitmasks[ GLOBAL_IDX ];

    if( !( b & 1 ) )
    {
        return;
    }

    const float value = data[ V_OFF + pIDX * N_STEPS + tIDX ];
    atomicAdd( & sums[   tIDX ], value );
    atomicAdd( & counts[ tIDX ], 1.0 );

    if( b & 2 )
    {
        atomicAdd( & sums[   N_STEPS + tIDX ], value );
        atomicAdd( & counts[ N_STEPS + tIDX ], 1.0 );
    }
}

extern "C" __global__
void computeCurrentHistograms(
    float * data,
    int   * bitmasks,
    float * ranges,
    float * histograms,
    const unsigned long N_PARTICLES,
    const unsigned long N_STEPS,
    const unsigned long STEP,
    const unsigned int N_VARS,
    const unsigned int N_BINS )
{
    const int pIDX = blockIdx.x * blockDim.x + threadIdx.x;

    if( pIDX >= N_PARTICLES )
    {
        return;
    }

    int b = bitmasks[ N_STEPS * pIDX + STEP ];

    for( unsigned int sIDX = 0; sIDX < 2; ++sIDX )
    {
        if( ! testSet( sIDX, b ) )
        {
            continue;
        }
        for( unsigned int vIDX = 0; vIDX < N_VARS; ++vIDX )
        {
            const float MN = ranges[ vIDX*2     ];
            const float MX = ranges[ vIDX*2 + 1 ];
            const float WDTH = MX - MN;

            const float value = data[ vIDX * N_STEPS * N_PARTICLES + pIDX * N_STEPS + STEP ];

            if( value < MN || value > MX )
            {
                continue;
            }

            int bIDX = umin( ( unsigned int ) ( ( value - MN ) / WDTH * N_BINS ), ( unsigned int )( N_BINS - 1 ) );

            atomicAdd( & histograms[ sIDX * N_VARS * N_BINS + vIDX * N_BINS + bIDX ], 1.0 );
        }
    }
}

extern "C" __global__
void computeRecurrenceHotSpots(
    float * data,
    int   * bitmasks,
    float * resultA,
    float * resultC,
    float * densityA,
    float * densityC,
    int * phaseIndices,
    float * phaseScaling,
    int * distanceType,
    int phaseDim,
    int weightIndex,
    const unsigned long N_STEPS,
    const unsigned long N_PARTICLES,
    const int varIdx1,
    const int varIdx2,
    const int heatmapRows,
    const int heatmapCols,
    const float heatmapX1Min,
    const float heatmapX1Max,
    const float heatmapX2Min,
    const float heatmapX2Max )
{
    const long int GLOBAL_IDX = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned long NPT = N_STEPS * N_PARTICLES;

    if( GLOBAL_IDX >= NPT )
    {
        return;
    }

    const int pIDX = GLOBAL_IDX / N_STEPS;
    const int tIDX = GLOBAL_IDX % N_STEPS;

    int b = bitmasks[ GLOBAL_IDX ];

    if( !( b & 1 ) )
    {
        return;
    }

    const bool C = ( b & 2 ) != 0;

    float x1 = data[ varIdx1 * NPT + pIDX * N_STEPS + tIDX ];
    float x2 = data[ varIdx2 * NPT + pIDX * N_STEPS + tIDX ];

    int r = floor( ( ( x2 - heatmapX2Min ) / ( heatmapX2Max - heatmapX2Min ) ) * heatmapRows );
    int c = floor( ( ( x1 - heatmapX1Min ) / ( heatmapX1Max - heatmapX1Min ) ) * heatmapCols );

    if( r > heatmapRows || c > heatmapCols )
    {
        return;
    }

    const unsigned long heatMapIdx = r * heatmapCols + c;

    for( int j = tIDX + N_STEPS / 10; j < N_STEPS; ++j )
    {
        float d = 0;
        for( int v = 0; v < phaseDim; ++v )
        {
            int vIdx = phaseIndices[ v ];
            float scale = phaseScaling[ v ];
            const float v1 = data[ vIdx * NPT + pIDX * N_STEPS + tIDX ];
            const float v2 = data[ vIdx * NPT + pIDX * N_STEPS + j    ];
            float dv;

            if( distanceType[ v ] == 0 )
            {
                dv = scale * ( v2 - v1 );
            }
            else
            {
                dv = atan2( sin( v1 - v2 ), cos( v1 - v2 ) );
            }

            d += dv*dv;
        }

        const float weight = data[ weightIndex * NPT + pIDX * N_STEPS + tIDX ];
        d = sqrt( d );

        if( d < 0.01 )
        {
           atomicAdd( & resultA[ heatMapIdx ], 1 );
           if( C )
           {
               atomicAdd( & resultC[ heatMapIdx ], 1 );
           }
        }
    }

    atomicAdd( & densityA[ heatMapIdx ], 1.0 );
    if( C )
    {
        atomicAdd( & densityC[ heatMapIdx ], 1.0 );
    }
}


/*
 * How predictable is the time evolution?
 *   - Velocity Distributions
 *   - analogous to phase space reconstruction, information theory
 *   - size of sufficiently small time delay relationship to noise and Lyapunov Exponent analogy
 */

extern "C" __global__
void computeLocalTemporalStatisticalInfluenceGradients()
{

}

/*
 * How does a small perturbence in the physical space affect the stochastic time evolution
 *   - Velocity Distributions
 *   - analogous to FTLE
 */

extern "C" __global__
void computeLocalSpatioemporalStatisticalCoherenceGradient()
{

}

/*
 * Finding Curves of "Minimal Entropy"
 *  - Stochastic Partitioning of the Phase Space into Statistically Invariant Structures
 */


extern "C" __global__
void computeLocalSpatialStatisticalBoundaryLayers()
{

}

/*
 * Determine Local Stochastic Influence Field
 *  - Which surrounding region is sufficient to predict the local region?
 */

extern "C" __global__
void computeLocalSpatialStatisticalInfluenceGradients()
{

}

/*
 *  Stochastic Divergence Fields
 *   - Velocity Distributions
 */

extern "C" __global__
void computeLocalSpatialStatisticalCoherenceGradient(
    float * data,
    int   * bitmasks,
    float * resultA,
    float * resultC,
    float * densityA,
    float * densityC,
    int * phaseIndices,
    float * phaseScaling,
    int * distanceType,
    int phaseDim,
    int weightIndex,
    const unsigned long N_STEPS,
    const unsigned long N_PARTICLES,
    const int varIdx1,
    const int varIdx2,
    const int heatmapRows,
    const int heatmapCols,
    const float heatmapX1Min,
    const float heatmapX1Max,
    const float heatmapX2Min,
    const float heatmapX2Max )
{
//    const long int GLOBAL_IDX = blockIdx.x * blockDim.x + threadIdx.x;
//    const long int N_CELLS = heatmapRows*heatmapCols;
//    const unsigned long WKSZ = N_BINS * N_STEPS;
//    const unsigned long NPT = N_PARTICLES * N_STEPS;

//    if( GLOBAL_IDX >= WKSZ )
//    {
//        return;
//    }

//    const unsigned long tIDX = GLOBAL_IDX % N_CELLS;
//    const unsigned long cIDX = GLOBAL_IDX / N_CELLS;

//    for( unsigned long int pIDX = 0; pIDX < N_PARTICLES; ++pIDX )
//    {
//        const unsigned long GLOBAL_IDX = pIDX * N_STEPS + tIDX;
//        int b = bitmasks[ GLOBAL_IDX ];

//        if( !( b & 1 ) )
//        {
//            continue;
//        }

//        const bool C = ( b & 2 ) != 0;

//        float x1 = data[ varIdx1 * NPT + pIDX * N_STEPS + tIDX ];
//        float x2 = data[ varIdx2 * NPT + pIDX * N_STEPS + tIDX ];

//        int r = floor( ( ( x2 - heatmapX2Min ) / ( heatmapX2Max - heatmapX2Min ) ) * heatmapRows );
//        int c = floor( ( ( x1 - heatmapX1Min ) / ( heatmapX1Max - heatmapX1Min ) ) * heatmapCols );

//        if( r > heatmapRows || c > heatmapCols )
//        {
//            return;
//        }

//        const unsigned long heatMapIdx = r * heatmapCols + c;
//    }

//    for( int j = tIDX + N_STEPS / 10; j < N_STEPS; ++j )
//    {
//        float d = 0;
//        for( int v = 0; v < phaseDim; ++v )
//        {
//            int vIdx = phaseIndices[ v ];
//            float scale = phaseScaling[ v ];
//            const float v1 = data[ vIdx * NPT + pIDX * N_STEPS + tIDX ];
//            const float v2 = data[ vIdx * NPT + pIDX * N_STEPS + j    ];
//            float dv;

//            if( distanceType[ v ] == 0 )
//            {
//                dv = scale * ( v2 - v1 );
//            }
//            else
//            {
//                dv = atan2( sin( v1 - v2 ), cos( v1 - v2 ) );
//            }

//            d += dv*dv;
//        }

//        const float weight = data[ weightIndex * NPT + pIDX * N_STEPS + tIDX ];
//        d = sqrt( d );

//        if( d < 0.01 )
//        {
//           atomicAdd( & resultA[ heatMapIdx ], 1 );
//           if( C )
//           {
//               atomicAdd( & resultC[ heatMapIdx ], 1 );
//           }
//        }
//    }

//    atomicAdd( & densityA[ heatMapIdx ], 1.0 );
//    if( C )
//    {
//        atomicAdd( & densityC[ heatMapIdx ], 1.0 );
//    }
}
