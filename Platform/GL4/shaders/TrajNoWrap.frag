#version 400



uniform vec4 color;
out vec4 fragColor;

flat in int select;

void main(void)
{
    if( select == 0 )
    {
        discard;
    }

    fragColor = color;
}
