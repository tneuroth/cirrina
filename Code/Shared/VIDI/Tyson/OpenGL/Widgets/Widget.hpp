


#ifndef WIDGET_HPP
#define WIDGET_HPP

#include "OpenGL/Widgets/Viewport/ViewPort.hpp"

namespace TN
{

class Widget
{
    Widget * m_parent;
    Vec2< float > m_maxPosition;
    Vec2< float > m_minSize;

protected:

    ViewPort m_viewPort;

    double height()
    {
        return m_viewPort.height();
    }

    double width()
    {
        return m_viewPort.width();
    }

public :

    Widget() : m_parent( 0 ), m_viewPort() { }

    Widget *parent() const
    {
        return m_parent;
    }

    void parent( Widget * p )
    {
        m_parent = p;
    }

    //! in screen space ( pixels )
    virtual void setSize( float width, float height )
    {
        m_viewPort.setSize( width, height );
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    //! in screen space ( pixels )
    virtual void setPosition( double x, double y )
    {
        m_viewPort.offsetX( x );
        m_viewPort.offsetY( y );
    }

    Vec2< float > size() const
    {
        return Vec2< float >( m_viewPort.width(), m_viewPort.height() );
    }

    // screen space, pixels
    Vec2< float > position() const
    {
        if( m_parent == 0 )
        {
            return Vec2< float >( m_viewPort.offsetX(), m_viewPort.offsetY() );
        }
        return Vec2< float >( m_viewPort.offsetX(), m_viewPort.offsetY() ) + m_parent->position();
    }

    const BoxShadowFrame & boxShadow()
    {
        return m_viewPort.boxShadow();
    }

    virtual bool pointInViewPort( const Vec2< float > & p )
    {
        return m_viewPort.pointInViewPort( p );
    }

    ViewPort & viewPort()
    {
        return m_viewPort;
    }

    void minSize( float x, float y )
    {
        m_minSize = Vec2< float >( x, y );
    }

    void maxPosition( float x, float y )
    {
        m_maxPosition = Vec2< float >( x, y );
    }

    const Vec2< float > & maxPosition() const
    {
        return m_maxPosition;
    }
    const Vec2< float > & minSize() const
    {
        return m_minSize;
    }

    void bkgColor( const Vec4 & cl )
    {
        m_viewPort.bkgColor( cl );
    }

    virtual ~Widget() { }
};

}

#endif // WIDGET_HPP

