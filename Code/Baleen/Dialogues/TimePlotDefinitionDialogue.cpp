
#include "Types/Vec.hpp"
#include "Algorithms/Standard/Util.hpp"

#include "Data/Definitions/Attributes.hpp"
#include "Data/Definitions/LinkedViewDefinition.hpp"
#include "Dialogues/TimePlotDefinitionDialogue.hpp"
#include "Expressions/Calculator/ExpressionWrapper.hpp"
#include "Expressions/Calculator/expressionsymbols.hpp"
#include "Expressions/Calculator/Expression.hpp"
#include "Dialogues/DialogHelpers.hpp"

#include <QMessageBox>
#include <QtMath>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QDebug>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QListWidget>
#include <QTextEdit>
#include <QShortcut>

#include <string>
#include <vector>
#include <set>
#include <unordered_map>

namespace TN {

    TimePlotDefinitionDialogue::~TimePlotDefinitionDialogue() {}

    void TimePlotDefinitionDialogue::plotDescriptionChanged()
    {

    }

    void TimePlotDefinitionDialogue::finalize()
    {
        std::string name = plotNameingLineEdit->text().toStdString();
        if( name.size() == 0 )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }
        if( ( m_status != "defined" ) && ( definedTimePlots.find( name ) != definedTimePlots.end() ) )
        {
            errorMessageLabel->setText( "Name/key already in use" );
            return;
        }
        if( name == "temp" )
        {
            errorMessageLabel->setText( "Invalid Name/key" );
            return;
        }

        TimePlotDefinition definition;
        definition.attr = spaceAttrCombo1->currentText().toStdString();
        definition.attr = definition.attr.substr( 0, definition.attr.find( "=" ) );
        if( definition.attr.back() == ' ' ){
            definition.attr.pop_back();
        }

        /////////////////////

        m_key = name;
        definition.name = m_key;
        definition.description = plotDescriptionBox->toPlainText().toStdString();

        if( m_status == "undefined" )
        {
            definedTimePlots.insert( { m_key, definition } );
        }
        else
        {
            definedTimePlots.find( m_key )->second = definition;
        }

        setStatus( "defined" );

        emit finalizeDefinition(
            m_key,
            definedTimePlots,
            baseVariables,
            derivedVariables,
            baseConstants,
            customConstants
        );

        close();
    }

    void TimePlotDefinitionDialogue::cancel()
    {
        emit cancelDefinition();
        close();
    }

    void TimePlotDefinitionDialogue::update(
        const std::map< std::string, TimePlotDefinition > & _histMap,
        const std::map< std::string, BaseVariable > & _baseVarMap,
        const std::map< std::string, DerivedVariable > & _derivedVarMap,
        const std::map< std::string, CustomConstant  > & _customConstMap,
        const std::map< std::string, BaseConstant   > & _baseConstMap )
    {
        baseVariables = _baseVarMap;
        derivedVariables = _derivedVarMap;
        customConstants = _customConstMap;
        baseConstants = _baseConstMap;
        definedTimePlots = _histMap;

        attributeList->setStyleSheet( "QListWidget { color : green }" );
        constantList->setStyleSheet( "QListWidget { color : green }" );

        constantList->clear();
        userConstantList->clear();
        derivedAttributeList->clear();
        attributeList->clear();

        spaceAttrCombo1->clear();

        for( auto & baseConst : _baseConstMap )
        {
            std::string _nm = baseConst.first + " = " + to_string_with_precision( baseConst.second.value(), 10 );
            constantList->addItem( _nm.c_str() );
        }

        for( auto & custConst : _customConstMap )
        {
            std::string _nm = custConst.second.name()
                    + " = " + custConst.second.expression();

            if( ! m_expressionCalculator->parser().validateNumber( custConst.second.expression() ) )
            {
                _nm += " = " + to_string_with_precision( m_expressionCalculator->parser().evaluateConstExpression( custConst.second.expression(), _baseConstMap, _customConstMap ), 10 );
            }

            userConstantList->addItem( _nm.c_str() );
        }

        for( auto & dv : _derivedVarMap )
        {
            std::string _nm = dv.second.name() + " = " + dv.second.expression();
            derivedAttributeList->addItem( _nm.c_str() );

            spaceAttrCombo1->addItem( _nm.c_str() );
        }

        for( auto & baseVarE : baseVariables )
        {
            std::string _nm = baseVarE.first.c_str();

            attributeList->addItem( _nm.c_str() );

            spaceAttrCombo1->addItem( _nm.c_str() );
        }
    }

    void TimePlotDefinitionDialogue::fromDefinition( const TimePlotDefinition & def )
    {
        plotDescriptionBox->setText( def.description.c_str() );

        int index = getAttrComboIndex( def.attr, spaceAttrCombo1 );
        if( index >= 0 )
        {
            spaceAttrCombo1->setCurrentIndex( index );
        }
        plotNameingLineEdit->setText( def.name.c_str() );
        errorMessageLabel->setText( "" );

        setStatus( "defined" );
    }

    void TimePlotDefinitionDialogue::attemptDeriveVariable( )
    {
        BaseDataDialogue::attemptDeriveVariable();
        std::string nm = deriveNameEdit->text().toStdString();
        std::string expr = deriveEdit->text().toStdString();
        spaceAttrCombo1->addItem( ( nm + " = " + expr ).c_str() );
        emit updateAttributes( baseVariables, derivedVariables, baseConstants, customConstants );
    }

    void TimePlotDefinitionDialogue::setStatus( const std::string & status )
    {
        m_status = status;
        if( status == "defined" )
        {
            acceptButton->setText( "update" );
            plotNameingLineEdit->setEnabled( false );
        }
    }

    TimePlotDefinitionDialogue::TimePlotDefinitionDialogue( std::string _key, QWindow *parentWindow ) : BaseDataDialogue( _key, parentWindow )
    {

        PlotAttributeSection = new QVBoxLayout;

        acceptButton = new QPushButton( "create" );
        cancelButton = new QPushButton( "cancel" );
        plotNameingLabel = new QLabel( "Plot Name/Key" );
        plotNameingLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        plotNameingLineEdit = new QLineEdit;

        connect( cancelButton, SIGNAL( clicked(bool ) ), this, SLOT(  cancel() ) );
        connect( acceptButton, SIGNAL( clicked(bool ) ), this, SLOT(finalize() ) );

        ///////////////////////////////////////////////////////////////////////////

        // space

        spaceAttrLabel = new QLabel( "Attribute" );

        QLabel * spatialRegionLabel = new QLabel(  "Plot Definition" );
        spatialRegionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );

        QHBoxLayout * frabskjdkjdskl = new QHBoxLayout;
        frabskjdkjdskl->addWidget(  spatialRegionLabel );
        frabskjdkjdskl->addStretch();

        spaceAttrCombo1 = new QComboBox;
        spaceAttrCombo1->setStyleSheet( "QComboBox { combobox-popup: 0; color: green; padding: 0px 0px 0px 0px; } QComboBox QListWidget{ color: green }" );

        spacec2 = new QVBoxLayout;
        spaceLayout = new QHBoxLayout;

        spacec2->addWidget( spaceAttrLabel  );
        spacec2->addWidget( spaceAttrCombo1 );

        spaceLayout->addLayout( spacec2 );

        //////////////////////////////////////////////////////////////////////////////

        QFrame *l1dsdds = new QFrame;
        l1dsdds->setFrameShape( QFrame::HLine );
        l1dsdds->setFrameShadow( QFrame::Sunken );
        l1dsdds->setStyleSheet( "QFrame { background-color : white; }" );
        PlotAttributeSection->addWidget( l1dsdds );
        PlotAttributeSection->addLayout( frabskjdkjdskl );
        PlotAttributeSection->addLayout( spaceLayout );

        // Description

        plotDescriptionLayout = new QVBoxLayout;
        plotDescriptionLabel = new QLabel( "Plot Description" );
        plotDescriptionLabel->setStyleSheet( "QLabel { color : rgb(70, 70, 70);  font-weight: bold; font-size : 12pt;  }" );
        plotDescriptionBox = new QTextEdit;
        plotDescriptionBox->setStyleSheet( "QTextEdit { background-color : white; }" );

        QHBoxLayout * kjncewionoicejnoinwe = new QHBoxLayout;
        kjncewionoicejnoinwe->addWidget( plotDescriptionLabel );
        kjncewionoicejnoinwe->addStretch();

        plotDescriptionLayout->addLayout( kjncewionoicejnoinwe );
        plotDescriptionLayout->addWidget( plotDescriptionBox );
        plotDescriptionLayout->addWidget( errorMessageLabel );

        //////////////////////////////////////////////////////////////////////////////

        // main layout

        m_mainLayout->addLayout( PlotAttributeSection );
        m_mainLayout->addLayout( plotDescriptionLayout );

        QHBoxLayout *dll = new QHBoxLayout;
        dll->addWidget( plotNameingLabel );
        dll->addWidget( plotNameingLineEdit );
        dll->addWidget( cancelButton );
        dll->addWidget( acceptButton );
        m_mainLayout->addLayout( dll );
    }
}

