

namespace TN
{
	struct PhaseComputeNode
	{
		// per plot
        std::vector< TextureLayer > phasePlotLayersAll;
        std::vector< TextureLayer > phasePlotLayersCombination;

        // per variable
        std::vector< TextureLayer1D > distributionTexturesAll;	    
        std::vector< TextureLayer1D > distributionTexturesCombination;	

        // per variable
        std::vector< TextureLayer1D > summaryStatisticTexturesAll;	
        std::vector< TextureLayer1D > summaryStatisticTexturesCombination;	

        // for computing intermediate results
	    TextureLayer tempTex1All;
		TextureLayer tempTex2All;
		TextureLayer tempTex1Combination;
		TextureLayer tempTex2Combination;
	};

    // number / path density
    void computeDensityLayers(
        PhasePlotCache & cache,
        std::pair< TextureLayer, TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M,
        bool withTangentialMotion );

    // * mean
    void computeMeanLayers(
        PhasePlotCache & cache,
        std::pair< TextureLayer, TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M );

    // * variance
    void computeVarianceLayers(
        PhasePlotCache & cache,
        std::pair< TextureLayer, TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const std::string & w,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M );

    // angle variance
    void computeAngleVarianceLayers(
        PhasePlotCache & cache,
        std::pair< TextureLayer, TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M );

    // stability
    void computeStabilityLayers(
        PhasePlotCache & cache,
        std::pair< TextureLayer, TextureLayer > & layers,
        const std::string & x,
        const std::string & y,
        const Vec2< int > & resolution,
        const std::string & ptype,
        const std::vector< GLsizei > & counts,
        const std::vector< GLint > & firsts,
        const QMatrix4x4 & M );
    
}