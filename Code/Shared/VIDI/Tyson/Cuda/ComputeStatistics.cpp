
#include "ComputeStatistics.hpp"
#include "../Expressions/GenerateBooleanExpressionProgram.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <chrono>
#include <ctime>
#include "../Algorithms/Standard/MyAlgorithms.hpp"
#include "Types/Vec.hpp"

#include <nvrtc.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define TEMPLATE_KERNEL_PATH "/media/dev/deploy/baleen/Code/Shared/VIDI/Tyson/Cuda/ComputeStatsKernel.cu"
#define TEMPLATE_KERNEL2_PATH "/media/dev/deploy/baleen/Code/Shared/VIDI/Tyson/Cuda/ComboCheck.cu"

#define NVRTC_SAFE_CALL(x)                                        \
  do {                                                            \
    nvrtcResult nvrt_result = x;                                       \
    if (nvrt_result != NVRTC_SUCCESS) {                                \
      std::cerr << "\nerror: " #x " failed with error "           \
                << nvrtcGetErrorString( nvrt_result ) << '\n';           \
      exit(1);                                                    \
    }                                                             \
  } while(0)
#define CUDA_SAFE_CALL(x)                                         \
  do {                                                            \
    CUresult cu_result = x;                                          \
    if (cu_result != CUDA_SUCCESS) {                                 \
      const char *msg;                                            \
      cuGetErrorName(cu_result, &msg);                               \
      std::cerr << "\nerror: " #x " failed with error "           \
                << msg << '\n';                                   \
      exit(1);                                                    \
    }                                                             \
  } while(0)

void StatsComputer::initRuntime()
{
    std::ifstream inFile( TEMPLATE_KERNEL_PATH );

    template_kernel = std::string(
        std::istreambuf_iterator<char>( inFile ),
        std::istreambuf_iterator<char>() );

    inFile.close();

    std::ifstream inFile2( TEMPLATE_KERNEL2_PATH );

    template_kernel2 = std::string(
        std::istreambuf_iterator<char>( inFile2 ),
        std::istreambuf_iterator<char>() );

    inFile2.close();

    CUDA_SAFE_CALL( cuInit( 0 ) );
    CUDA_SAFE_CALL( cuDeviceGet( & m_cuDevice, 0 ) );
    CUDA_SAFE_CALL( cuCtxCreate( & m_context, 0, m_cuDevice ) );

    char name[ 500 ];
    cuDeviceGetName ( name, 500, m_cuDevice );
//    std::cerr << name;

    ///////

    nvrtcProgram program;

    const char * options[] =
    {
        "--gpu-architecture=compute_52"
    };

    std::string source = template_kernel; //TN::generateCombinationProgram( expression, template_kernel );
    NVRTC_SAFE_CALL( nvrtcCreateProgram(
        & program,
        source.c_str(),
        "computeStatistics.cu",
        0,
        NULL,
        NULL ) );

    NVRTC_SAFE_CALL( nvrtcCompileProgram( program,  1, options ) );
    size_t ptxSize;
    NVRTC_SAFE_CALL( nvrtcGetPTXSize( program, & ptxSize ) );
    char * ptx = new char[ ptxSize ];
    NVRTC_SAFE_CALL( nvrtcGetPTX( program, ptx ) );
    NVRTC_SAFE_CALL( nvrtcDestroyProgram( & program ) );
    CUDA_SAFE_CALL( cuModuleLoadDataEx(  & m_module, ptx, 0, 0, 0 ) );
    CUDA_SAFE_CALL( cuModuleGetFunction( & m_currentHistogramKernel,  m_module, "computeCurrentHistograms"       ) );
    CUDA_SAFE_CALL( cuModuleGetFunction( & m_sumAndCountKernel,       m_module, "computeTimeSeriesSumsAndCounts" ) );
    CUDA_SAFE_CALL( cuModuleGetFunction( & m_radialSelectionKernel,   m_module, "computeRadialSelection"         ) );
    CUDA_SAFE_CALL( cuModuleGetFunction( & m_divideKernel,            m_module, "divide"                         ) );
    CUDA_SAFE_CALL( cuModuleGetFunction( & m_RecurrenceHotSpotKernel, m_module, "computeLocalSpatialStatisticalCoherenceGradient" ) );

    //std::cerr << "initialized runtime";
}

StatsComputer::StatsComputer()
{
    m_useGLInterop = true;

    m_buffer = 0;
    m_ranges = 0;
    m_bitmasks = 0;
    m_sums = 0;
    m_counts = 0;

    m_glResources = 0;
    m_nResources = 0;
}

StatsComputer::~StatsComputer()
{
    CUDA_SAFE_CALL( cuModuleUnload(m_module ) );
    CUDA_SAFE_CALL( cuCtxDestroy( m_context ) );

    cleanBuffers();
    cleanMappedBuffers();
    cleanResultArray();
}

void StatsComputer::cleanBuffers()
{
    if( ! m_useGLInterop )
    {
        CUDA_SAFE_CALL( cuMemFree( m_buffer                 ) );
        CUDA_SAFE_CALL( cuMemFree( m_bitmasks               ) );

        m_buffer = 0;
        m_bitmasks = 0;
    }

    CUDA_SAFE_CALL( cuMemFree( m_ranges                 ) );
    m_ranges = 0;
}

void StatsComputer::cleanResultArray()
{
    CUDA_SAFE_CALL( cuMemFree( m_currentHistograms      ) );
    CUDA_SAFE_CALL( cuMemFree( m_timeAveragedHistograms ) );
    CUDA_SAFE_CALL( cuMemFree( m_sums                   ) );
    CUDA_SAFE_CALL( cuMemFree( m_counts                 ) );

    m_currentHistograms = 0;
    m_timeAveragedHistograms = 0;
    m_sums = 0;
    m_counts = 0;
}

void StatsComputer::allocateResultArray(
    const size_t N_STEPS,
    const size_t N_VARS,
    const size_t N_BINS )
{
    cleanResultArray();
    CUDA_SAFE_CALL( cuMemAlloc( & m_currentHistograms, N_VARS * N_BINS * 2 * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & m_timeAveragedHistograms, N_VARS * N_BINS * 2 * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & m_sums,   N_STEPS * 2 * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & m_counts, N_STEPS * 2 * sizeof ( float ) ) );

    //std::cerr << " allocated sums: " << N_STEPS * N_SETS << "\n";
}

void StatsComputer::resizeBuffer(
    const size_t N_PARTICLES,
    const size_t N_STEPS,
    const size_t N_VARS  )
{
    cleanBuffers();
    CUDA_SAFE_CALL( cuMemAlloc( & m_buffer, N_STEPS * N_PARTICLES * N_VARS * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & m_ranges, N_VARS * 2 * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & m_bitmasks, N_STEPS * N_PARTICLES * sizeof ( int ) ) );
}

void StatsComputer::cleanMappedBuffers()
{
    if( m_nResources <= 0 )
    {
        return;
    }

    CUDA_SAFE_CALL( cuGraphicsUnmapResources( m_nResources, m_glResources, 0 ) );

    for( int i = 0; i < m_nResources; ++i )
    {
         CUDA_SAFE_CALL( cuGraphicsUnregisterResource( m_glResources[ i ] ) );
    }
    delete [ ] m_glResources;
}

void StatsComputer::mapBuffer(
    GLuint buffer,
    const size_t N_STEPS,
    const size_t N_PARTICLES,
    const size_t N_VARS )
{
    cleanMappedBuffers();

    m_nResources = 1;
    m_glResources = new CUgraphicsResource[ 1 ];

    CUDA_SAFE_CALL( cuGraphicsGLRegisterBuffer( & m_glResources[ 0 ], buffer, 0 ) );
    CUDA_SAFE_CALL( cuGraphicsMapResources( m_nResources, m_glResources, 0 ) );

    size_t sz;
    CUDA_SAFE_CALL( cuGraphicsResourceGetMappedPointer( & m_buffer, & sz, m_glResources[ 0 ] ) );

//    std::cerr << "mapped resource is of size " << sz << " and m_bitmasks starts at " << ( N_STEPS * N_PARTICLES * N_VARS );
    m_bitmasks = ( CUdeviceptr )( ( ( int * ) m_buffer ) + ( N_STEPS * N_PARTICLES * N_VARS ) );
}

void StatsComputer::allocateRanges(
   const size_t N_VARS )
{
    CUDA_SAFE_CALL( cuMemFree( m_ranges ) );
    CUDA_SAFE_CALL( cuMemAlloc( & m_ranges, N_VARS * 2 * sizeof ( float ) ) );
}

void StatsComputer::bufferRanges(
   float * ranges,
   const size_t N_VARS )
{
   CUDA_SAFE_CALL( cuMemcpyHtoD( m_ranges, ranges, N_VARS * 2 * sizeof( float ) ) );
}

void StatsComputer::bufferData(
   float * data,
   const size_t N )
{
    CUDA_SAFE_CALL( cuMemcpyHtoD( m_buffer, data, N * sizeof( float ) ) );
}

void StatsComputer::bufferBitmasks(
   int * bitmasks,
   const size_t N_PARTICLES,
   const size_t N_STEPS )
{
    CUDA_SAFE_CALL(  cuMemcpyHtoD( m_bitmasks, bitmasks, N_PARTICLES * N_STEPS * sizeof( int ) ) );
}

void StatsComputer::compileProgram( const std::string &expression )
{
    nvrtcProgram program2;
    std::string source2 = TN::generateCombinationProgram( expression, template_kernel2 );

    auto start = std::chrono::system_clock::now();

//    std::cerr << "compiling program " << expression;
//    std::cerr << source2;

    NVRTC_SAFE_CALL( nvrtcCreateProgram(
        & program2,
        source2.c_str(),
        "ComboCheck.cu",
        0,
        NULL,
        NULL ) );

    const char * options[] =
    {
        "--gpu-architecture=compute_52"
    };

    NVRTC_SAFE_CALL( nvrtcCompileProgram( program2, 1, options ) );

    size_t ptxSize2;
    NVRTC_SAFE_CALL( nvrtcGetPTXSize( program2, & ptxSize2 ) );
    char * ptx2 = new char[ ptxSize2 ];
    NVRTC_SAFE_CALL( nvrtcGetPTX( program2, ptx2 ) );

//    std::cerr << "\nPTX=\n" << ptx2 << "\n";

    NVRTC_SAFE_CALL( nvrtcDestroyProgram( & program2 ) );
    CUDA_SAFE_CALL( cuModuleLoadDataEx(  & m_module, ptx2, 0, 0, 0 ) );
    CUDA_SAFE_CALL( cuModuleGetFunction( & m_combinationKernel, m_module, "setCombination" ) );

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "CUDA program compilation time: "  << elapsed_seconds.count() << "s\n";
}

void StatsComputer::setCombination(
    int N_STEPS,
    int N_PARTICLES,
    std::vector< int > & result )
{
    auto start = std::chrono::system_clock::now();

    size_t N = N_STEPS * N_PARTICLES;

    void *args[] = {
        & m_bitmasks,
        & N
    };

    CUDA_SAFE_CALL(  cuLaunchKernel(
        m_combinationKernel,
        512 * std::ceil( ( float ) N / float( 512 ) ), 1, 1,
        512, 1, 1,
        0, NULL,
        args, 0 ) );

    CUDA_SAFE_CALL(  cuCtxSynchronize() );

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "set combination time with copy: "  << elapsed_seconds.count() << "s\n";

    CUDA_SAFE_CALL( cuMemcpyDtoH( result.data(), m_bitmasks, N * sizeof( int ) ) );
}

void StatsComputer::setCombination(
    int N_STEPS,
    int N_PARTICLES )
{
//    auto start = std::chrono::system_clock::now();

    size_t N = N_STEPS * N_PARTICLES;

    void *args[] = {
        & m_bitmasks,
        & N
    };

    CUDA_SAFE_CALL(  cuLaunchKernel(
        m_combinationKernel,
        256 * std::ceil( ( float ) N / float( 256 ) ), 1, 1,
        256, 1, 1,
        0, NULL,
        args, 0 ) );

//    CUDA_SAFE_CALL(  cuLaunchKernel(
//        m_combinationKernel,
//        512 * std::ceil( ( float ) N_PARTICLES / float( 512 ) ), 1, 1,
//        512, 1, 1,
//        0, NULL,
//        args, 0 ) );

    CUDA_SAFE_CALL(  cuCtxSynchronize() );

//    auto end = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "set combination time without copy: "  << elapsed_seconds.count() << "s\n";
}

void StatsComputer::computeRadialSelection(
    float radiusX,
    float radiusY,
    float centerX,
    float centerY,
    unsigned long N_PARTICLES,
    unsigned long N_STEPS,
    unsigned  int V1_IDX,
    unsigned  int V2_IDX )
{
    void *args[] = {
        & m_buffer,
        & m_bitmasks,
        & radiusX,
        & radiusY,
        & centerX,
        & centerY,
        & N_PARTICLES,
        & N_STEPS,
        & V1_IDX,
        & V2_IDX
    };

    CUDA_SAFE_CALL( cuLaunchKernel(
        m_radialSelectionKernel,
        512 * std::ceil( ( float ) N_PARTICLES / float( 512 ) ), 1, 1,
        512, 1, 1,
        0, NULL,
        args, 0 ) );

    CUDA_SAFE_CALL( cuCtxSynchronize() );
}

void StatsComputer::getSeriesStats(
    unsigned long N_STEPS,
    std::vector<float> & result )
{
//    auto start = std::chrono::system_clock::now();

    long int N = N_STEPS * 2;

    void *args[] = {
        & m_sums,
        & m_counts,
        & N
    };

    CUDA_SAFE_CALL(  cuLaunchKernel(
        m_divideKernel,
        512 * std::ceil( ( float ) N / float( 512 ) ), 1, 1,
        512, 1, 1,
        0, NULL,
        args, 0 ) );

    CUDA_SAFE_CALL(  cuCtxSynchronize() );
    CUDA_SAFE_CALL( cuMemcpyDtoH( result.data(), m_sums, N * sizeof( float ) ) );

//    auto end = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "get series stats"
//              << "NP="
//              << N_STEPS << ", N_SETS="
//              << N_SETS << ", N_VARS="
//              << elapsed_seconds.count() << "s\n";
}

void StatsComputer::computeTimeSeriesSumsAndCounts(
    unsigned long N_PARTICLES,
    unsigned long N_STEPS,
    unsigned int V_IDX,
    bool absolute )
{
//    std::cerr << N_SETS << "\n";

//    if( N_SETS >= 28 )
//    {
//        const int CHUNK = N_STEPS / 10;
//        for( size_t nsets = 4; nsets <= N_SETS; nsets += 6 )
//        {
//            for( size_t nsteps = CHUNK; nsteps < N_STEPS; nsteps += CHUNK )
//            {
//                auto start = std::chrono::system_clock::now();
                unsigned long N = N_STEPS * N_PARTICLES;
                unsigned long V_OFF = V_IDX * N_STEPS * N_PARTICLES;

                CUDA_SAFE_CALL( cuMemsetD32(   m_sums, 0, N_STEPS * 2 ) );
                CUDA_SAFE_CALL( cuMemsetD32( m_counts, 0, N_STEPS * 2 ) );

                void *args[] = {
                    & m_buffer,
                    & m_bitmasks,
                    & m_sums,
                    & m_counts,
                    & N,
                    & N_STEPS,
                    & V_OFF
                };

                CUDA_SAFE_CALL( cuLaunchKernel(
                    m_sumAndCountKernel,
                    512 * std::ceil( ( float ) N / float( 512 ) ), 1, 1,
                    512, 1, 1,
                    0, NULL,
                    args, 0 ) );

                CUDA_SAFE_CALL( cuCtxSynchronize() );

//                auto end = std::chrono::system_clock::now();
//                std::chrono::duration<double> elapsed_seconds = end-start;
//                std::cout << "compute time series sums and counts"
//                          << ", N=" << N
//                          << ", N_SETS=" << nsets
//                          << ", time : " << elapsed_seconds.count() << "s\n";
//            }
//        }
//    }
}

void StatsComputer::computeCurrentHistograms(
    unsigned long N_PARTICLES,
    unsigned long N_STEPS,
    unsigned long STEP,
    unsigned int N_VARS,
    unsigned int N_BINS,
    std::vector< float > & result,
    bool probability )
{
//    auto start = std::chrono::system_clock::now();

    const size_t RESULT_SIZE = N_VARS * N_BINS * 2;

    CUDA_SAFE_CALL( cuMemsetD32( m_currentHistograms, 0, RESULT_SIZE ) );

    void *args[] = {
        & m_buffer,
        & m_bitmasks,
        & m_ranges,
        & m_currentHistograms,
        & N_PARTICLES,
        & N_STEPS,
        & STEP,
        & N_VARS,
        & N_BINS
    };

    CUDA_SAFE_CALL( cuLaunchKernel(
        m_currentHistogramKernel,
        512 * std::ceil( ( float ) N_PARTICLES / float( 512 ) ), 1, 1,
        512, 1, 1,
        0, NULL,
        args, 0 ) );

    CUDA_SAFE_CALL( cuCtxSynchronize() );
    CUDA_SAFE_CALL( cuMemcpyDtoH( result.data(), m_currentHistograms, RESULT_SIZE * sizeof( float ) ) );

//    auto end = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "compute histograms"
//              << "NP="
//              << N_PARTICLES << ", NT="
//              << N_STEPS << ", N_SETS="
//              << N_SETS << ", N_VARS="
//              << N_VARS << ", time : "
//              << elapsed_seconds.count() << "s\n";

//    start = std::chrono::system_clock::now();


    if( probability )
    {
        for( int s = 0; s < 2; ++s )
        {
            for( int v = 0; v < N_VARS; ++v )
            {
                float sum = 0;
                for( int b = 0; b < N_BINS; ++b )
                {
                    sum += result[ s * N_VARS * N_BINS + v * N_BINS + b ];
                }
                if( sum > 0 )
                {
                    for( int b = 0; b < N_BINS; ++b )
                    {
                        result[ s * N_VARS * N_BINS + v * N_BINS + b  ] /= sum;
                    }
                }
            }
        }
    }

//    end = std::chrono::system_clock::now();
//    elapsed_seconds = end-start;
//    std::cout << "convert to PDF"
//              << "NP="
//              << N_PARTICLES << ", NT="
//              << N_STEPS << ", N_SETS="
//              << N_SETS << ", N_VARS="
//              << N_VARS << ", time : "
//              << elapsed_seconds.count() << "s\n";
}


void StatsComputer::computeRecurrenceHotSpots(
    unsigned long N_PARTICLES,
    unsigned long N_STEPS,
    int weightIndex,
    std::vector< int > & phaseIndexes,
    std::vector< float > & phaseScaling,
    std::vector< int > & distanceType,
    std::pair< int, int > & heatmapSpace,
    std::pair< int, int > & heatmapDims,
    std::pair< std::pair< float, float >, std::pair< float, float > > & heatmapRanges,
    std::vector< float > & resultsA,
    std::vector< float > & resultsC )
{
    CUdeviceptr resultBufferA;
    CUdeviceptr densityBufferA;
    CUdeviceptr resultBufferC;
    CUdeviceptr densityBufferC;
    CUdeviceptr d_phaseIndices;
    CUdeviceptr d_phaseScaling;
    CUdeviceptr d_distanceType;

    const size_t RESULT_SIZE = heatmapDims.first * heatmapDims.second;

    CUDA_SAFE_CALL( cuMemAlloc( & resultBufferA,  RESULT_SIZE * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & densityBufferA, RESULT_SIZE * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & resultBufferC,  RESULT_SIZE * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & densityBufferC, RESULT_SIZE * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & d_phaseIndices, phaseIndexes.size() * sizeof ( int ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & d_phaseScaling, phaseScaling.size() * sizeof ( float ) ) );
    CUDA_SAFE_CALL( cuMemAlloc( & d_distanceType, distanceType.size() * sizeof ( int ) ) );

    CUDA_SAFE_CALL( cuMemcpyHtoD( d_phaseIndices, phaseIndexes.data(), phaseIndexes.size() * sizeof( int   ) ) );
    CUDA_SAFE_CALL( cuMemcpyHtoD( d_phaseScaling, phaseScaling.data(), phaseScaling.size() * sizeof( float ) ) );
    CUDA_SAFE_CALL( cuMemcpyHtoD( d_distanceType, distanceType.data(), distanceType.size() * sizeof( float ) ) );

    int phaseDim = phaseIndexes.size();

    void *args[] = {
        & m_buffer,
        & m_bitmasks,
        & resultBufferA,
        & resultBufferC,
        & densityBufferA,
        & densityBufferC,
        & d_phaseIndices,
        & d_phaseScaling,
        & d_distanceType,
        & phaseDim,
        & weightIndex,
        & N_STEPS,
        & N_PARTICLES,
        & ( heatmapSpace.first ),
        & ( heatmapSpace.second ),
        & ( heatmapDims.first ),
        & ( heatmapDims.second ),
        & ( heatmapRanges.first.first ),
        & ( heatmapRanges.first.second ),
        & ( heatmapRanges.second.first ),
        & ( heatmapRanges.second.second ),
    };

    CUDA_SAFE_CALL( cuLaunchKernel(
        m_RecurrenceHotSpotKernel,
        512 * std::ceil( ( float ) ( N_PARTICLES * N_STEPS ) / float( 512 ) ), 1, 1,
        512, 1, 1,
        0, NULL,
        args, 0 ) );

    CUDA_SAFE_CALL( cuCtxSynchronize() );

    long int SZ = RESULT_SIZE;

    void *args2[] = {
        & resultBufferA,
        & densityBufferA,
        & SZ
    };

    CUDA_SAFE_CALL( cuLaunchKernel(
        m_divideKernel,
        512 * std::ceil( ( float ) SZ / float( 512 ) ), 1, 1,
        512, 1, 1,
        0, NULL,
        args2, 0 ) );

    void *args3[] = {
        & resultBufferC,
        & densityBufferC,
        & SZ
    };

    CUDA_SAFE_CALL( cuLaunchKernel(
        m_divideKernel,
        512 * std::ceil( ( float ) SZ / float( 512 ) ), 1, 1,
        512, 1, 1,
        0, NULL,
        args3, 0 ) );

    CUDA_SAFE_CALL( cuCtxSynchronize() );

    CUDA_SAFE_CALL( cuMemcpyDtoH( resultsA.data(), resultBufferA, RESULT_SIZE * sizeof( float ) ) );
    CUDA_SAFE_CALL( cuMemcpyDtoH( resultsC.data(), resultBufferC, RESULT_SIZE * sizeof( float ) ) );

    CUDA_SAFE_CALL( cuMemFree( resultBufferA  ) );
    CUDA_SAFE_CALL( cuMemFree( densityBufferA ) );
    CUDA_SAFE_CALL( cuMemFree( resultBufferC  ) );
    CUDA_SAFE_CALL( cuMemFree( densityBufferC ) );
    CUDA_SAFE_CALL( cuMemFree( d_phaseIndices ) );
    CUDA_SAFE_CALL( cuMemFree( d_phaseScaling ) );
    CUDA_SAFE_CALL( cuMemFree( d_distanceType ) );
}
