#version 400



out float fragColor[2];

in G2F{
    float valAll;
    float valComb;
}g2f;

void main(void)
{
    fragColor[ 0 ] = g2f.valAll;
    fragColor[ 1 ] = g2f.valComb;
}
