#ifndef PARAMETERKEYS_HPP
#define PARAMETERKEYS_HPP

#include <string>

namespace TN
{
    namespace ParameterKey
    {
        const std::string AUTHOR = "author";
        const std::string NAME = "name";
        const std::string DATA_TYPE = "type";
    }
}

#endif // PARAMETERKEYS_HPP
