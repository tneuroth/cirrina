
#include "BALEEN.hpp"
#include "MainWindow.hpp"
#include "ui_MainWindow.h"

#include <QMenuBar>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    baleen( this )
{
    ui->setupUi(this);
    ui->horizontalLayout->setSpacing( 0 );
    ui->horizontalLayout->setMargin( 0 );
    ui->horizontalLayout->addWidget( &baleen );
    baleen.GetRenderWindow()->renderLater();
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
    baleen.GetRenderWindow()->resizeEvent( e );
}

MainWindow::~MainWindow()
{
    delete ui;
}
