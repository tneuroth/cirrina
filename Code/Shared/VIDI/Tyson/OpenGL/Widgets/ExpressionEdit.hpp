


#ifndef TN_EXPRESSION_EDIT
#define TN_EXPRESSION_EDIT

#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"

namespace TN
{

class ExpressionEdit : public Widget
{
    bool m_flash;
    double m_flashPhase;

    int m_cursorPos;

    std::string m_text;

    bool m_focused;

    Vec3< float > m_validColor;
    Vec3< float > m_errorColor;
    Vec3< float > m_flashColor;

public:

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    ExpressionEdit() :
        m_flash( false ),
        m_flashPhase( 0 ),
        m_validColor( 1, .7, .6 ),
        m_errorColor( 1, .7, .6 ),
        m_flashColor( 1, .7, .6 ),
        m_cursorPos( 0 )
    {}

    void setFocused( bool c )
    {
        m_focused = c;
    }

    std::string text() const
    {
        return m_text;
    }

    void clear()
    {
        m_text = "";
    }

    void append( char c )
    {
        m_text.push_back( c );
        incrementCursorPos();
    }

    void deleteFromCursor()
    {
        if( m_text.size() > 0 )
        {
            m_text.erase( m_text.begin() + m_cursorPos );
            decrementCursorPos();
        }
    }

    void addFromCursor( char c )
    {
        if( m_cursorPos >= int( m_text.size() ) - 1 )
        {
            append( c );
        }
        else
        {
            m_text.insert( m_text.begin() + m_cursorPos + 1, c );
            incrementCursorPos();
        }
    }

    void pop()
    {
        if( m_text.size() > 0 )
        {
            m_text.pop_back();
            decrementCursorPos();
        }
    }

    bool hasFocus() const { return m_focused; }

    const Vec3< float > & flashColor() const
    {
        return m_flashColor;
    }
    const Vec3< float > & errorColor() const
    {
        return m_errorColor;
    }
    const Vec3< float > & validColor() const
    {
        return m_validColor;
    }

    void incrementCursorPos()
    {
        m_cursorPos = std::min( int( m_text.size() - 1 ), m_cursorPos + 1 );
    }

    void decrementCursorPos()
    {
        m_cursorPos = std::max( int( m_cursorPos - 1 ), 0 );
    }

    int cursorPos() const { return m_cursorPos; }

    bool flash() const { return m_flash; }
    void flash( bool b ) { m_flash = b; }

    double flashPhase() const { return m_flashPhase; }
    void flashPhase( double phase ) { m_flashPhase = phase; }
};

}


#endif // TN_EXPRESSION_EDIT

