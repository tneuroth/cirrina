#version 400



#define PIo2 1.57079632679

layout(lines) in;
layout(line_strip, max_vertices=4) out;

uniform sampler1D angleToNormalization;
uniform bool distanceWeighted;
uniform float width;
uniform float height;

in vData
{
    flat int select;
    flat int isReal;
} vertices[];

out G2F{
    float valAll;
    float valComb;
} g2f;

uniform float wrap1;
uniform float wrap2;

void main( void )
{
    int s = vertices[ 0 ].select * vertices[ 1 ].select;
    int r = vertices[ 0 ].isReal * vertices[ 1 ].isReal;

    if( bool( r ) )
    {
        vec4 pos1 = gl_in[0].gl_Position;
        vec4 pos2 = gl_in[1].gl_Position;

        float distX = abs( pos2.x - pos1.x ) * width;
        float distY = abs( pos2.y - pos1.y ) * height;

        vec2 a = normalize( vec2( distX, distY ) );
        vec2 b = vec2( 1, 0 );
        float angle = acos( dot( a, b ) );
        float aNorm = texture( angleToNormalization, angle / PIo2 ).r;
        float dist = max( length( vec2( distX, distY ) ), 1 );
        float norm;

        /////////////////////////////////////////////////////////////////////

        float x1 = gl_in[0].gl_Position.x;
        float x2 = gl_in[1].gl_Position.x;

        vec4 p1 = gl_in[0].gl_Position;
        vec4 p2 = gl_in[1].gl_Position;

        if( x2 < x1 )
        {
            float temp = x2;
            x2 = x1;
            x1 = temp;

            vec4 temp2 = p2;
            p2 = p1;
            p1 = temp2;
        }

        float wrapDist = ( x1 - wrap1 ) + ( wrap2 - x2 );
        float strtDist =   x2 - x1;

        if( wrapDist < strtDist )
        {
            float r = ( wrap2 - x2 ) / wrapDist;
            float yWrap = p2.y + r * ( p1.y - p2.y );

//            // 0 -------|___0

            distX = abs( wrap2 - p2.x ) * width;
            distY = abs( yWrap - p2.y ) * height;

            norm = aNorm;
            if( distanceWeighted )
            {
                float dist = max( length( vec2( distX, distY ) ), 1 );
                norm *= dist;
            }
            norm = r / norm;

            g2f.valAll    =  norm;
            g2f.valComb   =  norm * s;
            gl_Position = p2;
            EmitVertex();

            vec4 tmp = p2;
            tmp.x = wrap2;
            tmp.y = yWrap;

            g2f.valAll    =  norm;
            g2f.valComb   =  norm * s;
            gl_Position = tmp;
            EmitVertex();

            EndPrimitive();

            // 0 __|--------0

            distX = abs( p1.x - wrap1 ) * width;
            distY = abs( p1.y - yWrap ) * height;

            norm = aNorm;
            if( distanceWeighted )
            {
                float dist = max( length( vec2( distX, distY ) ), 1 );
                norm *= dist;
            }
            norm = ( 1.0 - r ) / norm;

            tmp.y = yWrap;
            tmp.x = wrap1;

            g2f.valAll    =  norm;
            g2f.valComb   =  norm * s;
            gl_Position   =  tmp;
            EmitVertex();

            g2f.valAll    =  norm;
            g2f.valComb   =  norm * s;
            gl_Position   = p1;
            EmitVertex();

            //EndPrimitive();
        }
        else
        {
            norm = aNorm;
            if( distanceWeighted )
            {
                norm *= dist;
            }
            norm = 1.0 / norm;

            g2f.valAll    =  norm;
            g2f.valComb   =  norm * s;

            gl_Position = pos1;
            EmitVertex();

            g2f.valAll    =   norm;
            g2f.valComb   =   norm * s;

            gl_Position = pos2;
            EmitVertex();

            //EndPrimitive();
        }
    }
}
