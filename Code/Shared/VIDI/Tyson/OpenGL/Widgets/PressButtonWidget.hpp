


#ifndef UIWIDGET
#define UIWIDGET

#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"

#include <QOpenGLTexture>
#include <QFile>

namespace TN
{

class PressButton : public Widget
{
    int m_id;
    Vec3< float > m_color;
    bool m_isPressed;
    bool m_flash;
    double m_flashPhase;
    Vec3< float > m_flashColor;

public:

    virtual ~PressButton()
    {}

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    PressButton() : m_isPressed( false ), m_flash( false ), m_flashPhase(0) {}

    PressButton( const Vec3< float > & color, int width, int height, int id ) : m_id( id ), m_color( color ), m_isPressed( false ), m_flash( false ), m_flashPhase(0)
    {
        resize( width, height );
    }

    const Vec3< float > & color() const
    {
        return m_color;
    }

    bool isPressed() const
    {
        return m_isPressed;
    }

    int id() const
    {
        return m_id;
    }

    void toggle()
    {
        m_isPressed = ! m_isPressed;
    }

    void setPressed( bool pressed )
    {
        m_isPressed = pressed;
    }

    bool flash() const { return m_flash; }
    void flash( bool b ) { m_flash = b; }

    void flashColor( const Vec3< float > & cl )
    {
        m_flashColor = cl;
    }

    Vec3< float > flashColor() const
    {
        return m_flashColor;
    }

    double flashPhase() const { return m_flashPhase; }
    void flashPhase( double phase ) { m_flashPhase = phase; }
};

class TexturedPressButton : public PressButton
{
    QOpenGLTexture * m_texPressed;
    QOpenGLTexture * m_tex;

public :

    void bind( bool pressed )
    {
        if( pressed )
        {
            m_texPressed->bind();
        }
        else
        {
            m_tex->bind();
        }
    }

    void release( bool pressed )
    {
        if( pressed )
        {
            m_texPressed->release();
        }
        else
        {
            m_tex->release();
        }
    }

    TexturedPressButton()
        : PressButton(), m_tex( nullptr ), m_texPressed( nullptr )
    {}

    TexturedPressButton( const Vec3< float > & color, int width, int height, int id )
        : PressButton( color, width, height, id ), m_tex( nullptr ), m_texPressed( nullptr )
    {}

    void setTexFromPNG( const std::string & pngPath, const std::string & pngPathPressed )
    {
         delete m_tex;
         delete m_texPressed;

         m_tex = new QOpenGLTexture( QImage( pngPath.c_str() ).mirrored() );
         m_texPressed = new QOpenGLTexture( QImage( pngPathPressed.c_str() ).mirrored() );
    }

    void resizeByHeight( double h )
    {
        if( m_tex == nullptr )
        {
            return;
        }

        double aspec = m_tex->width() / ( double ) m_tex->height();
        setSize( aspec*h, h );
    }

    virtual ~TexturedPressButton()
    {
        delete m_tex;
        delete m_texPressed;
    }
};

}

#endif // UIWIDGET

