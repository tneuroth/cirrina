#version 130



uniform mat4 MVP;
in float xAttr;
in float yAttr;
in float wAttr;

out float w;

void main(void)
{
    w = wAttr;
    gl_Position = MVP*vec4( xAttr, yAttr, 0, 1.0 );
}
