#version 400




in float xAttr;
in float yAttr;
in int c;

uniform bool combo;
uniform mat4 MVP;
flat out int select;

void main(void)
{
    if( combo )
    {
        select = int( ( ( ( c & 1 ) != 0 ) && ( ( c & 2 ) != 0 ) ) );
    }
    else
    {
        select = int( ( ( ( c & 1 ) != 0  ) ) );
    }

    gl_Position = MVP * vec4(
         xAttr,
         yAttr,
         0.0,
         1.0
    );
}
