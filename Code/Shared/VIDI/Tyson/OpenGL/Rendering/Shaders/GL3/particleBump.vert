#version 130



in vec3 posAttr;
in vec3 normalAttr;
in float memberAttr;

out vec3 fragPos;
out vec3 normal;

uniform mat4 MVP;
uniform int cm;


void main(void)
{
    normal = normalAttr;

    vec3 fp = posAttr;
    fp.x = fp.x * MVP[0][0];
    fp.y = fp.y * MVP[1][1];
    fp.z = fp.z * MVP[2][2];

    fragPos = fp;

    if( int( memberAttr ) != cm ) {
        gl_Position = vec4( 9999999, 999999, 0, 1 );
    }
    else {
        gl_Position = MVP * vec4( posAttr, 1 );
    }
}
