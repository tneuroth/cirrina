#version 400



//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2015-11-06
//UPDATED: 2015-11-06

layout(lines_adjacency) in;
layout(triangle_strip, max_vertices = 93) out;

uniform float radius;

uniform mat4 mvm;
uniform mat4 prj;
uniform mat3 normmtx;

in V2G{
    int breakHere;
}v2g[];

out G2F{
    vec3 e_n;//vtx normal in eye space
    vec3 e_pos;
}g2f;

void passThrough(){
    for (int i = 0; i < gl_in.length(); i++){
        gl_Position = prj * mvm * gl_in[i].gl_Position ;
        EmitVertex();
    }
    EndPrimitive();
}

const int nseg = 16;

#define PI 3.1415926
#define TwoPI 6.2831852
#define PIOver2 1.5707963
#define ThreePIOver2 4.7123889
#define EPS 1E-6

vec2 squazeeRadius(float radius, float angle, float curMag){
    float b = .0005;
    float a = .0005;//pow((1.0 - curMag*50.0),80.0);
    return vec2(a, b);
}

void pathtube(){

    if( v2g[2].breakHere == 1 ) {
        return;
    }

    vec3 pos0 = gl_in[0].gl_Position.xyz;
    vec3 pos1 = gl_in[1].gl_Position.xyz;
    vec3 pos2 = gl_in[2].gl_Position.xyz;
    vec3 pos3 = gl_in[3].gl_Position.xyz;
    vec3 tan10 = (pos1 - pos0);
    vec3 tan21 = (pos2 - pos1);
    vec3 tan32 = (pos3 - pos2);

    vec3 tan20 = (pos2 - pos0);
    vec3 tan31 = (pos3 - pos1);
    vec3 y1, y2, z1, z2;

    vec3 curvature1 = tan21 - tan10;//tan31 - tan20;/
    vec3 curvature2 = tan32 - tan21;//tan31 - tan20;/
    float curvatureMag1 = length(curvature1);
    float curvatureMag2 = length(curvature2);

    //create local coordinate system at cross-section
    vec3 y = vec3(0, 1, 0);
    z1 = normalize(cross(tan21, y));
    y1 = normalize(cross(z1, tan21));
    z2 = normalize(cross(tan32, y));
    y2 = normalize(cross(z2, tan32));

    vec3 quadvertices[4];
    vec3 n[4];
    float angleDelta = TwoPI / float(nseg);
    float angle = 0.0;
    vec2 ab1 = vec2(.0005), ab2 = vec2(.0005);//ellipse two axis length.
    ab1 = squazeeRadius(.0005, angle, curvatureMag1);
    ab2 = squazeeRadius(.0005, angle, curvatureMag2);

    n[0] = ab1.x * z1;//===ab1.x * cos(0)*z1 + ab1.y * sin(0)*y1
    n[1] = ab2.x * z2;//===ab2.x * cos(0)*z2 + ab1.y * sin(0)*y2

    quadvertices[0] = n[0] + pos1;
    quadvertices[1] = n[1] + pos2;

    n[0] = normalize(normmtx * n[0]);
    n[1] = normalize(normmtx * n[1]);

    vec4 e_pos;
    angle = angleDelta;
    for (int i = 0; i <= nseg+1; i++, angle += angleDelta)
    {
        ab1 = vec2(.0005);
        ab2 = vec2(.0005);
        ab1 = squazeeRadius(.0005, angle, curvatureMag1);
        ab2 = squazeeRadius(.0005, angle, curvatureMag2);
        n[2] = (ab1.x * cos(angle)*z1 + ab1.y * sin(angle)*y1);
        n[3] = (ab2.x * cos(angle)*z2 + ab2.y * sin(angle)*y2);
        quadvertices[2] = n[2] + pos1;
        quadvertices[3] = n[3] + pos2;
        //emit triangle
        //face normal
        n[2] = normalize(normmtx * n[2]);
        n[3] = normalize(normmtx * n[3]);

        e_pos = mvm * vec4(quadvertices[0], 1.0);
        gl_Position = prj * e_pos;
        g2f.e_n = n[0];
        g2f.e_pos = e_pos.xyz;
        EmitVertex();

        e_pos = mvm * vec4(quadvertices[1], 1.0);
        gl_Position = prj * e_pos;
        g2f.e_n = n[1];
        g2f.e_pos = e_pos.xyz;
        EmitVertex();

        e_pos = mvm * vec4(quadvertices[3], 1.0);
        gl_Position = prj * e_pos;
        g2f.e_n = n[3];
        g2f.e_pos = e_pos.xyz;
        EmitVertex();
        EndPrimitive();

        e_pos = mvm * vec4(quadvertices[0], 1.0);
        gl_Position = prj * e_pos;
        g2f.e_n = n[0];
        g2f.e_pos = e_pos.xyz;
        EmitVertex();

        e_pos = mvm * vec4(quadvertices[3], 1.0);
        gl_Position = prj * e_pos;
        g2f.e_n = n[3];
        g2f.e_pos = e_pos.xyz;
        EmitVertex();

        e_pos = mvm * vec4(quadvertices[2], 1.0);
        gl_Position = prj * e_pos;
        g2f.e_n = n[2];
        g2f.e_pos = e_pos.xyz;
        EmitVertex();
        EndPrimitive();

        quadvertices[0] = quadvertices[2];
        quadvertices[1] = quadvertices[3];
        n[0] = n[2];
        n[1] = n[3];
    }
}

void main(void)
{
    pathtube();
}
