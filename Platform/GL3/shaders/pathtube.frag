#version 400



//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2015-11-06
//UPDATED: 2015-11-06

const int SUN   = 0;
const int POINT = 1;
const int SPOT  = 2;

const int MAX_LIGHTS = 8;

struct Material {

    vec3 Ka;
    vec3 Kd;
    vec3 Ks;

    vec3 Ka_scale;
    vec3 Kd_scale;
    vec3 Ks_scale;

    bool overrideAmbient;
    float alpha;
    float opacity;
};

struct LightSource {
    int type;
    vec3 direction;
    vec3 Ia;
    vec3 Id;
    vec3 Is;
    vec3 position;
    float at1;
    float at2;
    float cutOff;
    float exponent;
};

//uniform float pathLength;
uniform Material M;
uniform LightSource L[ MAX_LIGHTS ];
uniform bool enabled[ MAX_LIGHTS ];

in G2F{
    vec3 e_n;
    vec3 e_pos;
}g2f;

out vec4 fragColor;

void main() {

    vec3 cl = vec3(0,0,0);

    vec3 Nm = normalize( g2f.e_n );
    vec3 Vm = normalize( -g2f.e_pos );

    for ( int i = 0; i < MAX_LIGHTS; ++i ) {

        if ( enabled[i] ) {

            float attenuation;

            vec3 lightDirection = normalize( vec3( -.5, 0.4f, 1.0f ) );
            attenuation    = 1.0;

            if ( M.overrideAmbient ) {
                cl += M.Ka;
            }
            else {
                cl += M.Ka;
            }

            float angle = dot( Nm, lightDirection );

            if ( angle > 0.0) {
                vec3 Rm = normalize( reflect( -lightDirection, Nm ) );
                cl += attenuation * (  M.Kd * angle * L[i].Id + M.Ks * pow( max( dot( Rm, Vm ), 0 ), M.alpha ) * L[i].Is );
            }
        }
    }
    fragColor = vec4( min( cl, vec3( 1.0 ) ), 1.0 );
}
