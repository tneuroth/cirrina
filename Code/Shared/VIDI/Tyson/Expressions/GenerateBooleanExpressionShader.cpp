
#include <string>
#include <iostream>
#include <set>
#include <array>

namespace TN
{

const std::string COMB_DEFAULT_SHADER =
    "#version 130                             \n"
    "in vec2 pos;                             \n"
    "in short ms;                             \n"
    "out bool remove;                         \n"
    "uniform mat4 MVP;                        \n"
    "void main(void)                          \n"
    "{                                        \n"
    "    remove = false;                      \n"
    "    gl_Position = MVP * vec4(posAttr,1); \n"
    "}";

const std::string COMB_TEMPLATE_SHADER =
    "#version 130                             \n"
    "in vec2 pos;                             \n"
    "in short ms;                             \n"
    "out bool remove;                         \n"
    "uniform mat4 MVP;                        \n"
    "void main(void)                          \n"
    "{                                        \n"
    "    remove = X;                          \n"
    "    gl_Position = MVP * vec4(posAttr,1); \n"
    "}";

const std::set< char > SET_IDS =
{
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'
};

const std::array< short, 16 > POWERS_OF_2  =
{
    0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384
};

std::string replacementText( char S )
{
    const char offset = 'A';
    std::string result = "( ms & " + std::to_string( POWERS_OF_2[ S - offset ] ) + " )";
    return result;
}

std::string generateCombinationShader( const std::string & expr )
{
    for( size_t i = 0; i < expr.size(); ++i )
    {
        if( SET_IDS.count( expr[ i ] ) )
        {
            std::string replacement = replacementText( expr[ i ] );
            expr.erase( expr.begin() + i );
            expr.insert( expr.begin() + i, replacement.begin(), replacement.end() );
        }
    }

    std::string shaderCode = COMB_TEMPLATE_SHADER;

    for( size_t i = 0; i < shaderCode.size(); ++i )
    {
        if( shaderCode[ i ] == 'X' )
        {
            shaderCode.erase( shaderCode.begin() + i );
            shaderCode.insert( shaderCode.begin() + i, expr.begin(), expr.end() );
        }
    }

    return shaderCode;
}

}
