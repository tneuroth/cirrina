
#ifndef TN_XGC_IMPORTER_HPP
#define TN_XGC_IMPORTER_HPP

#include "Types/Vec.hpp"
#include "Data/Interpolators/FieldInterpolator.h"
#include "../ParticleDataImporter.hpp"
#include "Data/Definitions/VisualContextModel.hpp"
#include "Algorithms/Standard/MyAlgorithms.hpp"

#include "hdf5.h"

#include <QDebug>

#include <fstream>
#include <map>
#include <string>

#include <vector>
#include <memory>
#include <limits>

#include <boost/filesystem.hpp>

namespace TN
{

template< typename TargetFloatType >
/**
 * @brief The XGCImporter class
 */
class XGCImporter : public ParticleDataImporter< TargetFloatType >
{
    bool m_psinInterpolatorInitialized;
    bool m_psinTextureLoaded;
    bool m_bfieldTexturesLoaded;

    std::vector< std::vector< std::int32_t > > baseSubsetFlags;

    FieldInterpolator21 m_psinInterpolator;
    FieldInterpolator23 m_bFieldInterpolator;

    LookupTexture< double > m_psinLookupTexture;
    LookupTexture< double > m_brLookupTexture;
    LookupTexture< double > m_bzLookupTexture;
    LookupTexture< double > m_bphiLookupTexture;

    typedef float OrigionalFloatType;

    std::vector< OrigionalFloatType > m_swap;
    std::vector< std::int32_t > m_simulationTimeSteps;

    std::map< std::string, std::size_t > m_numParticles;
    std::map< std::string, std::unordered_map< std::size_t, std::size_t > > m_idMaps;

    std::map< std::string, int >  m_attrKeyToPhaseIndex;

    std::vector< std::string > m_defaultSubsets;
    TN::Vec2< double > m_center;

    void initializeBfieldInterpolator( FieldInterpolator23 & interpolator )
    {
        const std::string BASE_PATH = this->m_basicDataInfo.location;

        std::vector< Vec3< double > > values;
        std::vector< Vec2< double > > rz;

        // Read rz
        hid_t file_id = H5Fopen( ( BASE_PATH + "/Grid/xgc.mesh.h5" ).c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);
        hid_t dataset_id = H5Dopen2( file_id, "rz", H5P_DEFAULT );
        hid_t dspace = H5Dget_space( dataset_id );
        int ndims = H5Sget_simple_extent_ndims( dspace );
        hsize_t dims[ ndims ];
        H5Sget_simple_extent_dims( dspace, dims, NULL );
        rz.resize( dims[ 0 ] );
        herr_t status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ( double * ) rz.data() );
        status = H5Dclose( dataset_id );
        status = H5Fclose(file_id);

        // Read BField
        file_id = H5Fopen( ( BASE_PATH + "/Grid/xgc.bfield.h5" ).c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);
        hid_t group = H5Gopen2( file_id, "node_data[0]", H5P_DEFAULT );
        dataset_id = H5Dopen2( group, "values", H5P_DEFAULT );
        dspace = H5Dget_space( dataset_id );
        ndims = H5Sget_simple_extent_ndims( dspace );
        hsize_t v_dims[ ndims ];
        H5Sget_simple_extent_dims( dspace, v_dims, NULL );
        values.resize( v_dims[ 0 ] );
        status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, values.data() );
        status = H5Dclose( dataset_id );

        status = H5Gclose( group );
        status = H5Fclose(file_id);

        interpolator.set( rz, values );
    }

    void initializePsinInterpolator( FieldInterpolator21 & interpolator, const std::string & name )
    {
        const std::string nm = name == "psin" ?  "psi" : name;
        const std::string BASE_PATH = this->m_basicDataInfo.location;

        std::vector< double > values;
        std::vector< Vec2< double > > rz;

        hid_t file_id = H5Fopen( ( BASE_PATH + "/Grid/xgc.mesh.h5" ).c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);

        // Read rz
        hid_t dataset_id = H5Dopen2( file_id, "rz", H5P_DEFAULT );
        hid_t dspace = H5Dget_space( dataset_id );
        int ndims = H5Sget_simple_extent_ndims( dspace );
        hsize_t dims[ ndims ];
        H5Sget_simple_extent_dims( dspace, dims, NULL );

        //////qDebug() << dims[ 0 ] << " " << dims[ 1 ];

        rz.resize( dims[ 0 ] );
        herr_t status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ( double * ) rz.data() );
        status = H5Dclose( dataset_id );

        // Read Psi
        dataset_id = H5Dopen2( file_id, nm.c_str(), H5P_DEFAULT );
        dspace = H5Dget_space( dataset_id );
        ndims = H5Sget_simple_extent_ndims( dspace );
        hsize_t v_dims[ ndims ];
        H5Sget_simple_extent_dims( dspace, v_dims, NULL );
        values.resize( v_dims[ 0 ] );
        status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, values.data() );
        status = H5Dclose( dataset_id );

        status = H5Fclose(file_id);

        if( nm == "psi" )
        {
            const double NM = values.back();
            const int SZ = values.size();

            #pragma omp parallel for simd
            for( int i = 0; i < SZ; ++i )
            {
                values[ i ] /= NM;
            }
        }

        interpolator.set( rz, values );
    }

    void generateSeparatrixLines()
    {
        const std::string BASE_PATH = this->m_basicDataInfo.location;

        std::vector< double > psin;
        std::vector< Vec2< double > > rz;

        hid_t file_id = H5Fopen( ( BASE_PATH + "/Grid/xgc.mesh.h5" ).c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);

        // Read rz
        hid_t dataset_id = H5Dopen2( file_id, "rz", H5P_DEFAULT );
        hid_t dspace = H5Dget_space( dataset_id );
        int ndims = H5Sget_simple_extent_ndims( dspace );
        hsize_t dims[ ndims ];
        H5Sget_simple_extent_dims( dspace, dims, NULL );
        rz.resize( dims[ 0 ] );
        herr_t status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ( double * ) rz.data() );
        status = H5Dclose( dataset_id );

        dataset_id = H5Dopen2( file_id, "psi", H5P_DEFAULT );
        dspace = H5Dget_space( dataset_id );
        ndims = H5Sget_simple_extent_ndims( dspace );
        hsize_t v_dims[ ndims ];
        H5Sget_simple_extent_dims( dspace, v_dims, NULL );
        psin.resize( v_dims[ 0 ] );
        status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ( double * ) psin.data() );
        status = H5Dclose( dataset_id );
        status = H5Fclose(file_id);

        const double NM = psin.back();
        const int SZ = psin.size();
        #pragma omp parallel for simd
        for( int i = 0; i < SZ; ++i )
        {
            psin[ i ] /= NM;
        }

        std::vector< size_t > indices;
        indices.reserve( 10000 );
        for( size_t i = 0, end = psin.size(); i < end; ++i )
        {
            if( std::abs( psin[ i ] - 1.0 ) < .00001 )
            {
                indices.push_back( i );
            }
        }

        const size_t NP = indices.size();

        std::vector< double > rStx( NP );
        std::vector< double > zStx( NP );

        #pragma omp for
        for( size_t i = 0; i < NP; ++i )
        {
            rStx[ i ] = rz[ indices[ i ] ].x();
            zStx[ i ] = rz[ indices[ i ] ].y();
        }

        std::ofstream outFile( this->m_basicDataInfo.location + "/Preprocessed/Geometry/separatrix.txt" );
        outFile << "r z\n";
        for( size_t i = 0; i < indices.size(); ++i )
        {
            outFile << rStx[ i ] << " " << zStx[ i ] << "\n";
        }
    }

    void generateLookupTexture3(
        LookupTexture< double > & tex1,
        LookupTexture< double > & tex2,
        LookupTexture< double > & tex3,
        FieldInterpolator23 & interpolator )
    {
        const std::string path1 = this->m_basicDataInfo.location + "/Preprocessed/Grid/br.bin";
        const std::string path2 = this->m_basicDataInfo.location + "/Preprocessed/Grid/bz.bin";
        const std::string path3 = this->m_basicDataInfo.location + "/Preprocessed/Grid/bphi.bin";

        std::ifstream f;
        f.open( path1, std::ios::binary | std::ios::in );

        if( ! f.good() )
        {
            if( ! interpolator.initialized )
            {
                initializeBfieldInterpolator( interpolator );
            }

            generateGridToTexture3( interpolator, tex1, tex2, tex3, 8192 );

            std::ofstream outFile( path1, std::ios::binary );
            outFile.write( (char*) & tex1.dims[0], sizeof( int ) );
            outFile.write( (char*) & tex1.dims[1], sizeof( int ) );
            outFile.write( (char*) & tex1.yRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex1.yRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) & tex1.xRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex1.xRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) tex1.data.data(), sizeof( double ) * tex1.data.size() );
            outFile.close();

            outFile.open( path2, std::ios::binary );
            outFile.write( (char*) & tex2.dims[0], sizeof( int ) );
            outFile.write( (char*) & tex2.dims[1], sizeof( int ) );
            outFile.write( (char*) & tex2.yRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex2.yRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) & tex2.xRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex2.xRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) tex2.data.data(), sizeof( double ) * tex2.data.size() );
            outFile.close();

            outFile.open( path3, std::ios::binary );
            outFile.write( (char*) & tex3.dims[0], sizeof( int ) );
            outFile.write( (char*) & tex3.dims[1], sizeof( int ) );
            outFile.write( (char*) & tex3.yRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex3.yRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) & tex3.xRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex3.xRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) tex3.data.data(), sizeof( double ) * tex3.data.size() );
            outFile.close();

            f.close();
        }
        else
        {
            tex1.dims = { 0, 0 };
            f.read( (char*) & tex1.dims[0], sizeof( int ) );
            f.read( (char*) & tex1.dims[1], sizeof( int ) );
            f.read( (char*) & tex1.yRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex1.yRange ) + 1 ), sizeof( double ) );
            f.read( (char*) & tex1.xRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex1.xRange ) + 1 ), sizeof( double ) );
            tex1.data.resize( tex1.dims[ 0 ] * tex1.dims[ 1 ] );
            f.read( (char*) tex1.data.data(), sizeof( double ) * tex1.data.size() );
            f.close();

            f.open( path2, std::ios::binary | std::ios::in );
            tex2.dims = { 0, 0 };
            f.read( (char*) & tex2.dims[0], sizeof( int ) );
            f.read( (char*) & tex2.dims[1], sizeof( int ) );
            f.read( (char*) & tex2.yRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex2.yRange ) + 1 ), sizeof( double ) );
            f.read( (char*) & tex2.xRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex2.xRange ) + 1 ), sizeof( double ) );
            tex2.data.resize( tex2.dims[ 0 ] * tex2.dims[ 1 ] );
            f.read( (char*) tex2.data.data(), sizeof( double ) * tex2.data.size() );
            f.close();

            f.open( path3, std::ios::binary | std::ios::in );
            tex3.dims = { 0, 0 };
            f.read( (char*) & tex3.dims[0], sizeof( int ) );
            f.read( (char*) & tex3.dims[1], sizeof( int ) );
            f.read( (char*) & tex3.yRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex3.yRange ) + 1 ), sizeof( double ) );
            f.read( (char*) & tex3.xRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex3.xRange ) + 1 ), sizeof( double ) );
            tex3.data.resize( tex3.dims[ 0 ] * tex3.dims[ 1 ] );
            f.read( (char*) tex3.data.data(), sizeof( double ) * tex3.data.size() );
            f.close();
        }
    }

    void generateLookupTexture(
        LookupTexture< double > & tex,
        FieldInterpolator21 & interpolator,
        const std::string & name )
    {
        const std::string path = this->m_basicDataInfo.location + "/Preprocessed/Grid/" + name + ".bin";
        std::ifstream f;
        f.open( path, std::ios::binary | std::ios::in );

        if( ! f.good() )
        {
            if( ! interpolator.initialized )
            {
                initializePsinInterpolator( interpolator, name );
            }

            generateGridToTexture( interpolator, tex, 2048 );

            std::ofstream outFile( path, std::ios::binary );
            outFile.write( (char*) & tex.dims[0], sizeof( int ) );
            outFile.write( (char*) & tex.dims[1], sizeof( int ) );
            outFile.write( (char*) & tex.yRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex.yRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) & tex.xRange, sizeof( double ) );
            outFile.write( (char*) ( ( ( double * ) &tex.xRange ) + 1 ), sizeof( double ) );
            outFile.write( (char*) tex.data.data(), sizeof( double ) * tex.data.size() );
            outFile.close();
        }
        else
        {
            tex.dims = { 0, 0 };
            f.read( (char*) & tex.dims[0], sizeof( int ) );
            f.read( (char*) & tex.dims[1], sizeof( int ) );
            f.read( (char*) & tex.yRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex.yRange ) + 1 ), sizeof( double ) );
            f.read( (char*) & tex.xRange, sizeof( double ) );
            f.read( (char*) ( ( ( double * ) &tex.xRange ) + 1 ), sizeof( double ) );
            tex.data.resize( tex.dims[ 0 ] * tex.dims[ 1 ] );
            f.read( (char*) tex.data.data(), sizeof( double ) * tex.data.size() );
        }
        f.close();
    }

    template< typename T >
    void getPoloidalAngles(
        const std::vector< T > & r,
        const std::vector< T > & z,
        const Vec2< double > & center,
        std::vector< TargetFloatType > & result )
    {
        const int NS = r.size();
        for( int s = 0; s < NS; ++s )
        {
            result[ s ] = ( Vec2< double >( r[ s ], z[ s ] ) - center ).angle( Vec2< double >( 1.0, 0.0 ) );
//            if( result[ s ] > 3.14159265359 )
//            {
//                result[s ] -= 2*3.14159265359;
//            }
        }
    }

    template< typename T >
    void getTangents(
        const std::vector< T > & r,
        const std::vector< T > & z,
        const Vec2< double > & center,
        std::vector< TargetFloatType > & result )
    {
        const int NS = r.size();
        for( int s = 0; s < NS; ++s )
        {
            result[ s ] = ( Vec2< double >( r[ s ], z[ s ] ) - center ).angle( Vec2< double >( 1.0, 0.0 ) );
        }
    }

    void getPsinFromMesh(
        const std::vector< OrigionalFloatType > & r,
        const std::vector< OrigionalFloatType > & z,
        std::vector< TargetFloatType > & result )
    {
        const int SZ = r.size();
        result.resize( r.size() );

        //////qDebug() << "loading psin";

        #pragma omp parallel for
        for( int i = 0; i < SZ; ++i )
        {
            result[ i ] = m_psinInterpolator.interpLin( Vec2< double >( r[ i ], z[ i ] ) );
        }
    }

    void texLookup(
        const std::vector< OrigionalFloatType > & r,
        const std::vector< OrigionalFloatType > & z,
        const LookupTexture< double > & tex,
        std::vector< TargetFloatType > & result )
    {
        const int ROWS = tex.dims[ 0 ];
        const int COLS = tex.dims[ 1 ];

        const int SZ = r.size();
        result.resize( SZ );

        const Vec2< double > X_RANGE = tex.xRange;
        const Vec2< double > Y_RANGE = tex.yRange;

        const double XWIDTH = X_RANGE.b() - X_RANGE.a();
        const double YWIDTH = Y_RANGE.b() - Y_RANGE.a();

        #pragma omp parallel for
        for( int i = 0; i < SZ; ++i )
        {
            int c = std::max( std::min( (int) std::floor( ( ( r[ i ] - X_RANGE.a() ) / XWIDTH ) * COLS ), COLS-1 ), 0 );
            int r = std::max( std::min( (int) std::floor( ( ( z[ i ] - Y_RANGE.a() ) / YWIDTH ) * ROWS ), ROWS-1 ), 0 );
            result[ i ] = tex.data[ r*COLS + c ];
        }
    }

public :

    virtual bool init( const BasicDataInfo & basicDataInfo )
    {
        this->m_basicDataInfo = basicDataInfo;
        this->m_initialized = true;
        m_psinInterpolatorInitialized = false;
        m_psinTextureLoaded = false;
        m_bfieldTexturesLoaded = false;

        m_attrKeyToPhaseIndex =
        {
            { "r",            0 },   // Major radius [m]
            { "z",            1 },   // Azimuthal direction [m]
            { "zeta",         2 },   // Toroidal angle
            { "rho_parallel", 3 },   // Parallel Larmor radius [m]
            { "w1",           4 },   // Grid weight 1
            { "w2",           5 },   // Grid weight 2
            { "mu",           6 },   // Magnetic moment
            { "w0",           7 },   // Grid weight
            { "f0",           8 }
        }; // Grid distribution function

        return true;
    }

    void loadTimeSteps(
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        // non-uniform temporal sampling is not yet supported ... will be later ...
        int first, int last, int stride  )
    {
        using namespace boost;

        const std::string BASE_PATH = this->m_basicDataInfo.location + "/ParticleData/";

        filesystem::path p( BASE_PATH );

        simulationTimeSteps.clear();
        realTime.clear();

        if( ! filesystem::is_directory( p ) )
        {
            //std::cout << p << " is not a directory";
        }
        else
        {
            //std::cout << p << " is a directory";
            filesystem::directory_iterator end_itr;
            for ( filesystem::directory_iterator itr( p ); itr != end_itr; ++itr )
            {
                if ( is_regular_file( itr->path() ) )
                {
                    std::string current_file = itr->path().filename().string();
                    std::stringstream sstr( current_file );
                    std::string a,b,c;
                    std::getline( sstr, a, '.' );
                    std::getline( sstr, b, '.' );
                    std::getline( sstr, c, '.' );

                    if( a == "xgc" && b == "particle" )
                    {
                        hid_t file_id = H5Fopen( ( BASE_PATH + current_file ).c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);
                        hid_t group = H5Gopen2( file_id, "/", H5P_DEFAULT );

                        int ts = 0;
                        hid_t dataset_id = H5Dopen2( group, "timestep", H5P_DEFAULT );
                        herr_t status = H5Dread( dataset_id, H5T_STD_I32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, & ts );
                        status = H5Dclose( dataset_id );

                        double rt = 0;
                        dataset_id = H5Dopen2( group, "time", H5P_DEFAULT );
                        status = H5Dread( dataset_id, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, & rt );
                        status = H5Dclose( dataset_id );

                        status = H5Gclose( group );
                        status = H5Fclose(file_id);

                        // non-uniform temporal sampling is not yet supported ... will be later ...
                        if( ( ts - first ) % stride == 0 && ts <= last )
                        {
                            simulationTimeSteps.push_back( ts );
                            realTime.push_back( rt );
                        }
                    }
                }
            }

            TN::Parallel::sortTogether( simulationTimeSteps, realTime );

            m_numParticles.clear();
            m_idMaps.clear();

            std::string nstr = std::to_string( simulationTimeSteps.front() );
            std::string path = this->m_basicDataInfo.location + "/ParticleData/" + "xgc.particle." + std::string( 5 - nstr.size(), '0' ) + nstr  +  ".h5";
            hid_t file_id = H5Fopen( path.c_str() , H5F_ACC_RDONLY, H5P_DEFAULT);
            ////////////////////////////////////////////////////////////
            // get standard order and number of particles for electrons

            hid_t group = H5Gopen2( file_id, "/", H5P_DEFAULT );
            hid_t dataset_id = H5Dopen2( group, "egid", H5P_DEFAULT );
            hid_t dataspace_id = H5Dget_space ( dataset_id );
            int ndims = H5Sget_simple_extent_ndims( dataspace_id );
            hsize_t dims[ ndims ];
            H5Sget_simple_extent_dims( dataspace_id, dims, NULL );

            m_numParticles.insert( { "electrons", dims[ 0 ] } );
            std::vector< long int > ids( dims[ 0 ] );

            herr_t status = H5Dread( dataset_id, H5T_STD_I64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ids.data() );

            status = H5Dclose( dataset_id );
            status = H5Gclose( group );

            m_idMaps.insert( { "electrons", std::unordered_map< std::size_t, std::size_t >() } );
            std::unordered_map< std::size_t, std::size_t > & emp =  m_idMaps.at( "electrons" );
            emp.reserve( dims[ 0 ]*3/2 );

            for( int i = 0; i < dims[ 0 ]; ++i )
            {
                emp.insert( { ids[ i ], i } );
            }

            ////////////////////////////////////////////////////////////
            // get standard order and number of particles for ions

            group = H5Gopen2( file_id, "/", H5P_DEFAULT );
            dataset_id = H5Dopen2( group, "igid", H5P_DEFAULT );
            dataspace_id = H5Dget_space ( dataset_id );
            ndims = H5Sget_simple_extent_ndims( dataspace_id );
            H5Sget_simple_extent_dims( dataspace_id, dims, NULL );

            m_numParticles.insert( { "ions", dims[ 0 ] } );
            ids.resize( dims[ 0 ] );

            status = H5Dread( dataset_id, H5T_STD_I64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ids.data() );
            status = H5Dclose( dataset_id );

            m_idMaps.insert( { "ions", std::unordered_map< std::size_t, std::size_t >() } );
            std::unordered_map< std::size_t, std::size_t > & imp =  m_idMaps.at( "ions" );
            imp.reserve( dims[ 0 ]*3/2 );

            for( int i = 0; i < dims[ 0 ]; ++i )
            {
                imp.insert( { ids[ i ], i } );
            }

            /////////////////////////////////////////////////////////////

            status = H5Gclose( group );
            status = H5Fclose(file_id);
        }

        //////qDebug() << "done with initialzing time steps " << m_idMaps.at( "ions" ).size() << " " << m_idMaps.at( "electrons" ).size();
    }

    void readPreprocessedStep(
        std::vector< TargetFloatType > & result,
        const std::string & ptype,
        const std::string & attr,
        size_t ts )
    {
//        ////////// WRAPAROUND
//        if( ts >= 570 )
//        {
//            ts %= 570;
//            if( ts & 1 )
//            {
//                ++ts;
//            }
//        }

        ////qDebug() << "reading " << attr.c_str();

        std::ifstream infoFile( this->m_basicDataInfo.location + "/Preprocessed/ParticleData/info." + ptype + ".txt" );
        std::string line;
        std::getline( infoFile, line );
        std::getline( infoFile, line );
        infoFile.close();

        std::stringstream sstr( line );
        std::size_t np;
        sstr >> np;

        ////qDebug() << np;

        std::string tstr = std::to_string( ts );
        std::ifstream pFile( this->m_basicDataInfo.location + "/Preprocessed/ParticleData/" + "xgc." + ptype + "." + std::string( 5 - tstr.size(), '0' ) + tstr  +  ".bin", std::ios::in | std::ios::binary );
        if( ! pFile.is_open() )
        {
            ////qDebug() << "couldn't open the file";
        }

        pFile.seekg( np * m_attrKeyToPhaseIndex.at( attr ) * sizeof( float ) );


        const size_t N_READ = m_numParticles.at( "ions" );
        result.resize( N_READ );

//        if( N_READ <= np )
//        {
            pFile.read( (char *) result.data(), N_READ * sizeof( float ) );
//        }
//        else
//        {
//            size_t toRead = N_READ;
//            int chunksRead = 0;
//            while( toRead > np )
//            {
//                pFile.seekg( np * m_attrKeyToPhaseIndex.at( attr ) * sizeof( float ) );
//                pFile.read( (char *) ( result.data()+np*chunksRead ), np * sizeof( float ) );
//                ++chunksRead;
//                toRead -= np;
//            }
//            pFile.seekg( np * m_attrKeyToPhaseIndex.at( attr ) * sizeof( float ) );
//            pFile.read( (char *) ( result.data()+np*chunksRead ), toRead * sizeof( float ) );
//        }

//        pFile.seekg( np * m_attrKeyToPhaseIndex.at( attr ) * sizeof( float ) );
//        pFile.read( (char *) ( result.data()+np ), np * sizeof( float ) );

//        pFile.seekg( np * m_attrKeyToPhaseIndex.at( attr ) * sizeof( float ) );
//        pFile.read( (char *) ( result.data()+np*2 ), np * sizeof( float ) );

//        pFile.seekg( np * m_attrKeyToPhaseIndex.at( attr ) * sizeof( float ) );
//        pFile.read( (char *) ( result.data()+np*3 ), np * sizeof( float ) );

        ////qDebug() << "read " << attr.c_str() << " " << result.front() << " " << result.back();

        pFile.close();
    }

    void readStep(
        std::vector< TargetFloatType > & result,
        std::vector< TargetFloatType > & swap,
        const std::string & ptype,
        const std::string & attr,
        size_t ts )
    {
        std::string nstr = std::to_string( ts );
        std::string path = this->m_basicDataInfo.location + "/ParticleData/" + "xgc.particle." + std::string( 5 - nstr.size(), '0' ) + nstr  +  ".h5";

        hid_t file_id = H5Fopen( path.c_str() , H5F_ACC_RDONLY, H5P_DEFAULT );
        hid_t group = H5Gopen2( file_id, "/", H5P_DEFAULT );
        hid_t dataset_id = H5Dopen2( group, ptype == "ions" ? "iphase" : "ephase", H5P_DEFAULT );
        hid_t dataspace_id = H5Dget_space ( dataset_id );

        int ndims = H5Sget_simple_extent_ndims( dataspace_id );
        hsize_t dims[ ndims ];
        H5Sget_simple_extent_dims( dataspace_id, dims, NULL );

        hsize_t offset[ 2 ];
        hsize_t  count[ 2 ];
        hsize_t stride[ 2 ];
        hsize_t  block[ 2 ];

        offset[ 0 ] = 0;
        offset[ 1 ] = m_attrKeyToPhaseIndex.at( attr );

        count[ 0 ]  = dims[ 0 ];
        count[ 1 ]  = 1;

        stride[ 0 ] = 1;
        stride[ 1 ] = 1;

        block[ 0 ] = 1;
        block[ 1 ] = 1;

        herr_t status = H5Sselect_hyperslab( dataspace_id, H5S_SELECT_SET, offset, stride, count, block );

        hsize_t dimsm[ 1 ];
        dimsm[ 0 ] = dims[ 0 ];
        hid_t memspace_id = H5Screate_simple( 1, dimsm, NULL );

        swap.resize( dims[ 0 ] );
        status = H5Dread( dataset_id, H5T_IEEE_F32LE, memspace_id, dataspace_id, H5P_DEFAULT, swap.data() );
        status = H5Dclose( dataset_id );

        std::vector< long int > ids( dims[ 0 ] );
        dataset_id = H5Dopen2( group, ptype == "ions" ? "igid" : "egid", H5P_DEFAULT );
        status = H5Dread( dataset_id, H5T_STD_I64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ids.data() );
        status = H5Dclose( dataset_id );

        status = H5Gclose( group );
        status = H5Fclose(file_id);

        ////////qDebug() << "read ids and closed file";

        result = std::vector< float >( m_numParticles.at( ptype ), NAN );
        std::unordered_map< std::size_t, std::size_t > & mp = m_idMaps.at( ptype );

        ////////qDebug() << "resized to " << result.size() << " now mapping with map of size " << mp.size();

        int numNotFound = 0;

        for( int i = 0; i < dims[ 0 ]; ++i )
        {
            if( mp.count( ids[ i ] ) )
            {
                result[ mp.at( ids[ i ] ) ] = swap[ i ];
            }
            else
            {
                ++numNotFound;
            }
        }
        ////////qDebug() << numNotFound;
    }

    void convertData()
    {
        std::vector< float > swap( m_numParticles.at( "ions" ) );
        std::vector< float > result( m_numParticles.at( "ions" ) );
        std::vector< float > allAttr( m_numParticles.at( "ions" ) * m_attrKeyToPhaseIndex.size() );

        std::vector< std::string > ptypes = { "ions", "electrons" };
        for( auto & p : ptypes )
        {
            size_t NP = m_numParticles.at( p );
            allAttr.resize(  NP * m_attrKeyToPhaseIndex.size() );

            ////qDebug() << "size is " << allAttr.size();

            std::ofstream infoFile( this->m_basicDataInfo.location + "/Preprocessed/ParticleData/info." + p + ".txt" );

            infoFile << "NumParticles NumVariables Variable1 Variable2 ... VarableNV\n";
            infoFile << NP << " " << m_attrKeyToPhaseIndex.size() << " ";
            for( auto & v : m_attrKeyToPhaseIndex )
            {
                infoFile << v.first <<  " ";
            }
            infoFile.close();

            for( auto & t : m_simulationTimeSteps )
            {
                for( auto & v : m_attrKeyToPhaseIndex )
                {
                    readStep(
                        result,
                        swap,
                        p,
                        v.first,
                        t );

                    ////qDebug() << "read " << result.back() << " " << result.front();

                    const int VOFFSET = v.second;

                    ////qDebug() << v.first.c_str() << v.second;

                    #pragma omp parallel for simd
                    for( size_t k = 0; k < NP; ++k )
                    {
                        allAttr[ k + VOFFSET*NP ] = result[ k ];
                    }
                }

                ////qDebug() << "writing " << p.c_str();

                // write
                std::string tstr = std::to_string( t );
                std::string path =  this->m_basicDataInfo.location + "/Preprocessed/ParticleData/" + "xgc." + p + "." + std::string( 5 - tstr.size(), '0' ) + tstr  +  ".bin";
                std::ofstream outFile( path, std::ios::out | std::ios::binary );
                outFile.write( (char *) allAttr.data(), allAttr.size() * sizeof( float ) );
                outFile.close();
            }
        }
    }

    void computeBaseSubsets()
    {
        const size_t NT = m_simulationTimeSteps.size();
        const size_t NP = m_numParticles.at( "ions" );

        generateLookupTexture( m_psinLookupTexture, m_psinInterpolator, "psin" );
        m_psinTextureLoaded = true;

        generateLookupTexture3( m_brLookupTexture, m_bzLookupTexture, m_bphiLookupTexture, m_bFieldInterpolator );
        m_bfieldTexturesLoaded = true;

        baseSubsetFlags = std::vector< std::vector< std::int32_t > >( NT, std::vector< std::int32_t >( NP, 0 ) );

        std::vector< std::int32_t > confineCheck0(  NP, 0 );
        std::vector< std::int32_t > confineCheck15( NP, 0 );
        std::vector< std::int32_t > escapeCheck( NP, 0 );
        std::vector< std::int32_t > dirZero( NP, 0 );
        std::vector< std::vector< std::int32_t > > dirChange( NT, std::vector< std::int32_t >( NP, 0 ) );
        std::vector< std::vector< std::int32_t > > angleT( NT, std::vector< std::int32_t >( NP, 0 ) );

        const double PI = 3.14159265359;

        for( size_t t = 0; t < NT; ++t )
        {
            static std::vector< TargetFloatType > r;
            static std::vector< TargetFloatType > z;
            static std::vector< TargetFloatType > w0;
            static std::vector< TargetFloatType > w1;
            static std::vector< TargetFloatType > mu;
            static std::vector< TargetFloatType > rho_parallel;
            static std::vector< TargetFloatType > br;
            static std::vector< TargetFloatType > bz;
            static std::vector< TargetFloatType > bphi;
            static std::vector< TargetFloatType > psin;

            double ptl_ion_charge_eu = 1;
            double mi_sim = 2*1.660539040e-27;
            double e = 1.609e-19;



            readPreprocessedStep(
                r,
                "ions",
                "r",
                m_simulationTimeSteps[ t ] );

            readPreprocessedStep(
                z,
                "ions",
                "z",
                m_simulationTimeSteps[ t ] );

            readPreprocessedStep(
                w0,
                "ions",
                "w0",
                m_simulationTimeSteps[ t ] );
            readPreprocessedStep(
                w1,
                "ions",
                "w1",
                m_simulationTimeSteps[ t ] );

            readPreprocessedStep(
                mu,
                "ions",
                "mu",
                m_simulationTimeSteps[ t ] );

            readPreprocessedStep(
                rho_parallel,
                "ions",
                "rho_parallel",
                m_simulationTimeSteps[ t ] );

            texLookup(
                r,
                z,
                m_psinLookupTexture,
                psin
            );

            texLookup(
                r,
                z,
                m_brLookupTexture,
                br
            );

            texLookup(
                r,
                z,
                m_bzLookupTexture,
                bz
            );

            texLookup(
                r,
                z,
                m_bphiLookupTexture,
                bphi
            );

            for( size_t p = 0; p < NP; ++p )
            {
                 double B = std::sqrt( br[ p ]*br[ p ] + bz[ p ]*bz[ p ] + bphi[ p ]*bphi[ p] );
                 double vpara = B * rho_parallel[ p ] * ( ( ptl_ion_charge_eu * e ) / mi_sim );
                 double vperp = sqrt( ( mu[ p ] * 2.0 * B ) / mi_sim );
                 double ke = 0.5*mi_sim*( vpara*vpara + vperp*vperp );
                 double angle = ( Vec2< double >( r[ p ], z[ p ] ) - m_center ).angle( Vec2< double >( 1.0, 0.0 ) );


//                 angleT[ t ][ p ] = angle;

//                 // "outside trapped=2^4",
//                 // "inside trapped=2^5",
//                 if( t == 0 )
//                 {
//                     dirZero[ p ] = vpara >= 0 ? 1 : -1;
//                 }

//                 // switched direction
//                 else if( dirZero[ p ] == 1 && vpara < 0 || dirZero[ p ] == -1 && vpara > 0 )
//                 {
//                     dirZero[ p ] = vpara > 0 ? 1 : -1;
//                     dirChange[ t ][ p ] = dirZero[ p ];
//                 }

                 if( z[ p ] >= -3.442893939000000 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 4 ) );
                 }

                 // "confined 1=2^9", after starting inside separatrix from t=0
                 // "confined 2=2^10", after starting inside separatrix from t=30
                 if( t == 0 )
                 {
                     if( psin[ p ] < 1.0 && z[ p ] > -3.4428939819335938 )
                     {
                         confineCheck0[ p ] = 1;
                     }
                 }

                 else if( t == 15 )
                 {
                     if( psin[ p ] < 1.0 && z[ p ] > -3.4428939819335938 )
                     {
                         confineCheck15[ p ] = 1;
                     }
                 }

                 if( psin[ p ] < 1.0 && z[ p ] > -3.4428939819335938 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 6 ) );
                 }

                 double lambda = (2*mu[ p ] * 5.3000001907348633 ) / ( vpara*vpara+ vperp*vperp );

                 if(  lambda > 2.2e-27 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 5 ) );
                 }

                 // "escapes=2^11", after starting inside separatrix at t=0
                 if( confineCheck0[ p ] == 1 )
                 {
                     if( psin[ p ] > 1.0 || z[ p ] <= -3.442893939000000 )
                     {
                         escapeCheck[ p ] = 1;
                     }
                 }

                 // "low ke=2^6",
                 // "medium ke=2^7",
                 // "high ke=2^8"


//                 qDebug() << ke;

                 if( ke < 0.3e-15 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 10 ) );
                 }
                 else if( ke < 1.0e-15 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 11 ) );
                 }
                 else
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 12 ) );
                 }

                 // "neg. vpara=2^10",
                 if( vpara < 0 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 13 ) );
                 }

                 // "neg weight=2^11",
                 double w0w1 = w0[ p ] * w1[ p ];
                 if( w0w1 < 1 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 14 ) );
                 }

                 // "small weight=2^12",
                 // "medium weight=2^13",
                 // "large weight=2^14"
                 double absw0w1 = std::abs( w0w1 );
                 if( absw0w1 > 0.5e+12 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 17 ) );
                 }
                 else if( absw0w1 > 0.5e+11 )
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 16 ) );
                 }
                 else
                 {
                     baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 15 ) );
                 }
            }
        }

        for( size_t t = 0; t < NT; ++t )
        {
            for( size_t p  = 0; p < NP; ++p )
            {
                if( confineCheck0[ p ] )
                {
                    baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 7 ) );
                }
                if( confineCheck15[ p ] && confineCheck0[ p ] )
                {
                    baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 8 ) );
                }
                if( escapeCheck[ p ] )
                {
                    baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 9 ) );
                }
            }
        }

//        // rules for flagging trapped/passing modes in high or low, using direction changes etc.
//        for( size_t p  = 0; p < NP; ++p )
//        {
//            bool trapped = false;
//            float a;
//            int d0 = 0;
//            float a0 = angleT[ 0 ][ p ];
//            for( size_t t = 0; t < NT; ++t )
//            {
//                a = angleT[ t ][ p ];

//                if( dirChange[ t ][ p ] /*&& ( a > PI / 2.0 && a < 3*PI/2.0 ) */ )
//                {
//                    d0 = dirChange[ t ][ p ];
//                    a0 = a;
//                    trapped = true;
//                }
//                else if( trapped == true && ! dirChange[ t ][ p ] )
//                {
//                    if( a0 < PI && d0 == -1 )
//                    {
//                        if( a < PI )
//                        {
//                            trapped = false;
//                        }
//                    }
//                    else if( a0 > PI && d0 == 1 )
//                    {
//                        if( a > PI )
//                        {
//                            trapped = false;
//                        }
//                    }
//                }
//                if( trapped )
//                {
//                    baseSubsetFlags[ t ][ p ] |= int( std::pow( 2, 4 ) );
//                }
//            }
//        }
    }

    virtual void loadTemporallyStaticAttributes(
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes,
        std::map< std::string, std::map< std::string, Vec2< double > > > & variantAttributeRanges,
        std::map< std::string, std::vector< TargetFloatType > >  & particleVariantAttributes,
        std::map< std::string, BaseConstant > & constants,
        std::map< std::string, std::map< std::string, DerivedVariable > > & derivedVariables,
        std::map< std::string, VisualContextModel< TargetFloatType > > & visualContextModels,
        std::map< std::string, std::map< std::string, LookupTexture< TargetFloatType > > > & lookupTextures,
        std::vector< std::int32_t > & simulationTimeSteps,
        std::vector< double > & realTime,
        std::int32_t & numTimeSteps,
        std::map< std::string, std::size_t > & numParticles )
    {
//            std::ifstream inFile( this->m_basicDataInfo.location + "/meta.txt" );
//            if( ! inFile.is_open() )
//            {
//                 //////qDebug() << "couln't open meta file";
//            }

//            std::string line;
//            size_t ts_first;
//            size_t ts_last;
//            size_t ts_stride;
//            std::string ptype;
//            std::string attribute;

//            while( std::getline( inFile, line ) )
//            {
//                if( line[ 0 ] == 't' && line[ 1 ] == 's' && line.size() == 2 )
//                {
//                    std::getline( inFile, line );
//                    std::istringstream ss( line );
//                    ss >> ts_first >> ts_last >> ts_stride;
//                    numTimeSteps = ( ts_last - ts_first + ts_stride ) / ts_stride;

//                    continue;
//                }
//                else if ( line[ 0 ] == 'o' && line.size() == 1 )
//                {
//                    std::getline( inFile, line );
//                    std::istringstream ss( line );
//                    ss >> ptype >> numParticles.at( ptype );

//                    particleAndTimeVariantAttributes.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
//                                                               ptype,
//                                                               std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > () ) );

//                    variantAttributeRanges.insert( std::pair< std::string, std::map< std::string, Vec2< double > > > (
//                                                       ptype,
//                                                       std::map< std::string, Vec2< double > >( ) ) );
//                    continue;
//                }
//                else if ( line[ 0 ] == 'v' && line.size() == 1 )
//                {
//                    std::getline( inFile, line );
//                    std::istringstream ss( line );
//                    int num;
//                    ss >> num;

//                    for( int i = 0; i < num; ++i )
//                    {
//                        std::getline( inFile, line );
//                        std::istringstream ss1( line );
//                        OrigionalFloatType a,b;
//                        ss1 >> attribute >> a >> b;

//                        particleAndTimeVariantAttributes.find( ptype )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
//                                                                                         attribute,
//                                                                                         std::vector< std::unique_ptr< std::vector< TargetFloatType > > > () ) );
//                        for( int t = 0; t < numTimeSteps; ++t ) {
//                             particleAndTimeVariantAttributes.find( ptype)->second.find( attribute )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
//                        }


//                        variantAttributeRanges.find( ptype )->second.insert( std::pair< std::string, Vec2< double > >(
//                                                           attribute,
//                                                           Vec2< double > ( a, b ) ) );
//                    }
//                    continue;
//                }
//            }

//            inFile.close();

        //////qDebug() << "loading particle data information";

//            for( size_t t = ts_first; t <= ts_last; t += ts_stride )
//            {
//                simulationTimeSteps.push_back( t );
//            }
//            m_simulationTimeSteps = simulationTimeSteps;

        loadTimeSteps( simulationTimeSteps, realTime, 2, 572, 2 );
        m_simulationTimeSteps = simulationTimeSteps;
        numTimeSteps = ( 572 - 2 ) / 2 + 1;

//        m_numParticles.at( "ions" ) = 50000*15;

        //WRAPAROUND//////////////////////////////////////////////////////////////
//        m_simulationTimeSteps = simulationTimeSteps;
//        numTimeSteps = ( 572 - 2 ) / 2 + 1;
//        for( int i = 0; i < 2000; ++i )
//        {
//            m_simulationTimeSteps.push_back( m_simulationTimeSteps.back() + 2 );
//            simulationTimeSteps.push_back( simulationTimeSteps.back() + 2 );
//            realTime.push_back( realTime.back() + 0.5 );
//        }
//        numTimeSteps = m_simulationTimeSteps.size();
        //////////////////////////////////////////////////////////////////////////

        //convertData();

        std::vector< std::string > additionalVariables = { "br", "bz", "bphi", "psin", "poloidal_angle" };

        for( auto & ptype : m_numParticles )
        {
            particleAndTimeVariantAttributes.insert( std::pair< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > >(
                        ptype.first,
                        std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > () ) );

            variantAttributeRanges.insert( std::pair< std::string, std::map< std::string, Vec2< double > > > (
                                               ptype.first,
                                               std::map< std::string, Vec2< double > >( ) ) );

            for( auto & attr : m_attrKeyToPhaseIndex )
            {
                particleAndTimeVariantAttributes.find( ptype.first )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                            attr.first,
                            std::vector< std::unique_ptr< std::vector< TargetFloatType > > > () ) );
                for( int t = 0; t < numTimeSteps; ++t )
                {
                    particleAndTimeVariantAttributes.find( ptype.first )->second.find( attr.first )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
                }


                variantAttributeRanges.find( ptype.first )->second.insert( std::pair< std::string, Vec2< double > >(
                            attr.first,
                            Vec2< double >() ) );
            }
            for( auto & attr : additionalVariables )
            {
                particleAndTimeVariantAttributes.find( ptype.first )->second.insert( std::pair< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > >(
                            attr,
                            std::vector< std::unique_ptr< std::vector< TargetFloatType > > > () ) );
                for( int t = 0; t < numTimeSteps; ++t )
                {
                    particleAndTimeVariantAttributes.find( ptype.first )->second.find( attr )->second.push_back( std::unique_ptr< std::vector< TargetFloatType > >( nullptr ) );
                }


                variantAttributeRanges.find( ptype.first )->second.insert( std::pair< std::string, Vec2< double > >(
                            attr,
                            Vec2< double >() ) );
            }
        }

        std::ifstream inFile( this->m_basicDataInfo.location + "/Preprocessed/Geometry/separatrix.txt" );
        if( !inFile.is_open() )
        {
            //////qDebug() << "couln't open separatrix file";
        }
        else
        {
            visualContextModels.insert(
                std::pair< std::string, VisualContextModel< TargetFloatType > >(
                    "separatrix",
                    VisualContextModel< TargetFloatType >() ) );

            visualContextModels.find( "separatrix" )->second.color = Vec4( .5, 1, .7, 1 );
            visualContextModels.find( "separatrix" )->second.primitiveType = GL_LINES;
            visualContextModels.find( "separatrix" )->second.coordinates.insert( { "r", std::vector< TargetFloatType >() } );
            visualContextModels.find( "separatrix" )->second.coordinates.insert( { "z", std::vector< TargetFloatType >() } );

            visualContextModels.find( "separatrix" )->second.boundingBox = std::map< std::string, Vec2< TargetFloatType > > (
            {
                {  "r", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) },
                {  "z", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) }
            } );

            std::vector< TargetFloatType > & stxRCoords = visualContextModels.find( "separatrix" )->second.coordinates.find( "r" )->second;
            std::vector< TargetFloatType > & stxZCoords = visualContextModels.find( "separatrix" )->second.coordinates.find( "z" )->second;

            Vec2< TargetFloatType > & stxRRange = visualContextModels.find( "separatrix" )->second.boundingBox.find( "r" )->second;
            Vec2< TargetFloatType > & stxZRange = visualContextModels.find( "separatrix" )->second.boundingBox.find( "z" )->second;

            std::string line;
            std::getline( inFile, line );
            int jk = 0;
            while( inFile )
            {
                if ( ! std::getline( inFile, line ) ) break;
                std::istringstream ss( line );
                TargetFloatType r,z;
                ss >> r >> z;
                ++jk;

                stxRCoords.push_back( r );
                stxZCoords.push_back( z );

                stxRRange.a( std::min( stxRRange.a(), r ) );
                stxRRange.b( std::min( stxRRange.b(), r ) );

                stxZRange.a( std::min( stxZRange.a(), z ) );
                stxZRange.b( std::min( stxZRange.b(), z ) );

                if ( jk > 1)
                {
                    stxRCoords.push_back( r );
                    stxZCoords.push_back( z );
                }
            }
        }
        inFile.close();

        inFile.open( this->m_basicDataInfo.location + "/Preprocessed/Geometry/walls.txt" );
        if( !inFile.is_open() )
        {
            //////qDebug() << "couln't device wall file";
        }
        else
        {
            visualContextModels.insert( std::pair< std::string, VisualContextModel< TargetFloatType > >(
                                            "device_walls",
                                            VisualContextModel< TargetFloatType >() ) );
            visualContextModels.find( "device_walls" )->second.color = Vec4( .2, .2, .2, 1 );
            visualContextModels.find( "device_walls" )->second.primitiveType = GL_LINES;
            visualContextModels.find( "device_walls" )->second.coordinates.insert( { "r", std::vector< TargetFloatType >() } );
            visualContextModels.find( "device_walls" )->second.coordinates.insert( { "z", std::vector< TargetFloatType >() } );
            visualContextModels.find( "device_walls" )->second.boundingBox = std::map< std::string, Vec2< TargetFloatType > > (
            {
                {  "r", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) },
                {  "z", Vec2< TargetFloatType >( std::numeric_limits< float >::max(), -std::numeric_limits< float >::max() ) }
            } );

            std::vector< TargetFloatType > & wallsRCoords = visualContextModels.find( "device_walls" )->second.coordinates.find( "r" )->second;
            std::vector< TargetFloatType > & wallsZCoords = visualContextModels.find( "device_walls" )->second.coordinates.find( "z" )->second;
            Vec2< TargetFloatType >        & wallsRRange  = visualContextModels.find( "device_walls" )->second.boundingBox.find( "r" )->second;
            Vec2< TargetFloatType >        & wallsZRange  = visualContextModels.find( "device_walls" )->second.boundingBox.find( "z" )->second;

            std::string line;
            std::getline( inFile, line );
            int jk = 0;
            while( inFile )
            {
                if ( ! std::getline( inFile, line ) ) break;
                std::istringstream ss( line );
                TargetFloatType r,z;
                ss >> r >> z;
                ++jk;

                wallsRCoords.push_back( r );
                wallsZCoords.push_back( z );

                wallsRRange.a( std::min( wallsRRange.a(), r ) );
                wallsRRange.b( std::min( wallsRRange.b(), r ) );

                wallsZRange.a( std::min( wallsZRange.a(), z ) );
                wallsZRange.b( std::min( wallsZRange.b(), z ) );

                if ( jk > 1)
                {
                    wallsRCoords.push_back( r );
                    wallsZCoords.push_back( z );
                }
            }
        }
        inFile.close();

        inFile.open( this->m_basicDataInfo.location + "/units.m" );
        std::string line;
        while( inFile )
        {
            if ( ! std::getline( inFile, line ) ) break;
            std::istringstream ss( line );
            TargetFloatType value;
            std::string name;
            std::string valueStr;
            ss >> name >> valueStr;

            if( valueStr == "=" )
            {
                ss >> value;
            }
            else if( valueStr[ 0 ] == '=' )
            {
                valueStr.erase( valueStr.begin() );
                value = std::stod( valueStr );
            }
            else
            {
                value = std::stod( valueStr );
            }

            if( name.back() == '=' )
            {
                name.pop_back();
            }

            BaseConstant bc;
            bc.value( value );
            bc.name( name );
            constants.insert( { name, bc } );
        }
        inFile.close();

        if( constants.count( "eq_axis_r" ) <= 0 || constants.count( "eq_axis_z" ) <= 0 )
        {
            std::cout << "error: missing eq_axis_r, and eq_axis_z, need to compute poloidal angles, these coinstants should be in units.m";
        }
        else
        {
            m_center.x( constants.at( "eq_axis_r" ).value() );
            m_center.y( constants.at( "eq_axis_z" ).value() );
        }

        numParticles = m_numParticles;

        computeBaseSubsets();
    }

    virtual void loadDefaultSubsetNames( std::vector< std::string > & names )
    {   
        m_defaultSubsets = {
            "above eq_x_z",
            "B-trapped anaylitic",
            "confined",
            "confined 0",
            "confined 30",
            "escapes",
            "low ke",
            "medium ke",
            "high ke",
            "neg. vpara",
            "neg weight",
            "small weight",
            "medium weight",
            "large weight" };

        names = m_defaultSubsets;

        return;
    }

    virtual void getDefaultSubsetNames( std::vector< std::string > & names )
    {
        names = m_defaultSubsets;
    }

    virtual void loadTimeStepsMasks( std::vector< std::int32_t > & masks, size_t t )
    {
        masks = std::vector< std::int32_t >( m_numParticles.at( "ions" ) );

        int flag = 1;
        if( t % 285 == 0 )
        {
            flag = 0;
        }

        for( int i = 0; i < m_numParticles.at( "ions" ); ++i )
        {
            masks[ i ] = baseSubsetFlags[ t ][ i ] | flag;
        }
    }

    /**
     * @brief loadTimeStep
     * @param timeStep
     * @param needToLoad
     * @param particleAndTimeVariantAttributes
     */
    virtual void loadTimeStep(
        int timeStep,
        const std::map< std::string, std::set< std::string > > & needToLoad,
        std::map< std::string, std::map< std::string, std::vector< std::unique_ptr< std::vector< TargetFloatType > > > > > & particleAndTimeVariantAttributes )
    {
        ////////qDebug() << "loading time step " << timeStep << ", " << m_simulationTimeSteps.size() << " available";

        int t = m_simulationTimeSteps[ timeStep ];

        const std::string PRECISION = sizeof( OrigionalFloatType ) == 8 ? "DoublePrecision" : "SinglePrecision";
        std::string leadingZeros =   t < 10   ? "0000"
                                     : t < 100  ?  "000"
                                     : t < 1000 ?   "00"
                                     : t < 1000 ?    "0"
                                     : "";

        for( const auto & partType : needToLoad )
        {
            for( const auto & attr : partType.second  )
            {
                if( attr == "psin" && m_psinTextureLoaded == false )
                {
                    generateLookupTexture( m_psinLookupTexture, m_psinInterpolator, "psin" );
                    m_psinTextureLoaded = true;
                }
                else if( ( attr == "br" || attr == "bz" || attr == "bphi" ) &&  m_bfieldTexturesLoaded == false )
                {
                    generateLookupTexture3( m_brLookupTexture, m_bzLookupTexture, m_bphiLookupTexture, m_bFieldInterpolator );
                    m_bfieldTexturesLoaded = true;
                }

                if( attr != "poloidal_angle" && attr != "psin" && attr != "br" &&  attr != "bz" && attr != "bphi" )
                {
                    ////////qDebug() << "loading " << timeStep << " " << partType.first.c_str() << " " << attr.c_str();

                    std::ifstream inFile( this->m_basicDataInfo.location + "/Particles/Binary/" + PRECISION + "/" + partType.first + "/" + leadingZeros + std::to_string( t ) +  "." + attr + ".bin" );
                    if( ! inFile.is_open() )
                    {
                        ////////qDebug() << "failed to load file: " << ( this->m_dataSetMetaData.filePath + "/Particles/Binary/" + PRECISION + "/" + partType.first + "/" + leadingZeros + std::to_string( t ) +  "." + attr + ".bin" ).c_str();
                    }

                    ////////qDebug() << "file opened " << partType.first.c_str() << " " << attr.c_str();

                    //////////////////////////////////////////////////////////////////////////

                    std::unique_ptr< std::vector< TargetFloatType > > & dat
                        = particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ];
                    if( dat == nullptr )
                    {
                        dat.reset( new std::vector< TargetFloatType > );
                    }

                    readPreprocessedStep(
                        *dat,
                        partType.first,
                        attr,
                        t );

//                    if( ! std::is_same< OrigionalFloatType, TargetFloatType >::value )
//                    {
//                        ////////qDebug() << "types on disk and in memory differ";
//                        inFile.read( (char*) m_swap.data(), m_numParticles*sizeof( OrigionalFloatType ) );
//                        particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ].reset( new std::vector< TargetFloatType >( m_swap.begin(), m_swap.end() ) );
//                    }
//                    else
//                    {
//                        ////////qDebug() << "types on disk and in memory are the same";
//                        particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ].reset( new std::vector< TargetFloatType >( m_numParticles ) );
//                        inFile.read( (char*) particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ]->data(), m_numParticles*sizeof( OrigionalFloatType ) );
//                    }

                    //////////////////////////////////////////////////////////////////////////

                    ////////qDebug() << "successfully loaded "  << partType.first.c_str() << " " << attr.c_str() << " closing file";
                    inFile.close();
                }
            }
        }

        for( const auto & partType : needToLoad )
        {
            for( const auto & attr : partType.second  )
            {
                if( attr == "poloidal_angle" || attr == "psin" || attr == "br" || attr == "bz" || attr == "bphi" )
                {

                    bool needToLoadR = false;
                    bool needToLoadZ = false;

                    if( particleAndTimeVariantAttributes.find( partType.first )->second.find( "r" )->second[ timeStep ] == nullptr )
                    {
                        needToLoadR = true;
                    }
                    else if( particleAndTimeVariantAttributes.find( partType.first )->second.find( "r" )->second[ timeStep ]->size() < m_numParticles.at( partType.first ) )
                    {
                        needToLoadR = true;
                    }

                    if( particleAndTimeVariantAttributes.find( partType.first )->second.find( "z" )->second[ timeStep ] == nullptr )
                    {
                        needToLoadZ = true;
                    }
                    else if( particleAndTimeVariantAttributes.find( partType.first )->second.find( "z" )->second[ timeStep ]->size() < m_numParticles.at( partType.first ) )
                    {
                        needToLoadZ = true;
                    }

                    particleAndTimeVariantAttributes.find( partType.first )->second.find( attr )->second[ timeStep ].reset( new std::vector< TargetFloatType >( m_numParticles.at( partType.first ) ) );

                    if( ! needToLoadR && ! needToLoadZ )
                    {
                        if( attr == "poloidal_angle" )
                        {
                            getPoloidalAngles(
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "r" )->second[ timeStep ] ),
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "z" )->second[ timeStep ] ),
                                m_center,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "poloidal_angle" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "psin" )
                        {
                            texLookup(
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "r" )->second[ timeStep ] ),
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "z" )->second[ timeStep ] ),
                                m_psinLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "psin" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "br" )
                        {
                            texLookup(
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "r" )->second[ timeStep ] ),
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "z" )->second[ timeStep ] ),
                                m_brLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "br" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "bz" )
                        {
                            texLookup(
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "r" )->second[ timeStep ] ),
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "z" )->second[ timeStep ] ),
                                m_bzLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "bz" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "bphi" )
                        {
                            texLookup(
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "r" )->second[ timeStep ] ),
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "z" )->second[ timeStep ] ),
                                m_bphiLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "bphi" )->second[ timeStep ] )
                            );
                        }
                    }
                    else
                    {
                        static std::vector< TargetFloatType > rTemp;
                        static std::vector< TargetFloatType > zTemp;
                        static std::vector< TargetFloatType > swapData;

                        readPreprocessedStep(
                            rTemp,
                            partType.first,
                            "r",
                            t );

                        readPreprocessedStep(
                            zTemp,
                            partType.first,
                            "z",
                            t );

//                        std::ifstream inFile( this->m_basicDataInfo.location + "/Particles/Binary/" + PRECISION + "/" + partType.first + "/" + leadingZeros + std::to_string( t ) +  ".r.bin" );
//                        inFile.read( (char*) rTemp.data(), m_numParticles*sizeof( OrigionalFloatType ) );
//                        inFile.close();

//                        inFile.open( this->m_basicDataInfo.location + "/Particles/Binary/" + PRECISION + "/" + partType.first + "/" + leadingZeros + std::to_string( t ) +  ".z.bin" );
//                        inFile.read( (char*) zTemp.data(), m_numParticles*sizeof( OrigionalFloatType ) );
//                        inFile.close();

                        //////////////////////

                        if( attr == "poloidal_angle" )
                        {
                            getPoloidalAngles(
                                rTemp,
                                zTemp,
                                m_center,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "poloidal_angle" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "psin" )
                        {
                            texLookup(
                                rTemp,
                                zTemp,
                                m_psinLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "psin" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "br" )
                        {
                            texLookup(
                                rTemp,
                                zTemp,
                                m_brLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "br" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "bz" )
                        {
                            texLookup(
                                rTemp,
                                zTemp,
                                m_bzLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "bz" )->second[ timeStep ] )
                            );
                        }
                        else if( attr == "bphi" )
                        {
                            texLookup(
                                rTemp,
                                zTemp,
                                m_bphiLookupTexture,
                                *( particleAndTimeVariantAttributes.find( partType.first )->second.find( "bphi" )->second[ timeStep ] )
                            );
                        }
                    }
                }
            }
        }
    }

    /**
     * @brief ~XGCImporter
     */
    virtual ~XGCImporter() {}
};

}

#endif // XGCIMPORTER_HPP
