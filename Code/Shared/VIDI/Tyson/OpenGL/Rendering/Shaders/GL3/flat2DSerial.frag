#version 130



uniform vec4 color;

in  vec3 col;
out vec4 fragColor;

void main(void)
{
    fragColor = color;
}
