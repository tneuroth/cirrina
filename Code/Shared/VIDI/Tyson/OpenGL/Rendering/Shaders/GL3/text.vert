#version 130



in vec2 ps;
in vec2 tc;

uniform mat4 P;

out vec2 uv;

void main(void)
{
    uv = tc;
    gl_Position = P * vec4( ps.x, ps.y, 0, 1 );
}
