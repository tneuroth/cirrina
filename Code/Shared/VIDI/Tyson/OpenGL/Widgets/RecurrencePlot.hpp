#ifndef RECURRENCEPLOT_HPP
#define RECURRENCEPLOT_HPP

#include "OpenGL/Widgets/Widget.hpp"

namespace TN
{

struct RecurrencePlot : public Widget
{
    std::vector< float > data;
};

}

#endif // RECURRENCEPLOT_HPP
