#version 130



uniform mat4 model;
uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

in vec3 posAttr;
in vec3 normalAttr;

out vec3 normal;
out vec3 eye;
out vec3 pos;

void main(void)
{
    pos = vec4( modelView * vec4( vec3( posAttr.x, posAttr.y, posAttr.z ), 1.0 ) ).xyz;
    normal = normalize( normalMatrix*normalAttr );
    eye = normalize( -vec4( modelView * vec4( posAttr, 1.0 ) ).xyz );
    gl_Position = mvp*vec4(posAttr.xyz,1.0);
}

