


#ifndef TN_GROUPING_WIDGET
#define TN_GROUPING_WIDGET

#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"


namespace TN
{

class ParticleEventWidget : public Widget
{
    std::map< std::string, int > m_eventIds;
    std::map< std::string, std::string > m_eventSymbols;
};

class ParticleGroupingWidget : public Widget
{
    std::map< std::string, Vec3< float > > m_subsetColorCodes;
    std::map< std::string, std::string > m_subsetSymbols;
};

}

#endif // TN_GROUPING_WIDGET

